/** @format */

import {
	AppRegistry
} from 'react-native'
import Application from './src/Application'
import {name as appName} from './app.json'

console.disableYellowBox = true

AppRegistry.registerComponent(appName, () => Application)
