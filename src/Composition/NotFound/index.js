import React, { Component } from 'React'
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import styles from 'Services/Styles'

class NotFound extends Component {

	constructor(props){
		super(props)

		this.renderType = this.renderType.bind(this)
	}

	goToScreen(screen, admins, id, merchant_id){
		let { navigate } = this.props.navigation
		if(admins && id){
			return navigate(screen, {
				admins:admins,
				id:id,
				merchant_id
			})
		}else{
			return navigate(screen)			
		}
	}

	renderType(type){

		switch(type){
			case 'MERCHANT_COMPANIES':
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="math-compass" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Вы еще не создали ни одной автомойки</Text>
						<TouchableOpacity onPress={this.goToScreen.bind(this, 'createCompany')} style={styles.notFound.buttonCreateCompany}>
							<Text style={styles.notFound.buttonCreateCompanyTitle}>Создать</Text>
						</TouchableOpacity>
					</View>
				)
			case 'MERCHANT_COMPANIES_FOR_ADMIN':
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="math-compass" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Автомоек нет</Text>
					</View>
				)
			case 'MERCHANT_CLIENTS' :
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="account-search" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>У вас пока нет ни одного клиента</Text>
					</View>
				)
			case 'NOT_MEMBERS' :
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="account-search" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Сотрудников нет! добавьте</Text>
					</View>
				)
			case 'MERCHANT_ORDERS' :
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Сегодня заявок не было</Text>
					</View>
				)
			case 'MERCHANT_SUPPLIER' :
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Поставщиков не найдено</Text>
					</View>
				)
			case 'NO_VACANCIES' :

				let { buttonHide } = this.props

				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Вакансий не найдено</Text>
						{
							
							buttonHide ? 
								<View></View>
							:
							<TouchableOpacity onPress={this.goToScreen.bind(this, 'createVacancies')} style={styles.notFound.buttonCreateCompany}>
								<Text style={styles.notFound.buttonCreateCompanyTitle}>Добавить</Text>
							</TouchableOpacity>

						}
					</View>
				)
			case 'QUEUE_NOT_FOUND' :
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Боксов нет</Text>
					</View>
				)
			case 'QUEUE_CAR_NOT_FOUND' :
				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Машин в очереди нет</Text>
					</View>
				)
			case 'MERCHANT_ADMINISTRATOR' :

				let { administrators, id, merchant_id } = this.props

				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Вы не добавляли администраторов</Text>
						<TouchableOpacity onPress={this.goToScreen.bind(this, 'addAdministrator', administrators, id, merchant_id)} style={styles.notFound.buttonCreateCompany}>
							<Text style={styles.notFound.buttonCreateCompanyTitle}>Добавить</Text>
						</TouchableOpacity>
					</View>
				)
			case 'MERCHANT_ADMINISTRATOR_NOT' :

				return(
					<View style={styles.notFound.viewContent}>
						<MaterialCommunityIcons name="notification-clear-all" size={98} color="rgba(0,0,0,0.17)" />
						<Text style={styles.notFound.title}>Ничего не найдено</Text>
					</View>
				)	
			default :
				return(
					<View>
						<Text>default render</Text>
					</View>
				)
		}

	}

	render(){

		const { type } = this.props

		return(
			<View style={styles.notFound.view}>
				{ this.renderType(type) }
			</View>
		)
	}

}

export default NotFound