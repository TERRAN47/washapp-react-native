import React, { Component } from 'react'

import {
	View,
	AppState,
	Text
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import styles from 'Services/Styles'

import { moment } from 'Services/Time'
import momentJs from 'moment'

class Timer extends Component {

	constructor(props){
		super(props)

		this.state = {
			appState: AppState.currentState,
			time:''
		}
	}

	componentDidMount(){

		let that = this
		AppState.addEventListener('change', this._handleAppStateChange);

		/*setTimeout(()=>{
			that.timer()
		}, 1500)*/
	}

	componentWillUnmount(){
		AppState.removeEventListener('change', this._handleAppStateChange);
		clearInterval(this.intervalGenerate)
	}

	_handleAppStateChange = (nextAppState) => {
	    if (
	      this.state.appState.match(/inactive|background/) &&
	      nextAppState === 'active'
	    ) {
	      console.log('App has come to the foreground!');     
	      this.timer()
	    }else{
	      clearInterval(this.intervalGenerate)
	    }
	    this.setState({appState: nextAppState});
	}		

	timer(){

		let { time } = this.props
		let that = this

		let timeNow = momentJs()
		let timeCreateQueu =  moment(time, 'DD-MM-YYYY HH:mm:ss')

	    let laterSeconds = timeNow.diff(timeCreateQueu, 'seconds')
	    let laterMinutes = timeNow.diff(timeCreateQueu, 'minutes')
	    let laterHours = timeNow.diff(timeCreateQueu, 'hours')

	    let startTimestamp = moment().hours(0).minute(0).second(0)

	    startTimestamp.add(laterHours, 'hours')
	    startTimestamp.add(laterMinutes, 'minutes')
	    startTimestamp.add(laterSeconds, 'seconds')

	    this.intervalGenerate = setInterval(() => {

	        startTimestamp.add(1.2, 'second')

			that.setState({
				time:startTimestamp.format('HH:mm:ss')
			})
	    }, 1000)
	}

	render(){

		let { time } = this.props

		let { defaultStyles } = this.props

		let renderTime = moment(time, 'DD-MM-YYYY HH:mm:ss').format('HH:mm:ss')

		return(
			<View style={defaultStyles ? styles.timer.viewTimerDefault : styles.timer.viewTimer}>
				<MaterialCommunityIcons name="clock-outline" size={18} style={styles.merchant.company.iconSettingTime} color="#5b5b5b" />
				<Text style={styles.timer.viewTimertitle}>{ renderTime }</Text>
			</View>
		)

	}

}

export default Timer

