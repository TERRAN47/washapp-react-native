import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { DrawerActions } from 'react-navigation'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MyText from '../MyText'

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu'

import styles from '../../Services/Styles'

class Header extends Component {

	constructor(props){
		super(props)

		this.renderRight = this.renderRight.bind(this)
		this.renderLeft = this.renderLeft.bind(this)
	}

	openDrawer(){
		return this.props.navigation.dispatch(DrawerActions.openDrawer())
	}

	goBack(){
		return this.props.navigation.goBack()
	}

	renderLeft(){

		let { isBack, menu } = this.props

		if(isBack){
			return(<TouchableOpacity onPress={this.goBack.bind(this)} style={[styles.header.buttonHeader, {paddingRight:30}]}>
				<Icon name="chevron-left" size={22} color="#fff"/>
			</TouchableOpacity>)
		}else if(menu){
			return(<TouchableOpacity onPress={this.openDrawer.bind(this)} style={styles.header.buttonHeader}>
				<Icon name="bars" size={18} color="rgba(255,255,255,0.95)"/>
			</TouchableOpacity>)
		}else{
			return(<View></View>)
		}

	}

	goScreen(screen, id){
		const { navigate } = this.props.navigation
		const {companyPrice, branchId, companyId} = this.props

		if(companyPrice && companyId && branchId){

			return navigate(screen, {
				companyPrice,
				companyId,
				branch_id:branchId,
				id
			})

		}else{

			return navigate(screen, {
				id
			})

		}
	}

	renderRight(){

		let { isSearch, serchFilter, addCarQueue, isProfile, isPay, isReport, id, isAddVacancy } = this.props

		if(isSearch){
			return(<TouchableOpacity onPress={this.goScreen.bind(this, 'Search')} style={[styles.header.buttonHeader, {paddingLeft:20}]}>
					<Icon name="search" size={22} color="#fff"/>
				</TouchableOpacity>)
		}else if(addCarQueue){
			return(<TouchableOpacity onPress={this.goScreen.bind(this, 'addCarInQueue')} style={[styles.header.buttonHeader, {paddingLeft:20}]}>
					<MaterialCommunityIcons name="plus-circle" size={25} color="#fff"/>
				</TouchableOpacity>)
		}else if(serchFilter){
			return(<TouchableOpacity onPress={this.goScreen.bind(this, 'serchFilter')} style={[styles.header.buttonHeader, {paddingLeft:20}]}>
					<MaterialIcons name="settings-input-composite" size={22} color="#fff"/>
				</TouchableOpacity>)
		}else if(isAddVacancy){
			return(<TouchableOpacity onPress={this.goScreen.bind(this, 'createVacancies')} style={[styles.header.buttonHeader, {paddingLeft:20}]}>
					<MaterialCommunityIcons name="plus-circle" size={25} color="#fff"/>
				</TouchableOpacity>)
		}else if(isProfile){

			return(
				<Menu>
			      	<MenuTrigger>
			      		<Icon style={[styles.header.headerMenuIconTrigger, {paddingLeft:20}]} name="ellipsis-v"/>
			      	</MenuTrigger>
			      	<MenuOptions style={styles.header.menuOptionsViewStyle}>
			        	<MenuOption style={styles.header.menuOptionStyle} onSelect={this.goScreen.bind(this, 'editProfile')}>
			        		<Icon style={styles.header.headerMenuIcon} name="edit"/>
			        		<Text style={styles.header.headerMenuText}>Редактирование</Text>
			        	</MenuOption>
			      	</MenuOptions>
			    </Menu>
        	)	

		}else if(isPay){
			return(<TouchableOpacity>
				<Text style={[styles.header.payText, {marginRight:10}]}>{ isPay }</Text>
			</TouchableOpacity>)
		}else if(isReport){
			return(<TouchableOpacity style={{marginRight:10}} onPress={this.goScreen.bind(this, 'savedReports', id)}>
				<MaterialCommunityIcons name="format-list-checks" size={25} color="#fff"/>
			</TouchableOpacity>)
		}else{
			return(<View></View>)
		}

	}

	render(){

		let { user, title, hideBackground } = this.props

		return(
			<View style={hideBackground ? styles.header.viewHide : (user.user && user.user.type_account == "client" && !user.user.status_admin ? styles.header.clientView : styles.header.view)}>
				<View  style={styles.header.viewLeft}>
					{this.renderLeft()}
				</View>
				<MyText style={styles.header.headerTitle}>{ title }</MyText>

				<View  style={styles.header.viewRight}>
					{ this.renderRight() }
				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		user:state.user
	}
}

export default connect(mapStateToProps)(Header)