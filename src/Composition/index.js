import Header from './Header'
import NotFound from './NotFound'
import Loader from './Loader'
import Timer from './Timer'
import MyText from './MyText'
export {
	Header,
	MyText,
	NotFound,
	Loader,
	Timer
}