import React, { Component } from 'React'
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native'
import fonts from 'Services/Styles/fonts'

class MyText extends Component {

	render(){

		const { style } = this.props

		return(
			<Text {...this.props} style={[style, {fontFamily:fonts.GOTHICB}]} />
		)
	}

}

export default MyText