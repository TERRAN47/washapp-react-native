import React, { Component } from 'React'
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import styles from 'Services/Styles'

class Loader extends Component {

	render(){

		const { title } = this.props

		return(
			<View style={styles.loader.view}>
				<MaterialCommunityIcons name="progress-download" size={48} color="rgba(0,0,0,0.25)" />
				<Text style={styles.loader.title}>{ title }</Text>
			</View>
		)
	}

}

export default Loader