import React, { Component } from 'react'
import { Provider, connect } from 'react-redux'
import LaunchScreen from './Containers/LaunchScreen'
import { NavigationActions, withNavigation } from "react-navigation"
import { 
  StatusBar,
  AppState,
  Text,
  BackHandler,
  Platform
} from 'react-native'
import { MenuContext } from 'react-native-popup-menu'

import Config from './Config'
import NavigationService from './NavigationService'

import AppNavigator from './Navigator'

import createStore from './Services/Store'
import { PersistGate } from 'redux-persist/lib/integration/react'

//import { socket } from 'Services/Utils'

import reducers from './Services/Store/reducers'

import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers'

import { OneSignal } from './Services/Utils'
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
/*createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
)

const App = reduxifyNavigator(AppNavigator, "root")

const mapStateToProps = (state) => ({
  state: state.nav
})

const AppWithNavigationState = connect(mapStateToProps)(App)*/

class Application extends Component {

  constructor(props){
    super(props)
    
  }

  onBackPress(){

    const {dispatch} = createStore().store

    dispatch(NavigationActions.back())
    return true

  }

  onReceived(notification) {
    console.log("Notification received: ", notification)
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body)
    console.log('Data: ', openResult.notification.payload.additionalData)
    console.log('isActive: ', openResult.notification.isAppInFocus)
    console.log('openResult: ', openResult)
  }

  componentDidMount(){
        OneSignal.configure()
    OneSignal.addEventListener('received', this.onReceived)
    OneSignal.addEventListener('opened', this.onOpened)
   
  
  }

  
  componentWillUnmount() {
   
  }

  render () {
    
    const {store, persistor} = createStore()

    return (
      <Provider store={store}>
        <PersistGate loading={<LaunchScreen />} persistor={persistor}>

          <MenuContext>
            <StatusBar hidden={Platform.OS === 'android' ? false : true} backgroundColor="#333" barStyle="light-content"/>
              <AppNavigator/>
{/*            <AppWithNavigationState
              ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef)
              }}
            />*/}
          </MenuContext>

        </PersistGate>
      </Provider>
    )
  }
}

export default Application
