import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground
} from 'react-native'
import styles from 'Services/Styles'
//import Api from 'Services/Api'
//import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {constans, icons} from 'Services/Utils'
import config from 'Config'
import { renderDistance } from 'Services/Utils'
import underscore from 'underscore'

class ListCompanies extends Component {

	constructor(props) {

	  	super(props);
	     
	    this.state = {
	    	freeBox:0
	  	};
	}
  componentDidMount(){
    let {item} = this.props
    let freeBox = underscore.where(item.boxes, {
      status:true
    });
    this.setState({freeBox:freeBox.length})
  }
  
	async openCarWash(item){
	  const { navigate } = this.props.navigation
    const {dispatch} = this.props
    let {freeBox} = this.state

    await dispatch({
      type:'TARGET_COMPANY',
      targetCompany:{...item, freeBox}
    })
    navigate('targetCartWash')
	}

	render(){
  	let {item} = this.props
    let {freeBox} = this.state

    return(
      <TouchableOpacity onPress={this.openCarWash.bind(this, item)}>
    	<View style={styles.carWashes.carWashBlock}>
    		<ImageBackground 
    			source={{uri:`${config.public_url}${item.logo}`}}  
    			style={styles.carWashes.washImage} 
    		/>
    		<View style={{flex:1, paddingHorizontal: 10}}>
    			<Text style={styles.carWashes.washName}>{item.title}</Text>
    			 <View style={styles.carWashes.rowBlock}>
            <View style={styles.carWashes.rowBlock}>
              <FontAwesome5 
                name="coins" 
                style={styles.profilescreen.iconText} 
                size={15} 
                color={constans.orangeColor} 
              />
              <Text style={styles.carWashes.text}>{item.summ} тг</Text>
            </View>
            <View style={styles.carWashes.rowBlock}>
              {
                item.distance &&
                  <View style={styles.carWashes.rowBlock}>
                    <Image
                        style={styles.carWashes.washIcon} 
                        source={icons.point} 
                      />
                    <Text style={styles.carWashes.text}>{renderDistance(item.distance)}</Text>
                  </View>
              } 
            </View>                 
           </View>
 			
    			<View style={styles.carWashes.rowBlock}>
    				<FontAwesome5 
    					name="map-marker-alt" 
    					style={styles.profilescreen.iconText} 
    					size={15} color={constans.orangeColor} 
    				/>
    				<Text style={styles.carWashes.text}>{item.adress}</Text>
    			</View>

    			<View style={styles.carWashes.rowBlock}>
    				<View>
        				<View style={styles.carWashes.washService}>
	            		<Image
	            			style={styles.carWashes.imageRenderService} 
	            			source={icons.countBox} 
	            		/>
        				</View>
    					<Text style={styles.carWashes.textService}> {freeBox} / {item.boxes.length}</Text>
    				</View>
    				<View>
        				<View style={styles.carWashes.washService}>
	            		<Image
	            			style={styles.carWashes.imageRenderService} 
	            			source={icons.countTurn} 
	            		/>
	            	</View>
	            	<Text style={styles.carWashes.textService}> {item.queue}</Text>
            	</View>
            	<View style={styles.carWashes.flexBlock}>
            		<View style={styles.carWashes.washService}>
	            		<Image
							      style={styles.carWashes.imageRenderService} 
	            			source={icons.timeWork} 
	            		/>
	            	</View>
                {
                  item.work_time.dayNight ?
                    <Text style={styles.carWashes.textService}> 24/7</Text>
                  :
                    <View style={styles.carWashes.flexBlock}>
                      <Text style={styles.carWashes.textService}> {item.work_time.start}</Text>
                      <Text style={styles.carWashes.textService}> {item.work_time.end}</Text>
                    </View>
                }
            	</View>
            	<View>
            		<View style={item.additionally[0].status ? styles.carWashes.washService : [styles.carWashes.washService, {backgroundColor:'#dedede'}]}>
	            		<Image
	            			style={styles.carWashes.imageRenderService} 
	            			source={icons.waitRoom} 
	            		/>
	            	</View>
            	</View>
          		<View style={item.additionally[1].status ? styles.carWashes.washService : [styles.carWashes.washService, {backgroundColor:'#dedede'}]}>
            		<Image
            			style={styles.carWashes.imageRenderService} 
            			source={icons.cofeRoom} 
            		/>
          		</View>
              <View style={item.additionally[2].status ? styles.carWashes.washService : [styles.carWashes.washService, {backgroundColor:'#dedede'}]}>
                <Image
                  style={styles.carWashes.imageRenderService} 
                  source={icons.game}  
                />
              </View>              
    			</View>           			
    		</View>
    		<View style={styles.carWashes.ratingBlock}>
    			<Ionicons name="ios-star"  size={27} color="#FFCD19" />
    			<Text>{item.rating} / 10</Text>
    		</View>
    	</View>
      </TouchableOpacity>
    )
	}
}

export default ListCompanies