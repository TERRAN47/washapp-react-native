import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  ImageBackground
} from 'react-native'
import styles from 'Services/Styles'
import { MyText } from 'Composition'
import Api from 'Services/Api'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {constans, icons} from 'Services/Utils'
import config from 'Config'

class ListBonuses extends Component {

	render(){
	  	let {item} = this.props

	    return(
	      	<View style={styles.carWashes.bonusBlock}>
				<View style={[styles.profilescreen.myCarItem, {paddingHorizontal: 10}]}>
					<View style={styles.profilescreen.imgBlock}>
						<Image 
							source={{uri: `${config.public_url}${item.car.autoMarka.logo}`}}  
							style={styles.profilescreen.carImage}
						/>							
					</View>
					<View style={styles.profilescreen.flexBlock}>
						<View style={styles.profilescreen.carModel}>
							<View style={styles.profilescreen.flexBlock}>
								<MyText>{item.car.autoModel}</MyText>
							</View>
							<View style={styles.profilescreen.flexBlock}>
								<MyText>{item.car.autoNum}</MyText>
							</View>
						</View>
						<View style={styles.profilescreen.carInfo}>
							<View style={styles.profilescreen.flexBlock}>
		                      <Image 
		                        source={{uri: `${config.public_url}${item.car.avtoType.image}`}}  
		                        style={styles.profilescreen.carImage}
		                      />
							</View>
							<View style={styles.profilescreen.flexBlock}>
								<View style={[styles.profilescreen.autoColor, {backgroundColor: item.car.autoColor}]} />
							</View>														
						</View>													
					</View>
				</View>
				<View style={styles.carWashes.carWashBonusBlock}>
		      		<ImageBackground 
		      			source={{uri:`${config.public_url}${item.company_info.logo}`}}  
		      			style={styles.carWashes.washImage} 
		      		/>	      			
		      		<View style={{flex:1, paddingHorizontal: 10}}>
			  			<MyText style={styles.carWashes.washName}>{item.company_info.title}</MyText>
			  			<View style={styles.carWashes.rowBlock}>
			  				<FontAwesome5 
			  					name="map-marker-alt" 
			  					style={styles.profilescreen.iconText} 
			  					size={15} color={constans.orangeColor} 
			  				/>
			  				<MyText style={styles.carWashes.text}>{item.company_info.adress}</MyText>
			  			</View>
			  			<View>
			  				{
			  					item.company_info.bonus.map((elem, index)=>{
			  						return(
			  							<View key={index}>
			  								<MyText>{elem.description}</MyText>
			  							</View>
			  						)
			  					})
			  				}
			        	</View>
		      		</View>
		      		<View style={{justifyContent:'center', alignItems: 'center', minWidth:70}}>
		      			<View style={styles.carWashes.rowCenterBlock}>
			  				<MaterialCommunityIcons 
			  					name="car-wash" 
			  					style={styles.profilescreen.iconText} 
			  					size={30} color={constans.defaultColor} 
			  				/>
		      				<MyText style={styles.carWashes.countBonustext}>{item.count}</MyText>
		      			</View>
		      			<View style={styles.carWashes.rowCenterBlock}>
			  				<FontAwesome5 
			  					name="gift" 
			  					style={styles.profilescreen.iconText} 
			  					size={30} color={constans.orangeColor} 
			  				/>
			  				<MyText style={styles.carWashes.countBonustext}>{item.used_count}</MyText>
		      			</View>
		      		</View>
		      	</View>
		    </View>
	    )
	}
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
  }
}

export default connect(mapStateToProps)(ListBonuses)