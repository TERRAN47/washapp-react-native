import React, { Component } from 'react'

import {
	View,
	Text,
	Switch
} from 'react-native'

class ListAdditionally extends Component {

	constructor(props){
		super(props)

		this.state= {
			item:{}
		}
	}

	componentDidMount(){

		let { item } = this.props

		this.setState({
			item
		})

	}

	changeStatus(value){

		let { dispatch, api, index, items, companyIndex, id } = this.props

		this.setState({
			item:{
				...this.state.item,
				status:value
			}
		})

		items[companyIndex].additionally[index].status = value

		return api.merchant.carwash.changeStatusAdditionally(dispatch, items, id, companyIndex)

	}

	render(){

		let { items, index, styles, api } = this.props
		let { item } = this.state

		return(
			<View style={styles.lists.mainItemView}>
				<Text style={styles.lists.mainItemtitle}>{ item.title }</Text>
				<Switch value={item.status} onValueChange={(value)=>{this.changeStatus(value)}}/>
			</View>
		)

	}

}

export default ListAdditionally