import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	Alert
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import _ from 'underscore'
class ListOrders extends Component {

	closeRequest(){
		return Alert.alert('Сообщение', 'В данный момент вы не можете удалить сотрудника')
	}

  orderBox(elem){
    let { list } = this.props
    let boxList = []

    boxList = _.where(list, {
      box:elem.box
    })
    let totalSumm = 0
    if(boxList.length > 0){
      Alert.alert(`Отчет бокса №${elem.box}`, 
        `,${boxList.map((item)=>{
            totalSumm += +(item.total)
            return(
              ' Мойщик: '+item.member.firstName+'. оплата: '+item.total+'\n'
            )
          })}
          Итог: ${totalSumm} тг`)     
    }
  }

  orderMember(elem){
    let { list } = this.props
    let memberList = []

    memberList = _.where(list, {
      member_phone:elem.member_phone
    })
    let totalSumm = 0

    let percent = +(elem.member.percent)
    if(memberList.length > 0){

      Alert.alert(`Отчет ${elem.member.firstName}`, 
        `,${memberList.map((item)=>{
            totalSumm += +(item.total)
            return(
              ` Бокс: ${item.box}. оплата: ${item.total},\n`
            )
          })}
          \n Итог: ${totalSumm} тг \n Процент Мастера: ${totalSumm * +(`.${percent}`)} тг`)  
    }
  }

	render(){
		let { item, list, countOrder, index } = this.props
		index = index + 1

		return(

            <View style={styles.merchant.cabinetMerchant.tableBody}>

              <View style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionFirstBody]}>
                <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{ countOrder }</Text>
              </View>

              <View style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionSecond]}>
                <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{ item.start_time }</Text>
              </View>

              <TouchableOpacity onPress={this.orderBox.bind(this, item)} style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionThird]}>
                <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{ item.box }</Text>
              </TouchableOpacity>

              <View style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionFour]}>

                <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{item.car.autoMarka.marka} { item.car.autoNum }</Text>
              </View>

              <View style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionFive]}>
                <Text key={index} style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{item.type_wash}</Text>
                { 
                  item.services && item.services.length > 0 ?                 
                    <Text>
                      {
                        item.services.map((price, index)=>{
                          if((item.services.length - 1) == index){
                            return `${price.title}`
                          }else{
                            return `${price.title}, `
                          }
                        })
                      }
                    </Text>
                  :
                    <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>Информации нет</Text>
                }
                
              </View>

              <TouchableOpacity onPress={this.orderMember.bind(this, item)} style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionSix]}>
                <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{ item.member.firstName }</Text>
              </TouchableOpacity>

              <View style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionComment]}>
                <Text style={item.comment != null && item.comment.length > 0 ? styles.merchant.cabinetMerchant.tableHeadSectionTitleBodyRed : styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{ item.total } тг.</Text>
              </View>

              <View style={[styles.merchant.cabinetMerchant.tableHeadSectionBody, styles.merchant.cabinetMerchant.tableHeadSectionSevenBody]}>
                
                <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitleBody}>{ item.comment != null && item.comment.length > 0 ? item.comment : '-' }</Text>
              </View>

            </View>
		)
	}
}

export default ListOrders