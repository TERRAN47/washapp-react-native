import ListCompanies from './ListCompanies'
import ListMembers from './ListMembers'
import ListClient from './ListClients'
import ListOrders from './ListOrder'
import ListSuppliers from './ListSuppliers'
import ListAdditionally from './ListAdditionally'
import ListTypePayments from './ListTypePayments'
import ListBonus from './ListBonus'

export default {
	ListCompanies,
	ListMembers,
	ListOrders,
	ListClient,
	ListSuppliers,
	ListAdditionally,
	ListTypePayments,
	ListBonus
}