import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	Alert,
  Linking
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

class ListSuppliers extends Component {


  goToScreen(screen, item, index){

    let { navigate } = this.props.navigation

    return navigate(screen, {
      item,
      index
    })

  }

  getOpen(url){
    return Linking.openURL(url)
  }

	render(){

		let { item, index } = this.props

		return(

            <TouchableOpacity onPress={this.goToScreen.bind(this, 'viewSupplier', item, index)} style={styles.merchant.lists.itemSupplier}>

              <View style={styles.merchant.lists.itemSupplierImage}>
                <Image style={styles.merchant.lists.itemSupplierImageStyle} source={{uri:`${config.public_url}${item.logo}`}}/>
              </View>

              <View style={styles.merchant.lists.itemSupplierCenter}>

                <Text  style={styles.merchant.lists.itemSupplierCenterTitle}>{ item.title }</Text>
                
                {/*<View style={styles.merchant.lists.itemSupplierCenterSection}>
                                  <MaterialCommunityIcons name="coin" style={styles.merchant.lists.itemSupplierCenterSectionIcon} size={28} color="#ffcd19" />
                                  <Text>{ item.price }</Text>
                                </View>*/}

                <View style={styles.merchant.lists.itemSupplierCenterSection}>
                  <MaterialCommunityIcons name="map-marker" style={styles.merchant.lists.itemSupplierCenterSectionIcon} size={28} color="#ffcd19" />
                  <Text style={styles.merchant.lists.itemSupplierCenterSectionTextAdress}>{ item.location.title }, { item.adress }</Text>
                </View>

              </View>

              {/*<View style={styles.merchant.lists.itemSupplierRight}>
                <TouchableOpacity onPress={this.getOpen.bind(this, `${item.link_file}`)} style={[styles.merchant.lists.itemSupplierRightButton, {marginBottom:10}]}>
                                  <Text style={styles.merchant.lists.itemSupplierRightButtonTitle}>Прайс</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.getOpen.bind(this, `tel:${item.phone}`)} style={styles.merchant.lists.itemSupplierRightButton}>
                                  <Text style={styles.merchant.lists.itemSupplierRightButtonTitle}>Позвонить</Text>
                                </TouchableOpacity>
              </View>*/}

            </TouchableOpacity>
		)
	}
}

export default ListSuppliers