import React, { Component } from 'react'
import {
	View,
	Text,
	Platform,
	TouchableOpacity,
	Linking,
	Image,
	Alert
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { renderDistance } from 'Services/Utils'
import call from 'react-native-phone-call'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

class ListClient extends Component {

	closeRequest(){
		return Alert.alert('Сообщение', 'В данный момент вы не можете удалить сотрудника')
	}

	goToScreen(screen, item){
		const { navigate } = this.props.navigation
		return navigate(screen, {
			item
		})
	}
  	callCompany(phone){

		if(Platform.OS === 'android'){

			return Linking.openURL(`tel:+7${phone}`)
		}else{

			const args = {
			  number: `+7${phone}`
			}
			 
			return call(args).catch(console.error)
		}
  	}
	render(){

		let { item, index } = this.props

		return(
			<View style={styles.merchant.lists.memberView}> 
				<TouchableOpacity onPress={this.callCompany.bind(this, item.car.phone)} style={styles.merchant.lists.memberViewRight}>
					<FontAwesome name="phone" size={35} color="#fff" />
				</TouchableOpacity>	
				<View style={styles.merchant.lists.memberViewLeft}>
					<Image style={styles.merchant.lists.memberViewLeftLogo} source={{uri:`${config.public_url}${item.car.autoMarka.logo}`}}/>
				</View>

				<View style={styles.merchant.lists.memberViewCenter}>
					<View style={styles.merchant.lists.memberViewCenterNumCar}>
						<Text style={styles.merchant.lists.memberViewCenterNumCarTitle}>{ item.car.autoNum }</Text>
					</View>

					<View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor:item.car.autoColor}]}></View>

					<Image style={styles.merchant.lists.memberViewLeftCarBody} source={{uri:`${config.public_url}${item.car.avtoType.image}`}}/>
				</View>

				<TouchableOpacity onPress={this.goToScreen.bind(this, 'viewClient', item)} style={styles.merchant.lists.memberViewRight}>
					<MaterialCommunityIcons name="information-variant" size={30} color="#fff" />
				</TouchableOpacity>
			</View>
		)
	}
}

export default ListClient