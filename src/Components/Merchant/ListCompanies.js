import React, { Component } from 'react'
import {
	View,
	Text,
	Alert,
	TouchableOpacity,
	Image
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { renderDistance } from 'Services/Utils'
import Api from 'Services/Api'

class ListCompanies extends Component {

	constructor(props){
		super(props)

		this.renderTime = this.renderTime.bind(this)
	}

	goToScreen(){
		let { item, index } = this.props
		const { navigate } = this.props.navigation
		return navigate(this.props.routeName, {
			item,
			companyIndex:index,
			id:item.id
		})
	}

	renderTime(time){
		return moment(time).fromNow()
	}
	deleteCompany(){
		let { item, companies, dispatch } = this.props

	    Alert.alert(
	      'ВАЖНО!',
	      'Поcле удаления автомойки удалятся все информация связанная с данной автомойкой, Вы уверены?',
	      [
	        {
	          text: 'Отмена',
	          onPress: () => console.log('Cancel'),
	          style: 'cancel',
	        },
	        {text: 'Удалить', onPress: () => Api.merchant.carwash.deletCompany(dispatch, item.id, companies) },
	      ],
	      {cancelable: false},
	    );
	}
	render(){

		let { item, merchStatus} = this.props

		return(
			<TouchableOpacity style={item.status.active ? [styles.merchant.lists.buttonMainCompany, {borderRightColor:"#71b527"}] : [styles.merchant.lists.buttonMainCompany, {borderRightColor:"#d4451a"}]} onPress={this.goToScreen.bind(this)}>
{/*				<View style={styles.merchant.lists.buttonMainCompanyStatus}>
					{
						item.status.active ? 
						<View style={styles.merchant.lists.buttonMainCompanyStatusActive}>
							<Text style={styles.merchant.lists.buttonMainCompanyStatusText}>Активно</Text>
						</View> 
						: 
						<View style={styles.merchant.lists.buttonMainCompanyStatusInactive}>
							<Text style={styles.merchant.lists.buttonMainCompanyStatusText}>Не активно</Text>
						</View>
					}
				</View>*/}
				<View style={styles.merchant.lists.buttonMainCompanyStatus}>
					{
						merchStatus && 
						<TouchableOpacity onPress={this.deleteCompany.bind(this)} style={{paddingRight:5}}>
                            <MaterialIcons name="close" size={35} color="#d4451a" />
						</TouchableOpacity> 
					}
				</View>
				<View style={styles.merchant.lists.companyImage}>
					<Image style={styles.merchant.lists.companyLogoStyle} source={{uri:`${config.public_url}${item.logo}`}} />
				</View>
				<View style={styles.merchant.lists.companyInfo}>

					<Text style={styles.merchant.lists.companyInfoTitle}>{ item.title }</Text>

					<Text style={styles.merchant.lists.companyInfoAdress}>{ item.adress }</Text>

					{
						item.distance ? 
							<View style={styles.merchant.lists.ViewSectionInfoItem}>
								<MaterialCommunityIcons name="map-marker" size={18} color="rgba(0,0,0,0.45)" />
								<Text style={styles.merchant.lists.ViewSectionInfoItemTextLocation}>{ renderDistance(item.distance) }</Text>
							</View>
						: 
							<View></View>
					}

					<View style={styles.merchant.lists.ViewSectionInfoItem}>
						<MaterialCommunityIcons name="timer" size={18} color="rgba(0,0,0,0.45)" />
						<Text style={styles.merchant.lists.ViewSectionInfoItemText}>создано { this.renderTime(item.createdAt) }</Text>
					</View>

				</View>
			</TouchableOpacity>
		)
	}
}

export default ListCompanies