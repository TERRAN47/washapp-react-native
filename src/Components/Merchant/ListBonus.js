import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	Alert
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { renderDistance } from 'Services/Utils'

class ListBonus extends Component {

	removeBonus(){

		let { item, index, bonuses, dispatch, api, id } = this.props
		let newBonuses = []

		bonuses.map((bonus, indexBonus)=>{
			if(indexBonus != index){
				newBonuses = [...newBonuses, bonus]
			}
		})

		return api.merchant.bonuses.removeBonus(dispatch, newBonuses, id)

	}

	render(){

		let { item, index, statusRequest } = this.props

		return(
			<View style={styles.merchant.lists.bonusView}>
				
				<View style={styles.merchant.lists.bonusViewLeft}>
					<Text>{ item.description }</Text>
				</View>

				{
					statusRequest ?
						<TouchableOpacity onPress={this.removeBonus.bind(this)} style={styles.merchant.lists.bonusViewRight}>
							<MaterialCommunityIcons name="clock-outline" size={30} color="#466dfa" />
						</TouchableOpacity>
					:
						<TouchableOpacity onPress={this.removeBonus.bind(this)} style={styles.merchant.lists.bonusViewRight}>
							<MaterialCommunityIcons name="delete-circle" size={30} color="#cc2b2b" />
						</TouchableOpacity>
					
				}

			</View>
		)
	}
}

export default ListBonus