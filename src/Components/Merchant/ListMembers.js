import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	Alert
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { renderDistance } from 'Services/Utils'

class ListMember extends Component {

	closeRequest(){
		return Alert.alert('Сообщение', 'В данный момент вы не можете удалить сотрудника')
	}

	openMemberView(screen){

	    const { navigate } = this.props.navigation
	    let { item, index, members, id } = this.props
	    return navigate(screen, {
	    	item,
	    	index,
	    	members,
	    	id
	    })

	}

	render(){

		let { item, index, statusRequest } = this.props

		return(
			<View style={styles.merchant.lists.memberView}>
				<View style={styles.merchant.lists.memberViewInfo}>
					<View style={styles.merchant.lists.memberViewAvatar}>
						{/*<MaterialCommunityIcons name="account" size={38} color="#fff" />*/}
						<Image style={styles.merchant.lists.memberViewAvatarImage} source={require('Media/moishikBlue.png')} />
					</View>
					<View  style={styles.merchant.lists.memberViewInfoNameAndAdress}>
						<Text style={styles.merchant.lists.memberViewUserName}>{ item.firstName } (Работает с: { item.date })</Text>
						<Text style={styles.merchant.lists.memberViewUserAdress}>{ item.adress }</Text>
					</View>
				</View>
				{
					statusRequest ? 
						<TouchableOpacity onPress={this.closeRequest.bind(this)} style={styles.merchant.lists.memberViewButtonClientDeleteOrAdd}>
							<Text style={styles.merchant.lists.memberViewButtonClientDeleteOrAddText}>Подождите...</Text>
						</TouchableOpacity>
					:
						<TouchableOpacity onPress={this.openMemberView.bind(this, 'viewMember')} style={styles.merchant.lists.memberViewButtonClientDeleteOrAdd}>
							<MaterialCommunityIcons name="information-variant" size={30} color="#fff" />
						</TouchableOpacity>

				}
			</View>
		)
	}
}

export default ListMember