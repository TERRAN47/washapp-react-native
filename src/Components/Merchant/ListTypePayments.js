import React, { Component } from 'react'

import {
	View,
	Text,
	Switch
} from 'react-native'

class ListTypePayments extends Component {

	constructor(props){
		super(props)

		this.state= {
			item:{}
		}
	}

	componentDidMount(){

		let { item } = this.props

		this.setState({
			item
		})

	}

	changeStatus(value){

		let { dispatch, api, index, items, companyIndex, id } = this.props

		this.setState({
			item:{
				...this.state.item,
				status:value
			}
		})

		items[companyIndex].type_payments[index].status = value

		return api.merchant.carwash.changeStatusPayments(dispatch, items, id, companyIndex)

	}

	render(){

		let { item, items, index, styles, api } = this.props

		return(
			<View style={styles.lists.mainItemView}>
				<Text style={styles.lists.mainItemtitle}>{ item.title }</Text>
				<Switch value={item.status} onValueChange={(value)=>{this.changeStatus(value)}}/>
			</View>
		)

	}

}

export default ListTypePayments