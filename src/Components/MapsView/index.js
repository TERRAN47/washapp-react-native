import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { mapStyle } from '../../Containers/Domain'
import MapView, { Marker, AnimatedRegion, Animated } from 'react-native-maps'

class MapsView extends Component {

	constructor(props){
		super(props)

	}

	render(){

		let { coords, height, title } = this.props

		return(
			<View style={{width:'100%',height:height,marginTop:15,marginBottom:15}}>
				<Text style={styles.maps.titleMapMain}>{ title }</Text>
		    	<MapView
		    		showUserLocation={true}
					followUserLocation={true}
					loadingEnabled={true}
					zoomEnabled = {false}
					customMapStyle={mapStyle}
		    		onPress={(event)=>{
		    			return alert(22)
		    		}}
		       		style={styles.maps.mapStyle}
			        initialRegion={{
			        	...coords,
	    				latitudeDelta: 0.004,
	    				longitudeDelta: 0.004,
			        }}
		     	>
					<Marker 
						coordinate={coords}
					>
						<View>
							<Image source={require('./Resources/Map-Marker.png')} style={styles.maps.markerStyleImage} />
						</View>
					</Marker>
		     	</MapView>	
	     	</View>
		)
	}
}

export default MapsView