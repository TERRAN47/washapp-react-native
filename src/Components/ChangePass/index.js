import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  Alert,
  ImageBackground
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { Header} from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {constans, icons} from 'Services/Utils'
import config from 'Config'
import { renderDistance } from 'Services/Utils'


class ChangePass extends Component {

	constructor(props) {

  	super(props);

	    this.state = {
	    	lastPass:'',
	    	newpassword:'',
	    	repassword:''
	  	};
	}

	savePass(){
		let {newpassword, repassword} = this.state
		let {dispatch} = this.props

		if(newpassword == repassword){
			Api.changePass.changePassword(dispatch)
		}else{
			Alert.alert('Внимание!', 'Пароль не совпадает! Попробуйте снова')
		}
	}

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

	render(){

	  	let { user, client } = this.props

	    return(
	      <TouchableOpacity style={client ? styles.merchant.cabinetMerchant.viewSectionBetweenClient : styles.merchant.cabinetMerchant.viewSectionBetween} onPress={this.goToScreen.bind(this, 'ResetPasswordScreen')}>
	        <View style={styles.merchant.cabinetMerchant.viewSectionSettings}>
	          <MaterialCommunityIcons name="lock-reset" style={styles.merchant.cabinetMerchant.iconSection} size={28} color="#5b5b5b" />
	          <Text style={styles.merchant.cabinetMerchant.sectionText}>Изменить пароль</Text> 
	        </View>
	        {
	          client ? 
	            <FontAwesome name="edit" size={30} color={constans.orangeColor} />
	          :
	            <MaterialCommunityIcons name="square-edit-outline" style={styles.merchant.cabinetMerchant.iconSection} size={28} color={constans.orangeColor} />
	        }
	      </TouchableOpacity>
	    )
	}

}

const mapStateToProps = (state) => {
  return {
    user:state.user,
  }
}

export default connect(mapStateToProps)(ChangePass)