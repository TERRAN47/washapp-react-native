import React, { Component } from 'react'
import {
  	View,
  	Text,
    Linking,
  	TouchableOpacity,
	Modal
} from 'react-native'
import styles from 'Services/Styles'
import {  MyText } from 'Composition'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {constans} from 'Services/Utils'
import call from 'react-native-phone-call'

class PhoneModal extends Component {

  render(){
    let {phones, closeModalPhone, phoneStatus} = this.props
   
    return(
        <Modal
          animationType="slide"
          onRequestClose={()=>{}}
          transparent={false}
          visible={phoneStatus}
        >
          <View style={styles.modal.modalViewBlock}>
            {
              phones && 
                phones.map((elem, index)=>{
                  return(
                    <TouchableOpacity style={styles.modal.buttonCall} onPress={()=>{
                      if(Platform.OS === 'android'){
                        Linking.openURL(`tel:+7${elem}`)
                      }else{                    
                        call({number: `+7${phones[0]}`}).catch(console.error)
                      }
                      closeModalPhone()
                    }}>
                      <FontAwesome
                        name="phone" 
                        style={styles.profilescreen.iconText} 
                        size={20} 
                      />
                      <MyText style={styles.modal.textModal} key={index}> {constans.probelText(`+7${elem}`)}</MyText>
                    </TouchableOpacity>
                  )
                })
            }
            <Text onPress={()=>{closeModalPhone()}}>ЗАКРЫТЬ</Text>           
          </View>
        </Modal>
    )

  }
}

export default PhoneModal