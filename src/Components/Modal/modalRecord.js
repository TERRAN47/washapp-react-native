import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native'
import { MyText,  } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {icons} from 'Services/Utils'

class ModalRecord extends Component {

  constructor(props) {

    super(props);
  
    this.state = {

    };
  }

  onCancel(){
    let {dispatch} = this.props

    dispatch({
      type:'UPDATE_MODAL',
      modalStatus:false
    })
  }

  deleteRecord(){
    let {dispatch, user, socket, invitedBox, queue} = this.props

    socket.emit('deleteQueue', {user_id:user.id, company_id:queue.company_id, boxIndex:invitedBox ? invitedBox.index : undefined})
    return dispatch({
      type:'UPDATE_MODAL',
      modalStatus:false
    }) 
  }

  async skipQueue(){
    let {queue, socket, dispatch, invitedBox, user, companyId} = this.props
    await socket.emit('slipQueueFromModal', {queue, userId:user.id, companyId, boxIndex:invitedBox.index})
    return dispatch({
      type:'UPDATE_MODAL',
      modalStatus:false
    })
  }
  confirm(){
    let {dispatch, invitedBox, navigation} = this.props
    Api.user.orders.confirmInvite(dispatch, navigation, invitedBox)
  }

  render(){
    let {modalStatus, queueCount, invitedBox, myIndex, content} = this.props
   
    return(
      <View>
        <View>
          <View style={styles.modal.view}></View>
          {
            content  == 'delete' &&
            <View style={styles.modal.modalBlock}>
              <FontAwesome name="close" size={35} color="red" />
              <MyText style={styles.modal.titleText}>ПОДТВЕРДИТЬ ОТМЕНУ?</MyText>
              <TouchableOpacity style={styles.modal.yesButton} onPress={this.deleteRecord.bind(this)}>
                <MyText style={styles.modal.yesTextButton}>ДА</MyText>
              </TouchableOpacity>
              <TouchableOpacity style={styles.modal.noButton} onPress={this.onCancel.bind(this)}>
                <MyText style={styles.modal.noTextButton}>НЕТ</MyText>
              </TouchableOpacity> 
            </View>              
          }
          {
            content  == 'invate' &&
            <View style={styles.modal.modalBlock}>
              <Image
                style={styles.modal.invitedIcon} 
                source={icons.qeueCarsGreen} 
              />
              <Text style={styles.modal.titleInvateText}>ВАША ОЧЕРЕДЬ ПОДОШЛА</Text>
              <Text style={styles.modal.titleInvateText}>ДОБРО ПОЖАЛОВАТЬ!</Text>
              <Image
                style={styles.modal.invitedIcon} 
                source={icons.boxGreen} 
              />
              <Text style={styles.modal.titleInvateText}>{invitedBox.index+1}</Text>
              <Image
                style={styles.modal.invitedIcon}
                source={icons.memberBox} 
              />
              <Text style={styles.modal.titleInvateText}>{invitedBox.member.firstName}</Text> 
              <TouchableOpacity onPress={this.confirm.bind(this)}>
                <Text style={styles.modal.confirmButton}>ПОДТВЕРДИТЬ</Text>
              </TouchableOpacity>
              {
                queueCount > 0 &&
                <TouchableOpacity onPress={this.skipQueue.bind(this)}>
                  <Text style={styles.modal.dafaultButton}>ПРОПУСТИТЬ</Text>
                </TouchableOpacity> 
              }
              <TouchableOpacity onPress={this.deleteRecord.bind(this)}>
                <Text style={styles.modal.dafaultButton}>ОТМЕНИТЬ</Text>
              </TouchableOpacity> 
            </View>              
          }
          {
            content  == '' &&
            <View style={styles.modal.modalBlock}>
              <TouchableOpacity onPress={this.onCancel.bind(this)}>
                <Text style={styles.modal.dafaultButton}>ЗАКРЫТЬ</Text>
              </TouchableOpacity> 
            </View>
          }
        </View>      
      </View>

    )

  }
}

export default ModalRecord