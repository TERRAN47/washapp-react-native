import React, { Component } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Alert,
  TextInput,
  Share
} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import DatePicker from 'react-native-datepicker'
import ModalWrapper from 'react-native-modal-wrapper'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class Modal extends Component {

  constructor(props){
    super(props)

    this.state = {
      member:{
        firstName:'',
        phone:'7',
        characteristic:'',
        date:'',
        adress:'',
        percent:''
      },
      sendMail:false,
      email:'',
      bonus:'',

      //report
      linkReport:'',
      dateReport:'',
      statusLink:false
    }

    this.renderModal = this.renderModal.bind(this)
  }

  sendMailOrderStatus(){

    return this.setState({
      sendMail:!this.state.sendMail
    })

  }

  async sendMailOrder(){

    let { dispatch, navigation, list, count, percent, cash, total, id, title } = this.props
    let { email } = this.state

    let { 
      linkReport,
      status,
      dateReport,
      message
    } = await Api.merchant.orders.sendAndSaveOrders(dispatch, navigation, list, count, percent, cash, total, email, id, title)

    if(status){

      return this.setState({
        linkReport,
        dateReport,
        statusLink:status
      })

    }else{

      return Alert.alert('Сообщение', message)

    }

  }

  async sendReportToSocials(){

    let {
      linkReport,
      dateReport
    } = this.state

    try {
      const result = await Share.share({
        message:`Отчет за ${dateReport}: ${linkReport}`
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      Alert.alert('Сообщение', error.message)
    }
  }

  renderModal(){

    let { type, modalOpened, count, percent, cash, total, statusRequest } = this.props
    let { member, sendMail } = this.state

    if(type == 'members'){

      return(<ModalWrapper
          onRequestClose={()=>{}}
          style={styles.merchant.forms.modalformView}
          visible={modalOpened}>
          <ScrollView>
            <View>
              <Text style={styles.merchant.forms.modalformTitle}>Новый сотрудник</Text>

              <View style={styles.merchant.forms.modalformContent}>
                
                <View style={styles.merchant.forms.modalformSectionInput}>
                  <Text style={styles.merchant.forms.modalformSectionInputTitle}>Имя</Text>
                  <TextInput onChangeText={(event)=>{ this.setState({member:{...this.state.member,firstName:event}}) }} style={styles.merchant.forms.modalformSectionInputStyle} autoFocus={true} placeholder='Имя' />
                </View>

                <View style={styles.merchant.forms.modalformSectionInput}>
                  <Text style={styles.merchant.forms.modalformSectionInputTitle}>Телефон</Text>

                  <TextInputMask
                    type={'cel-phone'}
                    options={{
                      withDDD: true,
                      dddMask: '+9 (999) 999 99 99'
                    }}
                    maxLength={18}
                    value={this.state.member.phone}
                    placeholder="Номер телефона"
                    style={styles.merchant.forms.modalformSectionInputStyle}
                    refInput={ref => { this.input = ref }}
                    onChangeText={(event)=>{ this.setState({member:{...this.state.member,phone:event}}) }}
                  />

                </View>

                <View style={styles.merchant.forms.modalformSectionInput}>
                  <Text style={styles.merchant.forms.modalformSectionInputTitle}>Адрес</Text>
                  <TextInput onChangeText={(event)=>{ this.setState({member:{...this.state.member,adress:event}}) }} style={styles.merchant.forms.modalformSectionInputStyle} placeholder='Адрес' />
                </View>

                <View style={styles.merchant.forms.modalformSectionInput}>
                  <Text style={styles.merchant.forms.modalformSectionInputTitle}>Характеристика</Text>
                  <TextInput onChangeText={(event)=>{ this.setState({member:{...this.state.member,characteristic:event}}) }} style={styles.merchant.forms.modalformSectionInputStyle} placeholder='Характеристика' />
                </View>

                <View style={styles.merchant.forms.modalformSectionInput}>
                  <Text style={styles.merchant.forms.modalformSectionInputTitle}>Процент</Text>
                  <TextInput keyboardType="numeric" onChangeText={(event)=>{ this.setState({member:{...this.state.member,percent:event}}) }} style={styles.merchant.forms.modalformSectionInputStyle} placeholder='Процент' />
                </View>

                <View style={styles.merchant.forms.modalformSectionInput}>
                  <Text style={styles.merchant.forms.modalformSectionInputTitle}>Дата приема на работу</Text>
                  <DatePicker
                    style={styles.merchant.forms.timeInputView}
                    date={member.date}
                    mode="date"
                    placeholder="Выберите дату"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                      dateInput: styles.merchant.forms.timeInputStyle
                    }}
                    onDateChange={(date) => {this.setState({
                      member:{...this.state.member,date:date}
                    })}}
                  />
                </View>

                <View style={styles.merchant.forms.modalformButtons}>
                  <TouchableOpacity style={styles.merchant.forms.modalformButtonsCancel} onPress={this.props.closeModal}>
                    <Text style={styles.merchant.forms.modalformButtonsCancelText}>Отмена</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.merchant.forms.modalformButtonsAdd} onPress={()=>{
                    this.setState({
                      member:{...this.state.member,date:''}
                    })
                    this.props.newMember(member)
                  }}>
                    <Text style={styles.merchant.forms.modalformButtonsAddText}>добавить</Text>
                  </TouchableOpacity>
                </View>

              </View>        
            </View>
          </ScrollView>
      </ModalWrapper>)

    }else if(type == 'statisticOrder'){

      let { statusLink } = this.state

      return(<ModalWrapper
          onRequestClose={()=>{}}
          style={styles.merchant.forms.modalformView}
          visible={modalOpened}>

          {
            sendMail ?

              <View>
                <Text style={styles.merchant.forms.modalformTitle}>Отправка отчета</Text>

                <View style={styles.merchant.forms.modalformContent}>
                  
                  <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooter}>
                    
                    <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionInputAndInfo}>

                      <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionInputAndInfoFirstText}>Создать файл отчета</Text>
                      <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionInputAndInfoSecondText}>После создания файла, Вы сможете отправить</Text>
                      {/*<TextInput value={this.state.email} keyboardType="email-address" placeholder="Email" style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionInputAndInfoStyle} onChangeText={(email)=>this.setState({email})}/>*/}

                      {
                        statusLink ?
                          <Text>Отчет успешно создан</Text>
                        :
                          <View></View>
                      }

                    </View>

                    {

                      statusLink ?
                        <TouchableOpacity onPress={this.sendReportToSocials.bind(this)} style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButton}>
                          <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonTitle}>Отправить отчет</Text>
                        </TouchableOpacity>
                      : statusRequest ?
                        <TouchableOpacity style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButton}>
                          <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonTitle}>Подождите...</Text>
                        </TouchableOpacity>
                      :
                        <TouchableOpacity onPress={this.sendMailOrder.bind(this)} style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButton}>
                          <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonTitle}>Создать отчет</Text>
                        </TouchableOpacity>

                    }

                    <TouchableOpacity onPress={this.sendMailOrderStatus.bind(this)} style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonClose}>
                      <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonCloseTitle}>Назад</Text>
                    </TouchableOpacity>

                  </View>

                </View>        
              </View>

            :

              <View>
                <Text style={styles.merchant.forms.modalformTitle}>Итог</Text>

                <View style={styles.merchant.forms.modalformContent}>
                  
                  <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooter}>
                    
                    <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSection}>

                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeft}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeftText}>КОЛ-ВО АВТО</Text>
                      </View>
                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRight}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRightText}>{ count }</Text>
                      </View>

                    </View> 

                    <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSection}>

                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeft}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeftText}>ПРОЦЕНТ</Text>
                      </View>
                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRight}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRightText}>{ percent } тг.</Text>
                      </View>

                    </View> 

                    <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSection}>

                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeft}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeftText}>В КАССУ</Text>
                      </View>
                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRight}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRightText}>{ cash } тг.</Text>
                      </View>

                    </View> 

                    <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSection}>

                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeft}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionLeftText}>ВСЕГО</Text>
                      </View>
                      <View style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRight}>
                        <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionRightText}>{ total } тг.</Text>
                      </View>

                    </View>

                    {

                      statusRequest ?
                        <TouchableOpacity style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButton}>
                          <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonTitle}>СОХРАНЯЕМ...</Text>
                        </TouchableOpacity>
                      :
                        <TouchableOpacity onPress={this.sendMailOrderStatus.bind(this)} style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButton}>
                          <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonTitle}>СОХРАНИТЬ И</Text>
                          <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonTitle}>ЗАКРЫТЬ СМЕНУ</Text>
                        </TouchableOpacity>

                    }

                    <TouchableOpacity onPress={this.props.closeModal} style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonClose}>
                      <Text style={styles.merchant.cabinetMerchant.mainViewStatisticFooterSectionButtonCloseTitle}>Закрыть</Text>
                    </TouchableOpacity>

                  </View>

                </View>        
              </View>   

          }

      </ModalWrapper>)

    }else if(type == 'BONUS'){
      return(<ModalWrapper
          onRequestClose={()=>{}}
          style={styles.merchant.forms.modalformView}
          visible={modalOpened}>

          <View style={styles.merchant.forms.modalformSectionInput}>
            <Text style={styles.merchant.forms.modalformSectionInputTitle}>Опишите бонус и нажмите добавить</Text>
            <TextInput onChangeText={(event)=>{ this.setState({bonus:event}) }} style={styles.merchant.forms.modalformSectionInputStyle} placeholder='Бонус' />
          </View>

          <View style={styles.merchant.forms.modalformButtons}>
            <TouchableOpacity style={styles.merchant.forms.modalformButtonsCancel} onPress={this.props.closeModal}>
              <Text style={styles.merchant.forms.modalformButtonsCancelText}>Отмена</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.merchant.forms.modalformButtonsAdd} onPress={()=>{this.props.newBonus(this.state.bonus)}}>
              <Text style={styles.merchant.forms.modalformButtonsAddText}>добавить</Text>
            </TouchableOpacity>
          </View>

      </ModalWrapper>)
    }else{
      return(<ModalWrapper
          onRequestClose={()=>{}}
          style={styles.merchant.forms.modalformView}
          visible={modalOpened}>
        <Text>default modal</Text>
        <TouchableOpacity style={styles.merchant.forms.modalformButtonsCancel} onPress={this.props.closeModal}>
          <Text style={styles.merchant.forms.modalformButtonsCancelText}>Закрыть</Text>
        </TouchableOpacity>
      </ModalWrapper>)
    }

  }

  render(){

    return this.renderModal()

  }
}

export default Modal