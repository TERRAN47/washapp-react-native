import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
	Alert,
	Linking,
	Platform,
  Keyboard,
  TextInput,
} from 'react-native'
import { MyText } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {constans} from 'Services/Utils'
import config from 'Config'
import { socket } from 'Services/Utils'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import call from 'react-native-phone-call'

class ModalBoxInfo extends Component {

	constructor(props) {

  	super(props);
  	this.socket = socket()
    this.state = {
    	editItogSumm:false,
    	newSumm:0,
    	payClick:false,
    	countUsedAdd:0,
    	reStatus:false,
    	comment:'',
    	commentBonus:'',
    	summServices:0
    };
	}
		
	componentWillUnmount() {
		let {dispatch} = this.props;

		dispatch({
			type:'CLIENT_INFO',
			clientInfo:null
		})
		return this.socket.disconnect()
	}
		
  async clientPaymed(boxInfo, summServices){
		let {newSumm, countUsedAdd, commentBonus, reStatus, comment} = this.state
		let {companyInfo, payBox} = this.props
		this.setState({payClick:true})
		summServices = parseInt(summServices)
		if(newSumm > 0 && newSumm != ''){
			summServices = parseInt(newSumm)
		}

		const user_id = boxInfo.car.user_id;
		const boxIndex = boxInfo.index;
	
		if(commentBonus != '') comment = `${comment}. ${commentBonus}`
		
		let requestPay = await Api.merchant.orders.payBoxComplit(boxIndex, companyInfo.id, comment, summServices, countUsedAdd, reStatus);

		if(requestPay == 'OK'){	
			this.socket.emit('payClientBox', {
					user_id
			})
			this.socket.emit('updateBoxesAdmin', {
				id:companyInfo.id
			})
			payBox(boxIndex, false)
		}else {
			this.setState({payClick:false})
			Alert.alert("Внимание!", 'Ошибка соединения попробуйте позже')
		}
	}
		
	componentDidMount(){
		let {content, boxInfo, companyInfo, dispatch} = this.props;
		console.log("boxInfo.car", boxInfo.car);

		if(content  != 'services'){
			const company_id = companyInfo.id;
			const user_id = boxInfo.car.user_id;
			const car = boxInfo.car.car
			Api.merchant.orders.getCountWashes(dispatch, user_id, company_id, car);
		}
	}
		
	onCancel(){
    let { switchModalStatus} = this.props

    switchModalStatus(false)
	}
		
	saveItogSumm(newSumm){
		let {commentBonus, comment} = this.state
		if(commentBonus != ''){
			this.setState({editItogSumm:false})
		}else{
		if(comment == '' && newSumm > 0){
  			Alert.alert("Внимание!", "Заполните поле коментарий")
  		}else{
  			this.setState({editItogSumm:false})
  		}  			
		}
	}
		
	editServices(){
		let {navigation, companyInfo} = this.props
		let {boxInfo, switchModalStatus} = this.props
		navigation.navigate('selectServices', {boxInfo, companyPrice:companyInfo.price, close:this.onCancel.bind(this), companyId:companyInfo.id})
		switchModalStatus(false)
	}
	
	addUsedCount(simvol){
		let {countUsedAdd} = this.state
		let {clientInfo} = this.props

		if(simvol == 'minus'){
			clientInfo.used_count > countUsedAdd && this.setState({countUsedAdd:countUsedAdd+1})  
		}else{
			countUsedAdd != 0 && this.setState({countUsedAdd:countUsedAdd-1})  
		}				
	}
		
	restartCount(){

		let {companyInfo, clientInfo, boxInfo} = this.props
		const user_id = boxInfo.car.user_id;
		const company_id = companyInfo.id;
		const clientCount = clientInfo.count

    Alert.alert(
      'ВНИМАНИЕ!',
      'Обнулить клиенту все мойки?',
      [
        {
          text: 'Отмена',
          onPress: () => console.log('Cancel'),
          style: 'cancel',
        },
        {text: 'Обнулить', onPress: async () => {
			this.setState({reStatus:true})
			const requestReset = await Api.merchant.orders.restartCountWash(company_id, user_id, true, clientCount);

			if(requestReset == "OK"){
				this.socket.emit('payClientBox', {
			    	user_id
				})
				this.setState({commentBonus:`Бесплатная ${clientCount}я мойка (бонус)`})		
			}else{
				Alert.alert("Внимание!", 'Ошибка соединения попробуйте позже')
				this.setState({reStatus:false})
			}
        } },
      ],
      {cancelable: false},
    );
	}

	callCompany(phone){
		if(Platform.OS === 'android'){
			return Linking.openURL(`tel:+7${phone}`)
		}else{
			const args = {
				number: `+7${phone}`
			}
			return call(args).catch(console.error)
		}	
	}	

	render(){
		let {editItogSumm, newSumm, payClick, countUsedAdd, reStatus, summServices} = this.state;
    let {clientInfo, companyInfo, payStatus, boxInfo, content} = this.props;

    return(  	
     	<View>
     		<View style={styles.modal.view}></View>
					<View  style={!editItogSumm ? styles.modal.modalBlock : [styles.modal.modalBlock]}>
					<ScrollView onPress={()=>{Keyboard.dismiss}}>
						<MyText style={styles.modal.boxTitle}>ПОСТ: №{boxInfo.index+1}</MyText>
						<View style={styles.modal.rowBlock}>
								<Image style={styles.merchant.queue.itemBoxContentStyleLeftImage} source={require('Media/moishikBlack.png')} />
								<MyText style={styles.modal.textServices}>{boxInfo.member.firstName}</MyText>

								<View style={styles.profilescreen.imgBlock}>
									<Image 
										source={{uri: `${config.public_url}${boxInfo.car.car.avtoType.image}`}}  
										style={styles.profilescreen.carImage}
									/>							
								</View>
								{boxInfo.car && boxInfo.car.car.autoColor ? 
									<View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor:boxInfo.car.car.autoColor}]}></View> 
									:
										null
								} 
						</View>
						<View>

							<View style={styles.modal.rowBlock}>
								<View style={styles.profilescreen.imgBlock}>
									<Image 
										source={{uri: `${config.public_url}${boxInfo.car.car.autoMarka.logo}`}}  
										style={styles.profilescreen.carImage}
									/>							
								</View>
								<View style={styles.merchant.lists.memberViewCenterNumCar}>
									<MyText style={styles.merchant.lists.memberViewCenterNumCarTitle}>{ boxInfo.car.car.autoNum } </MyText>
								</View>   				
							</View>
							<View>
								<MyText style={styles.modal.boxTitle}> {!boxInfo.car.custom_status && constans.firtUpCase(boxInfo.car.car.autoModel)} </MyText>
							</View>
						</View>

						{
							clientInfo && !reStatus && clientInfo.count > 0 && companyInfo.bonus.length > 0 && 
							<View style={styles.modal.bonusesBlock}>
								<View style={{alignItems: 'center'}}>
									{
										companyInfo.bonus.map((elem, index)=>{
											return(
												<MyText key={index}>{elem.description}</MyText>
											)
										})
									}
								</View>
							<View style={[styles.modal.rowBlock, {marginLeft:35}]}>
									<MyText>Количество моек: {clientInfo.count}</MyText>
									<TouchableOpacity style={styles.modal.minusButton} onPress={this.restartCount.bind(this)}>
										<MaterialCommunityIcons name="reload" size={30} color="#fff" />
									</TouchableOpacity>
								</View>
								<View style={[styles.modal.rowBlock, {marginLeft:15}]}>
									<TouchableOpacity style={[styles.modal.minusButton, {marginRight:10}]} onPress={this.addUsedCount.bind(this, 'plus')}>
										<MaterialCommunityIcons name="plus" size={30} color="#fff" />
									</TouchableOpacity>
									<MyText>Баллы: {(clientInfo.used_count)}{countUsedAdd > 0 ? `-${countUsedAdd}` : ''}</MyText>
									<TouchableOpacity style={styles.modal.minusButton} onPress={this.addUsedCount.bind(this, 'minus')}>
										<MaterialCommunityIcons name="minus" size={30} color="#fff" />
									</TouchableOpacity>
								</View>
							</View>	     				
						}

						<View style={styles.modal.servicesBlock}>
							{
								boxInfo.car.services.map((elem, index)=>{
									if(newSumm > 0){
										summServices = newSumm	
									}else{
										summServices += +(elem.price)
									}
									return(
										<View  key={index}>
											<Text style={styles.modal.textServices}>{elem.title}: {elem.price} {elem.currency}</Text>
										</View>
									)
								})
							}
							{
								content  == 'services' ?
									payStatus ?
									<TouchableOpacity onPress={this.onCancel.bind(this)}>
											<Text style={styles.modal.totalSummText}>ЗАКРЫТЬ</Text>
									</TouchableOpacity> 
									:
										<View style={{alignItems: 'center'}}>
											<View>
												<Text style={styles.modal.totalSummText}>Итог: {summServices} тг</Text>
											</View>
											<TouchableOpacity onPress={this.editServices.bind(this)} style={{alignItems: 'center'}}>
												<MaterialCommunityIcons name="plus-circle" size={40} color="#466dfa" />
												<Text style={styles.modal.boxTitle}>Изменить услуги</Text>
										</TouchableOpacity>
										<TouchableOpacity onPress={this.onCancel.bind(this)}>
												<Text style={styles.modal.totalSummText}>ЗАКРЫТЬ</Text>
										</TouchableOpacity> 
										</View>
								:
									<View style={{alignItems: 'center'}}>
									{
										editItogSumm ?
											<View style={{alignItems: 'center'}}>
												<TextInput 
													onChangeText={(newSumm)=>{this.setState({newSumm})}}
													keyboardType="numeric"
													maxLength={6}
													underlineColorAndroid='transparent' 
													style={styles.modal.inputForm} 
													value={newSumm}
													placeholder={`${summServices}`}
												/>
												<TextInput 
													onChangeText={(comment)=>{this.setState({comment})}}
													maxLength={200}
													multiline={true}
													underlineColorAndroid='transparent' 
													style={styles.modal.inputComent} 
													placeholder="Коментарий"
												/>
											</View>
										:
											<TouchableOpacity onPress={()=>{this.setState({editItogSumm:true})}} style={[styles.modal.rowBlock, {marginLeft:30}]}>
												<Text style={styles.modal.totalSummText}>Итог: {summServices} тг.</Text>
												<MaterialCommunityIcons name="square-edit-outline" size={30}  color="#5b5b5b" />
											</TouchableOpacity>
										}		              	
										{!editItogSumm ?
											<View style={{alignItems: 'center'}}>
												<TouchableOpacity onPress={this.onCancel.bind(this)}>
														<Text style={styles.modal.totalSummText}>ЗАКРЫТЬ</Text>
												</TouchableOpacity> 
												{
													!payClick &&
													<TouchableOpacity onPress={this.clientPaymed.bind(this, boxInfo, summServices)}>
															<MyText style={styles.modal.paidButton}>ОПЛАЧЕНО</MyText>
													</TouchableOpacity>
												}
											</View>
										:
											<TouchableOpacity onPress={this.saveItogSumm.bind(this, newSumm)}>
													<MyText style={styles.modal.saveButton}>СОХРАНИТЬ</MyText>
											</TouchableOpacity>			            		
										}
									</View>
							}
						</View> 
						<View style={{alignItems: 'center', marginTop:30}}>	
							<TouchableOpacity onPress={this.callCompany.bind(this, boxInfo.car.car.phone)} style={styles.modal.buttonsOval}>
								<FontAwesome5 name="phone" size={30} color="#fff" />
							</TouchableOpacity>
						</View>
					</ScrollView>			
				</View> 
      </View> 	     	
    )
  }
}

export default ModalBoxInfo
