import React, { PureComponent } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
	Linking,
	Platform,
  Keyboard,
} from 'react-native'
import { MyText } from 'Composition'
import styles from 'Services/Styles'
import {constans} from 'Services/Utils'
import config from 'Config'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import call from 'react-native-phone-call'

class ModalCarInfo extends PureComponent {

  constructor(props) {

		super(props);

		this.state = {
			newSumm:0,
			summServices:0
		};
	}
		
  componentWillUnmount() {
  	let {dispatch} = this.props;

		dispatch({
			type:'CLIENT_INFO',
			clientInfo:null
		})
	}
			
  onCancel(){
		let { switchModalStatus} = this.props

		switchModalStatus(false)
	}
				
	callCompany(phone){

		if(Platform.OS === 'android'){

			return Linking.openURL(`tel:+7${phone}`)
		}else{

			const args = {
				number: `+7${phone}`
			}
				
			return call(args).catch(console.error)
		}	
	}

	closeCar(){

		let { car, switchModalStatus } = this.props

		this.setState({
			loading:!this.state.loading
		})
		switchModalStatus(false)
		return this.props.closeCarForAdmin(car.id, car.company_id, car.user_id)

	}
	
	render(){
		let {newSumm, summServices} = this.state
		let { car } = this.props
		console.log('car',car)
		return(    	
			<View>
				<View style={styles.modal.view}></View>
					<View  style={styles.modal.modalBlock}>
					<ScrollView onPress={()=>{Keyboard.dismiss}}>
						<View>
							<View style={styles.modal.rowBlock}>
								<View style={styles.profilescreen.imgBlock}>
									<Image 
										source={{uri: `${config.public_url}${car.car.autoMarka.logo}`}}  
										style={styles.profilescreen.carImage}
									/>							
								</View>
								<View style={styles.merchant.lists.memberViewCenterNumCar}>
									<MyText style={styles.merchant.lists.memberViewCenterNumCarTitle}>{car.car.autoNum } </MyText>
								</View> 		
							</View>
							<View style={styles.modal.rowBlock}>
								<TouchableOpacity onPress={this.closeCar.bind(this)} style={styles.merchant.lists.memberViewRight}>
									<MaterialIcons name="delete" size={35} color="#fff" />
								</TouchableOpacity>
								<View style={styles.profilescreen.imgBlock}>
									<Image 
										source={{uri: `${config.public_url}${car.car.avtoType.image}`}}  
										style={styles.profilescreen.carImage}
									/>							
								</View>
								{car && car.car.autoColor ? 
									<View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor:car.car.autoColor}]}></View> 
									:	null
								}   				
							</View>
							{
								car.car.autoModel &&
								<View>
									<MyText style={styles.modal.boxTitle}> {car.custom_status && constans.firtUpCase(car.car.autoModel)} </MyText>
								</View>
							}
						</View>

						<View style={styles.modal.servicesBlock}>
							{
								car.services.map((elem, index)=>{
									if(newSumm > 0){
										summServices = newSumm	
									}else{
										summServices += +(elem.price)
									}
									return(
										<View  key={index}>
											<Text style={styles.modal.textServices}>{elem.title}: {elem.price} {elem.currency}</Text>
										</View>
									)
								})
							}
							<View style={{alignItems: 'center'}}>
								<Text style={styles.modal.totalSummText}>Итог: {summServices} тг</Text>
								<TouchableOpacity onPress={this.onCancel.bind(this)}>
									<Text style={styles.modal.totalSummText}>ЗАКРЫТЬ</Text>
								</TouchableOpacity> 
							</View>
						</View>
						<View style={{alignItems: 'center', marginTop:30}}>	
							<TouchableOpacity onPress={this.callCompany.bind(this, car.car.phone)} style={styles.modal.buttonsOval}>
								<FontAwesome5 name="phone" size={30} color="#fff" />
							</TouchableOpacity>
						</View>
					</ScrollView>			
				</View> 
			</View> 	     	
		)
  }
}

export default ModalCarInfo
