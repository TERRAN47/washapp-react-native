import MerchantComponents from './Merchant'
import UserComponents from './User'
import Modal from './Modal'
import ChangePass from './ChangePass'
import MapsView from './MapsView'

export {
	MerchantComponents,
	UserComponents,
	ChangePass,
	Modal,
	MapsView
}