const initialState = {
	request:false,
	locations:null,
	city_select:null
}

const locations = (state = initialState, action) => {
	switch(action.type){
		case 'REQUEST_STATUS' :
			return {
				...state,
				request:action.status
			}
		case 'SAVE_CITY_FROM_SELECT' :
			return {
				...state,
				city_select:action.city
			}
		case 'UPDATE_LOCATIONS' :
			return {
				...state,
				locations:action.data
			}
		default :
			return state
	}
}

export default locations