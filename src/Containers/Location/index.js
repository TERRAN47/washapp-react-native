import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	Image
} from 'react-native'
import config from 'Config'

import Api from 'Services/Api'
import styles from 'Services/Styles'

class Locations extends Component {

	componentDidMount(){
	    this.getLocations()
	}

	getLocations(){
	    const { dispatch } = this.props
	    return Api.auth.getLocations(dispatch)
	}

	goBack(){
		let { goBack } = this.props.navigation
		return goBack()
	}

	saveCity(city){
    	const { user} = this.props.user
		let {params} = this.props.navigation.state
		const { navigation, dispatch } = this.props
		return Api.auth.saveCity(navigation, dispatch, city, user, params, Api)
	}

	render(){

		let { request } = this.props.auth
		let { locations } = this.props.locations
		
		return(<View style={{flex:1,backgroundColor:'#fff'}}>
			{

				request ?
					<View style={styles.AuthScreen.notFoundCitiesView}>
						<Text style={styles.AuthScreen.notFoundCitiesViewTitle}>Загружаем города...</Text>
					</View>
				: locations ?

					<ScrollView>
						{
							locations.map((country, index)=>{
								return(<View key={index}>
									<Text style={styles.AuthScreen.titleCountry}>{ country.title }</Text>
									{
										country.cities ?
											country.cities.map((city, index)=>{
												return(<TouchableOpacity activeOpacity={0.8} style={styles.AuthScreen.cityButton} onPress={this.saveCity.bind(this, city)} key={index}>
													<Image source={{uri:`${config.public_url}${city.image}`}} style={styles.AuthScreen.cityImageBG}/>
													<View style={styles.AuthScreen.cityTitleView}>
														<Text style={styles.AuthScreen.cityTitleMain}>{ city.title }</Text>
													</View>
												</TouchableOpacity>)
											})
										:
											<TouchableOpacity style={styles.AuthScreen.noCitiesView}>
												<Text style={styles.AuthScreen.noCitiesTitle}>Городов нет</Text>
											</TouchableOpacity>
									}
								</View>)
							})
						}
					</ScrollView>
				: 
					<View style={styles.AuthScreen.notFoundCitiesView}>
						<Text style={styles.AuthScreen.notFoundCitiesViewTitle}>Ничего не найдено</Text>
						<TouchableOpacity onPress={this.goBack.bind(this)}>
							<Text>Назад</Text>
						</TouchableOpacity>
					</View>

			}
		</View>)
	}
}

const mapStateToProps = (state) => {
	return {
		auth:state.auth,
		user:state.user,
		locations:state.locations
	}
}

export default connect(mapStateToProps)(Locations)