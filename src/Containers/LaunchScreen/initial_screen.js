import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  View,
  Text,
  ImageBackground,
  Image,
  Alert
} from 'react-native'
import Api from 'Services/Api'
import { icons } from 'Services/Utils'
import { NavigationActions, StackActions } from 'react-navigation'
import styles from 'Services/Styles'

class InitialScreen extends Component {

  componentDidMount () {

    this.initialScreen()

  }

  async initialScreen(){

    const { dispatch, navigation, user } = this.props

    const typeMainAccount = user ? user.type_account : 'client'

    Api.updateAxiosConfig()
    return Api.auth.authentication(Api, navigation, 2400, dispatch, typeMainAccount, user)
  }

  render () {
    return (
      <View style={styles.launchscreen.view}>
        <Image style={styles.launchscreen.logo} source={icons.logo}/>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    user:state.user.user
  }
}

export default connect(mapStateToProps)(InitialScreen)
