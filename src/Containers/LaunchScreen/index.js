import React, { Component } from 'React'
import {
	View,
	Text,
	ImageBackground,
	Image
} from 'react-native'
import { icons } from 'Services/Utils'
import styles from 'Services/Styles'

class LaunchScreen extends Component {
	render(){
		return(
		    <View style={styles.launchscreen.view}>
		        <Image style={styles.launchscreen.logo} source={icons.logo}/>
		    </View>
		)
	}
}

export default LaunchScreen