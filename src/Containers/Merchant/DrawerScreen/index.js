import React, { Component } from 'react'
import { 
  Share,
  View,
  Text,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions, StackActions } from 'react-navigation'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import Api from 'Services/Api'
import config from 'Config'
import styles from 'Services/Styles'

class DrawerScreen extends Component {

  componentDidMount(){

    const { dispatch } = this.props
  }

  async logoutUser(){
    const { navigation, dispatch } = this.props
    return Api.auth.logout(dispatch, navigation, Api)
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  switchAccountType(){

    const { navigation, dispatch, user } = this.props
    return Api.merchant.main.switchAccountType(navigation, dispatch, user.type_account)

  }

  render () {

    const { user } = this.props

    return (
      <View style={styles.merchant.drawerMerchant.viewBack}>
        
        <ScrollView>

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer} onPress={this.goToScreen.bind(this, 'CabinetScreen')}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="account-edit" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Кабинет</Text> 
          </TouchableOpacity>
          
          {
          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer} onPress={this.goToScreen.bind(this, 'companiesScreen')}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="car-wash" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Автомойки</Text> 
          </TouchableOpacity>
          }

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer} onPress={this.goToScreen.bind(this, 'members')}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="account-card-details" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Сотрудники</Text> 
          </TouchableOpacity>

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer} onPress={this.goToScreen.bind(this, 'report')}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="briefcase-edit" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Отчет</Text> 
          </TouchableOpacity>

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer} onPress={this.goToScreen.bind(this, 'merchantClient')}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="account-group" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Клиенты</Text> 
          </TouchableOpacity>

          <TouchableOpacity onPress={this.switchAccountType.bind(this)} style={styles.merchant.drawerMerchant.buttonDrawer}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              {/*<MaterialCommunityIcons name="account-switch" size={28} color="#466dfa" />*/}
              <Image style={styles.merchant.drawerMerchant.buttonDrawerImage} source={require('Media/switchMerchant.png')} />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Переключиться</Text> 
          </TouchableOpacity>

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer}  onPress={this.goToScreen.bind(this, 'suppliers')} >
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              {/*<MaterialCommunityIcons name="car-pickup" size={28} color="#466dfa" />*/}
              <Image style={styles.merchant.drawerMerchant.buttonDrawerImage} source={require('Media/postavka.png')} />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Поставщики</Text> 
          </TouchableOpacity>

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer}  onPress={this.goToScreen.bind(this, 'vacancies')} >
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              {/*<MaterialCommunityIcons name="book-open-page-variant" size={28} color="#466dfa" />*/}
              <Image style={styles.merchant.drawerMerchant.buttonDrawerImage} source={require('Media/vakansy-3.png')} />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Вакансии</Text> 
          </TouchableOpacity>

          <TouchableOpacity style={styles.merchant.drawerMerchant.buttonDrawer}  onPress={this.goToScreen.bind(this, 'merchantHelp')} >
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="help-circle-outline" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Помощь</Text> 
          </TouchableOpacity>

          <TouchableOpacity onPress={this.logoutUser.bind(this)} style={styles.merchant.drawerMerchant.buttonDrawer}>
            <View style={styles.merchant.drawerMerchant.iconDrawer}>
              <MaterialCommunityIcons name="logout" size={28} color="#466dfa" />
            </View>
            <Text style={styles.merchant.drawerMerchant.drawerTitleLink}>Выйти</Text> 
          </TouchableOpacity>

        </ScrollView>

      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    user:state.user.user
  }
}

export default connect(mapStateToProps)(DrawerScreen)
