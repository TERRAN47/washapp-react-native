const initialState = {
	list:null
}

const bonuses = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_BONUSES_LIST' :
			return {
				...state,
				list:action.bonuses
			}

		default :
			return state
	}

}

export default bonuses 