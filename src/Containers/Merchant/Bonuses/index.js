import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'

import _ from 'underscore'

import { Header, NotFound, Loader } from 'Composition'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { MerchantComponents, Modal } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class Bonuses extends Component {

  constructor(props){
    super(props)

    this.state = {
      modalStatus:false
    }
  }

  componentDidMount(){


  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  changeStatusModal(){
    return this.setState({
      modalStatus:!this.state.modalStatus
    })
  }

  addBonus(bonus){

    let {  id } = this.props.navigation.state.params
    let { dispatch } = this.props

    let { companies } = this.props

    let item = _.where(companies.companies, {
      id
    })[0]

    this.changeStatusModal()
    
    return Api.merchant.bonuses.addBonus(dispatch, item.bonus, id, bonus)

  }

  render(){

    let { id } = this.props.navigation.state.params
    let { statusRequest } = this.props.merchant
    let { modalStatus } = this.state
    let { companies } = this.props

    let bonuses = _.where(companies.companies, {
      id
    })[0].bonus

    console.log(2222, companies.companies)

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title='Бонусы' />

          <Modal modalOpened={modalStatus} type="BONUS" newBonus={this.addBonus.bind(this)} closeModal={this.changeStatusModal.bind(this)}/>

          <View style={{flex:1}}>
            <ScrollView>
              {
                bonuses && bonuses.length > 0 ?
                  bonuses.map((bonus, index)=>{
                    return <MerchantComponents.ListBonus statusRequest={statusRequest} id={id} api={Api} dispatch={this.props.dispatch} bonuses={bonuses} navigation={this.props.navigation} item={bonus} key={index} index={index}/>
                  })
                : <View style={styles.merchant.cabinetMerchant.viewNoBonuses}>
                  <Text style={styles.merchant.cabinetMerchant.textNoBonuses}>Вы не добавили ни одного бонуса</Text>
                </View>
              }

              {
                statusRequest ?
                  <TouchableOpacity style={styles.merchant.cabinetMerchant.buttonOpenModalBonus}>
                    <MaterialCommunityIcons name="clock-outline" size={30} color="#466dfa" />
                    <Text style={styles.merchant.cabinetMerchant.buttonOpenModalBonusText}>Подождите...</Text>
                  </TouchableOpacity>
                :
                  <TouchableOpacity style={styles.merchant.cabinetMerchant.buttonOpenModalBonus} onPress={this.changeStatusModal.bind(this)}>
                    <MaterialCommunityIcons name="plus-circle" size={30} color="#466dfa" />
                    <Text style={styles.merchant.cabinetMerchant.buttonOpenModalBonusText}>Добавить новый бонус</Text>
                  </TouchableOpacity>

              }

            </ScrollView>
          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    merchant:state.merchant,
    companies:state.companies
  }
}

export default connect(mapStateToProps)(Bonuses)