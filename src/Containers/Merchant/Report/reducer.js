const initialState = {
	list:null,
	total:0,
	count:0,
	percent:0,
	cash:0,

	//saved reports
	reports:null
}

const report = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_SAVED_ORDERS' :

			let savedOrders = state.reports == null ? [action.report] : [...state.reports, action.report]

			return {
				...state,
				reports:savedOrders
			}

		case 'UPDATE_ORDERS_LIST' :
			return {
				...state,
				list:action.orders,
				total:action.total,
				count:action.count,
				percent:action.percent,
				cash:action.cash
			}

		default :
			return state
	}

}

export default report 