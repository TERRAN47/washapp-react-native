import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  TextInput
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents, Modal } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class ReportView extends Component {

  constructor(props){
    super(props)

    this.state = {
      modalOpened:false
    }
  }

  componentDidMount(){

    this.getOrders()

  }

  getOrders(){

    const { dispatch } = this.props
    const { item } = this.props.navigation.state.params
    let { user } = this.props.user
    return Api.merchant.orders.getOrders(dispatch, item.id)

  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  closeDay(){

  }

  modalChangedStatus(){
    return this.setState({
      modalOpened:!this.state.modalOpened
    })
  }

  render(){

    let { user } = this.props.user
    let { statusRequest } = this.props.merchant
    let { list, total, count, percent, cash } = this.props.report
    let { modalOpened } = this.state
    let { id, title } = this.props.navigation.state.params.item

    let countOrder = 0
    if(list) countOrder = list.length + 1

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isReport={true} id={id} isBack={true} title={'Отчет'}/>

          <Modal total={total} count={count} id={id} title={title} percent={percent} dispatch={this.props.dispatch} navigation={this.props.navigation} cash={cash} list={list} statusRequest={statusRequest} modalOpened={modalOpened} type="statisticOrder" closeDay={this.closeDay.bind(this)} closeModal={this.modalChangedStatus.bind(this)}/>

          <View style={styles.merchant.cabinetMerchant.tableStyleView}>

            {
              list ?
                <TouchableOpacity onPress={this.modalChangedStatus.bind(this)} style={styles.merchant.cabinetMerchant.openModalButtonReport}>
                  <Text style={styles.merchant.cabinetMerchant.openModalButtonReportTitle}>ПОДСЧИТАТЬ</Text>
                </TouchableOpacity>
              : <View></View>
            }

            {
              statusRequest ?
                <Loader title="Загрузка" />
              : list ?

                <View style={{flex:1,paddingBottom:62}}>
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                    <View style={{flexDirection:'column'}}>
                      <View style={styles.merchant.cabinetMerchant.tableStyle}>
                        <View style={styles.merchant.cabinetMerchant.tableHead}>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionFirst]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>№</Text>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>П/П</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionSecond]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>ВРЕМЯ</Text>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>ЗАЕЗДА</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionThird]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>ПОСТ</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionFour]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>АВТО</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionFive]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>ТИП МОЙКИ</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionSix]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>МОЙЩИК</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionComment]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>СУММА</Text>
                          </View>

                          <View style={[styles.merchant.cabinetMerchant.tableHeadSection, styles.merchant.cabinetMerchant.tableHeadSectionSeven]}>
                            <Text style={styles.merchant.cabinetMerchant.tableHeadSectionTitle}>КОММЕНТАРИЙ</Text>
                          </View>

                        </View>
                      </View>

                      <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.merchant.cabinetMerchant.tableStyle}>
                          {
                            list.map((item, index)=>{
                              countOrder -= 1
                              return <MerchantComponents.ListOrders navigation={this.props.navigation} countOrder={countOrder} key={index} index={index} list={list} item={item}/>
                            })
                          }
                        </View>
                      </ScrollView>
                    </View> 
                  </ScrollView>
                </View>
                
              :
                <NotFound navigation={this.props.navigation} type='MERCHANT_ORDERS' />
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant,
    report:state.report
  }
}

export default connect(mapStateToProps)(ReportView)