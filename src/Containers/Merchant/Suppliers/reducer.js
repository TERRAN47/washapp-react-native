const initialState = {
	list:null
}

const suppliers = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_SUPPLIERS_LIST' :
			return {
				...state,
				list:action.data
			}

		default :
			return state
	}

}

export default suppliers 