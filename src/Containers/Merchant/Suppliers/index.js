import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class SupplierScreen extends Component {

  componentDidMount(){
    this.loadSuppliers()
  }

  loadSuppliers(){

    const { dispatch, user } = this.props

    console.log(222, user)

    return Api.merchant.suppliers.getSuppliers(dispatch, user.user.city.id)
  }


  render(){

    let { user } = this.props.user
    let { statusRequest } = this.props.merchant
    let { list } = this.props.suppliers

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} menu={true} title={'Поставщики'}/>

          <View style={{flex:1}}>

            {
              statusRequest ?
                <Loader title="Загрузка" />
              : list ?

                <ScrollView>
                  {
                    list.map((supplier, index)=>{

                      return <MerchantComponents.ListSuppliers index={index} navigation={this.props.navigation} item={supplier}/>

                    })
                  }
                </ScrollView>  
              :
                <NotFound navigation={this.props.navigation} type='MERCHANT_SUPPLIER' />
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant,
    suppliers:state.suppliers
  }
}

export default connect(mapStateToProps)(SupplierScreen)