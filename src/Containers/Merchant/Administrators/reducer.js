const initialState = {
	list:null
}

const administrator = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_ADMIN_LIST' :
			return {
				...state,
				list:action.admins
			}

		default :
			return state
	}

}

export default administrator 