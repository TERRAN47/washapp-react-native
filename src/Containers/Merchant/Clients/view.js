import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import config from 'Config'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class ViewClient extends Component {

  render(){

    let { item } = this.props.navigation.state.params

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title='Информация о клиенте' />

          <View style={{flex:1}}>

            <View style={styles.merchant.lists.memberView}>
              
              <View style={styles.merchant.lists.memberViewLeft}>
                <Image style={styles.merchant.lists.memberViewLeftLogo} source={{uri:`${config.public_url}${item.car.autoMarka.logo}`}}/>
                <Text style={styles.merchant.lists.memberViewLeftCarName}>{ item.car.autoModel }</Text>
              </View>

              <View style={styles.merchant.lists.memberViewCenter}>

                <View style={styles.merchant.lists.memberViewCenterNumCar}>
                  <Text style={styles.merchant.lists.memberViewCenterNumCarTitle}>{ item.car.autoNum }</Text>
                </View>

                <View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor:item.car.autoColor}]}></View>

                <Image style={styles.merchant.lists.memberViewLeftCarBody} source={{uri:`${config.public_url}${item.car.avtoType.image}`}}/>

              </View>
            </View>

            <View style={{flexDirection:'row', padding:15, justifyContent:'space-between'}}>
              <View style={[styles.merchant.lists.memberViewRight,{ justifyContent:'center', alignItems:'center' }]}>
                <Text style={styles.merchant.lists.memberViewRightTitle}>Бонусных моек: { item.count }</Text>
              </View>

              <View style={[styles.merchant.lists.memberViewRight,{ justifyContent:'center',alignItems:'center' }]}>
                <Text style={styles.merchant.lists.memberViewRightTitle}>Всего моек { item.all_count }</Text>
              </View>
            </View>

          </View>

        </View>

    )
  }
}

export default ViewClient