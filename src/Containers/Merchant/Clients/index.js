import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class MerchantClients extends Component {

  componentDidMount(){
    this.getClients()
  }

  getClients(){
    let { dispatch, user } = this.props
    return Api.merchant.clients.getClients(dispatch, user.user.id)
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  render(){
    let { user } = this.props.user
    let { list } = this.props.clients
    let { statusRequest } = this.props.merchant

    return(
      <View style={styles.merchant.cabinetMerchant.viewBack}>
        <Header navigation={this.props.navigation} menu={true} title='Клиенты' />
        <View style={{flex:1}}>
          <ScrollView>
          {
            statusRequest ?
              <Loader title="Загрузка" />
            : list ?
              list.map((client, index)=>{
                return <MerchantComponents.ListClient navigation={this.props.navigation} item={client} index={index}/>
              })
            : <NotFound navigation={this.props.navigation} type='MERCHANT_CLIENTS' />

          }            
          </ScrollView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    maps:state.maps,
    merchant:state.merchant,
    clients:state.clients
  }
}

export default connect(mapStateToProps)(MerchantClients)