const initialState = {
	list:null
}

const clients = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_CLIENTS_LIST' :
			return {
				...state,
				list:action.clients
			}

		default :
			return state
	}

}

export default clients 