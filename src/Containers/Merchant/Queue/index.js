import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
	View,
	Text,
	Modal,
	Platform,
  AppState,
	TouchableOpacity,
	ScrollView,
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import _ from 'underscore'
import ModalBoxInfo from 'Components/Modal/modalBoxInfo'
import styles from 'Services/Styles'
import { Header, NotFound } from 'Composition'
import { socket } from 'Services/Utils'
import RenderBox from './helpers/RenderBox'
//import RenderParck from './helpers/RenderParck'

class Queue extends Component {

	constructor(props){
		super(props)

		this.socket = socket()

		this.state = {
			appState: AppState.currentState,
			car:null,
	  	modalStatus:false,
	  	boxInfo:null,
	  	payStatus:false,
	  	content:'',
			boxes:null,
			members:null,
			statusEvent:false
		}	
	}

	removeFirstCar(){
		return this.setState({car:null})
	}
	
	componentWillUnmount(){
		AppState.removeEventListener('change', this._handleAppStateChange);
		return this.socket.disconnect() 
	}

	_handleAppStateChange = (nextAppState) => {
		if (
			this.state.appState.match(/inactive|background/) &&
			nextAppState === 'active'
		) {
			this.socket = socket()  
			console.log('App has come to the foreground!');     
			this.updateSocket()
		}else{
			this.socket.disconnect()
		}
		this.setState({appState: nextAppState});
	}

  updateSocket(){
		let that = this
		let { id } = this.props.navigation.state.params.item
		this.updateMember()

    this.socket.on('selectFirstCarFromQueue', (data)=>{

			that.setState({
				car:data.data,
				statusEvent:false
			})
    })

		this.socket.on('updateBoxesAdmin', (data) => {

			that.setState({
				boxes:data.boxes
			});

			that.updateCar(id)
		});

		this.socket.on('updateQueue', (data)=>{

			let carsQEUElist = _.where(data.data, {
				status_type:'QEUE'
			})

			that.setState({
				car:carsQEUElist && carsQEUElist.length > 0 ? carsQEUElist[0] : null
			})
		})		
  }

	componentDidMount(){
		AppState.addEventListener('change', this._handleAppStateChange);

		this.updateSocket()
    if(Platform.OS === 'ios'){

      setTimeout(()=>{
        this.socket.disconnect()
        this.socket = socket()
        setTimeout(()=>{
          this.updateSocket()
        },1000)
      }, 1000)
    }
	}

	updateMember(){

		let { id, companyIndex } = this.props.navigation.state.params
		let { companies } = this.props

		this.socket.emit('updateBoxesAdmin', {
			id
		})

		let members = companies[companyIndex].members

		return this.setState({
			members
		});
	}

	updateCar(id){
		this.socket.emit('selectFirstCarFromQueue', {
			id
		})
	}

	goToScreen(screen){

		let { price, item, id } = this.props.navigation.state.params
		let { navigate } = this.props.navigation

		return navigate(screen, {
			id,
			branch_id:item.branch_id,
			companyPrice:price,
			socket:this.socket
		})

	}
	servicesInBox(status, boxIndex){
		let {boxes} = this.state
		this.setState({modalStatus:status, boxInfo:boxes[boxIndex], payStatus:boxes[boxIndex].pay_status, content:'services'})
	}

	inviteClientToPay(status, boxIndex){
		let {boxes} = this.state
		
		this.setState({modalStatus:status, boxInfo:boxes[boxIndex], content:'invitePay'})
	}
	switchModalStatus(status){
		this.setState({modalStatus:status})
	}
	payBox(boxIndex, modalStatus){
		let {boxes} = this.state
		boxes[boxIndex].pay_status = true
		this.setState({boxes, modalStatus})
	}

	saveCarToBox(status){
		return this.setState({
			statusEvent:true
		})
	}

	render(){
		let { id, item, companyIndex } = this.props.navigation.state.params
		let { dispatch, user, navigation } = this.props
		let { car } = this.state
		let {clientInfo} = this.props.queue
		let {boxes, members, payStatus, boxInfo, modalStatus, content} = this.state

		return(
			<View style={styles.merchant.queue.mainView}>
				<Modal
					animationType="slide"
					onRequestClose={()=>{}}
					transparent={false}
					visible={modalStatus}
				>
					<ModalBoxInfo 
						modalStatus={modalStatus}
						content={content}
						dispatch={dispatch}
						clientInfo={clientInfo}
						navigation={navigation}
						payStatus={payStatus}
						companyInfo={item}
						payBox={this.payBox.bind(this)}
						boxInfo={boxInfo}
						switchModalStatus={this.switchModalStatus.bind(this)} 
					/>
				</Modal>
				<Header navigation={navigation} isBack={true} title='Управление автомойкой'/>

				<View style={styles.merchant.queue.listBoxView}>
					{
						boxes && boxes.length > 0 ?
							<ScrollView>
								{
									boxes.map((box, index)=>{
										return(
											<RenderBox
												saveCarToBox={this.saveCarToBox.bind(this, true)}
												statusEvent={this.state.statusEvent}
												removeFirstCar={this.removeFirstCar.bind(this)} 
												key={index} 
												socket={this.socket} 
												car={car}
												user={user.user}
												inviteClientToPay={this.inviteClientToPay.bind(this)}
												servicesInBox={this.servicesInBox.bind(this)}
												companyIndex={companyIndex} 
												index={index}
												id={id} 
												dispatch={dispatch} 
												boxList={boxes} 
												navigation={this.props.navigation} 
												members={members} 
												item={box} 
											/>
										)
									})
								}
								
								{
									//<Text style={{paddingHorizontal:10,}}>Парковка</Text>
									// <RenderParck
									// 	saveCarToBox={this.saveCarToBox.bind(this, true)}
									// 	statusEvent={this.state.statusEvent}
									// 	removeFirstCar={this.removeFirstCar.bind(this)} 
									// 	key={0} 
									// 	socket={this.socket} 
									// 	car={car}
									// 	user={user.user}
									// 	inviteClientToPay={this.inviteClientToPay.bind(this)}
									// 	servicesInBox={this.servicesInBox.bind(this)}
									// 	companyIndex={companyIndex} 
									// 	index={0}
									// 	id={id} 
									// 	dispatch={dispatch} 
									// 	boxList={boxes} 
									// 	navigation={this.props.navigation} 
									// 	members={members} 
									// 	item={boxes[0]} 
									// />
								}
							</ScrollView>
						:
							<NotFound type="QUEUE_NOT_FOUND" />
					}
				</View>

				<TouchableOpacity onPress={this.goToScreen.bind(this, 'queueCars')} style={styles.merchant.queue.buttonMainListQueue}>

					<View style={styles.merchant.queue.buttonMainListQueueDevider}></View>

					<Text style={styles.merchant.queue.buttonMainListQueueTitle}>Очередь</Text>
					<MaterialCommunityIcons name="car" size={25} color="#fff" />

				</TouchableOpacity>

			</View>
		)
	}
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    queue:state.queue,
    companies:state.companies.companies,
    statusRequest:state.merchant.statusRequest
  }
}

export default connect(mapStateToProps)(Queue)