import React, { Component } from 'react'

import {
	View,
	Text,
	Image,
	Alert,
	TouchableOpacity
} from 'react-native'
import { Timer } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import config from 'Config'
import { moment } from 'Services/Time'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

class RenderParck extends Component {

	saveCarToBox(){

		let { item, index, car, boxList, dispatch, companyIndex, id, socket, user, statusEvent } = this.props

		car.invite_box_time = moment().format("DD-MM-YYYY HH:mm:ss")
		
		this.props.saveCarToBox()

		if(statusEvent){
			//return Alert.alert('Сообщение', 'В данный момент это сделать невозможно')
		}else if(car == null){
			return Alert.alert('Сообщение', 'В очереди нет машин')
		}else if(item.member == null){
			return Alert.alert('Сообщение', 'Добавьте мойщика в данный бокс')
		}else{
			return Api.merchant.carwash.saveCarToBox(dispatch, companyIndex, index, car, boxList, id, socket, item)
		}
	}

	complete(parckIndex, user_id, queue){
		let { socket, item, id } = this.props
		let that = this

	    Alert.alert(
	      'Завершение!',
	      'Выгнать авто?',
	      [
	        {
	          text: 'Отмена',
	          onPress: () => console.log('Cancel'),
	          style: 'cancel',
	        },
	        {text: 'Выгнать', onPress: () => {
				that.props.removeFirstCar()

				that.props.saveCarToBox()
				socket.emit('deleteQueue', {user_id, company_id:id, parckIndex, pay_status:item.pay_status, custom_status:queue.custom_status})

			    socket.emit('payClientBox', {
			    	user_id,
			    	company_id:id
			    })
	        } },
	      ],
	      {cancelable: false},
	    );

	}

	parckInviteToPay(parckIndex){
		let { parckInviteClientToPay } = this.props
			
	  parckInviteClientToPay(true, parckIndex)		
	}

	render(){

		let { item, index, car} = this.props
		let parckIndex = index
		index = index + 1
		console.log(123, car)
		return(
			<View style={styles.merchant.queue.itemBoxStyle}>

				{ item.car && <Timer defaultStyles={true} time={item.car.invite_box_time} /> }

				<View style={[styles.merchant.queue.itemBoxContentStyle, {paddingVertical: 30}]}>

					<View style={styles.merchant.queue.itemBoxContentStyleLeftMain}>
						<View style={styles.merchant.queue.itemBoxContentStyleLeft}>

              <View>
                <FontAwesome5 name="parking" size={50} style={{margin:10}} color="#23527c" />
              </View>

							<Text style={styles.merchant.queue.itemBoxContentStyleLeftTitle}>{ index }</Text>
						</View>

						<View style={styles.merchant.queue.itemBoxContentStyleCenter}>
							{
								car && car.car ?
									<View>
										<View style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
											<View style={styles.profilescreen.imgBlock}>
												<Image 
													source={{uri: `${config.public_url}${car.car.autoMarka.logo}`}}  
													style={styles.merchant.queue.carImage}
												/>							
											</View>											
											<Text style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}>{ car.car.autoNum }</Text>
										</View>
										<View style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
											<View style={styles.profilescreen.imgBlock}>
												<Image 
													source={{uri: `${config.public_url}${car.car.avtoType.image}`}}  
													style={styles.profilescreen.carImage}
												/>
											</View>
											{
												car.car.autoColor &&
												<View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor:car.car.autoColor}]}></View>
											}
										</View>	
									</View>	
								:
									null								
							}
						</View>
					</View>

					<View style={styles.merchant.queue.itemBoxContentStyleRight}>
						{
							item.status && item.car ?
                <View>
                  {
                    item.pay_status ?
                      <View style={styles.merchant.queue.boxPaySuccess}>
                        <Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>ОПЛАЧЕНО</Text>
                      </View>
                    :
                      <TouchableOpacity onPress={this.parckInviteToPay.bind(this, parckIndex)} style={styles.merchant.queue.itemBoxContentStyleRightBlueButton}>
                        <Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>Пригласить к</Text>
                        <Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>оплате</Text>
                      </TouchableOpacity>																				
                  }
                  <TouchableOpacity onPress={this.complete.bind(this, parckIndex, item.car.user_id, item.car)} style={styles.merchant.queue.itemBoxContentStyleRightRedButton}>
                    <Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>Завершить</Text>
                  </TouchableOpacity>
                </View>
							: null
						}
					</View>
				</View>

			</View>
		)
	}
}

export default RenderParck