import React, { Component } from 'react'

import {
	View,
	Text,
	Image,
	Alert,
	ScrollView,
	TouchableOpacity
} from 'react-native'
import underscore from 'underscore'
import config from '../../../../Config'
import styles from 'Services/Styles'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {constans} from 'Services/Utils'

class ServicesView extends Component {

	constructor(props){
		super(props)

		this.state = {
			bodyPrice:[]
		}
	}
	componentDidMount(){
		const {bodyName, openModal, companyPrice} = this.props

		if(bodyName){

			let body = underscore.findWhere(companyPrice, {
				title:bodyName
			})

			this.setState({bodyPrice:body.prices})
		}else{
			openModal('typeAuto')
		}
	}
  	selectServices(image, typeName){
  		let { user, selectType} = this.props
  		selectType(avtoType)
  	}

	services(title, status, index){
		let {bodyPrice} = this.state
		let {dispatch} = this.props

		let findPrice = underscore.findWhere(bodyPrice, {
			title
		})

	    if(title == 'КУЗОВ-САЛОН'){
	      
	      	let findKuzov = this.findService('КУЗОВ')
	      	let findSalon = this.findService('САЛОН')
	      	let findOtbivka = this.findService('ОТБИВКА')
	      	let findOtbivkaPenoi = this.findService('ОТБИВКА С ПЕНОЙ')
	      	let findPoliki = this.findService('ПОЛИКИ 2 ШТ')
	      	let findPoliki4 = this.findService('ПОЛИКИ 4 ШТ')

	      	findKuzov.status = false
	      	findSalon.status = false
	      	findOtbivka.status = false
	      	findOtbivkaPenoi.status = false
	      	findPoliki.status = false
	      	findPoliki4.status = false

	      	findOtbivkaPenoi.hidden = !status ? false : true
	      	findOtbivka.hidden = !status ? false : true
	      	findSalon.hidden = !status ? false : true
	      	findKuzov.hidden = !status ? false : true
	      	findPoliki.hidden = !status ? false : true
	      	findPoliki4.hidden = !status ? false : true
	    }else if(title == 'КУЗОВ'){
	      	let findKuzovSalon = this.findService('КУЗОВ-САЛОН')
	      	let findSalon = this.findService('САЛОН')
	     	let findOtbivka = this.findService('ОТБИВКА')
	      	let findOtbivkaPenoi = this.findService('ОТБИВКА С ПЕНОЙ')

	      	if(findKuzovSalon){
		      	findKuzovSalon.status = false
		      	findKuzovSalon.hidden = status && !findSalon.status ? true : false
	     	} 

	     	if(findOtbivka){
		      	findOtbivka.status = false
		      	findOtbivka.hidden = status && !status ? true : false
	      	}

	      	if(findOtbivkaPenoi){
		      	findOtbivkaPenoi.status = false
		      	findOtbivkaPenoi.hidden = status && !status ? true : false	      	
	     	}
	    }else if(title == 'САЛОН'){
	      	let findKuzovSalon = this.findService('КУЗОВ-САЛОН')
	     	let findKuzov = this.findService('КУЗОВ')
	     	let findPoliki = this.findService('ПОЛИКИ 2 ШТ')
	     	let findPoliki4 = this.findService('ПОЛИКИ 4 ШТ')

	     	findKuzovSalon.status = false
	     	findPoliki.status = false
	     	findPoliki4.status = false

	     	findKuzovSalon.hidden = status && !findKuzov.status ? true : false
	     	findPoliki.hidden = !status ? false : true
	     	findPoliki4.hidden = !status ? false : true
   	 	}       
	    
		findPrice.status = !status

		this.setState({bodyPrice})
	}

  	findService(title){
	    let {bodyPrice} = this.state
	    return underscore.findWhere(bodyPrice, {
	      title
	    })     
  	}	

	saveServices(bodyPrice){
		let {selectServices} = this.props
		let activeServices = underscore.where(bodyPrice, {
			status:true
		})
		if(activeServices.length > 0){
			selectServices(activeServices)
		}else{
			Alert.alert('Внимание!', 'Выберите хоть одну услугу!')
		}
	}

	render(){

		let { bodyPrice} = this.state

		return(
        	<View style={styles.profilescreen.categoryBlock}>
        		<ScrollView showsVerticalScrollIndicator={false}>
        		{
        			bodyPrice.length > 0 ?
            			bodyPrice.map((item, index)=>{
            				if(item.hidden == undefined || item.hidden){
	            				return(
					                <View key={index} style={styles.carWashes.priceList}>
					                  <Text style={styles.carWashes.servicesText}>{item.title}</Text>
					                  <TouchableOpacity onPress={this.services.bind(this, item.title, item.status, index)}>
					                    <FontAwesome size={30} color={constans.defaultColor} name={item.status ? 'toggle-on' : 'toggle-off'} />
					                  </TouchableOpacity>
					                </View>
	            				)
            				}
            			})
            		:
            			<Text>Поиск</Text>
        		}
        		</ScrollView>
              	<TouchableOpacity style={[styles.carWashes.priceFooter, {backgroundColor:constans.defaultColor}]} onPress={this.saveServices.bind(this, bodyPrice)}>          
                	<Text style={[styles.carWashes.servicesNext, {color:'#fff'}]}>СОХРАНИТЬ</Text>
              	</TouchableOpacity>
        	</View>
           
		)
	}
}

export default ServicesView