import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  Alert,
  Modal,
  TextInput,
} from 'react-native'
import { Header, MyText } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import config from 'Config'
import {constans, socket} from 'Services/Utils'
import CarViewType from './carViewType'
import ServicesView from './servicesView'
import { TextInputMask } from 'react-native-masked-text'
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_SERCH = ['marka']
class AddCarInQueue extends Component {

	constructor(props) {
	  	super(props);
		this.socket = socket()
	    this.state = {
	  		modalVisible:false,
	  		carMaks:[],
	  		filterCarMaks:{},
	  		services:[],
	  		companyPrice:[],
	  		autoMarka:{},
				phone:'7',
				autoColor:null,
	  		avtoType:{},
	  		autoNum:'',
	  	};
	}

	selectColor(color){

		this.setState({autoColor:color, modalVisible:false})
	}
	
	componentWillUnmount() {
		this.socket.removeAllListeners("createQueue")
		return this.socket.disconnect()
	}

	componentDidMount(){
		const {dispatch} = this.props
		let {companyPrice} = this.props.navigation.state.params

		if(companyPrice.length > 0){
			companyPrice.forEach((element, index)=>{
				companyPrice[index].status = false
			});	
			Api.user.userProfile.getMarks(dispatch)		
			this.setState({companyPrice})

		}else{
			Alert.alert("Внимание!", 'Заполните прайс автомойки')

			this.props.navigation.goBack()
		}
	}

	viewColor(){
		let {cars} = this.props.user
		return(
			<View style={styles.profilescreen.colorBlocs}>
			{
				cars.colors.map((item, index)=>{
					return(
						<TouchableOpacity 
							key={index} 
							onPress={this.selectColor.bind(this, item.color)} 
							style={[styles.profilescreen.color, {backgroundColor: item.color}]} 
						/>
					)
				})
			}
			</View>
		)
	}
	selectServices(services){
		this.setState({services,  modalVisible:false})
	}

	closeModal(){
		this.setState({modalVisible:false})
	}

	selectModel(marka){
		this.setState({autoMarka:marka, modalVisible:false})
	}

	searchMarka(term){

		let {carMaks} = this.state
		let filtered = carMaks.filter(createFilter(term, KEYS_SERCH))
		this.setState({filterCarMaks:filtered}) 
	}

	_keyExtractor = (item, index) => item.marka;
	
	viewCars(){
		let {filterCarMaks} = this.state
		return(
			<View>
				<View style={styles.profilescreen.serchBlock}>
					<SearchInput
						onChangeText={this.searchMarka.bind(this)}
						style={styles.profilescreen.serch}
						placeholder="Поиск..."
					/>					
				</View>
				<FlatList
					data={filterCarMaks}
					extraData={filterCarMaks}
					keyExtractor={this._keyExtractor}
					renderItem={({item}) => 
						<TouchableOpacity onPress={this.selectModel.bind(this, item)}  style={styles.profilescreen.carItem}>
							<View style={styles.profilescreen.flexBlock}>
								<Image 
									source={{uri: `${config.public_url}${item.logo}`}}  
									style={styles.profilescreen.carImage}
								/>							
							</View>
							<View style={styles.profilescreen.flexBlock}>
								<MyText style={styles.profilescreen.modelText}>{item.marka ? constans.firtUpCase(item.marka) : null}</MyText>						
							</View>
						</TouchableOpacity>	
					}
				/>
			</View>
		)
	}

	viewServices(services){
		this.setState({services, modalVisible:false})
	}

	openModal(modal){
		if(modal == 'marka'){
			let {cars} = this.props.user
			
			cars && this.setState({modalVisible:true, modal, carMaks:cars.brands, filterCarMaks:cars.brands})	
		}else{
			this.setState({modalVisible:true, modal})
		}
	}

	goToScreen(screen){
		const { navigate } = this.props.navigation
		return navigate(screen)
	}

	selectType(avtoType){

		this.setState({avtoType, modalVisible:false})
	}

	addCar(){
		let {autoNum, avtoType, autoColor, services, autoMarka, phone} = this.state
		let {navigation} = this.props
		let {companyId, branch_id} = this.props.navigation.state.params
	
		if(services.length > 0 != '' && autoColor && phone != '' && autoMarka.marka && autoNum != ''){
			phone = phone.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()\s]/g,"");
			phone = phone.slice(2);
			
			let customCar = {
				phone,
				avtoType,
				autoColor,
				autoModel:null,
				autoMarka,
				autoNum,
				status:true
			}

			function randomUserId(length) {
			  let text = "";
			  let possible = "0123456789";

			  for (let i = 0; i < length; i++)
			    text += possible.charAt(Math.floor(Math.random() * possible.length));

			  return text;
			}

			let queue = {
				car:customCar,
				services,
				user_device_id:'',
				branch_id,
				custom_status:true,
				user_id:+(`-${randomUserId(5)}`),
				company_id:companyId,
			}

			this.socket.emit('createQueue', queue)

			return navigation.goBack()
		}else{
			Alert.alert('Внимание!', 'Заполните все поля!')
		}
	}

  	render(){   
	    let {modal, autoMarka, services, autoColor, companyPrice, avtoType, modalVisible} = this.state
	    let {request} = this.props.auth
	    let {cars, user} = this.props.user
		
	    return(

	  		<View style={styles.profilescreen.viewBack}>

					<Modal
						animationType="slide"
						onRequestClose={()=>{}}
						transparent={false}
						visible={modalVisible}
					>
						<View style={{marginTop: 22}}>			           	
							{modal == 'marka' && this.viewCars()}
							{
								modal == 'typeAuto' && <CarViewType 
									user={user} 
									selectType={this.selectType.bind(this)} 
									cars={cars} 
								/>
							}	
							{modal == 'color' && this.viewColor()}
							{
								modal == 'services' && <ServicesView  
									bodyName={avtoType.typeName}
									selectServices={this.selectServices.bind(this)}
									openModal={this.openModal.bind(this)} 
									companyPrice={companyPrice} 
								/>
							}
						</View>
					</Modal>
						<Header navigation={this.props.navigation} isBack={true} title={'Добавление машины'}/>

						<View style={{flex:1}}>
							<ScrollView showsVerticalScrollIndicator={false}>
								<View style={styles.profilescreen.carBlock}>
									<TouchableOpacity onPress={this.openModal.bind(this, 'marka')} style={styles.merchant.queue.carButton}>
										{
											autoMarka.marka ?
												<View style={styles.profilescreen.selectMarka}>
										<View style={{marginLeft:10}}>
											<Image 
												source={{uri: `${config.public_url}${autoMarka.logo}`}}  
												style={styles.profilescreen.carImage}
											/>							
										</View>
										<View style={styles.profilescreen.flexBlock}>
											<MyText style={styles.profilescreen.modelText}>{constans.firtUpCase(autoMarka.marka)}</MyText>						
										</View>
												</View>
											:
												<MyText>Марка авто</MyText>
										}
									</TouchableOpacity>
									<TouchableOpacity onPress={this.openModal.bind(this, 'typeAuto')} style={styles.merchant.queue.carButton}>
										{
											avtoType.typeName ?
									<Image 
										source={{uri: `${config.public_url}${avtoType.image}`}}  
										style={styles.profilescreen.carImage}
									/>	
											: 
												<MyText>Тип кузова</MyText>
										}
									</TouchableOpacity>
									<TouchableOpacity onPress={this.openModal.bind(this, 'color')} style={styles.merchant.queue.carButton}>
										{
											autoColor ?
												<View style={[styles.profilescreen.color, {backgroundColor: autoColor}]} />
											:
												<MyText>Цвет авто</MyText>
										}
									</TouchableOpacity>
									<TouchableOpacity onPress={this.openModal.bind(this, 'services')} style={styles.merchant.queue.servicesButton}>
										{
											services.length > 0 ?
												<View style={styles.merchant.queue.serviceListBlock}>
													{
														services.map((el, index)=>{
															return(
																<MyText key={index} style={styles.merchant.queue.serviceText}>{el.title}</MyText>
															)
														})
													}
												</View>
											:
												<MyText>Услуги</MyText>
										}
									</TouchableOpacity>
									<View style={styles.merchant.queue.carButton}>
										<TextInputMask
											onChangeText={(phone)=>{this.setState({phone})}} 
											underlineColorAndroid='transparent'
											type={'cel-phone'}
											options={{
											  maskType: "BRL",
											  withDDD: true,
											  dddMask: '+9 (999) 999 99 99'
											}}
											maxLength={18}
											value={this.state.phone}
											style={styles.profilescreen.inputForm} 
											placeholder="Номер телефона"
										/>
									</View>

									<View style={styles.merchant.queue.carButton}>
										<TextInput
											onChangeText={(autoNum)=>{this.setState({autoNum})}} 
											underlineColorAndroid='transparent'
											keyboardType='phone-pad'
											maxLength={6}
											style={styles.profilescreen.inputForm} 
											placeholder="Номер машины (цифры)"
										/>
									</View>
									{
										request ? 
											<TouchableOpacity style={[styles.queue.carButton, {backgroundColor: constans.orangeColor, borderWidth: 0}]}>
												<MyText style={{color:'#fff'}}>ПОДОЖДИТЕ...</MyText>
											</TouchableOpacity>
										:
										<TouchableOpacity onPress={this.addCar.bind(this)} style={[styles.merchant.queue.carButton, {backgroundColor: '#365ce8', borderWidth: 0}]}>
											<MyText style={{color:'#fff'}}>ДОБАВИТЬ</MyText>
										</TouchableOpacity>
									}
								</View>
							</ScrollView>
						</View>
				</View>
    	)
    }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    auth:state.auth
  }
}

export default connect(mapStateToProps)(AddCarInQueue)