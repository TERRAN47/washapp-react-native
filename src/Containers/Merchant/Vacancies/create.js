import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  Alert,
  Switch,
  ImageBackground
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class CreateVacancy extends Component {

  constructor(props){
    super(props)

    this.state = {
      title:'',
      charge:'',
      requirements:'',
      work_mode:{
        start:'',
        end:''
      },
      salary:'',

      salaryStatus:false,

      //companies
      company:null
    }
  }

  componentDidMount(){

  }

  getCompany(carwash){
    this.setState({
      company:carwash
    }) 
  }

  goToScreen(screen){

    let { navigate } = this.props.navigation
    return navigate(screen, {
      getCompany: this.getCompany.bind(this)
    })

  }

  async createVacancy(){

    let { dispatch, navigation } = this.props

    await Api.merchant.vacancies.createVacancy(dispatch, navigation, this.state)
    await Api.merchant.vacancies.getVacancies(dispatch)

    return navigation.goBack()

  }

  render(){

    let { statusRequest } = this.props.merchant
    let { company } = this.state

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Создание вакансии'}/>

          <View style={{flex:1,padding:10}}>

            {
              statusRequest ?
                <Loader title="Загрузка" />
              :
                <ScrollView>

                  <TouchableOpacity onPress={this.goToScreen.bind(this, 'companiesForVacancies')} style={styles.merchant.cabinetMerchant.sectionViewFormVacancy}>
                    <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContent}>
                      <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>ВЫБЕРИТЕ АВТОМОЙКУ</Text>
                      
                      {
                        company ?
                          <Text>{ company.title }, г. { company.city.title }, адрес: { company.adress }</Text>
                        :
                          <Text>Автомойка не выбрана</Text>
                      }

                    </View>
                  </TouchableOpacity>

                  <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancy}>
                    <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContent}>
                      <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>ПОЗИЦИЯ</Text>
                      <TextInput 
                        value={this.state.title} 
                        onChangeText={(value)=>{this.setState({title:value})}} 
                        underlineColorAndroid='rgba(0,0,0,0)' 
                        style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentInput} 
                      />
                    </View>
                  </View>

                  <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancy}>
                    <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContent}>
                      <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>ОБЯЗАННОСТИ</Text>
                      <TextInput value={this.state.charge} onChangeText={(value)=>{this.setState({charge:value})}} 
                        underlineColorAndroid='rgba(0,0,0,0)' 
                        style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentInput} 
                      />
                    </View>
                  </View>  

                  <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancy}>
                    <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContent}>
                      <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>ТРЕБОВАНИЯ</Text>
                      <TextInput value={this.state.requirements} onChangeText={(value)=>{this.setState({requirements:value})}} 
                        underlineColorAndroid='rgba(0,0,0,0)' style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentInput} 
                      />
                    </View>
                  </View> 

                  <View style={styles.merchant.forms.sectionForm}>
                    <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>РЕЖИМ РАБОТЫ</Text>

                    <View style={styles.merchant.forms.timeView}>
                      <View style={styles.merchant.forms.timeViewSection}>

                        <Text>С</Text>
                          <DatePicker
                              style={styles.merchant.forms.timeInputView}
                              date={this.state.work_mode.start}
                              mode="time"
                              placeholder="Выберите время"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              showIcon={false}
                              customStyles={{
                                dateInput: styles.merchant.forms.timeInputStyle
                              }}
                              onDateChange={(date) => {this.setState({
                                work_mode:{
                                  ...this.state.work_mode,
                                  start:date
                                }
                              })}}
                          />

                      </View>
                      <View style={styles.merchant.forms.timeViewSection}>

                        <Text>До</Text>
                          <DatePicker
                              style={styles.merchant.forms.timeInputView}
                              date={this.state.work_mode.end}
                              mode="time"
                              placeholder="Выберите время"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              showIcon={false}
                              customStyles={{
                                dateInput: styles.merchant.forms.timeInputStyle
                              }}
                              onDateChange={(date) => {this.setState({
                                work_mode:{
                                  ...this.state.work_mode,
                                  end:date
                                }
                              })}}
                          />

                      </View>
                    </View>

                  </View>

                  <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancySalarySwitch}>
                    <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancySalarySwitchTitle}>ЗАРПЛАТА</Text>
                    <Switch value={this.state.salaryStatus} onValueChange={(value)=>{this.setState({salaryStatus:value})}}/>
                  </View>

                  <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancy}>
                    <View style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContent}>
                      <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>{ this.state.salaryStatus ? 'ЗАРПЛАТА' : 'ПРОЦЕНТЫ' }</Text>
                      <TextInput value={this.state.salary} keyboardType='numeric' 
                        onChangeText={(value)=>{this.setState({salary:value})}} 
                        underlineColorAndroid='rgba(0,0,0,0)' 
                        style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentInput} 
                      />
                    </View>
                  </View>

                  <TouchableOpacity onPress={this.createVacancy.bind(this)} style={styles.merchant.cabinetMerchant.sectionViewFormVacancyButton}>
                    <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyButtonTitle}>Создать</Text>
                  </TouchableOpacity>           

                </ScrollView> 
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant
  }
}

export default connect(mapStateToProps)(CreateVacancy)