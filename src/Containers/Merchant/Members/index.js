import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class Members extends Component {

  componentDidMount(){
    this.loadCompanies()
  }

  loadCompanies(){

    const { dispatch } = this.props
    const { userGeo } = this.props.maps

    return Api.merchant.carwash.getListCarWash(dispatch, userGeo.latitude, userGeo.longitude)
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  render(){

    let { user } = this.props.user
    let { companies, countCompanies } = this.props.companies
    let { statusRequest } = this.props

    return(
      <View style={styles.merchant.cabinetMerchant.viewBack}>
        <Header navigation={this.props.navigation} menu={true} title={'Сотрудники'}/>

        <View style={{flex:1}}>

          {
            statusRequest ?
              <Loader title="Загрузка" />
            : countCompanies > 0 ?
              <View style={styles.merchant.cabinetMerchant.mainContentWithCompanies}>
                <View style={styles.merchant.cabinetMerchant.viewCopaniesScroll}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    {
                      companies.map((item, index)=>{
                        return <MerchantComponents.ListCompanies navigation={this.props.navigation} routeName="viewMembers" key={index} item={item} />
                      })
                    }
                  </ScrollView>
                </View>
              </View>
            :
              <NotFound navigation={this.props.navigation} type='MERCHANT_COMPANIES' />
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    maps:state.maps,
    companies:state.companies,
    statusRequest:state.merchant.statusRequest
  }
}

export default connect(mapStateToProps)(Members)