import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import {
  View,
  Text,
  TouchableOpacity,
  Switch,
  Image,
  TextInput,
  ScrollView,
  Alert
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import underscore from 'underscore'
import config from 'Config'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class AdditionallyScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
      title:''
    }
  }

  componentDidMount(){

  }

  addAdditionally(additionally, companyIndex){

    let { title } = this.state
    let { dispatch } = this.props
    let { id } = this.props.navigation.state.params

    return Api.merchant.carwash.addAdditionally(dispatch, title, additionally, companyIndex, id)

  }

  goBack(){
    return this.props.navigation.goBack()
  }

  render(){

    let { id, companyIndex } = this.props.navigation.state.params
    let { companies } = this.props.companies

    let { additionally, type_payments } = underscore.where(companies, {
      id
    })[0]

    return(
      <View style={{flex:1}}>
        <Header navigation={this.props.navigation} isBack={true}  title='Дополнительно'/>
        <View style={styles.lists.mainView}>
          <ScrollView showsVerticalScrollIndicator={false}>

            {
              additionally.length > 0 ?
                <View style={styles.lists.viewItem}>

                  {
                    additionally.map((item, index)=>{
                      return <MerchantComponents.ListAdditionally id={id} companyIndex={companyIndex} api={Api} dispatch={this.props.dispatch} styles={styles} items={companies} item={item} key={index} index={index}/>
                    })
                  }

                </View>
              : <View></View>
            }

            <Text style={styles.lists.mainItenDescriptionText}>Вы можете добавить дополнительные услуги, например: Аксессуары для авто, Развлечения и т.д.</Text>

            <View style={styles.lists.formView}>
              <View style={styles.lists.formViewLeft}>
                <Text style={styles.lists.formViewLeftTitle}>Добавить свое</Text>
                <TextInput style={styles.lists.formViewLeftInput} 
                  underlineColorAndroid="transparent" 
                  value={this.state.title} 
                  onChangeText={(title)=>{this.setState({title})}} 
                />
              </View>
              <TouchableOpacity onPress={this.addAdditionally.bind(this, additionally, companyIndex)} style={styles.lists.formViewButtonAdd}>
                <MaterialCommunityIcons name="plus" size={38} color="#fff" />
              </TouchableOpacity>
            </View>

            <View style={styles.lists.formViewPayments}>
              <Text style={styles.lists.formViewPaymentsTitle}>Виды оплаты</Text>

              <View>
                {
                  type_payments.length > 0 ?
                    type_payments.map((item, index)=>{

                      return (<MerchantComponents.ListTypePayments 
                        id={id} 
                        companyIndex={companyIndex} 
                        api={Api} 
                        dispatch={this.props.dispatch} 
                        styles={styles} 
                        items={companies} 
                        item={item} 
                        key={index} 
                        index={index}/>)
                    })
                  :
                   <Text>Типы оплаты не найдены. Ожидайте обновления</Text>
                }
              </View>

            </View>

            <TouchableOpacity onPress={this.goBack.bind(this)} style={styles.lists.bottomBackSuccess}>
              <Text style={styles.lists.bottomBackSuccessTitle}>ОК</Text>
            </TouchableOpacity>

          </ScrollView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    statusRequest:state.merchant.statusRequest,
    companies:state.companies
  }
}

export default connect(mapStateToProps)(AdditionallyScreen)