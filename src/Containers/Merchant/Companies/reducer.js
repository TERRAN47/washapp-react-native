const initialState = {
	companies:null,
	countCompanies:0,
	uploadedLogo:''
}

const companies = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_LIST_BOXES' :

			state.companies.map((item, index)=>{

				if(item.id == action.id){
					state.companies[index].boxes = action.boxes
				}

			})

			return {
				...state,
				companies:state.companies
			}

		case 'UPDATE_MARCHANT_COMPANIES' :
			return {
				...state,
				companies:action.data,
				countCompanies:action.count
			}

		case 'ACTIVATED_COMPANY_MERCHANT' :

			state.companies[action.companyIndex].status.active = true

			return {
				...state,
				companies:state.companies
			}

		case 'DEACTIVATED_COMPANY_MERCHANT' :

			state.companies[action.companyIndex].status.active = false

			return {
				...state,
				companies:state.companies
			}

		case 'SAVE_CAR_TO_BOX' : 

			state.companies[action.companyIndex].boxes = action.boxList

			return {
				...state
			}	

		case 'UPDATE_MARCHANT_COMPANIES' :
			return {
				...state,
				companies:action.data,
				countCompanies:action.count
			}
		case 'UPDATE_BOX_COMPANY' :
			state.companies[action.companyIndex].boxes = action.boxList

			return {
				...state,
				companies:state.companies,
				
			}
		
		case 'UPDATE_PRICE_COMPANY' :

			state.companies.map((company, index)=>{

				if(company.id == action.id){
					state.companies[index].price = action.price
				}

			})

			return {
				...state,
				companies:state.companies
			}

		case 'UPLOADED_LOGO_SUCCESS' :
			return {
				...state,
				uploadedLogo:action.logo
			}	

		case 'UPDATE_LIST_ADDITIONALLY' :

			state.companies[action.index].additionally = action.list

			return {
				...state
			}

		case 'CHANGE_LIST_ADDITIONALLY' :

			return {
				...state,
				companies:action.items
			}

		case 'CHANGE_LIST_PAYMENTS' :

			return {
				...state,
				companies:action.items
			}

		case 'UPDATE_BONUSES_LIST' :

			state.companies.map((company, index)=>{

				if(company.id == action.id){
					state.companies[index].bonus = action.bonuses
				}

			})

			return {
				...state,
				companies:state.companies
			}

		case 'UPDATE_ADMINS_LIST' :

			state.companies.map((company, index)=>{

				if(company.id == action.id){
					state.companies[index].administrators = action.admins
				}

			})

			return {
				...state,
				companies:state.companies
			}

		default :
			return state
	}

}

export default companies