import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  ImageBackground
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, MyText, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class CabinetScreen extends Component {

  constructor(props){
    super(props)

  }

  componentDidMount(){

    this.getCoordsUser()

  }

  getCoordsUser(){

    const { dispatch } = this.props

    return Api.maps.saveUserGeo(dispatch)
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  render(){

    let { user } = this.props.user
    let { companies, countCompanies } = this.props.companies

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} menu={true} title={'Кабинет'}/>

          <View style={styles.merchant.cabinetMerchant.viewContent}>
            
            <MyText style={styles.merchant.cabinetMerchant.mainTitleSection}>Аккаунт</MyText>

            <View style={styles.merchant.cabinetMerchant.viewSection}>
              <MaterialCommunityIcons name="cellphone-iphone" style={styles.merchant.cabinetMerchant.iconSection} size={28} color="#5b5b5b" />
              <MyText style={styles.merchant.cabinetMerchant.sectionText}>+7{ user.phone }</MyText>
            </View>

            {/*<Text style={styles.merchant.cabinetMerchant.mainTitleSection}>Баланс</Text>
            
                        <TouchableOpacity onPress={this.goToScreen.bind(this, 'payment')} style={styles.merchant.cabinetMerchant.viewSectionBetween}>
                          <View style={styles.merchant.cabinetMerchant.viewSectionSettings}>
                            <MaterialCommunityIcons name="wallet" style={styles.merchant.cabinetMerchant.iconSection} size={28} color="#5b5b5b" />
                            <Text style={styles.merchant.cabinetMerchant.sectionText}>Пополнить баланс</Text> 
                          </View>
                          <MaterialCommunityIcons name="plus-box" style={styles.merchant.cabinetMerchant.iconSection} size={28} color="#3c5dd6" />
                        </TouchableOpacity>
            
                        <Text style={styles.merchant.cabinetMerchant.mainTitleSection}>Безопасность</Text>
            
                        <TouchableOpacity style={styles.merchant.cabinetMerchant.viewSectionBetween} onPress={this.goToScreen.bind(this, 'ResetPasswordScreen')}>
                          <View style={styles.merchant.cabinetMerchant.viewSectionSettings}>
                            <MaterialCommunityIcons name="lock-reset" style={styles.merchant.cabinetMerchant.iconSection} size={28} color="#5b5b5b" />
                            <Text style={styles.merchant.cabinetMerchant.sectionText}>Изменить пароль</Text> 
                          </View>
                          <MaterialCommunityIcons name="square-edit-outline" style={styles.merchant.cabinetMerchant.iconSection} size={28} color="#3c5dd6" />
                        </TouchableOpacity>*/}

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    maps:state.maps,
    companies:state.companies
  }
}

export default connect(mapStateToProps)(CabinetScreen)