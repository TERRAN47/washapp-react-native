const initialState = {
	clientInfo:null,
}

const queue = (state = initialState, action) => {

	switch(action.type){
		case 'CLIENT_INFO' :
			return {
				...state,
				clientInfo:action.clientInfo,
			}

		default :
			return state
	}

}

export default queue