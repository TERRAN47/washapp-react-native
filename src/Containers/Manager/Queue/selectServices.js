import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import {constans} from 'Services/Utils'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import underscore from 'underscore'
import config from 'Config'

class SelectServices extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      services:[]
    };
  }
  componentDidMount(){
  
    let {boxInfo, companyPrice} = this.props.navigation.state.params

    companyPrice = underscore.findWhere(companyPrice, {
      title:boxInfo.car.car.avtoType.typeName
    })

    companyPrice.prices.forEach((element, key)=>{
      companyPrice.prices[key].status = false
      boxInfo.car.services.forEach((item, index)=>{
        if(element.title == item.title){
          companyPrice.prices[key].status = true
        }
      });
    });

    this.setState({services:companyPrice.prices})
  }

  getCoordsUser(){
    const { dispatch } = this.props

    return Api.maps.saveUserGeo(dispatch)
  }  

  editServices(title, status){
    let {services} = this.state
    let {dispatch} = this.props
    let {boxInfo} = this.props.navigation.state.params

    let findPrice = underscore.findWhere(services, {
      title
    })
    findPrice.status = !status

    if(title == 'КУЗОВ-САЛОН'){
      
      let findKuzov = this.findService('КУЗОВ')
      let findSalon = this.findService('САЛОН')
      let findOtbivka = this.findService('ОТБИВКА')
      let findOtbivkaPenoi = this.findService('ОТБИВКА С ПЕНОЙ')
      let findPoliki = this.findService('ПОЛИКИ 2 ШТ')
      let findPoliki4 = this.findService('ПОЛИКИ 4 ШТ')

      findKuzov.status = false
      findSalon.status = false
      findOtbivka.status = false
      findOtbivkaPenoi.status = false
      findPoliki.status = false
      findPoliki4.status = false

      findOtbivkaPenoi.hidden = !status ? false : true
      findOtbivka.hidden = !status ? false : true
      findSalon.hidden = !status ? false : true
      findKuzov.hidden = !status ? false : true
      findPoliki.hidden = !status ? false : true
      findPoliki4.hidden = !status ? false : true
    }else if(title == 'КУЗОВ'){
      let findKuzovSalon = this.findService('КУЗОВ-САЛОН')
      let findSalon = this.findService('САЛОН')
      let findOtbivka = this.findService('ОТБИВКА')
      let findOtbivkaPenoi = this.findService('ОТБИВКА С ПЕНОЙ')

      findKuzovSalon.status = false
      findOtbivka.status = false
      findOtbivkaPenoi.status = false

      findKuzovSalon.hidden = status && !findSalon.status ? true : false
      findOtbivka.hidden = status && !status ? true : false
      findOtbivkaPenoi.hidden = status && !status ? true : false
    }else if(title == 'САЛОН'){
      let findKuzovSalon = this.findService('КУЗОВ-САЛОН')
      let findKuzov = this.findService('КУЗОВ')
      let findPoliki = this.findService('ПОЛИКИ 2 ШТ')
      let findPoliki4 = this.findService('ПОЛИКИ 4 ШТ')

      findKuzovSalon.status = false
      findPoliki.status = false
      findPoliki4.status = false

      findKuzovSalon.hidden = status && !findKuzov.status ? true : false
      findPoliki.hidden = !status ? false : true
      findPoliki4.hidden = !status ? false : true
    } 

    dispatch({
      type:'SELECT_SERVICES_ADMIN',
      selectServices:services
    })
    this.setState({services})
  }

  findService(title){
    let {services} = this.state
    return underscore.findWhere(services, {
      title
    })     
  }

  async saveServices(){
    let {services} = this.state

    let {dispatch, navigation} = this.props
    let {companyId, close, boxInfo} = this.props.navigation.state.params
    
    let request = await Api.merchant.carwash.updateQeueBoxServices(dispatch, navigation, companyId, services, boxInfo)
    if(request == 'ok'){
      close()
      navigation.goBack()
    }
  }
  
  render(){

    const { navigation } = this.props
    let {request} = this.props.auth
    let {services} = this.state
    let summ = 0

    return(
      <View style={styles.profilescreen.viewBack}>
        <Header navigation={this.props.navigation}  isBack={true} title={'Выбор услуги'}/>
         
        <ScrollView>
          <View style={styles.carWashes.priceServicesBlock}>
          {
            services && services.map((item, index)=>{
              if(item.status) summ += +(item.price)
              if(item.hidden == undefined || item.hidden){
                return(
                  <View key={index} style={styles.carWashes.priceList}>
                    <Text style={styles.carWashes.servicesText}>{item.title}</Text>
                    <TouchableOpacity style={{flexDirection: 'row'}} onPress={this.editServices.bind(this, item.title, item.status)}>
                      <Text style={styles.carWashes.servicesText}>{item.price} {constans.currency}  </Text>
                      <FontAwesome size={30} color={constans.defaultColor} name={item.status ? 'toggle-on' : 'toggle-off'} />
                    </TouchableOpacity>
                  </View>
                )
              }
            })
          }
              
          </View>             
        </ScrollView>

        {
          request ?
            <View style={styles.carWashes.priceFooter}>          
              <Text style={styles.carWashes.servicesNext}>ОЖИДАЙТЕ...</Text>
            </View>
          :
            <TouchableOpacity style={styles.carWashes.priceFooter} onPress={this.saveServices.bind(this)}>          
              <Text style={styles.carWashes.servicesNext}>К оплате: {summ} {constans.currency} СОХРАНИТЬ</Text>
            </TouchableOpacity>    
        }   
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    user:state.user,
    auth:state.auth,
    maps:state.maps
  }
}

export default connect(mapStateToProps)(SelectServices)