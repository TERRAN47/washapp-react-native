import React, { Component } from 'react'

import {
	View,
	Text,
	Image,
	Linking,
	Dimensions,
	TouchableOpacity,
	Platform
} from 'react-native'
import call from 'react-native-phone-call'
import config from '../../../../Config'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import styles from 'Services/Styles'

const { width } = Dimensions.get('window')
class RenderCar extends Component {

	constructor(props){
		super(props)

		this.state = {
			loading:false
		}
	}

	callCompany(phone){

		if(Platform.OS === 'android'){

			return Linking.openURL(`tel:+7${phone}`)

		}else{

			const args = {
				number: `+7${phone}`
			}
				
			return call(args).catch(console.error)

		}
		
	}

	closeCar(){

		let { item } = this.props

		this.setState({
			loading:!this.state.loading
		})
	
		return this.props.closeCarForAdmin(item.id, item.company_id, item.user_id)

	}

	canselTarget(){
		let { item, changeSwitchStatus } = this.props
		console.log('item', item)
		changeSwitchStatus(0, false)
	}

	switchCarSort(status){
		let { item, index, changeSwitchStatus } = this.props

		changeSwitchStatus(item.id, status, index)
		
		console.log('item', item)
	}

	render(){

		let { item, carId, switchStatus } = this.props
		return(
			<View style={item.drove_up ? [styles.merchant.lists.memberView, {backgroundColor:'#61bd4fa1'}] : styles.merchant.lists.memberView}>
				{
					switchStatus ?
						carId == item.id ?
							<TouchableOpacity onPress={this.canselTarget.bind(this)} style={styles.merchant.lists.buttonsBig}>
								<FontAwesome name="close" size={width*.1} color="#fff" />
							</TouchableOpacity>	
						:
							<TouchableOpacity onPress={this.switchCarSort.bind(this, false)} style={styles.merchant.lists.buttonsBig}>
								<FontAwesome name="arrow-up" size={width*.1} color="#fff" />
							</TouchableOpacity>
					:
						<TouchableOpacity onPress={this.switchCarSort.bind(this, true)} style={styles.merchant.lists.buttonsBig}>
							<FontAwesome name="retweet" size={width*.1} color="#fff" />
						</TouchableOpacity>
				}
		
				<View style={styles.merchant.lists.memberViewLeft}>
					<Image style={styles.merchant.lists.memberViewLeftLogo} source={{uri:`${config.public_url}${item.car.autoMarka.logo}`}}/>
					<Text style={styles.merchant.lists.memberViewLeftCarName}>{ item.car.autoModel }</Text>
				</View>

				<View style={styles.merchant.lists.memberViewCenter}>

					<View style={styles.merchant.lists.memberViewCenterNumCar}>
						<Text style={styles.merchant.lists.memberViewCenterNumCarTitle}>{ item.car.autoNum }</Text>
					</View>

					{item.car.autoColor ? 
						<View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor: item.car.autoColor}]}></View>
						: null
					}

					<Image style={styles.merchant.lists.memberViewLeftCarBody} source={{uri:`${config.public_url}${item.car.avtoType.image}`}}/>
				</View>
			</View>
		)
	}
}

export default RenderCar