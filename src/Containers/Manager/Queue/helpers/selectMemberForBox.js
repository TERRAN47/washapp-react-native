import React, { Component } from 'react'

import {
	View,
	Text,
	TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import { Header, NotFound } from 'Composition'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

class SelectMemberForBox extends Component {

	member(member){
		let {boxList, boxIndex, id, index, companyIndex} = this.props.navigation.state.params
		let {dispatch, navigation} = this.props

		Api.merchant.carwash.addMemberToBox(dispatch, navigation, member, boxList, boxIndex, id, companyIndex)
	}

	render(){

		let { members } = this.props.navigation.state.params
		
		return(
			<View style={styles.merchant.queue.mainView}>
				<Header navigation={this.props.navigation} isBack={true} title='Выбор сотрудника'/>
				<View style={{flex:1}}>
				{
					members && members.length > 0 ?
						members.map((elem, index)=>{
							return(
								<TouchableOpacity onPress={this.member.bind(this, elem)} style={styles.merchant.queue.contentMember} key={index}>
									<View>
										<MaterialCommunityIcons name="account" size={27} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
									</View>
									<View>
										<Text>{elem.firstName}</Text>
										<Text>{elem.phone}</Text>
									</View>
								</TouchableOpacity>
							)
						})
					:
						<NotFound type="NOT_MEMBERS" />
				}					
				</View>
			</View>
		)
	}
}
const mapStateToProps = (state) => {
  return {
  	
  }
}
export default connect(mapStateToProps)(SelectMemberForBox)