import React, { Component } from 'react'

import {
	View,
	Text,
	Image,
	Alert,
	TouchableOpacity
} from 'react-native'
import { Timer, MyText } from 'Composition'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import config from 'Config'
import { constans, icons} from 'Services/Utils'
import { moment } from 'Services/Time'

class RenderBox extends Component {

	selectMember(boxIndex){

		let {members, boxList, dispatch, companyIndex, id} = this.props
		let { navigate } = this.props.navigation

		return navigate('selectMemberForBox', {
			members,
			boxList,
			boxIndex,
			companyIndex,
			id
		})
	}

	saveCarToBox(){
		let { item, index, car, boxList, dispatch, companyIndex, id, socket, user, statusEvent } = this.props;

		car.invite_box_time = moment().format("DD-MM-YYYY HH:mm:ss");
		this.props.saveCarToBox();

		if(statusEvent){
			//return Alert.alert('Сообщение', 'В данный момент это сделать невозможно')
		}else if(car == null){
			return Alert.alert('Сообщение', 'В очереди нет машин')
		}else if(item.member == null){
			return Alert.alert('Сообщение', 'Добавьте мойщика в данный бокс')
		}else{
			return Api.merchant.carwash.saveCarToBox(dispatch, companyIndex, index, car, boxList, id, socket, item)
		}
	}

	complete(boxIndex, user_id, queue){
		let { socket, item, id } = this.props
		let that = this

    Alert.alert(
      'Завершение!',
      'Выгнать авто?',
      [
        {
          text: 'Отмена',
          onPress: () => console.log('Cancel'),
          style: 'cancel',
        },
        {text: 'Выгнать', onPress: () => {
			that.props.removeFirstCar()

			that.props.saveCarToBox()
			socket.emit('deleteQueue', {user_id, company_id:id, boxIndex, pay_status:item.pay_status, custom_status:queue.custom_status})

		    socket.emit('payClientBox', {
		    	user_id,
		    	company_id:id
		    })
        } },
      ],
      {cancelable: false},
    );
	}

	inviteToPay(boxIndex, car){
    let {inviteClientToPay} = this.props
    if(car.user_device_id){
    	Api.merchant.carwash.sendInvitePay(car.user_device_id)
    }
    
    inviteClientToPay(true, boxIndex) 		
	}

	openServices(boxIndex){
    let { servicesInBox} = this.props
    servicesInBox(true, boxIndex)
	}

	async boxSwitchStatus(status, boxIndex){
		let {boxList, id, dispatch} = this.props
		boxList[boxIndex].status = !status
		let reqestUpdate = await Api.merchant.carwash.updateBoxes(dispatch, boxList, id)
		if(reqestUpdate == 'ERROR'){
			boxList[boxIndex].status = !boxList[boxIndex].status
		}
	}

	skipCar(user_id){
		let { socket, id } = this.props
		socket.emit('slipQueue', {company_id:id, user_id:user_id})
	}

	render(){
		let { item, index, car} = this.props
		let boxIndex = index
		index = index + 1
		return(
			<View style={styles.merchant.queue.itemBoxStyle}>

				{ item.car && <Timer defaultStyles={true} time={item.car.invite_box_time} /> }

				<View style={styles.merchant.queue.itemBoxContentStyle}>

					<View style={styles.merchant.queue.itemBoxContentStyleLeftMain}>
						<View style={styles.merchant.queue.itemBoxContentStyleLeft}>
							{
								item.status ?
									(
										item.car ?
											<View style={styles.merchant.queue.boxStatusFalse}>
												<Image
													style={styles.merchant.queue.boxStatusImage} 
													source={icons.iconBoxStatus} 
												/>
											</View>
										:
											<TouchableOpacity onPress={this.boxSwitchStatus.bind(this, item.status, boxIndex)}>
												<View style={styles.merchant.queue.boxStatusOff}>
													<Image
														style={styles.merchant.queue.boxStatusImage} 
														source={icons.iconBoxStatus} 
													/>
												</View>
											</TouchableOpacity>
									)
								:

									<TouchableOpacity onPress={this.boxSwitchStatus.bind(this, item.status, boxIndex)}>
			            				<View style={styles.merchant.queue.boxStatusTrue}>
						            		<Image
						            			style={styles.merchant.queue.boxStatusImage} 
						            			source={icons.iconBoxStatus} 
						            		/>
			            				</View>
									</TouchableOpacity>
							}
							<Text style={styles.merchant.queue.itemBoxContentStyleLeftTitle}>Пост { index }</Text>
						</View>

						<View style={styles.merchant.queue.itemBoxContentStyleCenter}>
							{
								item.member ?
									<TouchableOpacity onPress={this.selectMember.bind(this, boxIndex)} style={[styles.merchant.queue.itemBoxContentStyleCenterMember, {marginLeft:2}]}>
										{/*<MaterialCommunityIcons name="account" size={27} style={styles.merchant.company.iconSetting} color="#5b5b5b" />*/}
										<Image style={styles.merchant.queue.itemBoxContentStyleLeftImage} source={require('Media/moishikBlack.png')} />
										<Text style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}>{ item.member.firstName }</Text>
									</TouchableOpacity>
								:
									<TouchableOpacity onPress={this.selectMember.bind(this, boxIndex)} style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
										{/*<MaterialCommunityIcons name="account" size={27} style={styles.merchant.company.iconSetting} color="#5b5b5b" />*/}
										<Image style={styles.merchant.queue.itemBoxContentStyleLeftImage} source={require('Media/moishikBlack.png')} />
										<Text style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}>Не добавлен</Text>
									</TouchableOpacity>
							}

							{
								item.car ?
									<View style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
										{
											item.car && item.car.car ?
												<View style={styles.profilescreen.imgBlock}>
													<Image 
														source={{uri: `${config.public_url}${item.car.car.autoMarka.logo}`}}  
														style={styles.merchant.queue.carImage}
													/>							
												</View>
											:
												<MaterialCommunityIcons name="car-connected" size={27} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										}
										
										<Text style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}>{ item.car && item.car.car ? item.car.car.autoNum : 'Не добавлена' }</Text>
									</View>
								:
									car && car.car ?
										<View style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
											<View style={styles.profilescreen.imgBlock}>
												<Image 
													source={{uri: `${config.public_url}${car.car.autoMarka.logo}`}}  
													style={styles.merchant.queue.carImage}
												/>							
											</View>											
											<Text style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}>{ car.car.autoNum }</Text>
										</View>
									:
										<View style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
											<MaterialCommunityIcons name="car-connected" size={27} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
											<Text 
												style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}
											>
												Машин в очереди нет
											</Text>
										</View>									
							}
 
							{
								item.status  && item.car &&
								<TouchableOpacity onPress={this.openServices.bind(this, boxIndex)} style={styles.merchant.queue.itemBoxContentStyleCenterMember}>
									<MaterialCommunityIcons name="information-variant" size={27} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
									<Text style={styles.merchant.queue.itemBoxContentStyleCenterMemberTitle}>Услуги</Text>
								</TouchableOpacity>
							}
							
						</View>
					</View>

					<View style={styles.merchant.queue.itemBoxContentStyleRight}>
						{
							item.status && item.car ?
								<View>
									{
										item.pay_status ?
											<View style={styles.merchant.queue.boxPaySuccess}>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>ОПЛАЧЕНО</Text>
											</View>
										:
											<TouchableOpacity onPress={this.inviteToPay.bind(this, boxIndex, item.car)} style={styles.merchant.queue.itemBoxContentStyleRightBlueButton}>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>Пригласить к</Text>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>оплате</Text>
											</TouchableOpacity>																				
									}

									<TouchableOpacity onPress={this.complete.bind(this, boxIndex, item.car.user_id, item.car)} style={styles.merchant.queue.itemBoxContentStyleRightRedButton}>
										<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>Завершить</Text>
									</TouchableOpacity>
								</View>	
							:
								!item.status && car && car.car ?
									car.drove_up || car.custom_status ?
										<View>
											<TouchableOpacity onPress={this.saveCarToBox.bind(this)} style={styles.merchant.queue.itemBoxContentStyleRightBlueButton}>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>Пригласить к</Text>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>заезду</Text>
											</TouchableOpacity>
											<TouchableOpacity onPress={this.skipCar.bind(this, car.user_id)} style={styles.merchant.queue.itemBoxContentStyleRightBlueButton}>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>{`Следующий`}</Text>
											</TouchableOpacity>
										</View>
									:
										<View>
											<MyText>{`Нет на месте`}</MyText>
											<TouchableOpacity onPress={this.skipCar.bind(this, car.user_id)} style={styles.merchant.queue.itemBoxContentStyleRightBlueButton}>
												<Text style={styles.merchant.queue.itemBoxContentStyleRightButtonText}>{`Следующий`}</Text>
											</TouchableOpacity>
										</View>
										
								:
									<View></View>
						}
					</View>

				</View>

			</View>
		)
	}
}

export default RenderBox