import React, { Component } from 'react'

import {
	View,
	Text,
	Image,
	ScrollView,
	TouchableOpacity
} from 'react-native'

import config from '../../../../Config'
import styles from 'Services/Styles'


class CarViewType extends Component {

	constructor(props){
		super(props)

		this.state = {
			loading:false
		}
	}

  	selectType(image, typeName){
  		let { user, selectType} = this.props

  		const avtoType = {
  			image,
  			typeName
  		}

  		selectType(avtoType)
  	}

	render(){

		let { cars} = this.props

		return(
			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={styles.profilescreen.categoryBlock}>
					{
						cars.bodyworks.map((item, index)=>{
							return(
								<TouchableOpacity 
									key={index} 
									onPress={this.selectType.bind(this, item.photo, item.title)} 
									style={styles.profilescreen.typeAvtoBlock}
								>
									<Image 
											style={styles.profilescreen.typeAvtoIcon} 
											source={{uri:`${config.public_url}${item.photo}`}}
									/>
									<Text style={styles.profilescreen.typeName}>{item.title}</Text>
								</TouchableOpacity>
							)
						})
					}
				</View>
			</ScrollView>
		)
	}
}

export default CarViewType