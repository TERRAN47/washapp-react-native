import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
	View,
	Modal,
	TouchableOpacity,
	ScrollView,

	Alert
} from 'react-native'

import Api from 'Services/Api'
import _ from 'underscore'
import styles from 'Services/Styles'
import { Header, Loader, NotFound } from 'Composition'
import { socket } from 'Services/Utils'
import RenderCar from './helpers/RenderCar'
import ModalCarInfo from 'Components/Modal/modalCarInfo'

class QueueCars extends Component {

	constructor(props) {

		super(props)
		this.socket = socket()

		this.state = {
			list:null,
			modalStatus:false,
			targetCar:null,
			carId:0,
			carIndex:0,
			switchStatus:false
		}

	}

	componentWillUnmount() {
		return this.socket.disconnect()
	}

	componentDidMount(){
		let { id } = this.props.navigation.state.params

		this.socket.emit('adminUpdateList', {
			id
		})

		this.socket.on('ERROR', (data)=>{
			Alert.alert('Внимание!', 'Произошла ошибка!')
		})   

		this.socket.on('updateQueue', (data)=>{
			let carsQEUElist = _.where(data.data, {
				status_type:'QEUE'
			})
			this.setState({
				list:carsQEUElist && carsQEUElist.length > 0 ? carsQEUElist : null
			})
		})
	}

	async changeSwitchStatus(switchCarId, status, targetIndex){
		if(!status && switchCarId == 0){
			this.setState({carId:0, switchStatus:status})
		}else if(!status && switchCarId){
			let {dispatch} = this.props
			let {carId} = this.state
			let { id } = this.props.navigation.state.params
			this.setState({carId:0,  switchStatus:status})
			let reques = await Api.merchant.carwash.switchCarSort(dispatch, carId, targetIndex, id)
			if(reques == 'ok'){
				this.socket.emit('adminUpdateList', {
					id
				})
			}
		}else{
			this.setState({carId:switchCarId, carIndex:targetIndex, switchStatus:status})
		}
	}

	closeCarForAdmin(id, company_id, user_id){
		this.socket.emit('closeCarFromAdmin', {
			id,
			company_id,
			user_id
		})

		this.socket.emit('adminUpdateList', {
			id:company_id
		})
	}
	
	switchModalStatus(status){
		this.setState({modalStatus:status})
	}

	openModalCar(car){
		this.setState({modalStatus:true, targetCar:car})
	}

	render(){
		let { list, targetCar, carId, switchStatus, modalStatus } = this.state
		let { id, companyPrice, branch_id } = this.props.navigation.state.params
		let {navigation, dispatch} = this.props
		let {request} = this.props.auth

		return(
			<View style={styles.merchant.queue.mainView}>
				<Modal
					animationType="slide"
					onRequestClose={()=>{}}
					transparent={false}
					visible={modalStatus}
				>
					{
						targetCar ?
							<ModalCarInfo 
								modalStatus={modalStatus}
								dispatch={dispatch}
								navigation={navigation}
								closeCarForAdmin={this.closeCarForAdmin.bind(this)} 
								car={targetCar}
								switchModalStatus={this.switchModalStatus.bind(this)} 
							/>
						: null
					}
				</Modal>
				<Header navigation={this.props.navigation} companyPrice={companyPrice} branchId={branch_id} addCarQueue={true} companyId={id} isBack={true} title='Очередь'/>
				{
					request ?
						<Loader title="Обновление" />
					:
						list ?
							<ScrollView>
								<View style={styles.merchant.queue.listBoxView}>
								{
									list.map((car, index)=>{
										return (
											<TouchableOpacity key={index} onPress={this.openModalCar.bind(this, car)} >
												<RenderCar
													closeCarForAdmin={this.closeCarForAdmin.bind(this)} 
													socket={this.socket} 
													changeSwitchStatus={this.changeSwitchStatus.bind(this)}
													switchStatus={switchStatus}
													index={index}
													carId={carId}
													dispatch={this.props.dispatch} 
													item={car} 
												/>
											</TouchableOpacity>
										)
									})
								}
								</View>
							</ScrollView>
						:
							<NotFound type="QUEUE_CAR_NOT_FOUND" />
				}
			</View>
		)
	}
}

const mapStateToProps = (state) => {
  return {
		user:state.user,
		auth:state.auth,
    queue:state.queue
  }
}

export default connect(mapStateToProps)(QueueCars)