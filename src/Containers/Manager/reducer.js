const initialState = {
	statusRequest:false
}

const merchant = (state = initialState, action) => {

	switch(action.type){

		case 'STATUS_REQUEST_MERCHANT' :
			return {
				...state,
				statusRequest:action.status
			}

		default :
			return state
	}

}

export default merchant