import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Share,
  Alert,
  ImageBackground
} from 'react-native'
import _ from 'underscore'
import { Header, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class SavedReports extends Component {

  constructor(props){
    super(props)

    this.state = {
      reports:null
    }
  }

  componentDidMount(){
    this.loadCompany()
  }

  loadCompany(){

    const { id } = this.props.navigation.state.params
    const { reports } = this.props

    let searchReports = _.where(reports, {
      id
    })

    return this.setState({
      reports:searchReports.length > 0 ? searchReports : null
    })

  }

  async sendReportToSocials(item){

    let {
      link,
      date
    } = item

    try {
      const result = await Share.share({
        message:`Отчет за ${date}: ${link}`
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      Alert.alert('Сообщение', error.message)
    }

  }

  render(){

    let { reports } = this.state

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Отчеты'}/>

          <View style={{flex:1}}>

            {
              reports != null ?
                <View style={styles.merchant.cabinetMerchant.mainContentWithCompanies}>
                  <View style={[styles.merchant.cabinetMerchant.viewCopaniesScroll]}>

                    <Text style={{padding:10}}>Выберите отчет, чтобы отправить</Text>

                    <ScrollView showsVerticalScrollIndicator={false}>
                    
                      {
                        reports.map((item, index)=>{
                          return (
                            <TouchableOpacity onPress={this.sendReportToSocials.bind(this, item)} style={styles.merchant.cabinetMerchant.listSavedOrderItem} key={index}>
                              <Text>{ item.title }</Text>
                              <Text>ID автомойки: { item.id }</Text>
                              <Text>Дата отчета: { item.date }</Text>
                            </TouchableOpacity>
                          )
                        })
                      }

                    </ScrollView>
                  </View>
                </View>
              :
                <Text style={{padding:10,textAlign:'center'}}>Отчетов не найдено</Text>

            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    reports:state.report.reports
  }
}

export default connect(mapStateToProps)(SavedReports)