import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  TextInput,
  Platform,
  ImageBackground
} from 'react-native'
import { ApplePayButton, PaymentRequest } from 'react-native-payments'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, MyText, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

import { dataPrices } from '../../Domain'

class Payment extends Component {

  constructor(props){
    super(props)

    this.state = {
      prices:[],
      select:null,
      promocode:'',
      receipt:null,
      summ:''
    }

  }

  componentDidMount(){

    this.setState({
      prices:dataPrices
    })

    this.getProfile()
  }

  getProfile(){
    let { dispatch } = this.props
    return Api.auth.getProfile(dispatch) 
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  checkPrice(index, price){

    let { prices } = this.state

    return this.setState({
      prices:prices.map((t, i)=>{
        if(i === index){
          return {
            ...t,
            status:true
          }
        }else{
          return {
            ...t,
            status:false
          }
        }
      }),
      select:price
    })
  }

  createPayment(){

    let { select } = this.state
    let { dispatch, navigation } = this.props

    return Api.merchant.payment.createPayment(dispatch, select, navigation)
  }

  promoCode(){

    let { promocode } = this.state
    let { dispatch } = this.props
    return Api.merchant.payment.promoCode(dispatch, promocode)
  }

  async iosPurchase(){
    let { dispatch } = this.props
    let { summ } = this.state

    if(summ != ''){
      const METHOD_DATA = [{
        supportedMethods: ['apple-pay'],
        data: {
          merchantIdentifier: 'merchant.org.washapp.shane',
          supportedNetworks: ['visa', 'mastercard', 'amex'],
          countryCode: 'KZ',
          currencyCode: 'KZT',
        }
      }]

      const DETAILS = {
        id: 'merchant-balance',
        total: {
          label: 'Пополнение баланса',
          amount: { 
            currency: 'KZT', 
            value: summ
          }
        }
      }

      const paymentRequest = new PaymentRequest(METHOD_DATA, DETAILS)

      let statusMakePayment = await paymentRequest.canMakePayments()

      if(statusMakePayment){

        let paymentResponse = await paymentRequest.show()

        if(paymentResponse){

          let { status } = await Api.merchant.payment.testIOSPAY(paymentResponse._details)

          console.log(887, paymentResponse._details)

          return paymentResponse.complete('success')

        }else{
          return paymentResponse.complete('failure') 
        }

      }

    }else{

      return Alert.alert('Сообщение', 'Заполните поле')

    }

  }

  render(){

    let { user } = this.props.user
    let { statusRequest } = this.props.merchant
    let { prices } = this.state
    let { list } = this.props.payments

    return(

        <View style={styles.merchant.payment.viewContent}>

          <Header navigation={this.props.navigation} isPay={user.balance} isBack={true} title={'Пополнить баланс'}/>
          <MyText style={styles.merchant.payment.headerTitlePaymentPhone}>Аккаунт: {user.phone}</MyText>
          <ScrollView>
            <View style={styles.merchant.payment.viewContent}>
              {
                user.bonus_percent > 0 &&
                <View style={styles.merchant.payment.viewContentPercent}>
                  <MyText style={styles.merchant.payment.viewContentPercentTitle}>Скидка на активацию боксов: { user.bonus_percent }%</MyText>
                </View>                
              }
              <View style={styles.merchant.payment.viewContentPromoCode}>
                <TextInput 
                  placeholder="Промокод" 
                  underlineColorAndroid={'transparent'} 
                  style={styles.merchant.payment.viewContentPromoCodeInput} 
                  value={this.state.promocode} 
                  onChangeText={(value)=>{ this.setState({promocode:value}) }} 
                />
                {
                  Platform.OS === 'android' ?

                    <View>
                      <View>
                        <Image source={require('Media/logoTerminal.jpg')} style={styles.merchant.payment.logoQiwi} />
                        <MyText style={styles.merchant.payment.headerTitlePayment}>Пополнение баланса через {'\n'} QIWI Терминал</MyText>
                        <Text style={styles.merchant.payment.headerTitlePaymentDescription}>
                          Найдите QIWI Терминал, нажмите «ОПЛАТА УСЛУГ», в категории 
                          «Другие услуги» или через ПОИСК найдите кнопку «WashApp», 
                          введите номер аккаунта (указанный при регистрации в приложении «WashApp») и внесите требуемую сумму в терминал. 
                          Не забудьте взять чек! Средства моментально будут зачислены на Ваш баланс в приложении WashApp.
                        </Text>
                        <MyText style={styles.merchant.payment.noCommision}>ОПЛАТА БЕЗ КОМИССИИ.</MyText>
        
                        <Image source={require('Media/logoSite.jpg')} style={styles.merchant.payment.logoQiwi} />
                        <MyText style={styles.merchant.payment.headerTitlePayment}>Пополнение баланса через {'\n'} QIWI Кошелек</MyText>
                        <Text style={styles.merchant.payment.headerTitlePaymentDescription}>
                          Зайдите в Ваш QIWI Кошелек, через строку Поиска найдите «WashApp»
                          (https://qiwi.com/payment/form/35111), введите номер аккаунта (указанный при регистрации в приложении «WashApp») и укажите желаемую сумму к оплате.
                          Нажмите «ОПЛАТИТЬ», подтвердите свои данные и введите код из СМС. Средства моментально будут зачислены на Ваш баланс в приложении WashApp.
                        </Text>
                        <MyText style={styles.merchant.payment.noCommision}>ОПЛАТА БЕЗ КОМИССИИ.</MyText>
                        <Image source={require('Media/qiwiBanner.jpg')} style={styles.merchant.payment.bannerQiwi} />
                      </View>
        
                      <View style={styles.merchant.payment.viewContentPromoCode}>
                        
                        <TextInput 
                          placeholder="Промокод" 
                          underlineColorAndroid={'transparent'} 
                          style={styles.merchant.payment.viewContentPromoCodeInput} 
                          value={this.state.promocode} 
                          onChangeText={(value)=>{ this.setState({promocode:value}) }} 
                        />
                        {
                          statusRequest ?
                            <TouchableOpacity style={styles.merchant.payment.viewContentPromoCodeButton}>
                              <Text style={styles.merchant.payment.viewContentPromoCodeButtonText}>Подождите...</Text>
                            </TouchableOpacity>
                          :
                            <TouchableOpacity onPress={this.promoCode.bind(this)} style={styles.merchant.payment.viewContentPromoCodeButton}>
                              <Text style={styles.merchant.payment.viewContentPromoCodeButtonText}>Пополнить баланс</Text>
                            </TouchableOpacity>
                        }
                      </View>
                    </View>
                :
                  <View style={styles.merchant.payment.mainBlockSumForIOS}>
                    <TextInput keyboardType="numeric" style={styles.merchant.payment.mainInputSumForIOS} placeholder="Сумма" onChangeText={(value)=>{ this.setState({summ:value}) }}/>
                    <TouchableOpacity style={styles.merchant.payment.mainSubmitSumForIOS} onPress={this.iosPurchase.bind(this)}>
                      <Text style={styles.merchant.payment.mainSubmitSumForIOSText}>Пополнить</Text>
                    </TouchableOpacity>
                  </View>
                }

              </View>

            </View>
          </ScrollView>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant,
    payments:state.payments
  }
}

export default connect(mapStateToProps)(Payment)