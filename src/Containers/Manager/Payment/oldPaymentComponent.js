import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  ImageBackground
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

import { dataPrices } from '../../../Domain'

class Payment extends Component {

  constructor(props){
    super(props)

    this.state = {
      prices:[],
      select:null
    }

  }

  componentDidMount(){

    this.setState({
      prices:dataPrices
    })

    this.getPayments()

  }

  getPayments(){
    let { dispatch } = this.props
    return Api.merchant.payment.payments(dispatch) 
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  checkPrice(index, price){

    let { prices } = this.state

    return this.setState({
      prices:prices.map((t, i)=>{
        if(i === index){
          return {
            ...t,
            status:true
          }
        }else{
          return {
            ...t,
            status:false
          }
        }
      }),
      select:price
    })
  }

  createPayment(){

    let { select } = this.state
    let { dispatch, navigation } = this.props

    return Api.merchant.payment.createPayment(dispatch, select, navigation)

  }

  render(){

    let { user } = this.props.user
    let { statusRequest } = this.props.merchant
    let { prices } = this.state
    let { list } = this.props.payments

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Пополнить баланс'}/>

          <View style={styles.merchant.payment.viewContent}>
            
            <Text style={styles.merchant.payment.headerTitlePayment}>Выберите сумму</Text>

            <ScrollView showsVerticalScrollIndicator={false}>
              {
                prices.map((value, index)=>{
                  return (<TouchableOpacity onPress={this.checkPrice.bind(this, index, value)} style={styles.merchant.payment.buttonView} key={index}>
                    <View>
                      {
                        value.status ?
                          <MaterialCommunityIcons name="checkbox-marked" size={28} color="#466dfa" />
                        :
                          <MaterialCommunityIcons name="checkbox-blank-outline" size={28} color="#466dfa" />
                      }
                    </View>
                    <View>
                      <Text style={styles.merchant.payment.buttonViewTitle}>{ value.price }</Text>
                    </View>
                  </TouchableOpacity>)
                })
              }

              {
                statusRequest ?
                  <TouchableOpacity style={styles.merchant.payment.buttonPayment}>
                    <Text style={styles.merchant.payment.buttonPaymentTitle}>Подождите</Text>
                  </TouchableOpacity>
                :
                  <TouchableOpacity onPress={this.createPayment.bind(this)} style={styles.merchant.payment.buttonPayment}>
                    <Text style={styles.merchant.payment.buttonPaymentTitle}>пополнить</Text>
                  </TouchableOpacity>
              }

              <View style={styles.merchant.payment.lineDevider}></View>

              <View style={styles.merchant.payment.viewHistoryPayments}>
                <Text>Последние 10 платежей</Text>
                <View style={styles.merchant.payment.viewHistoryPaymentsContent}>
                  {
                    list ?
                      list.map((price, index)=>{

                        index = index + 1

                        return (<View style={styles.merchant.payment.viewHistoryPaymentsItem} key={index}>

                          <View style={styles.merchant.payment.viewHistoryPaymentsItemLeft}>
                            <Text style={styles.merchant.payment.viewHistoryPaymentsItemLeftTitleIndex}>{ index }. </Text>
                            <Text style={styles.merchant.payment.viewHistoryPaymentsItemLeftTitleSum}>{ price.sum } тг.</Text>
                            <Text>{ price.code }</Text>
                          </View>
                          
                          <View style={styles.merchant.payment.viewHistoryPaymentsItemRight}>
                            {
                              price.status ?
                                <Text>Оплачено</Text>
                              :
                                <Text>Не Оплачено</Text>
                            }
                          </View>

                        </View>)
                      })
                    :
                      <Text>Не найдено</Text>
                  }
                </View>
              </View>

            </ScrollView>

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant,
    payments:state.payments
  }
}

export default connect(mapStateToProps)(Payment)