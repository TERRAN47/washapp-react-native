const initialState = {
	list:null
}

const payments = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_LIST_PAYMENTS_USER' :
			return {
				...state,
				list:action.list
			}

		case 'UPDATE_ITEM_PAYMENTS_USER' :
			return {
				...state,
				list:[action.item, ...state.list]
			}

		default :
			return state
	}

}

export default payments