import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  ImageBackground
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class PaymentSuccess extends Component {

  constructor(props){
    super(props)

    this.state = {

    }

  }

  render(){

    let { user } = this.props.user
    let { statusRequest } = this.props.merchant
    let { code } = this.props.navigation.state.params

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Пополнить баланс'}/>

          <View style={styles.merchant.payment.viewContent}>
            
            <Text style={styles.merchant.payment.headerTitlePayment}>Введите этот код в терминале для пополнения баланса</Text>

            <TouchableOpacity style={styles.merchant.payment.codeStyle}>
              <Text style={styles.merchant.payment.codeStyleTitle}>{ code }</Text>
            </TouchableOpacity>

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant
  }
}

export default connect(mapStateToProps)(PaymentSuccess)