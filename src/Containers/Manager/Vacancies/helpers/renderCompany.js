import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { renderDistance } from 'Services/Utils'

class RenderCompany extends Component {

	constructor(props){
		super(props)

		this.renderTime = this.renderTime.bind(this)
	}

	goToScreen(){

		let { item } = this.props

		const { navigate } = this.props.navigation

		return navigate('viewVacancies', {
			vacancies:item.vacancies,
			item
		})
	}

	renderTime(time){
		return moment(time).fromNow()
	}

	render(){

		let { item } = this.props

		return(
			<TouchableOpacity style={styles.merchant.lists.buttonMainCompany} onPress={this.goToScreen.bind(this)}>

				<View style={styles.merchant.lists.buttonMainCompanyStatus}>
					{
						item.status.active ? <View style={styles.merchant.lists.buttonMainCompanyStatusActive}><Text style={styles.merchant.lists.buttonMainCompanyStatusText}>Активно</Text></View> : <View style={styles.merchant.lists.buttonMainCompanyStatusInactive}><Text style={styles.merchant.lists.buttonMainCompanyStatusText}>Не активно</Text></View>
					}
				</View>

				<View style={styles.merchant.lists.companyImage}>
					<Image style={styles.merchant.lists.companyLogoStyle} source={{uri:`${config.public_url}${item.logo}`}} />
				</View>
				<View style={styles.merchant.lists.companyInfo}>

					<Text style={styles.merchant.lists.companyInfoTitle}>{ item.title }</Text>

					<Text style={styles.merchant.lists.companyInfoAdress}>{ item.adress }</Text>

					{
						item.distance ? 
							<View style={styles.merchant.lists.ViewSectionInfoItem}>
								<MaterialCommunityIcons name="map-marker" size={18} color="rgba(0,0,0,0.45)" />
								<Text style={styles.merchant.lists.ViewSectionInfoItemTextLocation}>{ renderDistance(item.distance) }</Text>
							</View>
						: 
							<View></View>
					}

					<View style={styles.merchant.lists.ViewSectionInfoItem}>
						<MaterialCommunityIcons name="timer" size={18} color="rgba(0,0,0,0.45)" />
						<Text style={styles.merchant.lists.ViewSectionInfoItemText}>создано { this.renderTime(item.createdAt) }</Text>
					</View>

				</View>
			</TouchableOpacity>
		)
	}
}

export default RenderCompany