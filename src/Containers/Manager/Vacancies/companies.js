import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  Alert,
  ImageBackground
} from 'react-native'
import _ from 'underscore'
import DatePicker from 'react-native-datepicker'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class CompaniesForVacancies extends Component {

  constructor(props){
    super(props)

    this.state = {
      //companies
      companies:null
    }
  }

  componentDidMount(){

    this.getCompanies()

  }

  async getCompanies(){

    const { dispatch } = this.props

    let {
      status,
      message,
      data
    } = await Api.merchant.carwash.getCompaniesForVacancions(dispatch)

    if(status){

      return this.setState({
        companies:data
      })

    }else{

      return Alert.alert('Сообщение', message)

    }

  }

  getCompany(carwash){

    this.props.navigation.state.params.getCompany(carwash)

    return this.props.navigation.goBack()

  }

  render(){

    let { statusRequest } = this.props.merchant
    let { companies } = this.state

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Выберите автомойку'}/>

          <View style={{flex:1,padding:10}}>

            {
              statusRequest ?
                <Loader title="Загрузка" />
              :
                <ScrollView>

                  {
                    companies ?
                      companies.map((carwash, index)=>{

                        return (<TouchableOpacity style={styles.merchant.cabinetMerchant.companyButtonVacancy} onPress={this.getCompany.bind(this, carwash)} key={index}>
                          <Text style={styles.merchant.cabinetMerchant.sectionViewFormVacancyContentTitle}>{ carwash.title }</Text>
                          <Text>г. { carwash.city.title }, адрес: { carwash.adress }</Text>
                        </TouchableOpacity>)

                      })
                    : <View></View>
                  }           

                </ScrollView> 
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant
  }
}

export default connect(mapStateToProps)(CompaniesForVacancies)