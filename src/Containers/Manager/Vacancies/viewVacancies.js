import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Linking,
  ImageBackground
} from 'react-native'
import config from 'Config'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome5'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import PhoneModal from 'Components/Modal/phonesModal'

class ViewVacancies extends Component {

  constructor(props){
    super(props)

    this.state = {
      vacancies:null,
      phoneStatus:false
    }
  }

  componentDidMount(){

    let { vacancies } = this.props.navigation.state.params

    this.setState({
      vacancies
    })

  }

  callCompany(phones){
    if(phones.length > 1){
      this.setState({phoneStatus:true, phones})
    }else{
      return Linking.openURL(`tel:+7${phones[0]}`)
    }
  }

  updateVacancy(fields, index){

    let { vacancies } = this.state

    vacancies[index] = {...fields}

    return this.setState({
      vacancies
    })

  }

  removeVacancy(indexRemove){

    let { vacancies } = this.state
    let { goBack } = this.props.navigation

    if(vacancies.length == 1){

      return goBack()

    }else{

      let newVacancies = []

      vacancies.map((vac, index)=>{

        if(index != indexRemove){
          newVacancies = [...newVacancies, vac]
        }

      })

      return this.setState({
        vacancies:newVacancies
      })

    }

    return this.setState({
      vacancies
    })

  }

  openVacancy(vacancy, item, index){

    let { navigate } = this.props.navigation
    return navigate('viewAndEditVacancy', {
      vacancy,
      item,
      index,
      updateVacancy: this.updateVacancy.bind(this),
      removeVacancy:this.removeVacancy.bind(this),
    })

  }
  closeModalPhone(){
    this.setState({phoneStatus:false})
  }
  render(){

    let { item } = this.props.navigation.state.params
    let { vacancies, phoneStatus, phones } = this.state

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header hideBackground={true} navigation={this.props.navigation} isBack={true} title={'Вакансии'}/>

          <View style={{flex:1}}>
          {
            phoneStatus &&
            <PhoneModal closeModalPhone={this.closeModalPhone.bind(this)} phoneStatus={phoneStatus} phones={phones} />
          }
            {
              vacancies ?

                <ScrollView showsVerticalScrollIndicator={false}>

                  <View style={styles.merchant.cabinetMerchant.imageCompanyView}>

                    <Image source={{uri:`${config.public_url}${item.logo}`}} style={styles.merchant.cabinetMerchant.imageCompanyForVacancy} />

                    <View style={styles.merchant.cabinetMerchant.imageCompanyViewInformation}>

                        <View style={styles.merchant.cabinetMerchant.imageCompanyViewInformationBottomLine}>

                          <View style={styles.merchant.cabinetMerchant.imageCompanyViewInformationBottomLineLeft}>
                            <MaterialCommunityIcons name="format-list-bulleted" size={27} color="#ffda20" />
                            <Text style={styles.merchant.cabinetMerchant.imageCompanyViewInformationBottomLineLeftTitle}>ВАКАНСИИ </Text>
                            <Text style={styles.merchant.cabinetMerchant.imageCompanyViewInformationBottomLineLeftTitleName}>{ vacancies.length }</Text>
                          </View>

                          <TouchableOpacity onPress={this.callCompany.bind(this, item.phones)} style={styles.merchant.cabinetMerchant.imageCompanyViewInformationBottomLineRight}>
                            <Text style={styles.merchant.cabinetMerchant.imageCompanyViewInformationBottomLineRightTitle}>Позвонить</Text>
                            <MaterialCommunityIcons name="phone-in-talk" size={27} color="#666666" />
                          </TouchableOpacity>

                        </View>

                        <MaterialCommunityIcons style={styles.merchant.cabinetMerchant.imageCompanyViewInformationTitle} name="map-marker" size={38} color="rgba(255,255,255,1)" />
                        <Text style={styles.merchant.cabinetMerchant.imageCompanyViewInformationTitle}>г. { item.city.title }</Text>
                        <Text style={styles.merchant.cabinetMerchant.imageCompanyViewInformationTitle}>{ item.adress }</Text>
                    </View>

                  </View>

                  <View style={styles.merchant.cabinetMerchant.companyViewContent}>

                    {
                      vacancies.map((vacancy, index)=>{
                        return (<TouchableOpacity key={index} onPress={this.openVacancy.bind(this, vacancy, item, index)} style={styles.merchant.cabinetMerchant.companyViewContentItem}>

                          <View style={styles.merchant.cabinetMerchant.companyViewContentItemLeft}>

                            <Text style={styles.merchant.cabinetMerchant.companyViewContentItemLeftTitle}>{ vacancy.title }</Text>

                            <Text style={styles.merchant.cabinetMerchant.companyViewContentItemLeftTitleSection}>ОБЯЗАННОСТИ</Text>
                            <Text style={styles.merchant.cabinetMerchant.companyViewContentItemLeftTitleName}>{ vacancy.charge }</Text>

                            <Text style={styles.merchant.cabinetMerchant.companyViewContentItemLeftTitleSection}>ТРЕБОВАНИЯ</Text>
                            <Text style={styles.merchant.cabinetMerchant.companyViewContentItemLeftTitleName}>{ vacancy.requirements }</Text>

                          </View>

                          <View style={styles.merchant.cabinetMerchant.companyViewContentItemRight}>
                            
                            <View style={styles.merchant.cabinetMerchant.companyViewContentItemRightSection}>
                              <MaterialCommunityIcons style={styles.merchant.cabinetMerchant.companyViewContentItemRightSectionIcon} name="timer" size={30} color="#ffda20" />
                              <View style={styles.merchant.cabinetMerchant.companyViewContentItemRightSectionInfo}>
                                <Text style={styles.merchant.cabinetMerchant.companyViewContentItemRightSectionTitle}>{ vacancy.work_mode.start }</Text>
                                <Text style={styles.merchant.cabinetMerchant.companyViewContentItemRightSectionTitle}>{ vacancy.work_mode.end }</Text>
                              </View>
                            </View>

                            <View style={styles.merchant.cabinetMerchant.companyViewContentItemRightSection}>
                              <FontAwesome style={[styles.merchant.cabinetMerchant.companyViewContentItemRightSectionIcon, {
                                position:'relative',
                                left:vacancy.salary_status ? 0 : 5
                              }]} name={ vacancy.salary_status ? "coins" : "percentage"} size={30} color="#ffda20" />
                              <View style={styles.merchant.cabinetMerchant.companyViewContentItemRightSectionInfo}>
                                <Text style={styles.merchant.cabinetMerchant.companyViewContentItemRightSectionTitle}>{ vacancy.salary }</Text>
                              </View>
                            </View>

                          </View>
                          
                        </TouchableOpacity>)
                      })
                    }

                  </View>
                  
                </ScrollView>  
              :
                <Text>Вакансий нет</Text>
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant,
    vacancies:state.vacancies.list
  }
}

export default connect(mapStateToProps)(ViewVacancies)