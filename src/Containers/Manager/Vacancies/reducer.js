const initialState = {
	list:null
}

const vacancies = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_LIST_VACANCIES' :

			return {
				...state,
				list:action.data
			}

		case 'ADD_VACANCY' :

			let newVacancios = state.list && state.list.length > 0 ? [...state.list, action.vacancy] : [action.vacancy]

			return {
				...state,
				list:newVacancios
			}

		case 'UPDATE_VACANCY' :

			return {
				...state
			}

		default :
			return state
	}

}

export default vacancies 