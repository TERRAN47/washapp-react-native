import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Header, NotFound } from 'Composition'
import _ from 'underscore'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput
} from 'react-native'
import config from 'Config'
import styles from 'Services/Styles'
import Api from 'Services/Api'

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu'

class ChangePrice extends Component {

  constructor(props){
    super(props)

    this.state = {
      status:false,
      indexCar:0,
      price:[],
      bodyCarsState:[]
    }
  }

  componentDidMount(){

    let { price } = this.props.navigation.state.params
    let { bodyCars } = this.props.companyPrice
    bodyCars = _.where(bodyCars, {
      status:true
    })

    this.setState({
      bodyCarsState:bodyCars,
      status:true
    })

  }

  changePrice(value, index){

    let { indexCar, bodyCarsState } = this.state
    let { dispatch } = this.props

    bodyCarsState[indexCar].status = true
    bodyCarsState[indexCar].prices[index].price = value

    return this.setState({
      bodyCarsState
    })

  }

  savePrice(){
    let { id } = this.props.navigation.state.params
    let { bodyCarsState } = this.state
    let { bodyCars } = this.props.companyPrice
    let { dispatch } = this.props

    return Api.merchant.carwash.savePrices(dispatch, id, bodyCarsState, bodyCars)

  }

  render(){

    let { indexCar, status } = this.state
    let { bodyCarsState } = this.state
    let { statusRequest } = this.props
    let photoCar = status ? config.public_url+bodyCarsState[indexCar].photo : ''

    return(
      <View style={{flex:1}}>
        <Header navigation={this.props.navigation} isBack={true}  title='Прайс'/>

        <View style={styles.merchant.lists.mainViewFormCar}>
          <View style={{flex:1,position:'relative'}}>
            <Menu>
              <MenuTrigger style={styles.merchant.lists.viewButtonSelectCar}>
                <View style={styles.merchant.lists.buttonSelectCar}>
                  <Image style={styles.merchant.lists.buttonSelectCarImage} source={{uri:photoCar}} />
                  <Text style={styles.merchant.lists.buttonSelectCarTitle}>{ status ? bodyCarsState[indexCar].title : '' }</Text>
                </View>
              </MenuTrigger>

              <MenuOptions customStyles={{
                optionsContainer:{width:'86%'},
                optionText:styles.merchant.lists.viewFormSelectListText
              }} style={styles.merchant.lists.viewFormSelectList}>

                {
                  bodyCarsState.map((car, index)=>{
                    return (<MenuOption 
                      key={index} 
                      style={styles.merchant.lists.viewFormSelectListContent} 
                      onSelect={() => { this.setState({indexCar:index}) }} 
                      text={car.title} 
                    />)
                  })
                }
              </MenuOptions>
            </Menu>

            <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
              <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss}}>
                <View style={styles.merchant.lists.mainViewParrentScroll}>
                {
                  status ?
                    bodyCarsState[indexCar].prices.map((price, index)=>{
                      return (
                        <View key={index} style={styles.merchant.lists.formPriceSection}>
                          <View style={styles.merchant.lists.formPriceSectionLeftTitle}>
                            <Text style={styles.merchant.lists.formPriceSectionTitle}>{price.title}</Text>
                          </View>
                          <View style={styles.merchant.lists.formPriceSectionRight}>
                            <TextInput 
                              defaultValue={price.price > 0 ? price.price : ''}
                              selectionColor="#fff"
                              keyboardType='numeric' 
                              underlineColorAndroid='transparent' 
                              style={styles.merchant.lists.formPriceSectionRightInput} 
                              onChangeText={(value)=>this.changePrice(value, index)} 
                            />
                            <Text style={styles.merchant.lists.formPriceSectionRightPrice}>{ price.currency }</Text>
                          </View>
                        </View>
                      )
                    })
                  : <View></View>
                }
                </View>
              </TouchableWithoutFeedback>
            </ScrollView>
            
            {
              statusRequest ? 
                <View style={styles.merchant.lists.buttonPriceSaveView}>
                  <TouchableOpacity style={styles.merchant.lists.buttonPriceSave}>
                    <Text style={styles.merchant.lists.buttonPriceSaveTitle}>Сохраняем...</Text>
                  </TouchableOpacity>
                </View>
              : 
                <View style={styles.merchant.lists.buttonPriceSaveView}>
                  <TouchableOpacity onPress={this.savePrice.bind(this)} style={styles.merchant.lists.buttonPriceSave}>
                    <Text style={styles.merchant.lists.buttonPriceSaveTitle}>Сохранить</Text>
                  </TouchableOpacity>
                </View>
            }
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    statusRequest:state.merchant.statusRequest,
    companyPrice:state.companyPrice,
  }
}

export default connect(mapStateToProps)(ChangePrice)