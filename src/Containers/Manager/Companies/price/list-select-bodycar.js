import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Header, NotFound, Loader } from 'Composition'
import {
  View,
  Text,
  TouchableOpacity,
  Switch,
  Image,
  ScrollView,
  Alert
} from 'react-native'
import _ from 'underscore'
import config from 'Config'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class ListSelectBodyCar extends Component {

  constructor(props){
    super(props)

    this.state = {
      bodyCars:[]
    }
  }

  componentDidMount(){

    this.getBodyCars()

  }

  getBodyCars(){

    let { id } = this.props.navigation.state.params
    const { dispatch } = this.props
    return Api.merchant.main.getBodyCars(dispatch, id)

  }

  goToSreen(screen){

    let { price, id } = this.props.navigation.state.params

    let searchPrices = _.where(price, {
      status:true
    })

    if(price.length > 0 && searchPrices.length > 0){

      const { navigate } = this.props.navigation
      return navigate(screen, {
        price,
        id
      })

    }else{
      return Alert.alert('Сообщение', 'Нужно выбрать несколько опций')
    }

  }

  changeValueSwitch(value, car, index){

    let { bodyCars } = this.props.companyPrice
    let { price } = this.props.navigation.state.params
    const { dispatch } = this.props

    bodyCars[index].title = car.title
    bodyCars[index].photo = car.photo
    bodyCars[index].status = value

    this.props.navigation.state.params.price = bodyCars

    return Api.merchant.main.changeListBodyCars(dispatch, bodyCars, car, value, index)

  }

  render(){

    let { bodyCars } = this.props.companyPrice
    const { statusRequest } = this.props
    let { price } = this.props.navigation.state.params

    return(
      <View style={{flex:1}}>

        <Header navigation={this.props.navigation} isBack={true}  title='Прайс'/>

        <View style={{flex:1}}>
            {
              statusRequest ?
                <Loader title="Загрузка" />
              : bodyCars ?
                <View style={{flex:1, position:'relative'}}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.merchant.lists.mainListCheck}>

                      {
                        bodyCars.map((car, index)=>{
                          return(<View key={index} style={styles.merchant.lists.buttonCheckView}>
                            <View style={styles.merchant.lists.buttonCheckViewLeft}>
                              <Image style={styles.merchant.lists.buttonCheckViewPhoto} source={{uri:`${config.public_url}${car.photo}`}} />
                              <View style={styles.merchant.lists.buttonCheckViewText}>
                                <Text style={styles.merchant.lists.buttonCheckViewTitle}>{ car.title }</Text>
                              </View>
                            </View>
                            <View style={styles.merchant.lists.buttonCheckViewRight}>
                              <Switch value={car.status} onValueChange={(value)=>{this.changeValueSwitch(value, car, index)}}/>
                            </View>
                          </View>)
                        })                  
                      }

                    </View>
                  </ScrollView>

                  <View style={styles.merchant.lists.viewNextScreen}>
                    <TouchableOpacity style={styles.merchant.lists.buttonNextScreen} onPress={this.goToSreen.bind(this, 'ChangePrice')}>
                      <Text style={styles.merchant.lists.buttonNextScreenText}>Далее</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              : <Text>Ничего не найдено</Text>
            }
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    companyPrice:state.companyPrice,
    statusRequest:state.merchant.statusRequest,
    companies:state.companies.companies
  }
}

export default connect(mapStateToProps)(ListSelectBodyCar)