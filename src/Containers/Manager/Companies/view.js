import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
	View,
	Text,
	Image,
	Linking,
	TouchableOpacity,
	ScrollView,
	Alert,
	Platform
} from 'react-native'
import call from 'react-native-phone-call'
import _ from 'underscore'
import { Header, NotFound } from 'Composition'
import styles from 'Services/Styles'
import config from 'Config'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Api from 'Services/Api'
import PhoneModal from 'Components/Modal/phonesModal'
const iconSise = 26

class CompanyView extends Component {

	constructor(props){
		super(props)

		this.state = {
			item:null,
			phoneStatus:false,
			status_active:false
		}
	}

  	callCompany(phones){
  		if(phones.length > 1){
  			this.setState({phoneStatus:true, phones})
  		}else{

  			if(Platform.OS === 'android'){

  				return Linking.openURL(`tel:+7${phones[0]}`)

  			}else{

				const args = {
				  number: `+7${phones[0]}`
				}
				 
				return call(args).catch(console.error)
  			}
  		}
  	}

	editCompany(screen){

		let { item, companyIndex } = this.props.navigation.state.params
		const { navigate } = this.props.navigation

		return navigate(screen, {
			item,
			id:item.id,
		})

	}

	goToScreen(screen){

		let { id, companyIndex } = this.props.navigation.state.params
		let { companies } = this.props
		let item = _.where(companies, {
			id
		})[0]

		const { navigate } = this.props.navigation
		return navigate(screen, {
			price:item.price,
			item,
			bonuses:item.bonus,
			administrators:item.administrators,
			companyIndex,
			merchant_id:item.user_id,
			id
		})
	}

	componentDidMount(){
		let { id } = this.props.navigation.state.params
		let { companies } = this.props
/*		let item = _.where(companies, {
			id
		})[0]	

		this.setState({
			item
		})*/
	}

	activateCompany(){
		let { id, companyIndex } = this.props.navigation.state.params
		const { dispatch } = this.props

		this.setState({
			status_active:true
		})

		return Api.merchant.carwash.activateCompany(dispatch, id, companyIndex)
	}

	activatedCompanyStatus(){
		return Alert.alert('Сообщение','Попробуйте позже')
	}

	activatedCompany(){
		return Alert.alert('Сообщение', 'Автомойка активирована')
	}
  	closeModalPhone(){
    	this.setState({phoneStatus:false})
 	} 
	render(){
		let { id, companyIndex } = this.props.navigation.state.params
		let { companies } = this.props
		let item = companies[companyIndex]
		const { statusRequest } = this.props.merchant
		let { status_active, phoneStatus, phones } = this.state

		return(
			<View style={styles.merchant.main.viewBack}>
		        {
		          phoneStatus &&
		          <PhoneModal closeModalPhone={this.closeModalPhone.bind(this)} phoneStatus={phoneStatus} phones={phones} />
		        }

		        <Header navigation={this.props.navigation} isBack={true} title="Автомойка" />
				{
					<View style={styles.merchant.main.maincontentWithWhiteBackground}>
						<View style={styles.merchant.main.headerSection}>
							<Image style={styles.merchant.main.logoHeader} source={{uri:`${config.public_url}${item.logo}`}}/>
							<View style={styles.merchant.main.shadowViewBG}></View>
							<Text style={styles.merchant.main.headerSectionNameCompany}>{ item.title }</Text>	
						</View>

						<ScrollView>

							<View style={styles.merchant.main.viewContentCompany} >

								<View style={styles.merchant.company.sectionInfo}>

									<View style={styles.merchant.company.sectionSettingInformationHead}>
										<Text style={styles.merchant.company.sectionSettingInformationHeadText}>Информация</Text>
									</View>
									
									<View style={styles.merchant.company.sectionSetting}>
										<MaterialCommunityIcons name="city" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										<Text style={styles.merchant.company.textSetting}>{ item.city.title }</Text>
									</View>

									<View style={styles.merchant.company.sectionSetting}>
										<MaterialCommunityIcons name="map-marker" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										<Text style={styles.merchant.company.textSetting}>{ item.adress }</Text>
									</View>

									{
										item.phones.map((el, index)=>{
											return(
												<View key={index} style={styles.merchant.company.sectionSetting}>
													<TouchableOpacity style={styles.merchant.company.sectionSetting} onPress={this.callCompany.bind(this, item.phones)}>
														<MaterialCommunityIcons name="phone" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
														<Text style={styles.merchant.company.textSetting}>+7{el}</Text>
													</TouchableOpacity>
												</View>
											)
										})
									}

									<View style={styles.merchant.company.sectionSetting}>
										<MaterialCommunityIcons name="clock-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										<Text style={styles.merchant.company.textSetting}>{ item.work_time.dayNight ? 'Круглосуточно' : `${item.work_time.start} - ${item.work_time.end}` }</Text>
									</View>

								</View>

								{/*<TouchableOpacity onPress={this.goToScreen.bind(this, 'ListSelectBodyCar')} style={styles.merchant.company.otherSettingsView}>
								
																	<View style={styles.merchant.company.otherSettingsViewLeft}>
																		<MaterialCommunityIcons name="format-list-bulleted" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
																		<Text style={styles.merchant.company.textSetting}>Прайс</Text>	
																	</View>
								
																	
																	<MaterialCommunityIcons name="square-edit-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
																	
																</TouchableOpacity>*/}

								{/*<TouchableOpacity onPress={this.goToScreen.bind(this, 'boxes')} style={styles.merchant.company.otherSettingsView}>
								
																	<View style={styles.merchant.company.otherSettingsViewLeft}>
																		<MaterialCommunityIcons name="wallet" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
																		<Text style={styles.merchant.company.textSetting}>Боксы</Text>	
																	</View>
								
																	<MaterialCommunityIcons name="square-edit-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
																
																</TouchableOpacity>

								<TouchableOpacity onPress={this.goToScreen.bind(this, 'additionallyScreen')} style={styles.merchant.company.otherSettingsView}>

									<View style={styles.merchant.company.otherSettingsViewLeft}>
										<MaterialCommunityIcons name="dots-horizontal" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										<Text style={styles.merchant.company.textSetting}>Дополнительно</Text>	
									</View>

									<MaterialCommunityIcons name="square-edit-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
								
								</TouchableOpacity>

								{/*<TouchableOpacity onPress={this.goToScreen.bind(this, 'administrators')} style={styles.merchant.company.otherSettingsView}>
								
																	<View style={styles.merchant.company.otherSettingsViewLeft}>
																		<MaterialCommunityIcons name="cogs" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
																		<Text style={styles.merchant.company.textSetting}>Администраторы</Text>	
																	</View>
								
																	<MaterialCommunityIcons name="square-edit-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
																</TouchableOpacity>

								<TouchableOpacity onPress={this.goToScreen.bind(this, 'bonuses')} style={styles.merchant.company.otherSettingsView}>

									<View style={styles.merchant.company.otherSettingsViewLeft}>
										<MaterialCommunityIcons name="gift" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										<Text style={styles.merchant.company.textSetting}>Бонусы</Text>	
									</View>

									<MaterialCommunityIcons name="square-edit-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />										
								</TouchableOpacity>*/}

								{
									item.status.active ?
										<TouchableOpacity onPress={this.goToScreen.bind(this, 'queue')} style={styles.merchant.company.otherSettingsView}>

											<View style={styles.merchant.company.otherSettingsViewLeft}>
												<MaterialCommunityIcons name="clipboard-arrow-down" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
												<Text style={styles.merchant.company.textSetting}>Управление автомойкой</Text>	
											</View>

											<MaterialCommunityIcons name="square-edit-outline" size={iconSise} style={styles.merchant.company.iconSetting} color="#5b5b5b" />
										</TouchableOpacity>
									:
										<View></View>

								}
							</View>
						</ScrollView>
					</View>

				}

			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		user:state.user.user,
		companies:state.companies.companies,
		merchant:state.merchant,
		statusRequest:state.merchant.statusRequest
	}
}

export default connect(mapStateToProps)(CompanyView)