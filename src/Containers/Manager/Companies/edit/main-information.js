import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
	View,
	Text,
	TouchableOpacity,
	TouchableWithoutFeedback,
	Keyboard,
	Alert,
	Switch,
	ScrollView,
	Image,
	TextInput
} from 'react-native'
import config from 'Config'
import _ from 'underscore'
import { TextInputMask } from 'react-native-masked-text'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { Header, NotFound } from 'Composition'
import styles from 'Services/Styles'
import DocumentPicker from 'react-native-document-picker'
import DatePicker from 'react-native-datepicker'
import moment from 'Services/Time'
import Api from 'Services/Api'

class MainEditWashCar extends Component {

	constructor(props){
		super(props)

		this.state = {
			logo:null,
			uploadedLogo:'',
			createdCity:null,
			title:'',
			description:'',
			adress:'',
			newPhone:'7',
			clearPhone:'',
			city:null,
			phones:null,
			time:{
				start:'',
				end:'',
				dayNight:false
			}
		}
	}

	componentDidMount(){

		let { id } = this.props.navigation.state.params

		this.updateItem(id)

	}

	updateItem(id){

		let { dispatch, companies } = this.props

		let item = _.where(companies, {
			id
		})[0]

		this.setState({
			uploadedLogo:item.logo,
			title:item.title,
			city:item.city,
			phones:item.phones,
			description:item.description,
			adress:item.adress,
			createdCity:item.city,
			time:{
				start:item.work_time.start,
				end:item.work_time.end,
				dayNight:item.work_time.dayNight
			}
		})

		Api.auth.saveCityForCompanyEdit(dispatch, item.city)
		return Api.maps.saveCoordsForEditCompany(dispatch, item.location.coordinates[0], item.location.coordinates[1])
	}

	async selectLogo(){

	    let that = this
	    let { logo } = this.state

	    let response = await DocumentPicker.pick()
	    let fileSize = response.size/1000000

	    let typeImage = response.type

	    if(typeImage == "image/jpeg" && fileSize < 1 || typeImage == "image/png" && fileSize < 1){

	      return that.setState({
	        logo:response
	      })

	    }else{
	      return Alert.alert('сообщение', 'Достопны только фотографии меньше 1мб')
	    }

	}

	getCity(){

	    const { navigate } = this.props.navigation
	    return navigate('location')

	}

	goToScreen(screen){
		let {navigate} = this.props.navigation
		return navigate(screen)
	}

	updateCompany(){

		let { id } = this.props.navigation.state.params
		const { selectCoords, selectCoordStatus } = this.props.maps
		const { navigation, dispatch } = this.props
		const { user } = this.props.user
		const { city_select } = this.props.locations
		const { userGeo } = this.props.maps

		let data = {
			selectCoords,
			selectCoordStatus,
			city_select,
			user,
			company_id:id,
			...this.state
		}

		return Api.merchant.carwash.updateCompany(dispatch, navigation, data, userGeo.latitude, userGeo.longitude)
	}
	addPhone(){
		let {newPhone, phones} = this.state
		if(newPhone != ''){
			phones.push(newPhone)
			this.inputPhone.clear()
			this.setState({phones, newPhone:'7'})			
		}else{
			alert("Заполните новый номер телефона")
		}
	}
	deletePhone(phone){
		let {phones} = this.state
		phones = _.without(phones, phone)
		this.setState({phones})
	}
	render(){

		let {
			logo,
			time,
			uploadedLogo,
			title,
			phones,
			description,
			adress,
		} = this.state

		const { statusRequest } = this.props.merchant
		let { city_select } = this.props.locations
		let { selectCoordStatus } = this.props.maps

		return(
			<View style={styles.merchant.main.viewWhite}>
				<Header navigation={this.props.navigation} isBack={true} title="Редактирование автомойки" />
				
				<ScrollView
					keyboardShouldPersistTaps="always"
				>
					<TouchableWithoutFeedback  onPress={()=>{Keyboard.dismiss}}>
						<View>
							<View style={styles.merchant.forms.sectionFormCenter}>
								<Text style={styles.merchant.forms.title}>Логотип автомойки</Text>
								<TouchableOpacity style={styles.merchant.forms.buttonSelect} onPress={this.selectLogo.bind(this)}>
									{
										logo ?
											<Image style={styles.merchant.forms.logoCompany} source={{uri:logo.uri}} />
										: uploadedLogo ?
											<Image style={styles.merchant.forms.logoCompany} source={{uri:`${config.public_url}${uploadedLogo}`}} />
										:
											<MaterialCommunityIcons name="image" size={85} color="#fff" />
									}
								</TouchableOpacity>
							</View>

							<View style={styles.merchant.forms.sectionForm}>
								<Text style={styles.merchant.forms.title}>Название автомойки</Text>

								<View style={styles.merchant.forms.inputFormView}>
									<TextInput value={title} underlineColorAndroid='rgba(0,0,0,0)' style={styles.merchant.forms.inputText} onChangeText={(value)=>{this.setState({title:value})}} placeholder="Название автомойки" editable={true} maxLength={40}/>
					            </View>

							</View>

							<View style={styles.merchant.forms.sectionForm}>
								<Text style={styles.merchant.forms.title}>Краткое описание</Text>
								<View style={styles.merchant.forms.inputFormView}>
									<TextInput value={description} underlineColorAndroid='rgba(0,0,0,0)' style={styles.merchant.forms.inputText} onChangeText={(value)=>{this.setState({description:value})}} multiline={true} numberOfLines={2} placeholder="Краткое описание" editable={true} />
								</View>
							</View>

							<View style={styles.merchant.forms.sectionForm}>

								<Text style={styles.merchant.forms.title}>Адрес автомойки</Text>

								<View style={styles.merchant.forms.inputFormView}>
									<TextInput value={adress} underlineColorAndroid='rgba(0,0,0,0)' style={styles.merchant.forms.inputText} onChangeText={(value)=>{this.setState({adress:value})}} placeholder="Адрес автомойки" editable={true} />
					      </View>

							</View>

							<View style={styles.merchant.forms.sectionForm}>
							
								<Text style={styles.merchant.forms.title}>Отметить на карте</Text>

								{
									selectCoordStatus ? 
										<TouchableOpacity style={styles.merchant.forms.inputFormView} onPress={this.goToScreen.bind(this, 'maps')}>
											<Text style={styles.merchant.forms.inputFormtitle}>Локация выбрана, если хотите изменить локацию, нажмите еще раз</Text>
										</TouchableOpacity>
									:
										<TouchableOpacity style={styles.merchant.forms.inputFormView} onPress={this.goToScreen.bind(this, 'maps')}>
											<Text style={styles.merchant.forms.inputFormtitle}>Открыть карту</Text>
										</TouchableOpacity>
								}

							</View>
                <View style={styles.merchant.forms.sectionForm}>
                  <Text style={styles.merchant.forms.title}>Выберите город</Text>
                  <TouchableOpacity style={styles.merchant.forms.inputFormView} onPress={this.getCity.bind(this)}>
                      {
                        city_select ?
                          <Text style={styles.AuthScreen.inputFormViewCitytitle}>{ city_select.title }</Text>
                        :
                          <Text style={styles.AuthScreen.inputFormViewCitytitle}>Выбрать город</Text>
                      }
                    </TouchableOpacity>
                </View>
							<View style={styles.merchant.forms.sectionForm}>
								<Text style={styles.merchant.forms.title}>Телефон автомойки</Text>	            
				              	{
				              		phones && phones.map((el, index)=>{
				              			return(
				              				<View  key={index} style={styles.merchant.forms.inputFormView}>
								              	<TextInputMask
                                  type={'cel-phone'}
                                  options={{
                                  maskType: "BRL",
                                  withDDD: true,
                                  dddMask: '+9 (999) 999 99 99'
                                  }}
                                  maxLength={18}
                                  value={phones[index]}
									                placeholder="Номер телефона"
									                underlineColorAndroid='transparent'
									                style={styles.AuthScreen.inputForm}
									                refInput={ref => { this.input = ref }}
									                onChangeText={(phone) => {
									                	phones[index] = phone
									                }}
								              	/>
                                <TouchableOpacity onPress={this.deletePhone.bind(this, el)}>
                                  <FontAwesome name="close" size={35} color="red" />
                                </TouchableOpacity> 
				              				</View>
				              			)
				              		})
				              	}
				              	<View style={styles.merchant.forms.inputFormView}>
                        <TextInputMask
                          type={'cel-phone'}
                          options={{
                          maskType: "BRL",
                          withDDD: true,
                          dddMask: '+9 (999) 999 99 99'
                          }}
                          maxLength={18}
                          value={this.state.newPhone}
                          placeholder="Новый номер"
                          underlineColorAndroid='transparent'
                          style={styles.AuthScreen.inputForm}
                          refInput={ref => { this.inputPhone = ref }}
                          onChangeText={(newPhone) => {
                            this.setState({newPhone})
                          }}
                        />
				              	</View>  
								<TouchableOpacity style={styles.merchant.company.mainMemberSectionButton} onPress={this.addPhone.bind(this)}>
				                  <Text style={styles.merchant.company.mainMemberSectionButtonTitle}>Добавить</Text>
				                </TouchableOpacity>
							</View>

							<View style={styles.merchant.forms.sectionForm}>
								<Text style={styles.merchant.forms.title}>Режим работы</Text>

								{
									this.state.time.dayNight ?
										<View></View>
									:
										<View style={styles.merchant.forms.timeView}>

											<View style={styles.merchant.forms.timeViewSection}>

												<Text>С</Text>
											    <DatePicker
											        style={styles.merchant.forms.timeInputView}
											        date={time.start}
											        mode="time"
											        placeholder="Выберите время"
											        confirmBtnText="Confirm"
											        cancelBtnText="Cancel"
											        showIcon={false}
											        customStyles={{
											          dateInput: styles.merchant.forms.timeInputStyle
											        }}
											        onDateChange={(date) => {this.setState({
											        	time:{
											        		...time,
											        		start:date
											        	}
											        })}}
											    />

											</View>
											<View style={styles.merchant.forms.timeViewSection}>

												<Text>До</Text>
											    <DatePicker
											        style={styles.merchant.forms.timeInputView}
											        date={time.end}
											        mode="time"
											        placeholder="Выберите время"
											        confirmBtnText="Confirm"
											        cancelBtnText="Cancel"
											        showIcon={false}
											        customStyles={{
											          dateInput: styles.merchant.forms.timeInputStyle
											        }}
											        onDateChange={(date) => {this.setState({
											        	time:{
											        		...time,
											        		end:date
											        	}
											        })}}
											    />

											</View>
										</View>

								}

								<View style={styles.merchant.forms.timeViewSectionNight}>
									<Text style={styles.merchant.forms.timeViewSectionNightTitle}>Круглосуточно</Text>
									<Switch value={this.state.time.dayNight} onValueChange={(value)=>{this.setState({time:{...this.state.time,dayNight:value}})}}/>
								</View>

							</View>

							{
								statusRequest ? 
						            <View style={styles.merchant.forms.sectionFormNoBackground}>
							            <TouchableOpacity style={styles.AuthScreen.regButton}>
							            	<Text style={styles.AuthScreen.regButtonText}>Редактируем, подождите...</Text>
							            </TouchableOpacity>
						            </View>
						        :
						            <View style={styles.merchant.forms.sectionFormNoBackground}>
							            <TouchableOpacity onPress={this.updateCompany.bind(this)} style={styles.AuthScreen.regButton}>
							            	<Text style={styles.AuthScreen.regButtonText}>Редактировать</Text>
							            </TouchableOpacity>
						            </View>
							}
						</View>
					</TouchableWithoutFeedback>
				</ScrollView>

			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		locations:state.locations,
		companies:state.companies.companies,
		maps:state.maps,
		merchant:state.merchant,
		user:state.user
	}
}

export default connect(mapStateToProps)(MainEditWashCar)