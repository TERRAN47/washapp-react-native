import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
import { Header, NotFound } from 'Composition'
import config from 'Config'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import _ from 'underscore'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { socket } from 'Services/Utils'

class Boxes extends Component {

	constructor(props){
		super(props)
		this.socket = socket()

		this.state = {
			count:0,
			searchBoxes:[],
			boxesComp:[]
		}
	}

	componentDidMount(){

		let { companies } = this.props
		let { companyIndex } = this.props.navigation.state.params
		let boxes = companies[companyIndex].boxes

		const boxesComp = _.where(boxes, {
			status:false
		})
		const searchBoxes = _.where(boxes, {
			status:true
		})
		this.setState({
			count:boxes.length,
			searchBoxes,
			boxesComp
		})

	}

	addBox(){
		let {boxesComp} = this.state

		boxesComp.push({
			status:false,
			car:null,
			pay_status:false,
			member:null,
			index:boxesComp.length-1
		})

		this.setState({
			count:boxesComp.length,
			boxesComp
		})
	}

	removeBox(){

		let {boxesComp} = this.state

		if(boxesComp.length > 0){
			boxesComp.pop()
			this.setState({
				count:boxesComp.length,
				boxesComp
			})
		}

	}
	async saveBoxes(){
		let { id } = this.props.navigation.state.params.item
		const { dispatch , navigation} = this.props
		let {boxesComp} = this.state
		let saveReq = await Api.merchant.carwash.updateBoxes(dispatch, boxesComp, id)
		if(saveReq == 'ok'){
			navigation.goBack()
		}
	}

	render(){
		const { count, searchBoxes, boxesComp } = this.state
		const { statusRequest } = this.props
		return(
	      <View style={{flex:1}}>

	        <Header navigation={this.props.navigation} isBack={true}  title='Боксы'/>

        	{
        		searchBoxes.length == 0 ?
        			<View>
	        			<Text style={[styles.merchant.forms.viewCountText, {textAlign:'center'}]}>Количество боксов</Text>
	        			<View style={{flexDirection:'column',alignItems:'center',justifyContent:'center'}}>

					        <View style={styles.merchant.forms.viewCountBoxes}>
					        	<TouchableOpacity onPress={this.removeBox.bind(this)} style={styles.merchant.forms.viewCountButton}>
					        		<MaterialCommunityIcons name="minus" size={32} style={styles.merchant.company.iconSetting} color="#fff" />
					        	</TouchableOpacity>
					        	<Text style={styles.merchant.forms.viewCountTextBox}>{ count }</Text>
					        	<TouchableOpacity onPress={this.addBox.bind(this)} style={styles.merchant.forms.viewCountButton}>
					        		<MaterialCommunityIcons name="plus" size={32} style={styles.merchant.company.iconSetting} color="#fff" />
					        	</TouchableOpacity>
					        </View>
					        {
					        	statusRequest ?
						        	<View style={[styles.lists.bottomBackSuccess, {width:styles.width-10}]}>
						              	<Text style={styles.lists.bottomBackSuccessTitle}>Сохраняем...</Text>
						            </View>
					        	:
						        	<TouchableOpacity onPress={this.saveBoxes.bind(this)} style={[styles.lists.bottomBackSuccess, {width:styles.width-10}]}>
						              	<Text style={styles.lists.bottomBackSuccessTitle}>Сохранить</Text>
						            </TouchableOpacity>
					        }
			            </View>        				
        			</View>
			    :
			    	<Text style={styles.merchant.forms.viewCountText}>Во время активных боксов, изменение запрещено</Text>
        	}
	      </View>
		)
	}

}

const mapStateToProps = (state) => {
  return {
    companies:state.companies.companies,
    statusRequest:state.merchant.statusRequest
  }
}

export default connect(mapStateToProps)(Boxes)