import React, { Component } from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	Alert,
  Linking,
  ScrollView
} from 'react-native'
import config from 'Config'
import { moment } from 'Services/Time'
import { Header, NotFound, Loader } from 'Composition'
import styles from 'Services/Styles'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

class ViewSupplier extends Component {


  getOpen(url){
    return Linking.openURL(url)
  }

	render(){

		let { item, index } = this.props.navigation.state.params

		return(

      <View style={{flex:1}}>

        <Header navigation={this.props.navigation} isBack={true} title={'Поставщик'}/>

        <View style={styles.merchant.lists.itemSupplierFull}>

          <View style={styles.merchant.lists.itemSupplierImage}>
            <Image style={styles.merchant.lists.itemSupplierImageStyle} source={{uri:`${config.public_url}${item.logo}`}}/>
          </View>

          <View style={styles.merchant.lists.itemSupplierCenter}>

            <Text  style={styles.merchant.lists.itemSupplierCenterTitle}>{ item.title }</Text>
            
            {/*<View style={styles.merchant.lists.itemSupplierCenterSection}>
                              <MaterialCommunityIcons name="coin" style={styles.merchant.lists.itemSupplierCenterSectionIcon} size={28} color="#ffcd19" />
                              <Text>{ item.price }</Text>
                            </View>*/}

            <View style={styles.merchant.lists.itemSupplierCenterSection}>
              <MaterialCommunityIcons name="map-marker" style={styles.merchant.lists.itemSupplierCenterSectionIcon} size={28} color="#ffcd19" />
              <Text>{ item.location.title }, { item.adress }</Text>
            </View>

          </View>

          {/*<View style={styles.merchant.lists.itemSupplierRight}>
            <TouchableOpacity onPress={this.getOpen.bind(this, `${item.link_file}`)} style={[styles.merchant.lists.itemSupplierRightButton, {marginBottom:10}]}>
                              <Text style={styles.merchant.lists.itemSupplierRightButtonTitle}>Прайс</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.getOpen.bind(this, `tel:${item.phone}`)} style={styles.merchant.lists.itemSupplierRightButton}>
                              <Text style={styles.merchant.lists.itemSupplierRightButtonTitle}>Позвонить</Text>
                            </TouchableOpacity>
          </View>*/}

        </View>

        <ScrollView>
          <View style={styles.merchant.lists.itemSupplierFullInfoView}>
            <TouchableOpacity style={styles.merchant.lists.itemSupplierFullInfoViewSection} onPress={this.getOpen.bind(this, `tel:${item.phone}`)}>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextTop}>Телефон #1</Text>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextBottom}>{ item.phone }</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.merchant.lists.itemSupplierFullInfoViewSection} onPress={this.getOpen.bind(this, `tel:${item.phone_two}`)}>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextTop}>Телефон #2</Text>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextBottom}>{ item.phone_two }</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.merchant.lists.itemSupplierFullInfoViewSection} onPress={this.getOpen.bind(this, `${config.public_url}${item.link_file}`)}>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextTop}>Прайс (нажмите чтобы просмотреть)</Text>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextBottom}>Смотреть</Text>
            </TouchableOpacity>

            <View style={styles.merchant.lists.itemSupplierFullInfoViewSection}>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextTop}>Краткая информация</Text>
              <Text style={styles.merchant.lists.itemSupplierFullInfoViewSectionTextBottom}>{ item.description }</Text>
            </View>
          </View>
        </ScrollView>

      </View>
		)
	}
}

export default ViewSupplier