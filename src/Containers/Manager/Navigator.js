import React, { Component } from 'react'
import {
  View,
  Text,
  Dimensions
}from 'react-native'
import { createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation'

import { fromRight } from 'react-navigation-transitions'

import DrawerScreen from './DrawerScreen'
import CabinetScreen from './Cabinet'
import CreateCompany from './Companies/create'
import MainEditWashCar from './Companies/edit/main-information'
import CompaniesScreen from './Companies'
import CompanyView from './Companies/view'
import Members from './Members'
import ViewMembers from './Members/view'
import ViewMember from './Members/viewMember'
import Report from './Report'
import ReportView from './Report/view'
import SavedReports from './Report/savedReports'

import MerchantClients from './Clients'
import ViewClient from './Clients/view'

import MerchantHelp from './Help'
import SupplierScreen from './Suppliers'
import ViewSupplier from './Suppliers/view'

//price
import ListSelectBodyCar from './Companies/price/list-select-bodycar'
import ChangePrice from './Companies/price/change-price'

//Additionally
import AdditionallyScreen from './Companies/additionally'

//Payments
import Payment from './Payment'
import PaymentSuccess from './Payment/success'

//Queue
import Queue from './Queue'
import QueueCars from './Queue/QueueCars'
import SelectServices from './Queue/selectServices'

import AddCarInQueue from './Queue/helpers/addCarInQueue'

//Bonuses
import Bonuses from './Bonuses'

//Administrators
import Administrators from './Administrators'
import AddAdministrator from './Administrators/add-administrator'

//boxes
import Boxes from './Companies/boxes'

//Members list
import SelectMemberForBox from './Queue/helpers/selectMemberForBox'
//Vacancies
import CreateVacancy from './Vacancies/create'
import Vacancies from './Vacancies'
import CompaniesForVacancies from './Vacancies/companies'
import ViewVacancies from './Vacancies/viewVacancies'
import ViewAndEditVacancy from './Vacancies/viewAndEditVacancy'
const { width } = Dimensions.get('window')

export default createStackNavigator({
  drawer: createDrawerNavigator({
    companiesScreen: {
      screen:CompaniesScreen
    },
    CabinetScreen:{
      screen:CabinetScreen
    },
    members: {
      screen:Members
    },
    report: {
      screen:Report
    },
    merchantClient: {
      screen:MerchantClients
    },
    merchantHelp: {
      screen:MerchantHelp
    },
    vacancies:{
      screen:Vacancies
    },
    suppliers: {
      screen:SupplierScreen
    } 
  }, {
    contentComponent:DrawerScreen,
    drawerWidth: width-80
  }),
  createCompany: {
    screen:CreateCompany
  },
  savedReports:{
    screen:SavedReports
  },
  companyView: {
    screen:CompanyView
  },
  viewMembers: {
    screen:ViewMembers
  },
  //Vacancies
  createVacancies:{
    screen:CreateVacancy
  },
  companiesForVacancies:{
    screen:CompaniesForVacancies
  },
  viewVacancies:{
    screen:ViewVacancies
  },
  viewAndEditVacancy:{
    screen:ViewAndEditVacancy
  },

  viewMember:{
    screen:ViewMember
  },
  reportView: {
    screen:ReportView
  },
  editWashCar:{
    screen:MainEditWashCar
  },
  viewClient:{
    screen:ViewClient
  },
  //price
  ListSelectBodyCar:{
    screen:ListSelectBodyCar
  },
  selectServices:{
    screen:SelectServices
  },
  ChangePrice:{
    screen:ChangePrice
  },
  additionallyScreen:{
    screen:AdditionallyScreen
  },
  //payments
  payment:{
    screen:Payment
  },
  paymentSuccess:{
    screen:PaymentSuccess
  },

  viewSupplier:{
    screen:ViewSupplier
  },

  //Queue
  queue:{
    screen:Queue
  },
  queueCars:{
    screen:QueueCars
  },
  selectMemberForBox: {
    screen:SelectMemberForBox
  },
  addCarInQueue: {
    screen:AddCarInQueue
  },
  //bonuses
  bonuses:{
    screen:Bonuses
  },

  //Administrators
  administrators:{
    screen:Administrators
  },
  addAdministrator:{
    screen:AddAdministrator
  },

  //boxes
  boxes:{
    screen:Boxes
  }
}, {
  initialRouteName: 'drawer',
  transitionConfig: () => fromRight(),
  headerMode: 'none',
  cardStyle:{
    backgroundColor:'#fff'
  }
})
