import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'

import _ from 'underscore'

import { Header, NotFound, Loader } from 'Composition'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { MerchantComponents, Modal } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class Administrators extends Component {

  constructor(props){
    super(props)

    this.state = {

    }
  }

  removeAdmin(admin, index){

    let { dispatch, companies } = this.props
    let { id } = this.props.navigation.state.params

    let administrators = _.where(companies.companies, {
      id
    })[0].administrators

    return Api.merchant.administrator.removeAdministrator(dispatch, administrators, id, admin, index)     

  }

  goToScreen(screen, administrators, id){

    let { navigate } = this.props.navigation
    let { user_id } = this.props.navigation.state.params.item


    alert(user_id)
    return navigate(screen, {
      admins:administrators,
      id,
      merchant_id:user_id
    })

  }

  render(){

    let { id } = this.props.navigation.state.params
    let { statusRequest } = this.props.merchant
    let { companies } = this.props

    let administrators = _.where(companies.companies, {
      id
    })[0].administrators

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title='Администраторы' />

          <View style={{flex:1}}>

            {
              administrators && administrators.length > 0 ?
                <ScrollView>
                  
                  {
                    administrators.map((admin, index)=>{
                      return (<View key={index} style={styles.merchant.cabinetMerchant.mainViewListAdmins}>
                        <View>
                          <Text>{ admin.phone }</Text>
                          <Text>{ admin.firstname && admin.firstname.length > 0 ? admin.firstname : 'Без имени' }</Text>
                        </View>
                      
                        {
                          statusRequest ?
                            <TouchableOpacity>
                              <MaterialCommunityIcons name="clock-outline" size={30} color="#747474" />
                            </TouchableOpacity>
                          :
                            <TouchableOpacity onPress={this.removeAdmin.bind(this, admin, index)}>
                              <MaterialCommunityIcons name="delete-circle" size={30} color="#cc2b2b" />
                            </TouchableOpacity>

                        }
                      </View>)
                    })
                  }

                  {
                    statusRequest ?
                      <TouchableOpacity style={styles.merchant.cabinetMerchant.buttonOpenModalBonus}>
                        <MaterialCommunityIcons name="clock-outline" size={30} color="#466dfa" />
                        <Text style={styles.merchant.cabinetMerchant.buttonOpenModalBonusText}>Подождите...</Text>
                      </TouchableOpacity>
                    :
                      <TouchableOpacity style={styles.merchant.cabinetMerchant.buttonOpenModalBonus} onPress={this.goToScreen.bind(this, 'addAdministrator', administrators, id)}>
                        <MaterialCommunityIcons name="plus-circle" size={30} color="#466dfa" />
                        <Text style={styles.merchant.cabinetMerchant.buttonOpenModalBonusText}>Добавить администратора</Text>
                      </TouchableOpacity>

                  }

                </ScrollView>
              :
                <NotFound navigation={this.props.navigation} id={id} administrators={administrators} type='MERCHANT_ADMINISTRATOR' />

            }
          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    merchant:state.merchant,
    companies:state.companies
  }
}

export default connect(mapStateToProps)(Administrators)