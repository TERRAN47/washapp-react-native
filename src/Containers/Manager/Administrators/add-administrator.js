import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  ImageBackground
} from 'react-native'

import _ from 'underscore'

import { Header, NotFound, Loader } from 'Composition'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { MerchantComponents, Modal } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class AddAdministrator extends Component {

  constructor(props){
    super(props)

    this.state = {
      letter:''
    }
  }

  findUsers(value){

    this.setState({
      letter:value
    })

    let { dispatch } = this.props

    return Api.merchant.administrator.findAdministrators(dispatch, value) 

  }

  addAdmin(admin){

      let { id, merchant_id } = this.props.navigation.state.params
      let { dispatch, companies } = this.props

      let administrators = _.where(companies.companies, {
        id
      })[0].administrators

      return Api.merchant.administrator.addAdministrator(dispatch, administrators, admin, id, merchant_id) 

  }

  render(){

    let { list } = this.props.administrator
    let { statusRequest } = this.props.merchant

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title='Администраторы' />

          <View style={{flex:1}}>

            <TextInput style={styles.merchant.cabinetMerchant.inputSearchAdmins} value={this.state.letter} placeholder="Введите телефон или имя пользователя" onChangeText={(value)=>{this.findUsers(value)}}/>

            {
              list && list.length > 0 ?
                <ScrollView>
                  
                  {
                    list.map((admin, index)=>{
                      return (<View key={index} style={styles.merchant.cabinetMerchant.viewRenderAdminList}>

                        <View>
                          <Text>+7{ admin.phone }</Text>
                          <Text>{ admin.firstname && admin.firstname.length > 0 ? admin.firstname : 'Без имени' }</Text>
                        </View>

                        {
                          statusRequest ?
                            <TouchableOpacity>
                              <MaterialCommunityIcons name="clock-outline" size={30} color="#747474" />
                            </TouchableOpacity>
                          :
                            <TouchableOpacity onPress={this.addAdmin.bind(this, admin)}>
                              <MaterialCommunityIcons name="plus-circle" size={30} color="#466dfa" />
                            </TouchableOpacity>
                        }
                        
                      </View>)
                    })
                  }

                </ScrollView>
              :
                <NotFound navigation={this.props.navigation} type='MERCHANT_ADMINISTRATOR_NOT' />

            }
          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    administrator:state.administrator,
    companies:state.companies,
    merchant:state.merchant,
  }
}

export default connect(mapStateToProps)(AddAdministrator)