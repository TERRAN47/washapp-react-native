import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  TextInput,
  ImageBackground
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, NotFound } from 'Composition'
import { MerchantComponents, Modal } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class ViewMembers extends Component {

  constructor(props){
    super(props)

    this.state = {
      modalOpened:false
    }
  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  modalOpened(){
    return this.setState({
      modalOpened:!this.state.modalOpened
    })
  }

  newMember(member){

    let { dispatch } = this.props
    let { item } = this.props.navigation.state.params
    let { members } = this.props.navigation.state.params.item

    let {
      firstName,
      phone,
      characteristic,
      date,
      adress,
      percent
    } = member

    if(firstName && date && percent){

      this.modalOpened()
      members.push(member)
      this.props.navigation.state.params.item.members = members

      return Api.merchant.members.newMember(dispatch, members, item.id)

    }else{
      return Alert.alert('Сообщение', 'Заполните все поля')
    }
  }

  deleteMember(index){

    let { dispatch } = this.props
    let { item } = this.props.navigation.state.params
    let { members } = this.props.navigation.state.params.item
    let that = this

    Alert.alert(
      'Сообщение',
      'Вы точно хотите удалить этого сотрудника?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Удалить', onPress: () => {

          members.splice(index, 1)
          that.props.navigation.state.params.item.members = members

          return Api.merchant.members.deleteMember(dispatch, members, item.id)

        }}
      ],
      { cancelable: false }
    )

  }

  render(){
    let { user } = this.props.user
    let { modalOpened } = this.state
    let { members, id } = this.props.navigation.state.params.item
    let { statusRequest } = this.props.merchant

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Сотрудники'}/>

          <Modal modalOpened={modalOpened} type="members" newMember={this.newMember.bind(this)} closeModal={this.modalOpened.bind(this)}/>

          <View style={{flex:1}}>

            {
              members.length > 0 ?
                <ScrollView showsVerticalScrollIndicator={false}>
                  {
                    members.map((member, index)=>{
                      return <MerchantComponents.ListMembers id={id} members={members} navigation={this.props.navigation} statusRequest={statusRequest} deleteMember={this.deleteMember.bind(this)} key={index} index={index} item={member} />
                    })
                  }

                  {

                    statusRequest ?
                      <TouchableOpacity style={styles.merchant.lists.memberView}>
                        <View style={styles.merchant.lists.memberViewInfo}>
                          <View style={styles.merchant.lists.memberViewAvatar}>
                            <MaterialCommunityIcons name="clock-outline" size={38} color="#fff" />
                          </View>
                          <View>
                            <Text style={styles.merchant.lists.memberViewUserName}>Обрабатываем, подождите...</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    :
                      <TouchableOpacity onPress={this.modalOpened.bind(this)} style={styles.merchant.lists.memberView}>
                        <View style={styles.merchant.lists.memberViewInfo}>
                          <View>
                            <Text style={styles.merchant.lists.memberViewUserName}>Новый сотрудник</Text>
                          </View>
                        </View>
                        <View style={styles.merchant.lists.memberViewButtonClientDeleteOrAdd}>
                          <MaterialCommunityIcons name="plus" size={30} color="#fff" />
                        </View>
                      </TouchableOpacity>

                  }

                </ScrollView>
              : 
                <View >
                  <Text style={styles.merchant.forms.viewNotFoundMembersText}>У данной автомойки нет сотрудников</Text>
                  <TouchableOpacity onPress={this.modalOpened.bind(this)} style={[styles.merchant.lists.memberView, {borderWidth:0}]}>
                    <View style={styles.merchant.lists.memberViewInfo}>
                      <View>
                        <Text style={styles.merchant.lists.memberViewUserName}>Новый сотрудник</Text>
                      </View>
                    </View>
                    <View style={styles.merchant.lists.memberViewButtonClientDeleteOrAdd}>
                      <MaterialCommunityIcons name="plus" size={30} color="#fff" />
                    </View>
                  </TouchableOpacity>
                </View>
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    maps:state.maps,
    merchant:state.merchant
  }
}

export default connect(mapStateToProps)(ViewMembers)