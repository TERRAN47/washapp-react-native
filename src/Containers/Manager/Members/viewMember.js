import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  TextInput,
  Linking
} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class ViewMember extends Component {

  constructor(props){
    super(props)

    this.state = {
      phoneStatusChange:false,
      harakterStatusChange:false,
      percentStatusChange:false,
      adressStatusChange:false,
      nameStatusChange:false,
      item:{phone:"7"}
    }
  }

  componentDidMount(){

    let { item, } = this.props.navigation.state.params

    this.setState({
      item
    })

  }

  deleteMember(index){

    let { dispatch, navigation } = this.props
    let { item, members, id } = this.props.navigation.state.params
    let that = this

    Alert.alert(
      'Сообщение',
      'Вы точно хотите удалить этого сотрудника?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Удалить', onPress: () => {

          members.splice(index, 1)
          that.props.navigation.state.params.item.members = members

          return Api.merchant.members.deleteMember(dispatch, members, id, navigation)

        }}
      ],
      { cancelable: false }
    )

  }

  updateMember(index){

    this.setState({
      phoneStatusChange:false,
      percentStatusChange:false,
      harakterStatusChange:false,
      adressStatusChange:false,
      nameStatusChange:false
    })

    let { members, id } = this.props.navigation.state.params
    let { dispatch } = this.props
    let { item } = this.state

    members[index] = item

    this.props.navigation.state.params.item.members = members
    this.props.navigation.state.params.item = item

    return Api.merchant.members.updateMember(dispatch, members, id)

  }

  callMember(phone){
    return Linking.openURL(`tel:${phone}`)
  }

  render(){

    let { user } = this.props.user
    let { item, index } = this.props.navigation.state.params
    let { statusRequest } = this.props.merchant

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Сотрудники'}/>

          <View style={{flex:1}}>

            <ScrollView showsVerticalScrollIndicator={false}>

              <View style={styles.merchant.company.mainViewMemberInfo}>

                <View style={styles.merchant.company.mainMemberSection}>

                  <View style={styles.merchant.company.mainMemberSectionLeft}>
                    <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                      <MaterialCommunityIcons name="account" size={30} color="#fff" />
                    </View>

                    {
                      this.state.nameStatusChange ?

                        <TextInput style={styles.merchant.company.mainMemberSectionLeftInput} value={this.state.item.firstName} autoFocus={true} onChangeText={(value)=>this.setState({item:{...this.state.item,firstName:value}})} />

                      :
                         <Text style={styles.merchant.company.mainMemberSectionText}>{ item.firstName }</Text>
                    }
                    
                  </View>

                  {

                    this.state.nameStatusChange ? 

                      <TouchableOpacity onPress={this.updateMember.bind(this, index)} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="check" size={30} color="#466dfa" />
                      </TouchableOpacity>

                    :

                      <TouchableOpacity onPress={()=>this.setState({nameStatusChange:!this.state.nameStatusChange})} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="square-edit-outline" size={30} color="#466dfa" />
                      </TouchableOpacity>

                  }

                </View>

                <View style={styles.merchant.company.mainMemberSection}>
                  <View style={styles.merchant.company.mainMemberSectionLeft}>
                    <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                      <MaterialCommunityIcons name="cellphone-iphone" size={30} color="#fff" />
                    </View>

                    {
                      this.state.phoneStatusChange ?

                        <TextInputMask
                          type={'cel-phone'}
                          options={{
                            withDDD: true,
                            dddMask: '+9 (999) 999 99 99'
                          }}
                          maxLength={18}
                          value={this.state.item.phone}
                          placeholder="Номер телефона"
                          style={styles.merchant.company.mainMemberSectionLeftInput}
                          refInput={ref => { this.input = ref }}
                          onChangeText={(value)=>this.setState({item:{...this.state.item,phone:value}})}
                        />

                      :
                        <TouchableOpacity onPress={this.callMember.bind(this, `${this.state.item.phone}`)}>
                          <Text style={styles.merchant.company.mainMemberSectionText}>{ this.state.item.phone }</Text>
                        </TouchableOpacity>
                    }
                    
                  </View>

                  {

                    this.state.phoneStatusChange ? 

                      <TouchableOpacity onPress={this.updateMember.bind(this, index)} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="check" size={30} color="#466dfa" />
                      </TouchableOpacity>

                    :

                      <TouchableOpacity onPress={()=>this.setState({phoneStatusChange:!this.state.phoneStatusChange})} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="square-edit-outline" size={30} color="#466dfa" />
                      </TouchableOpacity>

                  }

                </View>

                <View style={styles.merchant.company.mainMemberSection}>
                  <View style={styles.merchant.company.mainMemberSectionLeft}>
                    <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                      <MaterialCommunityIcons name="percent" size={30} color="#fff" />
                    </View>

                    {
                      this.state.percentStatusChange ?
                        <TextInput style={styles.merchant.company.mainMemberSectionLeftInput} value={this.state.item.percent} autoFocus={true} keyboardType='numeric' onChangeText={(value)=>this.setState({item:{...this.state.item,percent:value}})} />
                      :
                        <Text style={styles.merchant.company.mainMemberSectionText}>{ this.state.item.percent }</Text>
                    }

                  </View>

                  {
                    
                    this.state.percentStatusChange ? 

                      <TouchableOpacity onPress={this.updateMember.bind(this, index)} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="check" size={30} color="#466dfa" />
                      </TouchableOpacity>

                    :

                      <TouchableOpacity onPress={()=>this.setState({percentStatusChange:!this.state.percentStatusChange})} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="square-edit-outline" size={30} color="#466dfa" />
                      </TouchableOpacity>

                  }

                </View>

                <View style={styles.merchant.company.mainMemberSection}>
                  <View style={styles.merchant.company.mainMemberSectionLeft}>
                    <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                      <MaterialCommunityIcons name="map-marker" size={30} color="#fff" />
                    </View>

                    {
                      this.state.adressStatusChange ?
                        <TextInput style={styles.merchant.company.mainMemberSectionLeftInput} value={this.state.item.adress} autoFocus={true} onChangeText={(value)=>this.setState({item:{...this.state.item,adress:value}})} />
                      :
                        <Text style={styles.merchant.company.mainMemberSectionText}>{ this.state.item.adress }</Text>
                    }

                  </View>

                  {
                    
                    this.state.adressStatusChange ? 

                      <TouchableOpacity onPress={this.updateMember.bind(this, index)} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="check" size={30} color="#466dfa" />
                      </TouchableOpacity>

                    :

                      <TouchableOpacity onPress={()=>this.setState({adressStatusChange:!this.state.adressStatusChange})} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="square-edit-outline" size={30} color="#466dfa" />
                      </TouchableOpacity>

                  }

                </View>

                <View style={styles.merchant.company.mainMemberSection}>
                  <View style={styles.merchant.company.mainMemberSectionLeft}>
                    <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                      <MaterialCommunityIcons name="calendar-range" size={30} color="#fff" />
                    </View>
                    <Text style={styles.merchant.company.mainMemberSectionText}>Принят на работу </Text>
                    <View style={styles.merchant.company.mainMemberSectionTextDateColor}>
                      <Text style={styles.merchant.company.mainMemberSectionTextDateColortitle}>{ item.date }</Text>
                    </View>
                  </View>
                </View>

                <View style={styles.merchant.company.mainMemberSection}>
                  <View style={styles.merchant.company.mainMemberSectionViewText}>
                    <Text style={styles.merchant.company.mainMemberSectionTitle}>Характеристика</Text>
                    {
                      this.state.harakterStatusChange ? 
                        <TextInput 
                          style={styles.merchant.company.mainMemberSectionLeftInput} 
                          value={this.state.item.characteristic} 
                          autoFocus={true} 
                          onChangeText={(value)=>this.setState({item:{...this.state.item,characteristic:value}})} 
                        />
                      :
                        <Text style={styles.merchant.company.mainMemberSectionText}>{ `${item.characteristic}` }</Text>
                    }
                  </View>
                  {
                    
                    this.state.harakterStatusChange ? 

                      <TouchableOpacity onPress={this.updateMember.bind(this, index)} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="check" size={30} color="#466dfa" />
                      </TouchableOpacity>

                    :

                      <TouchableOpacity onPress={()=>this.setState({harakterStatusChange:!this.state.harakterStatusChange})} style={styles.merchant.company.mainMemberSectionRight}>
                        <MaterialCommunityIcons name="square-edit-outline" size={30} color="#466dfa" />
                      </TouchableOpacity>

                  }
                </View>

                {
                  statusRequest ?
                    <TouchableOpacity style={styles.merchant.company.mainMemberSection}>
                      <View style={styles.merchant.company.mainMemberSectionLeft}>
                        <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                          <MaterialCommunityIcons name="timer" size={30} color="#fff" />
                        </View>
                        <Text style={styles.merchant.company.mainMemberSectionText}>Подождите...</Text>
                      </View>
                    </TouchableOpacity>
                  :

                    <TouchableOpacity style={styles.merchant.company.mainMemberSection} onPress={this.deleteMember.bind(this, index)}>
                      <View style={styles.merchant.company.mainMemberSectionLeft}>
                        <View style={styles.merchant.company.mainMemberSectionLeftIcon}>
                          <MaterialCommunityIcons name="close" size={30} color="#fff" />
                        </View>
                        <Text style={styles.merchant.company.mainMemberSectionText}>Уволить</Text>
                      </View>
                    </TouchableOpacity>
                }

{/*                <TouchableOpacity style={styles.merchant.company.mainMemberSectionButton} onPress={()=>{ this.props.navigation.goBack() }}>
                  <Text style={styles.merchant.company.mainMemberSectionButtonTitle}>Сохранить</Text>
                </TouchableOpacity>*/}

              </View>

            </ScrollView>

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant
  }
}

export default connect(mapStateToProps)(ViewMember)