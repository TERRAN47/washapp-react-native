import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Linking,
  ImageBackground
} from 'react-native'
import { Header, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class MerchantHelp extends Component {

  componentDidMount(){

  }

  goToScreen(screen){
    const { navigate } = this.props.navigation
    return navigate(screen)
  }

  callPhone(phone){
    return Linking.openURL(`tel:${phone}`)
  }

  writeEmail(email){
    return Linking.openURL(`mailto:${email}`)
  }

  render(){

    let { user } = this.props.user

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} menu={true} title='Помощь' />

          <View style={{flex:1,padding:10}}>

            <Text style={styles.merchant.cabinetMerchant.helpTextMain}>В случае возникновения вопросов просим Вас обращаться по следующему контактному номеру:</Text>

            <TouchableOpacity onPress={this.callPhone.bind(this, '+77758880771')}>
              <Text style={styles.merchant.cabinetMerchant.helpTextMainLink}>+7 775 888 0771</Text>
            </TouchableOpacity>

            <View>
              <Text style={styles.merchant.cabinetMerchant.helpTextMain}>или написать на почту:</Text>
              <TouchableOpacity onPress={this.writeEmail.bind(this, 'info@washapp.com.kz')} ><Text style={styles.merchant.cabinetMerchant.helpTextMainLink}>help@washapp.com.kz</Text></TouchableOpacity>
            </View>

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    maps:state.maps
  }
}

export default connect(mapStateToProps)(MerchantHelp)