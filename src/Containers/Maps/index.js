import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
	View,
	Text,
	Image,
	TouchableOpacity
} from 'react-native'
import MapView, { Marker, AnimatedRegion, Animated } from 'react-native-maps'
import styles from 'Services/Styles'
import { Header, NotFound } from 'Composition'
import { mapStyle } from '../Domain'
import Api from 'Services/Api'

class Maps extends Component {
	constructor(props){
		super(props)

		this.state = {
			status:false,
			region:	{
				latitude:51.156144,
				longitude:71.493068,
	    		latitudeDelta: 0.010,
	    		longitudeDelta: 0.010,
    		},
		    coords:{
		    	user:{
		    		latitude: 0,
					longitude: 0,
	    			latitudeDelta: 0.010,
	    			longitudeDelta: 0.010,
		    	}
		    }
		}
	}

	componentDidMount(){
		this.getCurrentPositionUser()
	}

	getCurrentPositionUser(){

		let that = this
		let { selectCoordStatus, selectCoords } = this.props.maps

		if(selectCoordStatus){

			that.setState({
				region:	{
					...that.state.region,
	      			latitude: selectCoords.latitude,
	 				longitude: selectCoords.longitude
	    		},
				coords:{
					...that.state.coords,
			    	user:{
			    		latitude: selectCoords.latitude,
						longitude: selectCoords.longitude
			    	}
				}
			})

		}else{

			navigator.geolocation.getCurrentPosition((data)=>{
				that.setState({
					region:	{
						...that.state.region,
		      			latitude: data.coords.latitude,
		 				longitude: data.coords.longitude
		    		},
					coords:{
						...that.state.coords,
				    	user:{
				    		latitude: data.coords.latitude,
							longitude: data.coords.longitude
				    	}
					}
				})
			},(error) => this.setState({ error: error.message }),
      		{
      			enableHighAccuracy: false, 
      			timeout: 5000, 
      			maximumAge: 10000,
      			enableHighAccuracy: true
      		})

		}

	}

	onMapPress(coords){
		this.setState({
			status:true,
			region:{
				...this.state.region,
	    		latitude: coords.latitude,
				longitude: coords.longitude
			},
			coords:{
				...this.state.coords,
				user:{
		    		latitude: coords.latitude,
					longitude: coords.longitude	
				}
			}
		})
	}

	saveCoords(){
		const { dispatch, navigation } = this.props
		const { coords } = this.state
		return Api.maps.saveCoords(navigation, dispatch, coords.user)
	}

	render(){

		let { region, coords, status } = this.state
		let { selectCoordStatus } = this.props.maps

		return(
			<View style={styles.maps.view}>
				<Header navigation={this.props.navigation} isBack={true} title="Покажите на карте" />
				<View style={styles.maps.mapContent}>

					{
						status ? 
							<TouchableOpacity onPress={this.saveCoords.bind(this)} style={styles.maps.buttonSelectCoords}>
								<Text style={styles.maps.buttonSelectCoordsTitle}>Подтвердить</Text>
								<Text style={styles.maps.buttonSelectCoordsTitle}>локацию</Text>
							</TouchableOpacity>
						:
							<View style={styles.maps.viewInfo}>
								<Text  style={styles.maps.viewInfoText}>Выберите на карте локацию автомойки</Text>
							</View>
					}

			    	<MapView
			    		showUserLocation={true}
						followUserLocation={true}
						loadingEnabled={true}
						zoomEnabled = {false}
						customMapStyle={mapStyle}
			    		onPress={(event)=>{
			    			this.onMapPress(event.nativeEvent.coordinate)
			    		}}
			       		style={styles.maps.mapStyle}
				        initialRegion={region}
			     	>

						<Marker 
							coordinate={selectCoordStatus ? region : coords.user}
						>
							<View>
								<Image source={require('./Resources/Map-Marker.png')} style={styles.maps.markerStyleImage} />
							</View>
						</Marker>

			     	</MapView>
				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		maps:state.maps
	}
}

export default connect(mapStateToProps)(Maps)