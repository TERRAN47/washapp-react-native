const initialState = {
	selectCoords:{},
	selectCoordStatus:false,
	userGeo:{}
}

const maps = (state = initialState, action) => {
	switch(action.type){

		case 'SELECT_CURRENT_COORDS' :
			return {
				...state,
				selectCoordStatus:true,
				selectCoords:{
		    		latitude: action.latitude,
					longitude: action.longitude	
				}
			}

		case 'CURENT_USER_GEOLOCATION' :
			return {
				...state,
				userGeo:{
		    		latitude: action.latitude,
					longitude: action.longitude	
				}
			}

		default :
			return state
	}
}

export default maps