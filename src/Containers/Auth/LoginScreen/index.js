import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  ScrollView
} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class LoginScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
      phone:'7',
      password:'',
      ids:'',
      status:false
    }
  }

  goToScreen(screen){
    return this.props.navigation.navigate(screen)
  }

  loginUser(){

    let {
      password,
      phone,
      ids
    } = this.state

    const { 
      dispatch, 
      navigation 
    } = this.props

    return Api.auth.login(password, phone, dispatch, navigation, Api, ids)
  }

  render(){
    let { request } = this.props.auth
    let { status } = this.state

    return(
      <View>
        <ScrollView
          keyboardShouldPersistTaps="always"
        >
          <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss}}>
            <View style={styles.AuthScreen.loginView}>
              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="mobile" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    maskType: "BRL",
                    withDDD: true,
                    dddMask: '+9 (999) 999 99 99'
                  }}
                  maxLength={18}
                  value={this.state.phone}
                  placeholder="Номер телефона"
                  underlineColorAndroid='transparent'
                  style={styles.AuthScreen.inputForm}
                  refInput={ref => { this.input = ref }}
                  onChangeText={(phone) => {
                    this.setState({phone})
                  }}
                />
              </View>
              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="unlock-alt" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInput 
                  secureTextEntry={true} 
                  onChangeText={(password)=>{this.setState({password})}} 
                  underlineColorAndroid='transparent' 
                  style={styles.AuthScreen.inputForm} 
                  placeholder="Пароль"
                />
              </View>
              {
                !request ? 
                  <TouchableOpacity onPress={this.loginUser.bind(this)} style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ВОЙТИ</Text>
                  </TouchableOpacity>
                : status ?
                  <TouchableOpacity onPress={this.loginUser.bind(this)} style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ВОЙТИ</Text>
                  </TouchableOpacity>
                :  <TouchableOpacity style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ПОДОЖДИТЕ...</Text>
                  </TouchableOpacity>
              }

              <TouchableOpacity onPress={this.goToScreen.bind(this, 'resetPassword')} style={styles.AuthScreen.resetButton}>
                <Text style={styles.AuthScreen.resetButtonText}>Забыли пароль?</Text>
              </TouchableOpacity>

            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth:state.auth
  }
}

export default connect(mapStateToProps)(LoginScreen)