import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  Alert
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import { Header } from 'Composition'

class ResetPasswordFormScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
      password:'',
      repeatPassword:''
    }
  }

  resetPassword(){

    let { phone } = this.props.navigation.state.params
    let { password, repeatPassword } = this.state
    let { dispatch, navigation } = this.props

    return Api.auth.resetPassword(phone, password, repeatPassword, dispatch, navigation, Api)

  }

  render(){

    let { request } = this.props.auth

    return(
      <View style={styles.AuthScreen.regView}>
        <Header isBack={true} navigation={this.props.navigation} title={'Сброс пароля'}/>
        <ScrollView
          keyboardShouldPersistTaps="always"
        >
          <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss}}>
            <View style={styles.AuthScreen.mainViewForm}>

              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="unlock-alt" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInput secureTextEntry={true} onChangeText={(password)=>{this.setState({password})}} underlineColorAndroid='transparent' style={styles.AuthScreen.inputForm} placeholder="Пароль"/>
              </View>

              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="unlock-alt" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInput secureTextEntry={true} onChangeText={(repeatPassword)=>{this.setState({repeatPassword})}} underlineColorAndroid='transparent' style={styles.AuthScreen.inputForm} placeholder="Повторите пароль"/>
              </View>

              {
                !request ?
                  <TouchableOpacity onPress={this.resetPassword.bind(this)} style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ИЗМЕНИТЬ ПАРОЛЬ</Text>
                  </TouchableOpacity>
                :
                  <TouchableOpacity style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ПОДОЖДИТЕ...</Text>
                  </TouchableOpacity>
              }

            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth:state.auth
  }
}

export default connect(mapStateToProps)(ResetPasswordFormScreen)