import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Alert
} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from '../../../Services/Styles'
import Api from '../../../Services/Api'
import { Header } from '../../../Composition'

class ResetPasswordScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
      phone:'7'
    }
  }

  regScreen(){
    return this.props.navigation.navigate('RegistrationScreen')
  }

  sendCode(){

    let {
      phone
    } = this.state

    const {
      dispatch,
      navigation
    } = this.props

    return Api.auth.sendCode(phone, dispatch, navigation)

  }

  render(){

    let { request } = this.props.auth

    return(
      <View style={styles.AuthScreen.regView}>
        <Header isBack={true} navigation={this.props.navigation} title={'Сброс пароля'}/>
        <ScrollView>
          <View style={styles.AuthScreen.mainViewForm}>

            <View style={styles.AuthScreen.inputFormView}>
              <Icon name="mobile" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
              <TextInputMask
                type={'cel-phone'}
                options={{
                  withDDD: true,
                  dddMask: '+9 (999) 999 99 99'
                }}
                maxLength={18}
                value={this.state.phone}
                placeholder="Номер телефона"
                underlineColorAndroid='transparent'
                style={styles.AuthScreen.inputForm}
                refInput={ref => { this.input = ref }}
                onChangeText={(phone) => {
                  this.setState({phone})
                }}
              />              
            </View>

            {
              !request ?
                <TouchableOpacity onPress={this.sendCode.bind(this)} style={styles.AuthScreen.regButton}>
                  <Text style={styles.AuthScreen.regButtonText}>ОТПРАВИТЬ КОД</Text>
                </TouchableOpacity>
              :
                <TouchableOpacity style={styles.AuthScreen.regButton}>
                  <Text style={styles.AuthScreen.regButtonText}>ПОДОЖДИТЕ...</Text>
                </TouchableOpacity>
            }

          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth:state.auth
  }
}

export default connect(mapStateToProps)(ResetPasswordScreen)