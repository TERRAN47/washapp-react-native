import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header, NotFound, Loader } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

import RenderCompany from './helpers/renderCompany'

class VacanciesforClients extends Component {

  constructor(props){
    super(props)

    this.state = {

    }
  }

  componentDidMount(){
    this.getVacancies()
  }

  getVacancies(){

    const { dispatch, user } = this.props

    return Api.merchant.vacancies.getVacanciesForClients(dispatch)

  }

  render(){

    let { statusRequest } = this.props.merchant
    let { vacancies } = this.props

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title={'Вакансии'}/>

          <View style={{flex:1}}>

            {
              statusRequest ?
                <Loader title="Загрузка" />
              : vacancies ?

                <ScrollView>

                  {
                    vacancies.map((vacancy, index)=>{
                      return <RenderCompany navigation={this.props.navigation} dispatch={this.props.dispatch} key={index} item={vacancy} />
                    })
                  }
                  
                </ScrollView>  
              :
                <NotFound navigation={this.props.navigation} type='NO_VACANCIES' buttonHide={true}/>
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    merchant:state.merchant,
    vacancies:state.vacancies.list
  }
}

export default connect(mapStateToProps)(VacanciesforClients)