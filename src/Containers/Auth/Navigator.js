import React, { Component } from 'react'
import { createStackNavigator, createTabNavigator, createMaterialTopTabNavigator, createDrawerNavigator } from 'react-navigation'

import {
  Platform
} from 'react-native'

import { fromRight } from 'react-navigation-transitions'

import LoginScreen from './LoginScreen'
import RegistrationScreen from './RegistrationScreen'
import SelectTypeUserScreen from './SelectTypeUserScreen'
import ResetPasswordScreen from './ResetPasswordScreen'
import ResetPasswordFormScreen from './ResetPasswordScreen/formResetPassword'
import GrandScreen from './GrandScreen'

import VacanciesforClients from './Vacancies'
import ViewVacancies from './Vacancies/viewVacancies'
//fonts
import styles from '../../Services/Styles'

export default createStackNavigator({
	auth:createTabNavigator({
	    selectTypeUser: {
	    	screen: SelectTypeUserScreen,
	        navigationOptions: { title: 'РЕГИСТРАЦИЯ' }
	    },
	  	login:{
	    	screen:LoginScreen,
	        navigationOptions: { title: 'ВХОД' }
	  	}
	},{
    animationEnabled: false,
    swipeEnabled: true,
    tabBarPosition: 'top',
    lazy:false,
    tabBarOptions: {
      indicatorStyle:{
        height:5,
        backgroundColor:'#ffcd19'
      },
    	activeTintColor:"rgba(255,255,255,1)",
    	inactiveTintColor:"rgba(255,255,255,0.45)",
			labelStyle: {
			    fontSize: 12,
			    color:'#fff',
          position:'relative',
          top:Platform.OS === 'android' ? 0 : -15,
			    fontFamily:styles.fonts.GOTHICB
			},
      style:{
          backgroundColor:'#466df9',
          height:50
      }
    }
	}),
	registration:{
    screen:RegistrationScreen
  },
	resetPassword:{
  	screen:ResetPasswordScreen
	},
  vacanciesForClient:{
    screen:VacanciesforClients
  },
  viewVacanciesForClient:{
    screen:ViewVacancies
  },
  grant:{
    screen:GrandScreen
  },
  resetPasswordForm:{
    screen:ResetPasswordFormScreen
  }
}, {
	initialRouteName: 'auth',
  transitionConfig: () => fromRight(),
  headerMode: 'none',
  cardStyle:{
    backgroundColor:'#fff'
  }
})
