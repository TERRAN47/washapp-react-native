import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import styles from 'Services/Styles'

import Api from 'Services/Api'

class GrandScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
      code:'',
      seconds:60
    }
  }

  componentDidMount(){
    let that = this
    this.intervalTicker = setInterval(that.ticker.bind(this), 1000)
  }

  ticker(){

    let seconds = (this.state.seconds - 1)

    if(seconds == 0){
      this.props.navigation.goBack()
    }else{
      this.setState({
        seconds:Number(seconds)
      })
    }

  }

  componentWillUnmount () {
    clearInterval(this.intervalTicker)
  }

  regScreen(){
    return this.props.navigation.navigate('RegistrationScreen')
  }

  protectUser(){

    let { code } = this.state
    let { navigation, user, dispatch } = this.props

    return Api.auth.protectUser(code, Api, dispatch, navigation, user.user.phone)

  }

  protectSMS(){
    const { sms, phone } = this.props.navigation.state.params
    const { code } = this.state

    if(sms == code){
      
      return this.props.navigation.navigate('resetPasswordForm', {
        phone
      })

    }else{
      
      return Alert.alert('Сообщение', 'Код неверный')

    }

  }

  render(){

    let { user } = this.props.user
    let { request } = this.props.auth
    let { seconds } = this.state
    const { type } = this.props.navigation.state.params

    return(
      <View style={styles.AuthScreen.grandView}>
        <Text style={styles.AuthScreen.grandtitle}>Введите 6-значный код подтверждения</Text>

        <View style={styles.AuthScreen.inputFormView}>
          <Icon name="unlock-alt" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
          <TextInput keyboardType={'numeric'} autoFocus={true} secureTextEntry={false} onChangeText={(code)=>{this.setState({code})}} underlineColorAndroid='transparent' style={styles.AuthScreen.inputForm} placeholder="Введите код"/>
        </View>

        {
          !request ? 
            <TouchableOpacity onPress={ type == 'resetPassword' ? this.protectSMS.bind(this) : this.protectUser.bind(this) } style={styles.AuthScreen.regButton}>
              <Text style={styles.AuthScreen.regButtonText}>ПОДТВЕРДИТЬ</Text>
            </TouchableOpacity>
          :
            <TouchableOpacity style={styles.AuthScreen.regButton}>
              <Text style={styles.AuthScreen.regButtonText}>ПРОВЕРЯЕМ...</Text>
            </TouchableOpacity>
        }

        <View style={styles.AuthScreen.grandViewTimer}>
          <Text style={styles.AuthScreen.grandViewTimerText}>{ seconds }</Text>
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    auth:state.auth
  }
}

export default connect(mapStateToProps)(GrandScreen)