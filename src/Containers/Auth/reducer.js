const initialState = {
	request:false
}

const auth = (state = initialState, action) => {
	switch(action.type){
		case 'REQUEST_STATUS' :
			return {
				...state,
				request:action.status
			}
		default :
			return state
	}
}

export default auth