import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Linking,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Switch
} from 'react-native'
import config from 'Config'
import { TextInputMask } from 'react-native-masked-text'
import Icon from 'react-native-vector-icons/FontAwesome'

import Api from 'Services/Api'
import { Header } from 'Composition'

import styles from 'Services/Styles'

class RegistrationScreen extends Component {
  constructor(props){
    super(props)

    this.state = {
      phone:'7',
      password:'',
      repeatPassword:'',
      firstname:'',

      checkStatus:false
    }
  }
  backNavigation(){
    return this.props.navigation.goBack()
  }

  regUser(){

    const { type } = this.props.navigation.state.params
    const { dispatch, navigation } = this.props
    const { city_select } = this.props.locations

    const {
      phone,
      password,
      repeatPassword,
      firstname,
      checkStatus
    } = this.state

    const params = {
      phone,
      password,
      repeatPassword,
      firstname,
      type,
      checkStatus,
      city_id:city_select ? city_select.id : undefined
    }

    return Api.auth.registration(dispatch, navigation, params)

  }

  getCity(){

    const { navigate } = this.props.navigation
    return navigate('location')

  }

  readLicense(){

    return Linking.openURL(`${config.public_url}/license.pdf`)

  }

  render(){
    let { request } = this.props.auth
    let { city_select } = this.props.locations

    return(
      <View style={styles.AuthScreen.regView}>
        <Header isBack={true} navigation={this.props.navigation} title={'РЕГИСТРАЦИЯ'}/>
        <ScrollView
          keyboardShouldPersistTaps="always"
        >
          <View style={{paddingBottom:200}} >
          <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss}}>
            <View style={styles.AuthScreen.mainViewForm}>

              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="user" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInput onChangeText={(firstname)=>{this.setState({firstname})}} underlineColorAndroid='transparent' style={styles.AuthScreen.inputForm} placeholder="Имя"/>
              </View>

              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="unlock-alt" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInput 
                secureTextEntry={true} 
                onChangeText={(password)=>{this.setState({password})}} 
                underlineColorAndroid='transparent' 
                style={styles.AuthScreen.inputForm} 
                placeholder="Пароль"/>
              </View>

              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="unlock-alt" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInput 
                  secureTextEntry={true} 
                  onChangeText={(repeatPassword)=>{this.setState({repeatPassword})}} 
                  underlineColorAndroid='transparent' 
                  style={styles.AuthScreen.inputForm} 
                  placeholder="Повторите пароль"
                />
              </View>

              <View style={styles.AuthScreen.inputFormView}>
                <Icon name="mobile" style={styles.AuthScreen.inputFormIcon} size={30} color="#a69490"/>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    withDDD: true,
                    dddMask: '+9 (999) 999 99 99'
                  }}
                  maxLength={18}
                  value={this.state.phone}
                  placeholder="Номер телефона"
                  underlineColorAndroid='transparent'
                  style={styles.AuthScreen.inputForm}
                  refInput={ref => { this.input = ref }}
                  onChangeText={(phone) => {
                    this.setState({phone})
                  }}
                />              
              </View>

              <TouchableOpacity style={styles.AuthScreen.inputFormViewCity} onPress={this.getCity.bind(this)}>
                {
                  city_select ?
                    <Text style={styles.AuthScreen.inputFormViewCitytitle}>{ city_select.title }</Text>
                  :
                    <Text style={styles.AuthScreen.inputFormViewCitytitle}>Выбрать город</Text>
                }
              </TouchableOpacity>

              <View style={styles.AuthScreen.checkButtonView}>
                <TouchableOpacity style={styles.AuthScreen.checkButtonViewInfo} onPress={this.readLicense.bind(this)}>
                  <Text style={styles.AuthScreen.checkButtonViewInfoReadText}>Я принимаю условия соглашения и конфиденциальности информации</Text>
                  <Text style={styles.AuthScreen.checkButtonViewInfoReadTitle}>Читать</Text>
                </TouchableOpacity>
                <Switch style={styles.AuthScreen.checkButtonViewInfoSwitch} value={this.state.checkStatus} onValueChange={(value)=>{this.setState({checkStatus:value})}}/>
              </View>

              {
                !request ? 
                  <TouchableOpacity onPress={this.regUser.bind(this)} style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ЗАРЕГИСТРИРОВАТЬСЯ</Text>
                  </TouchableOpacity>
                :
                  <TouchableOpacity style={styles.AuthScreen.regButton}>
                    <Text style={styles.AuthScreen.regButtonText}>ПОДОЖДИТЕ...</Text>
                  </TouchableOpacity>
              }

            </View>
          </TouchableWithoutFeedback>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth:state.auth,
    locations:state.locations
  }
}

export default connect(mapStateToProps)(RegistrationScreen)