import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native'

import styles from 'Services/Styles'

class SelectTypeUserScreen extends Component {

  goToScreen(type){
    return this.props.navigation.navigate('registration', {
      type
    })
  }

  goToVacancies(){
    return this.props.navigation.navigate('vacanciesForClient')  
  }

  render(){
    return(
      <View style={styles.AuthScreen.selectTypesView}>

        <TouchableOpacity style={styles.AuthScreen.clientButtonVacancies} onPress={this.goToVacancies.bind(this)}>
          <Image style={styles.AuthScreen.clientButtonVacanciesImage} source={require('Media/vakansy-2.png')}/>
          <Text style={styles.AuthScreen.clientButtonVacanciesTitle}>Вакансии</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.goToScreen.bind(this, 'client')} activeOpacity={0.83} style={styles.AuthScreen.clientButton}>
          <Image style={styles.AuthScreen.buttonSelectImage} source={require('Media/carClient.png')}/>
          <Text style={styles.AuthScreen.buttonTitle}>КЛИЕНТ</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.goToScreen.bind(this, 'merchant')} activeOpacity={0.83} style={styles.AuthScreen.merchantButton}>
          <Image style={styles.AuthScreen.buttonSelectImage} source={require('Media/carMerch.png')}/>
          <Text style={styles.AuthScreen.buttonTitle}>АВТОМОЙКА</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    
  }
}

export default connect(mapStateToProps)(SelectTypeUserScreen)