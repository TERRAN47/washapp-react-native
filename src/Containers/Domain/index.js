import mapStyle from './map-style.json'
import dataPrices from './prices'

export {
	mapStyle,
	dataPrices
}