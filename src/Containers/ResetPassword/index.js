import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  ImageBackground
} from 'react-native'
import { Header, NotFound } from 'Composition'
import { MerchantComponents } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'

class ResetPasswordScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
      password:'',
      newPassword:'',
      repeatNewPassword:''
    }
  }

  resetPassword(){

    let {
      password,
      newPassword,
      repeatNewPassword 
    } = this.state

    const { dispatch, navigation } = this.props

    return Api.auth.resetPasswordFromProfile(dispatch, navigation, password, newPassword, repeatNewPassword)

  }

  render(){

    let { user } = this.props.user
    let { request } = this.props.auth

    return(

        <View style={styles.merchant.cabinetMerchant.viewBack}>

          <Header navigation={this.props.navigation} isBack={true} title='Сброс пароля' />

          <View style={styles.merchant.cabinetMerchant.mainStyleViewResetPass}>

            <View style={styles.merchant.cabinetMerchant.sectionResetPass}>
              <Text style={styles.merchant.cabinetMerchant.sectionResetPassTitle}>Введите действующий пароль</Text>
              <TextInput secureTextEntry={true} onChangeText={(value)=>this.setState({password:value})}/>
            </View>

            <View style={styles.merchant.cabinetMerchant.sectionResetPass}>
              <Text style={styles.merchant.cabinetMerchant.sectionResetPassTitle}>Придумайте новый пароль</Text>
              <TextInput secureTextEntry={true} onChangeText={(value)=>this.setState({newPassword:value})}/>
            </View>

            <View style={styles.merchant.cabinetMerchant.sectionResetPass}>
              <Text style={styles.merchant.cabinetMerchant.sectionResetPassTitle}>Повторите новый пароль</Text>
              <TextInput secureTextEntry={true} onChangeText={(value)=>this.setState({repeatNewPassword:value})}/>
            </View>

            {
              request ?
                <TouchableOpacity style={styles.merchant.cabinetMerchant.sectionResetPassButton}>
                  <Text style={styles.merchant.cabinetMerchant.sectionResetPassButtonTitle}>Подождите...</Text>
                </TouchableOpacity>
              :
              <TouchableOpacity style={styles.merchant.cabinetMerchant.sectionResetPassButton} onPress={this.resetPassword.bind(this)}>
                <Text style={styles.merchant.cabinetMerchant.sectionResetPassButtonTitle}>Сбросить пароль</Text>
              </TouchableOpacity>
            }

          </View>

        </View>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    auth:state.auth,
  }
}

export default connect(mapStateToProps)(ResetPasswordScreen)