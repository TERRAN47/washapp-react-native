import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import underscore from 'underscore'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import { NavigationActions, StackActions } from 'react-navigation'

class HomeScreen extends Component {

  componentDidMount(){

    const { navigation, dispatch } = this.props
    const { user} = this.props.user

    this.getCoordsUser()
    Api.auth.getLocations(dispatch)
    let { locations } = this.props.locations
    let findCity = underscore.findWhere(locations, {
      id:user.city_id
    });

    Api.auth.saveCity(navigation, dispatch, findCity);

    navigation.dispatch(StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({routeName: 'profile'})]
    }));
  }

  getCoordsUser(){

    const { dispatch } = this.props

    return navigator.geolocation.getCurrentPosition((data)=>{
      return Api.maps.saveUserGeo(dispatch, data.coords.latitude, data.coords.longitude)
    })
  }

  render(){

    return(
      <View style={styles.userscreen.viewBack}>
        <View style={{flex:1}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Text>ЗАГРУЗКА</Text>
          </ScrollView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    locations:state.locations
  }
}

export default connect(mapStateToProps)(HomeScreen)