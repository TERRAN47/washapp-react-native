import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ListView,
  Image,
  ImageBackground
} from 'react-native'
import { Header } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import {ListBonuses} from 'Components/User'
import underscore from 'underscore'

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class Bonus extends Component {

	componentDidMount(){
    let {dispatch} = this.props
    Api.user.bonus.getBonuses(dispatch) 
	}
	
	render(){
  	const { navigation } = this.props
  	let {bonuses, countBonuses} = this.props.clientBonus

    if(bonuses) bonuses = ds.cloneWithRows(bonuses)

    return(

        <View style={styles.profilescreen.viewBack}>
          <Header navigation={this.props.navigation} menu={true} title="Бонусы" />
          <View style={styles.carWashes.mainView}>
        		{
        			countBonuses > 0 && bonuses ?
				      	<ListView
    							dataSource={bonuses}
    							enableEmptySections={true}
    							keyExtractor={(item, index) => item.id}
    							renderRow={(elem) => 
    								<ListBonuses navigation={this.props.navigation} item={elem} />
    				      }
		      			/>	          				
        			:
                countBonuses == 0 ?
          				<View style={styles.profilescreen.view}>
                    <Text>Бонусов нет</Text>
                  </View>
                :
                  <View style={styles.profilescreen.view}>
                    <Text>ПОИСК...</Text>
                  </View>         
        		}
		     </View>
        </View>
    )
	}
}

const mapStateToProps = (state) => {
  return {
    clientBonus:state.clientBonus,
    user:state.user,
    maps:state.maps
  }
}

export default connect(mapStateToProps)(Bonus)