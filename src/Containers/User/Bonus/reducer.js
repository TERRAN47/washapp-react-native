

const initialState = {
	bonuses:[],
	countBonuses:-1
}

const clientBonus = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_BONUS' :
			return {
				...state,
				bonuses:action.bonuses,
				countBonuses:action.countBonuses
			}
		
		default :
			return state
	}

}

export default clientBonus