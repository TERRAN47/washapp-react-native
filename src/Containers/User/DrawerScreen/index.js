import React, { Component } from 'react'
import { 
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native'
import { connect } from 'react-redux'
//import { NavigationActions, StackActions } from 'react-navigation'

import Api from 'Services/Api'
//import config from 'Config'
import styles from 'Services/Styles'

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {constans, icons} from 'Services/Utils'

class DrawerScreen extends Component {
  componentDidMount(){

    this.updateTokenForClient()
  }

  updateTokenForClient(){

    return Api.updateAxiosConfig()
  }

  async logoutUser(){
    const { navigation, dispatch } = this.props
    return Api.auth.logout(dispatch, navigation, Api)
  }

  goToScreen(screen){
    const { navigation, dispatch } = this.props
    if(screen == "favorites"){
      dispatch({
        type:'FAVORITE',
        favoriteStatus:true
      })
    }else if(screen == 'priceList'){
      dispatch({
        type:'FAVORITE',
        favoriteStatus:false
      })      
    }
    return navigation.navigate(screen)  
  }

  switchAccountType(){
    const { navigation, dispatch, user } = this.props
    return Api.merchant.main.switchAccountType(navigation, dispatch, user.type_account, 'merchant')
  }

  managerAccount(){
    const { navigation, dispatch, user } = this.props
    return Api.merchant.main.managerAccount(navigation, dispatch, 'manager') 
  }

  render () {

    const { user } = this.props

    return (
      <View style={styles.drawerClient.viewBack}>
        <ScrollView>
          <TouchableOpacity onPress={this.goToScreen.bind(this, "profile")} style={styles.drawerClient.buttonDrawer}>
            <View style={styles.drawerClient.iconDrawer}>
              <MaterialCommunityIcons name="account-edit" size={28} color={constans.orangeColor} />
            </View>
            <Text style={styles.drawerClient.drawerTitleLink}>Профиль</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.goToScreen.bind(this, "priceList")} style={styles.drawerClient.buttonDrawer}>
            <View style={styles.drawerClient.iconDrawer}>
              <MaterialCommunityIcons name="map-search" size={28} color={constans.orangeColor} />
            </View>
            <Text style={styles.drawerClient.drawerTitleLink}>Поиск</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.goToScreen.bind(this, "bonus")} style={styles.drawerClient.buttonDrawer}>
            <View style={styles.drawerClient.iconDrawer}>
              <Ionicons name="ios-gift" size={28} color={constans.orangeColor} />
            </View>
            <Text style={styles.drawerClient.drawerTitleLink}>Бонусы</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.goToScreen.bind(this, "favorites")} style={styles.drawerClient.buttonDrawer}>
            <View style={styles.drawerClient.iconDrawer}>
             <FontAwesome name="star" size={26} color={constans.orangeColor} />
            </View>
            <Text style={styles.drawerClient.drawerTitleLink}>Избранное</Text>
          </TouchableOpacity> 

          {
            user.status_admin ?
              <TouchableOpacity onPress={this.managerAccount.bind(this)} style={styles.drawerClient.buttonDrawer}>
                <View style={styles.drawerClient.iconDrawer}>
                  <Image style={styles.drawerClient.iconDrawer} source={icons.switchUser}/>
                </View>
                <Text style={styles.drawerClient.drawerTitleLink}>Менеджер аккаунт</Text>
              </TouchableOpacity>
            :
              <TouchableOpacity onPress={this.switchAccountType.bind(this)} style={styles.drawerClient.buttonDrawer}>
                <View style={styles.drawerClient.iconDrawer}>
                  <Image style={styles.drawerClient.iconDrawer} source={icons.switchUser}/>
                </View>
                <Text style={styles.drawerClient.drawerTitleLink}>Переключиться</Text>
              </TouchableOpacity>        
          }

          <TouchableOpacity onPress={this.logoutUser.bind(this)} style={styles.drawerClient.buttonDrawer}>
            <View style={styles.drawerClient.iconDrawer}>
              <MaterialCommunityIcons name="logout" size={28} color={constans.orangeColor} />
            </View>
            <Text style={styles.drawerClient.drawerTitleLink}>Выход</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    user:state.user.user
  }
}

export default connect(mapStateToProps)(DrawerScreen)
