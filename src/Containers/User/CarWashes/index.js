import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ListView,
  Image,
  ImageBackground
} from 'react-native'
import { Header, Loader } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import {ListCompanies} from 'Components/User'
//import underscore from 'underscore'

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class CarWashes extends PureComponent {

	componentDidMount(){
		let {dispatch, user, maps} = this.props
		let {selectServices, activCar} = this.props.carWash
    let {params} = this.props.navigation.state
    
    if(params){
      Api.user.favorites.getFavorites(dispatch, maps.userGeo, activCar.avtoType.typeName, selectServices, user.user.favorites) 
    }else{
      Api.user.serchCarwash.getCarwash(dispatch, maps.userGeo, activCar.avtoType.typeName, selectServices, user.user)
    }	
	}
	
	render(){
  	const { navigation, dispatch } = this.props
  	let {carWashes, countCarWashes} = this.props.carWash
    let {params} = this.props.navigation.state
    let {request} = this.props.auth

    if(carWashes) carWashes = ds.cloneWithRows(carWashes)

    return(

        <View style={styles.profilescreen.viewBack}>

          <Header navigation={navigation} serchFilter={params ? false : true} isBack={true} title={params ? 'Избранное' : 'Автомойки'}/>
          <View style={styles.carWashes.mainView}>
        		{
              request ? 
                <Loader title="Загрузка" /> 
              :
                countCarWashes > 0 && carWashes ?
                  <ListView
                    dataSource={carWashes}
                    enableEmptySections={true}
                    keyExtractor={(item, index) => item.id}
                    renderRow={(elem) => 
                      <ListCompanies navigation={navigation} dispatch={dispatch} item={elem} />
                    }
                  />                    
                :
                  <View style={styles.profilescreen.view}>
                    <Text>{params ? "Избранного нет" : "Нет в Вашем городе"}</Text>
                  </View>
        		}
		     </View>
        </View>
    )
	}
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    user:state.user,
    auth:state.auth,
    maps:state.maps
  }
}

export default connect(mapStateToProps)(CarWashes)