import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Linking,
  ListView,
  Image,
  ImageBackground,
  Platform
} from 'react-native'
import call from 'react-native-phone-call'
import { Header, MyText } from 'Composition'
import { MapsView } from 'Components'
import PhoneModal from 'Components/Modal/phonesModal'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import Foundation from 'react-native-vector-icons/Foundation'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {constans, renderDistance, icons} from 'Services/Utils'
import config from 'Config'
import underscore from 'underscore'

class TargetCartWash extends PureComponent {

	constructor(props) {
	  	super(props);

	  	this.state = {
	  		favorite:-1,
	  		phoneStatus:false,
	  		phones:[]
	  	}
	}

	componentDidMount(){
		let {user} = this.props.user
		
		try{
			let item = this.props.carWash.targetCompany
			let findFavotite = underscore.indexOf(user.favorites, item.id)
			this.setState({favorite:findFavotite})
		}catch(err){
			alert('Ошибка')
		}
	}

	shouldComponentUpdate(nextProps, nextState){
		return nextState.favorite !== this.state.favorite ||
		nextState.phoneStatus !== this.state.phoneStatus	
	}

	createApplication(company){
		let {dispatch, navigation} = this.props
		let {user} = this.props.user
		let item = this.props.carWash.targetCompany
		let {activCar, selectServices} = this.props.carWash
		let queue = {user_id:user.id, services:selectServices, custom_status:false, car:activCar, user_device_id:user.device_ids, branch_id:company.branch_id, company_id:company.id}

		Api.user.orders.createQueue(dispatch, navigation, item, queue, company)
	}
	
	callCompany(phones){
		if(phones.length > 1){
			this.setState({phoneStatus:true, phones})
		}else{

			if(Platform.OS === 'android'){
				return Linking.openURL(`tel:+7${phones[0]}`)
			}else{
				const args = {
					number: `+7${phones[0]}`
				}
				return call(args).catch(console.error)
			}
		}
	}

	addFavorite(id){
		let {dispatch} = this.props
		let {user} = this.props.user
		user.favorites.push(id)
		this.setState({favorite:id})
		Api.user.userProfile.updateUser(dispatch, user)
	}

	removeFavorite(id){
		let {dispatch, navigation} = this.props
		let {favorites, user} = this.props.user

		Api.user.favorites.removeFavorite(dispatch, navigation, favorites, user, id)
		this.setState({favorite:-1})
	}

	closeModalPhone(){
		this.setState({phoneStatus:false})
 	}

  	render(){
    	let item = this.props.carWash.targetCompany
    	let {favorite, phoneStatus, phones} = this.state
    	let {request} = this.props.auth

	    return(
	        <View style={styles.profilescreen.viewBack}>
	          	<Header navigation={this.props.navigation} isBack={true} title='Автомойка' />
		        {
		          phoneStatus ?
		          <PhoneModal closeModalPhone={this.closeModalPhone.bind(this)} phoneStatus={phoneStatus} phones={phones} />
		          : null
		        }
		        {
		        	item ?
			          	<ScrollView showsVerticalScrollIndicator={false}>
			       			<ImageBackground 
			        			source={{uri:`${config.public_url}${item.logo}`}}  
			        			style={styles.carWashes.headerImage} 
			        		>
					          	<View style={styles.carWashes.headerBlock}>
					          		<View style={styles.carWashes.infoBlock}>
					          			<View style={styles.carWashes.centerItems}>
					          				<FontAwesome5 
					        					name="coins" 
					        					style={styles.profilescreen.iconText} 
					        					size={20} 
					        					color="#fff"
					        				/>
					        				<MyText style={{color:'#fff'}}>{item.summ} тг</MyText>
					          			</View>

						          			<View style={styles.carWashes.centerItems}>
							       				<FontAwesome5
						        					name="map-marker-alt" 
						        					style={styles.profilescreen.iconText} 
						        					size={20} color="#fff"
							        			/>
							        			<MyText style={{color:'#fff'}}>{item.adress}</MyText>
						          			</View>	
						          			{
						          				item.distance ?
								          			<View style={styles.carWashes.centerItems}>
								        				<Image
									            			style={styles.carWashes.washIcon} 
									            			source={icons.pointWrite} 
									            		/>
								        				<MyText style={styles.carWashes.headerText}>  {renderDistance(item.distance)}</MyText>
								          			</View>
							          			:
							          				null
						          			}
					          		</View>
							    
				        			<View style={styles.carWashes.rowServiceDeral}>
				        				<View>
				            				<View style={styles.carWashes.washDetalService}>
							            		<Image
							            			style={styles.carWashes.imageService} 
							            			source={icons.countBox} 
							            		/>
				            				</View>
				        					<Text style={styles.carWashes.headerText}>{item.freeBox} / {item.boxes.length}</Text>
				        				</View>
				        				<View>
				            				<View style={styles.carWashes.washDetalService}>
							            		<Image
							            			style={styles.carWashes.imageService} 
							            			source={icons.countTurn} 
							            		/>
							            	</View>
							            	<Text style={styles.carWashes.headerText}>{item.queue}</Text>
						            	</View>
						            	<View>
						            		<View style={styles.carWashes.washDetalService}>
							            		<Image
													style={styles.carWashes.imageService} 
							            			source={icons.timeWork} 
							            		/>
							            	</View>
							            	{
							            		item.work_time.dayNight ?
							            			<Text style={styles.carWashes.headerText}>24/7</Text>
							            		:
								            		<View>
								            			<MyText style={styles.carWashes.headerText}>{item.work_time.start}</MyText>
							            				<MyText style={styles.carWashes.headerText}>{item.work_time.end}</MyText>
								            		</View>
							            	}
						            	</View>
						            	<View>
						            		<View style={item.additionally[0].status ? styles.carWashes.washDetalService : [styles.carWashes.washDetalService, {backgroundColor:'#dedede'}]}>
							            		<Image
							            			style={styles.carWashes.imageService} 
							            			source={icons.waitRoom} 
							            		/>
							            	</View>
						            	</View>
					            		<View style={item.additionally[1].status ? styles.carWashes.washDetalService : [styles.carWashes.washDetalService, {backgroundColor:'#dedede'}]}>
						            		<Image
						            			style={styles.carWashes.imageService} 
						            			source={icons.cofeRoom} 
						            		/>
					            		</View> 
					            		<View style={item.additionally[2].status ? styles.carWashes.washDetalService : [styles.carWashes.washDetalService, {backgroundColor:'#dedede'}]}>
						            		<Image
						            			style={styles.carWashes.imageService} 
						            			source={icons.game} 
						            		/>
					            		</View>
				        			</View>
				        		</View>
						    </ImageBackground>
						    <View style={styles.carWashes.washDetalBody}>
						    	<View style={styles.carWashes.flexBlock}>
							    	<View style={styles.carWashes.blockButtons}>
							    		<TouchableOpacity onPress={this.callCompany.bind(this, item.phones)} style={styles.carWashes.callButton}>
							    			<MyText style={{color:'#666'}}>ПОЗВОНИТЬ</MyText>
					          				<FontAwesome5 
					        					name="phone" 
					        					style={styles.carWashes.iconButton} 
					        					size={18} 
					        					color="#666"
					        				/>
							    		</TouchableOpacity>
							    		{
							    			request ?
							    				<View style={styles.carWashes.writeButton}>
							    					<MyText  style={{color:'#fff'}}>...ИДЕТ ЗАПИСЬ</MyText>
							    				</View>
							    			:
									    		<TouchableOpacity onPress={this.createApplication.bind(this, item)} style={styles.carWashes.writeButton}>
									    			<MyText style={{color:'#fff'}}>ЗАПИСАТЬСЯ</MyText>
							          				<Foundation 
							        					name="page-edit" 
							        					style={styles.carWashes.iconButton} 
							        					size={20} 
							        					color="#fff"
							        				/>
									    		</TouchableOpacity>
							    		}
							    	</View>	
						    	</View>
						    	<MyText  style={styles.carWashes.title}>{item.title}</MyText>
							    <View style={styles.carWashes.blockDescript}>					    	
							    	<MyText style={{textAlign: 'justify'}}>{`${item.description}`}</MyText>
							    	<View style={{flexDirection: 'row', marginTop:10, flexWrap: 'wrap'}}>
							    		<MyText>У нас есть: </MyText>
								    	{
								    		item.additionally.map((element, index)=>{
								    			if(element.status){
									    			return(
									    				<MyText key={index} >{element.title}, </MyText>
									    			)
								    			}
								    		})
								    	}					    		
							    	</View>
							    	{
							    		item.bonus.length > 0 &&
							    		<View>
									    	<MyText style={{fontSize:18, textAlign: 'center'}}>Наши бонусы</MyText>
									    	<View>
										    	{
										    		item.bonus.map((element, index)=>{
										    			return(
										    				<MyText key={index} >{element.description}.</MyText>
										    			)
										    		})
										    	}					    		
									    	</View>						    			
							    		</View>
							    	}
							    </View>
							    {
							    	favorite > -1 ?
							    		<TouchableOpacity onPress={this.removeFavorite.bind(this, item.id)} style={styles.carWashes.flexBlock}>
							    			<MyText style={styles.carWashes.textFavirites}>Убрать из избранного</MyText>
							    		</TouchableOpacity>
									:
									    <TouchableOpacity onPress={this.addFavorite.bind(this, item.id)} style={styles.carWashes.flexBlock}>
						            		<Image
						            			style={styles.carWashes.favoriteIcont} 
						            			source={icons.favorites} 
						            		/>
						            		<MyText style={styles.carWashes.textFavirites}>Добавить в избранное</MyText>
									    </TouchableOpacity>
							    }

							   <MapsView height={300} title="На карте" coords={{latitude:item.location.coordinates[0], longitude:item.location.coordinates[1]}} />

						    </View>
						</ScrollView>
		        	:
		        		<View></View>
		        }
	        </View>
	    )
  	}
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    user:state.user,
    auth:state.auth
  }
}

export default connect(mapStateToProps)(TargetCartWash)