

const initialState = {
	carWashes:[],
	countCarWashes:null,
	activCar:{},
	services:[],
	modalStatus:false,
	companyPrice:null,
	selectServices:[],
	targetCompany:null
}

const carWash = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_COMPANIES' :
			return {
				...state,
				carWashes:action.carWashes,
				countCarWashes:action.countCarWashes
			}
			
		case 'CLEAR_CARWASH' :
			return {
				...state,
				carWashes:[],
				modalStatus:false,
				companyPrice:null,
				countCarWashes:null,
				selectServices:[],
				activCar:{},
				services:[]
			}

		case 'TARGET_COMPANY' :
			return {
				...state,
				targetCompany:action.targetCompany,
			}

		case 'ORDER_SERVICES' :
			return {
				...state,
				services:action.services,
				selectServices:action.selectServices
			}

		case 'UPDATE_MODAL' :
			return {
				...state,
				modalStatus:action.modalStatus,
			}			
		case 'CLIENT_SERVICES' :
			return {
				...state,
				services:action.services,
			}
		case 'UPDATE_COMP_PRICE' :
			return {
				...state,
				companyPrice:action.companyPrice,
			}

		case 'SELECT_SERVICES' :
			return {
				...state,
				selectServices:action.selectServices,
			}
		case 'ACTIVE_CAR' :
			return {
				...state,
				activCar:action.activCar,
			}		
		
		default :
			return state
	}

}

export default carWash