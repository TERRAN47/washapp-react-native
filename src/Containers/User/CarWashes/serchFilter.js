import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Alert,
  ListView,
  AsyncStorage,
  Image,
  ImageBackground
} from 'react-native'
import { Header, MyText } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {constans, icons} from 'Services/Utils'
import config from 'Config'
import underscore from 'underscore'

class SerchFilter extends PureComponent {
  	constructor(props) {
      super(props);
    
      this.state = {
        distanceFind:8000
      }
  	}

	async getCarWashDist(){
		let {distanceFind} = this.state
		let {dispatch, navigation, user, maps} = this.props

		let {selectServices, activCar} = this.props.carWash
		await Api.user.serchCarwash.getCarwash(dispatch, maps.userGeo, activCar.avtoType.typeName, selectServices, user.user, +(distanceFind))
		navigation.goBack()
	}

  	async sortCarWashes(type, sort){
	    let {carWashes} = this.props.carWash
	    let {dispatch} = this.props

	    if(carWashes.length > 0){
	      await Api.user.serchCarwash.sortCarWash(dispatch, carWashes,  sort, type)
	    }
  	}
  	render(){
  		let {sortSumm, distance, rating} = this.props.records

	    return(

	        <View style={styles.profilescreen.viewBack}>
	          	<Header navigation={this.props.navigation} isBack={true} title="Фильтр" />
	          	<ScrollView showsVerticalScrollIndicator={false}>
	          		<View style={styles.carWashes.filterBlock}>
		          		<View style={styles.carWashes.rowCenterBlock}>
		          			<MyText style={styles.carWashes.servicesText}>Радиус обхвата: </MyText>
			                <TextInput 
			                 	onChangeText={(distanceFind)=>{this.setState({distanceFind})}} 
			                  	underlineColorAndroid='transparent' 
			                  	keyboardType="numeric"
			                  	style={styles.carWashes.inputDistance} 
			                  	placeholder="8000 метров"
			                />
			                <TouchableOpacity onPress={this.getCarWashDist.bind(this)}>
			                	<MyText style={styles.carWashes.findButton}>Поиск</MyText>
			                </TouchableOpacity>
		          		</View>
		          		<View>
		          			<MyText style={styles.carWashes.titleText}>Сортировка</MyText>
		          			<View >
		          				<TouchableOpacity onPress={this.sortCarWashes.bind(this, 'summ', sortSumm)} style={styles.carWashes.rowCenterBlock}>
		          					<MyText style={styles.carWashes.servicesText}>Цена: </MyText>
		          					<FontAwesome name={sortSumm == 'asc' ? 'long-arrow-down' : sortSumm == '' ? 'arrows-v' : 'long-arrow-up'} size={20} />
		          				</TouchableOpacity>
		          				<TouchableOpacity onPress={this.sortCarWashes.bind(this, 'distance', distance)} style={styles.carWashes.rowCenterBlock}>
		          					<MyText style={styles.carWashes.servicesText}>Расстояние: </MyText>
		          					<FontAwesome name={distance == 'asc' ? 'long-arrow-down' : distance == '' ? 'arrows-v' : 'long-arrow-up'} size={20} />
		          				</TouchableOpacity>
		          				<TouchableOpacity onPress={this.sortCarWashes.bind(this, 'rating', rating)} style={styles.carWashes.rowCenterBlock}>
		          					<MyText style={styles.carWashes.servicesText}>Рейтинг: </MyText>
		          					<FontAwesome name={rating == 'asc' ? 'long-arrow-down' : rating == '' ? 'arrows-v' : 'long-arrow-up'} size={20} />
		          				</TouchableOpacity>
		          			</View>
		          		</View>	          			
	          		</View>
				</ScrollView>
	        </View>
	    )
  	}
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    records:state.records,
    user:state.user,
    maps:state.maps
  }
}

export default connect(mapStateToProps)(SerchFilter)