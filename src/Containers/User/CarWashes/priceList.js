import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header, Loader } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
//import {UserComponents} from 'Components'
import {constans, icons} from 'Services/Utils'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import underscore from 'underscore'
import config from 'Config'

class PriceList extends PureComponent {

  constructor(props) {
    super(props);
  
    this.state = {
      renderComp:false,
      isBack:true
    };
  }

	nextScreen(){
    let {dispatch} = this.props
		let {services} = this.props.carWash 
    const { navigate } = this.props.navigation
    let {favoriteStatus} = this.props.records
    let filterServices = underscore.where(services, {
      status:true
    })
    if(filterServices.length > 0){
      dispatch({
        type:'SELECT_SERVICES',
        selectServices:filterServices
      })  
      return navigate('carWashes', favoriteStatus)
    }else{

      Alert.alert('Внимание!', 'Выберите хоть одну услугу!')
    }   
	}
  componentWillUnmount() {
    const { dispatch } = this.props
    dispatch({
      type:'FAVORITE',
      favoriteStatus:false
    })
  }
  openCars(){
    const { navigate } = this.props.navigation
    return navigate('selectMyCar')
  }

	componentDidMount(){

    let {params} = this.props.navigation.state //company_id
    let {companyPrice} = this.props.carWash
    let {dispatch} = this.props
    if(params){
      dispatch({
        type:'CLIENT_SERVICES',
        services:companyPrice
      })
    }else{
      this.updateServices()
    }
	}

  async updateServices(){
    let {recordInfo} = this.props.records
    let {dispatch, navigation, maps} = this.props
    
    let {user} = this.props.user
    
    if(recordInfo){
      await Api.user.serchCarwash.gitCarWashInfo(dispatch, navigation, recordInfo)  
    } 
    this.getCoordsUser()
    
    if(user.cars.length > 0){
      let activCar = underscore.findWhere(user.cars, {
        status:true
      })
      if(activCar){
        Api.user.serchCarwash.getPrices(dispatch, activCar)
      }else{
        dispatch({
          type:"ACTIVE_CAR",
          activCar:"select"
        })
      }        
    }else{
      Alert.alert("Внимание!", 'Добавьте машину для мойки')
      navigation.navigate('profile')
    }    
  }
  getCoordsUser(){
    const { dispatch } = this.props
    return Api.maps.saveUserGeo(dispatch)
  }  

	services(title, status, renderComp){
		
		let {dispatch} = this.props
    let {services} = this.props.carWash
		let findPrice = underscore.findWhere(services, {
			title
		})
    findPrice.status = !status

    if(title == 'КУЗОВ-САЛОН'){
      
      let findKuzov = this.findService('КУЗОВ')
      let findSalon = this.findService('САЛОН')
      let findOtbivka = this.findService('ОТБИВКА')
      let findOtbivkaPenoi = this.findService('ОТБИВКА С ПЕНОЙ')
      let findPoliki = this.findService('ПОЛИКИ 2 ШТ')
      let findPoliki4 = this.findService('ПОЛИКИ 4 ШТ')

      findKuzov.status = false
      findSalon.status = false
      findOtbivka.status = false
      findOtbivkaPenoi.status = false
      findPoliki.status = false
      findPoliki4.status = false

      findOtbivkaPenoi.hidden = !status ? false : true
      findOtbivka.hidden = !status ? false : true
      findSalon.hidden = !status ? false : true
      findKuzov.hidden = !status ? false : true
      findPoliki.hidden = !status ? false : true
      findPoliki4.hidden = !status ? false : true
    }else if(title == 'КУЗОВ'){
      let findKuzovSalon = this.findService('КУЗОВ-САЛОН')
      let findSalon = this.findService('САЛОН')
      let findOtbivka = this.findService('ОТБИВКА')
      let findOtbivkaPenoi = this.findService('ОТБИВКА С ПЕНОЙ')

      findKuzovSalon.status = false
      findOtbivka.status = false
      findOtbivkaPenoi.status = false

      findKuzovSalon.hidden = status && !findSalon.status ? true : false
      findOtbivka.hidden = status && !status ? true : false
      findOtbivkaPenoi.hidden = status && !status ? true : false
    }else if(title == 'САЛОН'){
      let findKuzovSalon = this.findService('КУЗОВ-САЛОН')
      let findKuzov = this.findService('КУЗОВ')
      let findPoliki = this.findService('ПОЛИКИ 2 ШТ')
      let findPoliki4 = this.findService('ПОЛИКИ 4 ШТ')

      findKuzovSalon.status = false
      findPoliki.status = false
      findPoliki4.status = false

      findKuzovSalon.hidden = status && !findKuzov.status ? true : false
      findPoliki.hidden = !status ? false : true
      findPoliki4.hidden = !status ? false : true
    }      
    
    this.setState({renderComp:!renderComp, isBack:false})
    dispatch({
      type:'ORDER_SERVICES',
      services,
      selectServices:services
    })
	}
  
  findService(title){
    let {services} = this.props.carWash
    return underscore.findWhere(services, {
      title
    })     
  }

  saveServices(){
    let {services, activCar} = this.props.carWash
    activCar.avtoType.prices
    let {dispatch, navigation} = this.props
    let {user} = this.props.user
    let {params} = this.props.navigation.state //company_id

    Api.user.orders.updateQeueServices(dispatch, navigation, params, user.id, services)
  }

	render(){
  	const { navigation } = this.props
  	let {services, activCar} = this.props.carWash
    let {params} = this.props.navigation.state
    let {request} = this.props.auth
    let {renderComp, isBack} = this.state

    return(
      <View style={styles.profilescreen.viewBack}>
        {
          params ?
            
            <Header navigation={navigation}  isBack={isBack} title={'Выбор услуги'}/>
          :
            <Header navigation={navigation}  menu={true} title={'Выбор услуги'}/>
        }
        <ScrollView>
        {
          activCar && activCar.avtoType && params == undefined &&          
            <TouchableOpacity onPress={this.openCars.bind(this)} style={styles.profilescreen.myCarItem}>
              <View style={styles.profilescreen.imgBlock}>
                <Image 
                  source={{uri: `${config.public_url}${activCar.autoMarka.logo}`}}  
                  style={styles.profilescreen.carImage}
                />              
              </View>
              <View style={styles.profilescreen.flexBlock}>
                <View style={styles.profilescreen.carModel}>
                  <View style={styles.profilescreen.flexBlock}>
                    <Text>{activCar.autoModel}</Text>
                  </View>
                  <View style={styles.profilescreen.flexBlock}>
                    <Text>{activCar.autoNum}</Text>
                  </View>
                </View>
                <View style={styles.profilescreen.carInfo}>
                  <View style={styles.profilescreen.flexBlock}>
                    <Image 
                      source={{uri: `${config.public_url}${activCar.avtoType.image}`}}  
                      style={styles.profilescreen.carImage}
                    />
                  </View>
                  <View style={styles.profilescreen.flexBlock}>
                    <View style={[styles.profilescreen.autoColor, {backgroundColor: activCar.autoColor}]} />
                  </View>                           
                </View>                         
              </View>
            </TouchableOpacity>
        }  
        {
          activCar == 'select' &&
          <TouchableOpacity onPress={this.openCars.bind(this)} style={styles.profilescreen.myCarItem}>
            <Text>Выберите машину</Text>
          </TouchableOpacity>
        }   

          <View style={styles.carWashes.priceServicesBlock}>
          {
            request ?
              <Loader title="Загрузка" />
            :
              services && services.map((item, index)=>{
                if(item.hidden == undefined || item.hidden){
                  return(
                    <View key={index} style={styles.carWashes.priceList}>
                      <Text style={styles.carWashes.servicesText}>{item.title}</Text>
                      <TouchableOpacity onPress={this.services.bind(this, item.title, item.status, renderComp)}>
                        <FontAwesome size={30} color={constans.orangeColor} name={item.status ? 'toggle-on' : 'toggle-off'} />
                      </TouchableOpacity>
                    </View>
                  )
                }
              })
          }            
          </View>              
        </ScrollView>
        {
          request ?
            <View style={styles.carWashes.priceFooter}>          
              <Text style={styles.carWashes.servicesNext}>ОЖИДАЙТЕ...</Text>
            </View>
          :
            params ?
              <TouchableOpacity style={styles.carWashes.priceFooter} onPress={this.saveServices.bind(this)}>          
                <Text style={styles.carWashes.servicesNext}>СОХРАНИТЬ</Text>
              </TouchableOpacity>
            :
              <TouchableOpacity style={styles.carWashes.priceFooter} onPress={this.nextScreen.bind(this)}>          
                <Text style={styles.carWashes.servicesNext}>ДАЛЕЕ</Text>
              </TouchableOpacity>   	 
        }   
      </View>
    )
	}
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    user:state.user,
    records:state.records,
    auth:state.auth,
    maps:state.maps
  }
}

export default connect(mapStateToProps)(PriceList)