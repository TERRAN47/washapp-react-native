import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native'
import { Header } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import {UserComponents} from 'Components'
import {constans, icons} from 'Services/Utils'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import underscore from 'underscore'
import config from 'Config'

class SelectMyCar extends PureComponent {

  constructor(props) {
    super(props);
  
    this.state = {
      activCar:[]
    };
  }

	selectCar(car){
		const {dispatch, navigation} = this.props
		let {user} = this.props.user

        let activCar = underscore.findWhere(user.cars, {
          autoNum:car.autoNum,
          autoModel:car.autoModel
        })
        activCar.status = true
	    dispatch({
	      type:"ACTIVE_CAR",
	      activCar
	    })
	    Api.user.userProfile.updateUser(dispatch, user)
	    Api.user.serchCarwash.getPrices(dispatch, activCar)
    	return navigation.goBack()
	}

	render(){
  	let {user} = this.props.user

    return(

        <View style={styles.profilescreen.viewBack}>
        	<Header navigation={this.props.navigation}  isBack={true} title={'Выбор машины'}/>  
          	<ScrollView>
          	<View style={styles.carWashes.mainView}>
          	{
            	user.cars.map((item, index)=>{
	              return(
		              <TouchableOpacity onPress={this.selectCar.bind(this, item)} style={styles.profilescreen.myCarItem}>
		                <View style={styles.profilescreen.imgBlock}>
		                  <Text></Text>
		                  <Image 
		                    source={{uri: `${config.public_url}${item.autoMarka.logo}`}}  
		                    style={styles.profilescreen.carImage}
		                  />              
		                </View>
		                <View style={styles.profilescreen.flexBlock}>
		                  <View style={styles.profilescreen.carModel}>
		                    <View style={styles.profilescreen.flexBlock}>
		                      <Text>{item.autoModel}</Text>
		                    </View>
		                    <View style={styles.profilescreen.flexBlock}>
		                      <Text>{item.autoNum}</Text>
		                    </View>
		                  </View>
		                  <View style={styles.profilescreen.carInfo}>
		                    <View style={styles.profilescreen.flexBlock}>
		                      <Text>{item.avtoType.typeName}</Text>
		                    </View>
		                    <View style={styles.profilescreen.flexBlock}>
		                      <View style={[styles.profilescreen.autoColor, {backgroundColor: item.autoColor}]} />
		                    </View>                           
		                  </View>                         
		                </View>
		              </TouchableOpacity>
	              )
	            })
          	}           		
          	</View>           
          	</ScrollView>  
        </View>
    )
	}
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
  }
}

export default connect(mapStateToProps)(SelectMyCar)