const initialState = {
	name:'Anonim',
	user:null,
	cars:null,
	city:'',
	favorites:[]
}

const user = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_USER' :
			return {
				...state,
				user:action.user
			}

		case 'UPDATE_MARCHANT_BALANCE' :
			return {
				...state,
				user:{
					...state.user,
					balance:action.balance
				}
			}

		case 'UPDATE_CITY' :
			return {
				...state,
				city:action.city
			}

		case 'SWITCH_TYPE_ACCOUNT' :
			return {
				...state,
				user:{
					...state.user,
					type_account:action.typeAccount
				}
			}
		case 'UPDATE_CARS' :
			return {
				...state,
				cars:action.cars
			}
		case 'UPDATE_FAVORITES' :
			return {
				...state,
				favorites:action.favorites
			}			
		case 'CLEAR_INFO_USER' :
			return {
				...state,
				name:'Anonim',
				user:null,
				cars:null,
				city:'',
				favorites:[]
			}

		default :
			return state
	}

}

export default user