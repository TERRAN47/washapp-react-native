import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Alert,
  Image,
} from 'react-native'
import { Header, Loader, MyText } from 'Composition'
import {ChangePass} from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {constans} from 'Services/Utils'
import config from 'Config'
//import underscore from 'underscore'


class Profile extends Component {

	constructor(props) {
	  	super(props);
	
	    this.state = {
	    	edit:''
	  	};
	}

  	goToScreen(screen){
	    const { navigate } = this.props.navigation
	    return navigate(screen)
  	}

  	componentDidMount(){
	    const { navigation, dispatch } = this.props
		Api.maps.saveUserGeo(dispatch)

		let {recordInfo} = this.props.records
		if(recordInfo == 'ERROR'){
			Alert.alert("Внимание!", "Нет соединения с сервером")
		}else if(recordInfo){
			//console.log('QEUE',recordInfo)
			setTimeout(()=>{
				Api.user.serchCarwash.gitCarWashInfo(dispatch, navigation, recordInfo)
			}, 300)
		}
  	}
  	
  	saveUser(){
  		let {name} = this.state
  		let { dispatch, user } = this.props
  		user.user.firstname = name
  		Api.user.userProfile.updateUser(dispatch, user.user)
  		this.setState({edit:''})
  	}

  	getCity(){

	    const { navigate } = this.props.navigation
	    return navigate('location', 'client')
  	}

  	deleteMyCar(car){
  		let {dispatch, user} = this.props
	    Alert.alert(
	      'Внимание!',
	      'Удалить машину?',
	      [
	        {
	          text: 'Отмена',
	          onPress: () => console.log('Cancel'),
	          style: 'cancel',
	        },
	        {text: 'Удалить', onPress: () => Api.user.userProfile.deletMyCar(dispatch, user.user, car) },
	      ],
	      {cancelable: false},
	    );
  	}

  	openEdit(editName){
		this.setState({edit:editName})

		setTimeout(()=>{
			switch(editName){
				case 'firstName' :
				return this.refs.nameInput.focus()

				default : return
			}
		}, 500)
  	}

  	render(){
    	const { user} = this.props.user
    	const {edit} = this.state 	
    	const myCars = user.cars
    	let {request} = this.props.auth
    	console.log(1,'USER', this.props.user)
	    return(

	        <View style={styles.profilescreen.viewBack}>

	          	<Header navigation={this.props.navigation} menu={true} title={'Профиль'}/>
	          	{
	          		request ?
	          			<Loader title="Загрузка" />
	          		:
			          	<View style={styles.profilescreen.mainView}>
			            <ScrollView showsVerticalScrollIndicator={false}>
			            	<View style={styles.profilescreen.categoryBlock}>
					        <MyText style={styles.profilescreen.titleCat}>Автомобили</MyText>
				            	{
				            		myCars.length > 0 &&
					            		myCars.map((elem, index)=>{
					            			return(
					            				<View key={index} style={styles.profilescreen.myCarItem}>
													<View style={styles.profilescreen.imgBlock}>
														<Image 
															source={{uri: `${config.public_url}${elem.autoMarka.logo}`}}  
															style={styles.profilescreen.carImage}
														/>							
													</View>
													<View style={styles.profilescreen.flexBlock}>
														<View style={styles.profilescreen.carModel}>
															<View style={styles.profilescreen.flexBlock}>
																<MyText>{elem.autoModel}</MyText>
															</View>
															<View style={styles.profilescreen.flexBlock}>
																<MyText>{elem.autoNum}</MyText>
															</View>
														</View>
														<View style={styles.profilescreen.carInfo}>
															<View style={styles.profilescreen.flexBlock}>
										                      <Image 
										                        source={{uri: `${config.public_url}${elem.avtoType.image}`}}  
										                        style={styles.profilescreen.carImage}
										                      />
															</View>
															<View style={styles.profilescreen.flexBlock}>
																<View style={[styles.profilescreen.autoColor, {backgroundColor: elem.autoColor}]} />
															</View>														
														</View>													
													</View>

													<TouchableOpacity onPress={this.deleteMyCar.bind(this, elem)} style={{alignItems:'flex-end', flex:1, justifyContent: 'center'}}>
														<FontAwesome name="close" size={35} color="red" />
													</TouchableOpacity>				
					            				</View>
					            			)
					            		})
				            	}
					            <View style={styles.profilescreen.userInfoBlock}>
					            	<View style={styles.profilescreen.childBLock}>
						              	<Ionicons name="ios-car" style={styles.profilescreen.iconText} size={35} color="#333" />
						              	<MyText>Добавить автомобиль</MyText>		            		
					            	</View>
					            	<TouchableOpacity onPress={this.goToScreen.bind(this, 'addCar')} style={styles.profilescreen.iconRight}>
					            		<Ionicons name="ios-add-circle" size={35} color={constans.orangeColor} />
					            	</TouchableOpacity>
					            </View>	
			            	</View>
			            	<View style={styles.profilescreen.categoryBlock}>
					            <MyText style={styles.profilescreen.titleCat}>Личные данные</MyText>
				            	{
				            		edit == 'firstName' ?
					            		<View style={styles.profilescreen.userInfoBlock}>
							            	<View style={styles.profilescreen.childBLock}>
								              	<FontAwesome name="user" style={[styles.profilescreen.iconText, {marginLeft:4}]} size={30} color="#333" />
									            <TextInput 
									                onChangeText={(name)=>{this.setState({name})}} 
									                underlineColorAndroid='transparent'
									                ref="nameInput"
									               	defaultValue={user.firstname}
									                onSubmitEditing={this.saveUser.bind(this)}
									                style={styles.profilescreen.inputForm} 
									                placeholder={user.firstname}
									            />					
							            	</View>
							            	<TouchableOpacity 
								            	onPress={this.saveUser.bind(this)} 
								            	style={styles.profilescreen.iconRight}
								            >
							            		<FontAwesome name="check-square-o" size={30} color={constans.orangeColor} />
							            	</TouchableOpacity>	
					            		</View>
				            		:
					            		<View style={styles.profilescreen.userInfoBlock}>
							            	<View style={styles.profilescreen.childBLock}>
								              	<FontAwesome name="user" style={[styles.profilescreen.iconText, {marginLeft:4}]} size={30} color="#333" />
								              	<MyText>{user.firstname}</MyText>            		
							            	</View>
							            	<TouchableOpacity 
								            	onPress={this.openEdit.bind(this, 'firstName')} 
								            	style={styles.profilescreen.iconRight}
								            >
							            		<FontAwesome name="edit" size={30} color={constans.orangeColor} />
							            	</TouchableOpacity>	
					            		</View>
				            	}
					            <View style={styles.profilescreen.userInfoBlock}>
					            	<View style={styles.profilescreen.childBLock}>
						              	<Entypo name="mobile" style={styles.profilescreen.iconText} size={30} color="#333" />
						              	<MyText>+7 {user.phone}</MyText>		            		
					            	</View>
					            </View>
					            <View style={styles.profilescreen.userInfoBlock}>
					            	<View style={styles.profilescreen.childBLock}>
						              	<MaterialCommunityIcons name="city" style={styles.profilescreen.iconText} size={30} color="#333" />
						              	<MyText>{user.city.title}</MyText>		            		
					            	</View>
					            	<TouchableOpacity onPress={this.getCity.bind(this)} style={styles.profilescreen.iconRight}>
					            		<FontAwesome name="edit" size={30} color={constans.orangeColor} />
					            	</TouchableOpacity>
					            </View>
			            	</View>
			            	<View style={styles.profilescreen.categoryBlock}>
					            <MyText style={styles.profilescreen.titleCat}>Безопасность</MyText>
					            <View>
					            	<ChangePass client={true} navigation={this.props.navigation}/>
					            </View>	
			            	</View>
			            </ScrollView>
			          	</View>
	          	}
	        </View>
	    )
  	}
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    records:state.records,
    auth:state.auth,
    locations:state.locations
  }
}

export default connect(mapStateToProps)(Profile)