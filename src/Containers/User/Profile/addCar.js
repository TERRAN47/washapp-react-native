import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ListView,
  Alert,
  Modal,
  TextInput,
} from 'react-native'
import { Header, MyText } from 'Composition'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import config from 'Config'
import {constans} from 'Services/Utils'
import SearchInput, { createFilter } from 'react-native-search-filter';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const KEYS_SERCH = ['marka'] 
class AddCar extends Component {

	constructor(props) {
	  	super(props);
		
	    this.state = {
	  		modalVisible:false,
	  		carMaks:[],
	  		filterCarMaks:{},
	  		autoMarka:{},
	  		autoModel:'',
	  		modal:'',
	  		avtoType:{},
	  		autoNum:'',
	  		autoColor:''
	  	};
	}

	selectColor(color){

		this.setState({autoColor:color, modalVisible:false})
	}

	componentDidMount(){
		const {dispatch} = this.props
		Api.user.userProfile.getMarks(dispatch)
	}

	viewColor(){
		let {cars} = this.props.user
		return(
			<View style={styles.profilescreen.colorBlocs}>
			{
				cars.colors.map((item, index)=>{
					return(
						<TouchableOpacity 
							key={index} 
							onPress={this.selectColor.bind(this, item.color)} 
							style={[styles.profilescreen.color, {backgroundColor: item.color}]} 
						/>
					)
				})
			}
			</View>
		)
	}

	searchMarka(term){
		let {carMaks} = this.state
		let filtered = carMaks.filter(createFilter(term, KEYS_SERCH))
		this.setState({filterCarMaks:ds.cloneWithRows(filtered)}) 
	}

	selectModel(marka){
		this.setState({autoMarka:marka, modalVisible:false})
	}

	viewCars(){
		let {filterCarMaks} = this.state
		return(
			<View>
				<View style={styles.profilescreen.serchBlock}>
				    <SearchInput
				        onChangeText={this.searchMarka.bind(this)}
				        style={styles.profilescreen.serch}
				        placeholder="Поиск..."
				    />					
				</View>
		      	<ListView
			        dataSource={filterCarMaks}
			        renderRow={(elem) => 
						<TouchableOpacity onPress={this.selectModel.bind(this, elem)}  style={styles.profilescreen.carItem}>
							<View style={styles.profilescreen.flexBlock}>
								<Image 
									source={{uri: `${config.public_url}${elem.logo}`}}  
									style={styles.profilescreen.carImage}
								/>							
							</View>
							<View style={styles.profilescreen.flexBlock}>
								<MyText style={styles.profilescreen.modelText}>{constans.firtUpCase(elem.marka)}</MyText>						
							</View>
						</TouchableOpacity>	
			        }
		      	/>
			</View>
		)
	}

	openModal(modal){
		if(modal == 'marka'){
			let {cars} = this.props.user
			
			cars && this.setState({modalVisible:true, modal, carMaks:cars.brands, filterCarMaks:ds.cloneWithRows(cars.brands)})			
		}else{
			this.setState({modalVisible:true, modal})
		}
	}
  	goToScreen(screen){
	    const { navigate } = this.props.navigation
	    return navigate(screen)
  	}
  	viewCarType(){
  		let {cars} = this.props.user

  		return(
            <ScrollView showsVerticalScrollIndicator={false}>
            	<View style={styles.profilescreen.categoryBlock}>
            		{
            			cars.bodyworks.map((item, index)=>{
            				return(
								<TouchableOpacity 
									key={index} 
									onPress={this.selectType.bind(this, item.photo, item.title)} 
									style={styles.profilescreen.typeAvtoBlock}
								>
					       			<Image 
			            				style={styles.profilescreen.typeAvtoIcon} 
			            				source={{uri:`${config.public_url}${item.photo}`}}
					        		/>
			            			<MyText style={styles.profilescreen.typeName}>{`${item.title}`}</MyText>
			            		</TouchableOpacity>
            				)
            			})
            		}
            	</View>
            </ScrollView>
  		)
  	}
  	selectType(image, typeName){
  		let { user} = this.props.user
  		const avtoType = {
  			image,
  			typeName
  		}
  		this.setState({avtoType, modalVisible:false})
  	}
	addCar(){
		let {autoNum, autoColor, avtoType, autoMarka, autoModel} = this.state
		let {dispatch, navigation, user} = this.props

		if(autoColor != '' && autoModel != '' && autoMarka.marka && autoNum != ''){
			let cars = user.user.cars
			cars.forEach((elem)=>{
				elem.status = false
			})
			cars.push({autoNum, avtoType, autoColor, autoMarka, phone:user.user.phone, autoModel, status:true})
			Api.user.userProfile.addCar(navigation, dispatch, user.user)			
		}else{
			Alert.alert('Внимание!', 'Заполните все поля!')
		}
	}

  	render(){
	    let { user} = this.props.user	
	    let {autoColor, modal, autoMarka, avtoType, modalVisible} = this.state
	    let {request} = this.props.auth

	    return(

	        <View style={styles.profilescreen.viewBack}>

				<Modal
		            animationType="slide"
		            onRequestClose={()=>{}}
		          	transparent={false}
		          	visible={modalVisible}
				>
		          	<View style={{marginTop: 22}}>			           	
		           		{modal == 'marka' && this.viewCars()}
		           		{modal == 'typeAuto' && this.viewCarType()}	
			           	{modal == 'color' && this.viewColor()}
		          	</View>
		        </Modal>

	          	<Header navigation={this.props.navigation} isBack={true} title={'Добавление автомобиля'}/>

	          	<View style={{flex:1}}>
		            <ScrollView showsVerticalScrollIndicator={false}>
			            <View style={styles.profilescreen.carBlock}>
			            	<TouchableOpacity onPress={this.openModal.bind(this, 'marka')} style={[styles.profilescreen.carButton, {backgroundColor: '#FFCD19', borderWidth: 0}]}>
			            		{
			            			autoMarka.marka ?
			            				<View style={styles.profilescreen.selectMarka}>
											<View style={{marginLeft:10}}>
												<Image 
													source={{uri: `${config.public_url}${autoMarka.logo}`}}  
													style={styles.profilescreen.carImage}
												/>							
											</View>
											<View style={styles.profilescreen.flexBlock}>
												<MyText style={[styles.profilescreen.modelText, {color:'#fff'}]}>{constans.firtUpCase(autoMarka.marka)}</MyText>						
											</View>
			            				</View>
			            			:
			            				<MyText style={{color:'#fff'}}>Марка авто</MyText>
			            		}
			            	</TouchableOpacity>
			            	<TouchableOpacity style={styles.profilescreen.carButton}>
				              <TextInput
				                onChangeText={(autoModel)=>{this.setState({autoModel})}} 
				                underlineColorAndroid='transparent'
				                maxLength={20}
				                style={styles.profilescreen.inputForm} 
				                placeholder="Модель"
				              />
			            	</TouchableOpacity>
			            	<TouchableOpacity onPress={this.openModal.bind(this, 'typeAuto')} style={styles.profilescreen.carButton}>
			            		{
			            			avtoType.typeName ?
										<Image 
											source={{uri: `${config.public_url}${avtoType.image}`}}  
											style={styles.profilescreen.carImage}
										/>	
			            			: 
			            				<MyText>Тип кузова</MyText>
			            		}
			            	</TouchableOpacity>
			            	<TouchableOpacity onPress={this.openModal.bind(this, 'color')} style={styles.profilescreen.carButton}>
			            		{
			            			autoColor ?
			            				<View style={[styles.profilescreen.color, {backgroundColor: autoColor}]} />
			            			:
			            				<MyText>Цвет авто</MyText>
			            		}
			            	</TouchableOpacity>
			            	<View style={styles.profilescreen.carButton}>
				              <TextInput
				                onChangeText={(autoNum)=>{this.setState({autoNum})}} 
				                underlineColorAndroid='transparent'
				                keyboardType='phone-pad'
				                maxLength={6}
				                style={styles.profilescreen.inputForm} 
				                placeholder="Цифры номера"
				              />
			            	</View>
				            {
				              request ? 
				                <TouchableOpacity style={[styles.profilescreen.carButton, {backgroundColor: constans.orangeColor, borderWidth: 0}]}>
				                  <MyText style={{color:'#fff'}}>ПОДОЖДИТЕ...</MyText>
				                </TouchableOpacity>
				              :
				            	<TouchableOpacity onPress={this.addCar.bind(this)} style={[styles.profilescreen.carButton, {backgroundColor: constans.orangeColor, borderWidth: 0}]}>
				            		<MyText style={{color:'#fff'}}>ДОБАВИТЬ</MyText>
				            	</TouchableOpacity>
				            }
			            </View>
		            </ScrollView>
	          	</View>
	        </View>
    	)
    }
}

const mapStateToProps = (state) => {
  return {
    user:state.user,
    auth:state.auth
  }
}

export default connect(mapStateToProps)(AddCar)