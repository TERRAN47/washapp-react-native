const initialState = {
	records:[],
	queue:null,
	recordInfo:null,
	itemCompany:null,
	invitedBox:null,
	favoriteStatus:false,
	sortSumm:'',
	distance:'',
	rating:''
}

const records = (state = initialState, action) => {

	switch(action.type){

		case 'UPDATE_RECORDS' :
			return {
				...state,
				records:action.records
			}	
		case 'UPDATE_SORTSUMM' :
			return {
				...state,
				sortSumm:action.sortSumm
			}
		case 'UPDATE_DISTANCE' :
			return {
				...state,
				distance:action.distance
			}
		case 'UPDATE_RATING' :
			return {
				...state,
				rating:action.rating
			}					
		case 'INVITED_BOX' :
			return {
				...state,
		        invitedBox:action.invitedBox,
			}
		case 'FAVORITE' :
			return {
				...state,
		        favoriteStatus:action.favoriteStatus,
			}
		case 'UPDATE_QEUE' :
			return {
				...state,
				queue:action.queue,
				itemCompany:action.itemCompany
			}
		case 'RECORD_INFO' :
			return {
				...state,
				recordInfo:action.recordInfo,
			}		
		default :
			return state
	}

}

export default records