import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Platform,
  AppState,
  Linking,
  Alert,
  Image,
  ImageBackground
} from 'react-native'
import { Loader, MyText } from 'Composition'
import PhoneModal from 'Components/Modal/phonesModal'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import config from 'Config'
import {icons, socket} from 'Services/Utils'
import underscore from 'underscore'
import ModalRecord from 'Components/Modal/modalRecord'
import call from 'react-native-phone-call'

class MyRecords extends PureComponent {

  constructor(props) {

    super(props)
    this.socket = socket()
    this.state = {
      appState: AppState.currentState,
      firstOpen:true,
      carWash:{},
      driveUpStatus:false,
      content:'',
      myCar:null,
      queueCars:[],
      phoneStatus:false,
      myQueue:0,
      queue:{},
      queueCount:0
    }
  }

  componentWillUnmount() {
    let {dispatch} = this.props
    dispatch({
      type:'UPDATE_MODAL',
      modalStatus:false
    })

    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);

    AppState.removeEventListener('change', this._handleAppStateChange);

    this.socket.disconnect()
  }
  
  callCompany(phones){
    if(phones.length > 1){
      this.setState({phoneStatus:true, phones})
    }else{

      if(Platform.OS === 'android'){

        return Linking.openURL(`tel:+7${phones[0]}`)
      }else{

        const args = {
          number: `+7${phones[0]}`
        }
         
        return call(args).catch(console.error)
      }
    }
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) && nextAppState === 'active'
    ) {
      let {queue} = this.state
      this.socket = socket()  
     // console.log('App has come to the foreground!');     
      this.updateListQueue(queue)
    }else{
      this.socket.disconnect()
    }
    this.setState({appState: nextAppState});
  }

  updateListQueue(queue){
   
    let {navigation}  = this.props
    
    this.socket.on('ERROR', (data)=>{
      Alert.alert('Внимание!', 'Произошла ошибка!')
      navigation.navigate('priceList')
    })

    this.socket.on('updateQueue', (data)=>{
      this.updateQueue(data)
    })
    this.socket.emit('createQueue', queue)
  }

  backPressed = async () => {
    await this.goBack()
    return true;
  }

  componentDidMount(){
    let {queue} = this.props.records
    let { dispatch}  = this.props
    let {companyPrice, services} = this.props.carWash
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    AppState.addEventListener('change', this._handleAppStateChange);

    if(companyPrice && services){
      services.forEach((el)=>{
        companyPrice.forEach((item, key)=>{
          if(el.title === item.title && el.status){
            companyPrice[key].status = true
          }
        })
      })

      let filterServices = underscore.where(companyPrice, {
        status:true
      })

      dispatch({
        type:'ORDER_SERVICES',
        services:companyPrice,
        selectServices:filterServices
      })
      
      queue = {...queue, services:filterServices}
    }
    this.updateListQueue(queue)

    if(Platform.OS === 'ios'){

      setTimeout(()=>{
        this.socket.disconnect()
        this.socket = socket()
        setTimeout(()=>{
          this.updateListQueue(queue)
        },1000)
      }, 2000)
    }
    this.setState({queue})
  }

  updateQueue(data){
    let {user} = this.props.user
    let {dispatch, navigation} = this.props   
    let newArrayCars = []
    let findUser = underscore.findWhere(data.data, {
      user_id:user.id
    })

    let filterQeueBoxStatus = underscore.where(data.data, {
      status_type:'QEUE'
    })

    if(findUser == undefined){     
      dispatch({
        type:'UPDATE_MODAL',
        modalStatus:false
      })
      dispatch({
        type:'RECORD_INFO',
        recordInfo:null
      })
      navigation.navigate('profile')    
    }else if(findUser && findUser.status_type == "BOX" && findUser.invite_status){
      dispatch({
        type:'INVITED_BOX',
        invitedBox:findUser.invited_box,
      })
      navigation.navigate('carInBox')
    }else if(findUser && findUser.status_type == "BOX"){
      dispatch({
        type:'UPDATE_MODAL',
        modalStatus:true
      })
      dispatch({
        type:'INVITED_BOX',
        invitedBox:findUser.invited_box,
      })
      
      this.setState({content:'invate', myCar:findUser, queueCount:filterQeueBoxStatus.length})      
    }else{
      let findIndex = filterQeueBoxStatus.findIndex(car => car.user_id == user.id)
      let lastUser = findIndex+1
     
      if(filterQeueBoxStatus.length > 2){
        if(findIndex == 0){
          newArrayCars = [findUser, filterQeueBoxStatus[findIndex+1]]
        }else if(lastUser == filterQeueBoxStatus.length){
          newArrayCars = [filterQeueBoxStatus[findIndex-1], findUser]
        }else{
          newArrayCars = [filterQeueBoxStatus[findIndex-1], findUser, filterQeueBoxStatus[findIndex+1]]
        }
     
        this.setState({queueCars:newArrayCars, myCar:findUser, driveUpStatus:findUser.drove_up, myQueue:(findIndex+1), queueCount:filterQeueBoxStatus.length})
      }else{
    
        this.setState({queueCars:filterQeueBoxStatus, myCar:findUser, driveUpStatus:findUser.drove_up, myQueue:(findIndex+1), queueCount:filterQeueBoxStatus.length})
      }      
    }
  }

  deleteQueue(){
    let {dispatch} = this.props

    this.setState({content:'delete'})

    dispatch({
      type:'UPDATE_MODAL',
      modalStatus:true
    })
  }

  skipQueue(){
    let {queue} = this.props.records
    this.socket.emit('slipQueue', queue)
  }

  addServices(){
    const { navigate } = this.props.navigation
    let {queue} = this.props.records
    
    return navigate('selectServices', queue.company_id)
  }

  async driveUp(){
    let {itemCompany} = this.props.records
    let {dispatch} = this.props
    let {myCar} = this.state
    this.setState({driveUpStatus:true})
    let request = await Api.user.orders.driveUp(dispatch, itemCompany.id, itemCompany.branch_id, myCar, this.socket)

    if(request == 'ERROR'){
      this.setState({driveUpStatus:false})
    }
  }

  closeModalPhone(){
    this.setState({phoneStatus:false})
  }

  render(){
    let {itemCompany, invitedBox, queue} = this.props.records
    let {queueCars, myQueue, phones, phoneStatus, driveUpStatus, content, queueCount} = this.state
    let {user} = this.props.user
    let {modalStatus, selectServices} = this.props.carWash
    let {navigation, dispatch} = this.props
    let servicesSumm = 0
    selectServices.forEach((el)=>{
      servicesSumm += +(el.price)
    })
   
    return(
      <View style={styles.myRecords.viewBlock}>
        {
          phoneStatus &&
          <PhoneModal closeModalPhone={this.closeModalPhone.bind(this)} phoneStatus={phoneStatus} phones={phones} />
        }
       
        {
          modalStatus &&  
            <ModalRecord 
              queue={queue}
              invitedBox={invitedBox}
              user={user}
              socket={this.socket}
              companyId={queue.company_id}
              content={content}
              queueCount={queueCount}
              navigation={navigation} 
              modalStatus={modalStatus} 
              dispatch={dispatch}
            />          
        }
        <ScrollView >
          <ImageBackground 
            source={{uri:`${config.public_url}${itemCompany.logo}`}}  
            style={styles.carWashes.headerImage} 
          >
            <View style={styles.carWashes.headerBlock}>
              <MyText style={styles.myRecords.titleHeader}>Моя запись</MyText>
              <View style={styles.carWashes.infoBlock}>
                <View style={styles.carWashes.centerItems}>
                  <FontAwesome5 
                    name="coins" 
                    style={styles.profilescreen.iconText} 
                    size={20} 
                    color="#fff"
                  />
                  <MyText style={{color:'#fff'}}>{servicesSumm} тг</MyText>
                </View>
                <View style={styles.carWashes.centerItems}>
  {/*                      <FontAwesome5
                    name="map-marker-alt" 
                    style={styles.profilescreen.iconText} 
                    size={20} color="#fff"
                  />
                  <Text style={{color:'#fff'}}>{itemCompany.adress}</Text>*/}
                  {
                    selectServices.map((el, index)=>{
                      return(
                        <MyText style={styles.carWashes.headerText} key={index}>{el.title} {el.price} тг</MyText>
                      )
                    })
                  }
                  <TouchableOpacity onPress={this.addServices.bind(this)}>
                    <Ionicons
                      name="md-add-circle" 
                      style={styles.myRecords.addIcon} 
                      size={30} 
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.carWashes.centerItems}>
                  <View style={styles.carWashes.ternIcon}>
                    <Image
                      style={{width:25,height:25}} 
                      source={icons.countTurn} 
                    />
                  </View>
                  <MyText style={styles.carWashes.headerText}>{queueCount}</MyText>                   
                </View>
              </View>
          </View>
        </ImageBackground>

        <MyText style={styles.myRecords.titleBlock}>Очередь в {itemCompany.title}</MyText>

        <View style={styles.myRecords.paddingBlock}>
          {
            queueCars && queueCars.length > 0 ?
              queueCars.map((el, index)=>{
                if(el){
                  return(
                    <View key={index} style={styles.carWashes.queueBlock}>
                      <View style={styles.profilescreen.imgBlock}>
                        <Image 
                          source={{uri: `${config.public_url}${el.car.autoMarka.logo}`}}  
                          style={styles.profilescreen.carImage}
                        />              
                      </View>
                      <View style={styles.merchant.lists.memberViewCenterNumCar}>
                        <Text style={styles.merchant.lists.memberViewCenterNumCarTitle}>
                          { el.car.autoNum }
                        </Text>
                      </View>
                                            
                      {
                        el.user_id == user.id ? 
                          <View style={styles.myRecords.myQueueBlock}>
                            <View style={styles.carWashes.queueBlock}>
                              <Text> Вы </Text>
                              <View style={styles.myRecords.queueCount}>
                                <Text>{myQueue}</Text>
                              </View>
                            </View>
                            <TouchableOpacity onPress={this.deleteQueue.bind(this, el)}>
                              <FontAwesome name="close" size={35} color="red" />
                            </TouchableOpacity>                      
                           </View>
                        : 
                          <View style={{flexDirection:'row', alignItems:'center'}}>
                            {el.car.autoColor && <View style={[styles.merchant.lists.memberViewCenterCarColor, {backgroundColor:el.car.autoColor}]}></View>}
                            {/* <MyText> {el.car.autoMarka.marka}</MyText> */}
                            <Image 
                              source={{uri: `${config.public_url}${el.car.avtoType.image}`}}  
                              style={styles.profilescreen.carImage}
                            />
                          </View>
                      }
                      {
                        (queueCars.length-1) == index && el.user_id != user.id &&
                          <View style={styles.myRecords.skipBlock}>
                            <TouchableOpacity onPress={this.skipQueue.bind(this)}>
                              <Text style={styles.myRecords.skipButton}>Пропустить</Text>
                            </TouchableOpacity>
                          </View>
                      }
                    </View>
                  ) 
                }
              })
            :
              <Loader title="Загрузка" />
          }
        </View>
        </ScrollView>
        
        {
          !driveUpStatus ?
            <View style={styles.myRecords.footerBLock}>
              <TouchableOpacity onPress={this.driveUp.bind(this)} style={styles.myRecords.iComing}>
                <MyText>Я ПОДЪЕХАЛ </MyText>
                <Entypo
                  name="hand" 
                  style={styles.profilescreen.iconText} 
                  size={20}
                />
              </TouchableOpacity>
              <View style={styles.myRecords.line}></View>
              <TouchableOpacity onPress={this.callCompany.bind(this, itemCompany.phones)} style={styles.myRecords.calling}>
                <MyText>ПОЗВОНИТЬ </MyText>
                <FontAwesome
                  name="phone" 
                  style={styles.profilescreen.iconText} 
                  size={18} 
                />
              </TouchableOpacity>
            </View>
          :
            <View style={styles.myRecords.footerBLock}>
              <TouchableOpacity onPress={this.callCompany.bind(this, itemCompany.phones)} style={styles.myRecords.callingOn}>
                <MyText>ПОЗВОНИТЬ </MyText>
                <FontAwesome
                  name="phone" 
                  style={styles.profilescreen.iconText} 
                  size={18} 
                />
              </TouchableOpacity>
            </View>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    records:state.records,
    user:state.user
  }
}

export default connect(mapStateToProps)(MyRecords)