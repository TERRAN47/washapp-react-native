import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  AppState,
  Platform,
  //TextInput,
  BackHandler,
  Linking,
  Alert,
  Image,
  ImageBackground
} from 'react-native'
import { MyText, Timer } from 'Composition'
import { MapsView } from 'Components'
import styles from 'Services/Styles'
import Api from 'Services/Api'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import config from 'Config'
import {constans, renderDistance, icons, socket} from 'Services/Utils'
import PhoneModal from 'Components/Modal/phonesModal'
import call from 'react-native-phone-call'

class CarInBox extends PureComponent {

  constructor(props) {

    super(props)
    this.socket = socket()
    this.state = {
      appState: AppState.currentState,
      serchQueue:{},
      timeInBox:'',
      phoneStatus:false,
      payStatus:false,
      rating:[1,2,3,4,5,6,7,8,9,10],
      selectRating:0,
    }
  }

  componentWillUnmount() {
    let {dispatch} = this.props
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    AppState.removeEventListener('change', this._handleAppStateChange);
    dispatch({
      type:'RECORD_INFO',
      recordInfo:null
    })
    return this.socket.disconnect()
  }

  callCompany(phones){
    if(phones.length > 1){
      this.setState({phoneStatus:true, phones})
    }else{
      if(Platform.OS === 'android'){
        return Linking.openURL(`tel:+7${phones[0]}`)
      }else{
        const args = {
          number: `+7${phones[0]}`
        }
        return call(args).catch(console.error)
      }
    }
  }

  closeModalPhone(){
    this.setState({phoneStatus:false})
  }

  backPressed = async () => {
    await this.goBack()
    return true;
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      this.socket = socket()  
      console.log('App has come to the foreground!');     
      this.updateSocket()
    }else{
      this.socket.disconnect()
    }
    this.setState({appState: nextAppState});
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    this.updateSocket()
    let timeInBox = this.props.navigation.state.params
    this.setState({timeInBox})
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  updateSocket(){
    let {user} = this.props.user
    let {navigation} = this.props

    this.socket.emit('payClientBox', {
      user_id:user.id
    })

    this.socket.on('payRequestBox', (serchQueue)=>{

      if(serchQueue){
        this.setState({serchQueue:serchQueue, timeInBox:serchQueue.invite_box_time})
      }else{
        navigation.navigate('profile')
      }
    })

    this.socket.on('ERROR', (data)=>{
      Alert.alert('Внимание!', 'Произошла ошибка соединения!')
    })
  }
  postRating(rating){
    let {navigation, dispatch} = this.props
    let {itemCompany} = this.props.records
    let {selectRating} = this.state
    let {user} = this.props.user

    if(selectRating == 0){
      this.setState({selectRating:rating})
      Api.user.serchCarwash.saveRating(dispatch, navigation, itemCompany.id, rating, user.id)      
    }
  }
  deletQueue(){
    let {navigation, dispatch} = this.props
    Api.user.serchCarwash.deletQueue(dispatch, navigation)
  }

  render(){
    let {itemCompany, invitedBox} = this.props.records
    let {user} = this.props.user
    let {serchQueue, phoneStatus, timeInBox, selectRating, rating} = this.state
    let {activCar, modalStatus, selectServices} = this.props.carWash
    let {navigation, dispatch} = this.props
    let servicesSumm = 0

    selectServices.forEach((el)=>{
      servicesSumm += +(el.price)
    })

    return(
      <View style={styles.myRecords.viewBlock}>
        {
          phoneStatus &&
          <PhoneModal closeModalPhone={this.closeModalPhone.bind(this)} phoneStatus={phoneStatus} phones={phones} />
        }
        <ScrollView >
          <ImageBackground 
            source={{uri:`${config.public_url}${itemCompany.logo}`}}  
            style={styles.carWashes.headerImage} 
          >
            <View style={styles.carWashes.headerBlock}>
              <View style={styles.carWashes.infoBlock}>
                <View style={styles.carWashes.centerItems}>
                  <View style={styles.carWashes.inboxIcon}>
                    <Image
                      style={{width:25,height:25}} 
                      source={icons.countBox} 
                    />
                  </View>
                  <Text style={styles.carWashes.headerText}>№{invitedBox.index+1}</Text>              
                </View>
                <View style={styles.carWashes.centerItems}>
                	{
                    selectServices.map((el, index)=>{
                      	return(
                        	<Text style={styles.carWashes.headerText} key={index}>{el.title} {el.price} тг</Text>
                      	)
                    })
                	}
                  
                  <Text style={styles.carWashes.totalSumm}>Итог: {servicesSumm} тг</Text>
                  {serchQueue.pay_status && <Text style={styles.carWashes.payText}> Оплачено </Text>}
                
                </View>
                <View style={styles.carWashes.centerItems}>
                  <View style={styles.carWashes.inboxIcon}>
                    <Image
                      style={{width:25,height:25}} 
                      source={icons.memberBoxBlack} 
                    />
                  </View>
                  <Text style={styles.carWashes.headerText}>{invitedBox.member.firstName}</Text>                   
                </View>
              </View>
          </View>
        </ImageBackground>

      	<Text style={styles.myRecords.titleBlock}>{itemCompany.title}</Text>

        <View style={styles.myRecords.paddingBlock}>
          {
            serchQueue.pay_status &&
              serchQueue.count_used_add > 0 &&
              <Text style={styles.modal.titleInvateText}>Использование баллов -{serchQueue.count_used_add}</Text>
          }
          {
            activCar && activCar.avtoType &&          
              <View style={styles.profilescreen.myCarItem}>
                <View style={styles.profilescreen.imgBlock}>
                  <Image 
                    source={{uri: `${config.public_url}${activCar.autoMarka.logo}`}}  
                    style={styles.profilescreen.carImage}
                  />              
                </View>
                <View style={styles.profilescreen.flexBlock}>
                  <View style={styles.profilescreen.carModel}>
                    <View style={styles.profilescreen.flexBlock}>
                      <Text>{activCar.autoModel}</Text>
                    </View>
                    <View style={styles.profilescreen.flexBlock}>
                      <Text>{activCar.autoNum}</Text>
                    </View>
                  </View>
                  <View style={styles.profilescreen.carInfo}>
                    <View style={styles.profilescreen.flexBlock}>
                      <Image 
                        source={{uri: `${config.public_url}${activCar.avtoType.image}`}}  
                        style={styles.profilescreen.carImage}
                      />
                    </View>
                    <View style={styles.profilescreen.flexBlock}>
                      <View style={[styles.profilescreen.autoColor, {backgroundColor: activCar.autoColor}]} />
                    </View>                           
                  </View>                         
                </View>
              </View>
          }
          {
            serchQueue && timeInBox != '' && 
            <View style={{flexDirection: 'row', alignItems:  'center'}}>
              <MyText style={{fontSize:15}}>Время заезда: </MyText>
              <Timer defaultStyles={false} style={{flex:1}} time={timeInBox} />
            </View>
          }
          {
            serchQueue.rating_status ?
            <View>
              <Image
                source={{uri: `${icons.ratingCar}`}}  
                style={styles.profilescreen.carImage}
              /> 
              <Text style={styles.modal.titleInvateText}>МОЙКА ЗАВЕРШЕНА!</Text>
              <Text style={styles.myRecords.ratingText}>ПОЖАЛУЙСТА, ОЦЕНИТЕ НАШУ РАБОТУ</Text>
              <View style={styles.myRecords.ratingBlock}>
              {
                rating.map((el, idnex)=>{
                  return(
                    <TouchableOpacity key={idnex}  onPress={this.postRating.bind(this, el)}>
                      <FontAwesome
                        name={selectRating < el ? "star-o" : 'star'}
                        style={styles.profilescreen.iconText} 
                        size={25}
                        color={constans.orangeColor}
                      />
                    </TouchableOpacity>
                  )
                })
              }            
              </View>
              <TouchableOpacity style={{alignItems: 'center', marginTop:20}} onPress={this.deletQueue.bind(this)}>
                <Text style={styles.myRecords.closeButton}>ЗАКРЫТЬ</Text>
              </TouchableOpacity>   
            </View>
            :
              itemCompany.location ? <MapsView height={220} title="На карте" coords={{latitude:itemCompany.location.coordinates[0], longitude:itemCompany.location.coordinates[1]}} /> : <View></View>     
          }
        </View>
        </ScrollView>
        <View style={styles.myRecords.footerBLock}>
          <TouchableOpacity onPress={this.callCompany.bind(this, itemCompany.phones)} style={styles.myRecords.callingOn}>
            <Text>ПОЗВОНИТЬ </Text>
            <FontAwesome
              name="phone" 
              style={styles.profilescreen.iconText} 
              size={18} 
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    carWash:state.carWash,
    records:state.records,
    user:state.user
  }
}

export default connect(mapStateToProps)(CarInBox)