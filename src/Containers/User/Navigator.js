import React, { Component } from 'react'
import {
  View,
  Text,
  Dimensions
}from 'react-native'
import { createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation'
import { fromRight } from 'react-navigation-transitions'
import DrawerScreen from './DrawerScreen'
import Profile from './Profile'
//import HomeScreen from './HomeScreen'
import AddCar from './Profile/addCar'
import CarWashes from './CarWashes'
import TargetCartWash from './CarWashes/targeCartWash'
import MyRecords from './MyRecords'
import SerchFilter from './CarWashes/serchFilter'
import PriceList from './CarWashes/priceList'
import SelectMyCar from './CarWashes/selectMyCar'
import CarInBox from './MyRecords/carInBox'
import Bonus from './Bonus'
const { width } = Dimensions.get('window')

export default createStackNavigator({
  drawer: createDrawerNavigator({
    profile:{
      screen:Profile
    },
    priceList:{
      screen:PriceList
    },
    favorites:{
      screen:PriceList
    },
    bonus:{
      screen:Bonus
    },
    myRecords:{
      screen:MyRecords
    },
    carInBox:{
      screen:CarInBox
    },

  }, {
    contentComponent:DrawerScreen,
    drawerWidth: width-80
  }),
    selectServices:{
      screen:PriceList
    },
    targetCartWash:{
      screen:TargetCartWash
    },
    addCar:{
      screen:AddCar
    },
    carWashes:{
      screen:CarWashes
    },
    selectMyCar:{
      screen:SelectMyCar
    },
    serchFilter:{
      screen:SerchFilter
    },

}, {
  initialRouteName: 'drawer',
  transitionConfig: () => fromRight(),
  headerMode: 'none',
  cardStyle:{
    backgroundColor:'#fff'
  }
})
