import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createStackNavigator } from 'react-navigation'

import { fromRight } from 'react-navigation-transitions'

import AuthNavigatorStack from './Containers/Auth/Navigator'
import InitialScreen from './Containers/LaunchScreen/initial_screen'
import UserNavigatorStack from './Containers/User/Navigator'
import merchantNavigatorStack from './Containers/Merchant/Navigator'
import managerNavigatorStack from './Containers/Manager/Navigator'
import Locations from 'Containers/Location'
import Maps from 'Containers/Maps'
import ResetPasswordScreen from 'Containers/ResetPassword'


const AppNavigator = createStackNavigator({
  launch: {screen: InitialScreen},
  auth: {screen: AuthNavigatorStack},
  user: {screen: UserNavigatorStack},
  merchant: {screen: merchantNavigatorStack},
  manager: {screen: managerNavigatorStack},
  location: {screen: Locations},
  maps: {screen: Maps},
  ResetPasswordScreen: {screen: ResetPasswordScreen}
}, {
  initialRouteName: 'launch',
  transitionConfig: () => fromRight(),
  headerMode: 'none',
  cardStyle:{
    backgroundColor:'#466df9'
  }  
})

export default AppNavigator
