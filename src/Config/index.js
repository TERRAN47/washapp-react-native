
export default {
	api:'http://washapp.com.kz/api/v1',
	public_url:'http://washapp.com.kz',
	socket_url:'ws://washapp.com.kz',
	socket_port:7734,
	app_version:'0.0.1',
	one_signal_id:'1ed30a8c-a188-4d7c-9a16-9e54fbdfae0c'
}