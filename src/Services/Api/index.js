import { AsyncStorage } from 'react-native'
import { updateAxiosConfig } from './axios'
import config from '../../Config'
import auth from './auth'
import user from './user'
import merchant from './merchant'
import maps from './maps'
import changePass from './changePass'

export default {
	getToken: async () => {
		const token = await AsyncStorage.getItem('Authorization')

		return token
	},
	saveToken: async (token) => {
		const setToken = await AsyncStorage.setItem('Authorization', token)
		return setToken
	},
	saveTokenForManager: async (token) => {
		const setToken = await AsyncStorage.setItem('AuthManager', token)
		return setToken
	},
	removeToken: async () => {
		await AsyncStorage.removeItem('Authorization')
		await AsyncStorage.removeItem('AuthManager')
		return 
	},
	public_url:config.public_url,
	updateAxiosConfig,
	auth,
	user,
	changePass,
	merchant,
	maps
}