import axios from 'axios'
import config from '../../Config'
import { AsyncStorage } from 'react-native'

const updateAxiosConfig = async (manager = false) => {

	let token

	if(manager){
		token = await AsyncStorage.getItem('AuthManager')
	}else{
		token = await AsyncStorage.getItem('Authorization')
	}

	if(token != null){
		axios.defaults.headers.common['Authorization'] = token

	}else{
		axios.defaults.headers.common['Authorization'] = ''
	}
	axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8;';
	//axios.defaults.headers.get = {['Content-Type']: "application/x-www-form-urlencoded"}
	console.log('axios', axios.defaults)

	axios.defaults.baseURL = `${config.api}`

}

updateAxiosConfig()

//axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

export {
	updateAxiosConfig
}

export default axios