import axios from '../axios'
import { Alert } from 'react-native'
import underscore from 'underscore'
import Api from '../index'
export default {
	
	getBonuses: async (dispatch)=>{

		try{
			let {data} = await axios.get(`/user/bonus`)

			return dispatch({
				type:'UPDATE_BONUS',
				bonuses:data.data,
				countBonuses:data.data.length
			})
		}catch(err){
			Alert.alert("Внимание!", 'Ошибка соединения. Пропробуйте позже')
			return
		}
	},
}