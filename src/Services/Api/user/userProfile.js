import axios from '../axios'
import { Alert } from 'react-native'
import underscore from 'underscore'

export default {
	
	getMarks: async (dispatch)=>{
		try{
			let {data} = await axios.get('/cars')
          
           
          	console.log('parse', data)
			return dispatch({
				type:'UPDATE_CARS',
				cars:data
			})
		}catch(err){
			console.log(err)
			return
		}
	},
	checkMyRecord: async (dispatch)=>{
		try{
			let {data} = await axios.get('/check-my/record')
			
			if(data.data){
				
		        dispatch({
		          type:'INVITED_BOX',
		          invitedBox:data.data.invited_box,
		        })
		        dispatch({
		          type:'SELECT_SERVICES',
		          selectServices:data.data.services,
		        })

				return dispatch({
					type:'RECORD_INFO',
					recordInfo:data.data
				})
			}else{
				return dispatch({
					type:'RECORD_INFO',
					recordInfo:null
				})			
			}
		}catch(err){
			console.log('INITIAL', err)
			dispatch({
				type:'RECORD_INFO',
				recordInfo:'ERROR'
			})	
			return
		}
	},
	updateUser: async (dispatch, user)=>{

		try{
			let {data} = await axios.post('/customer', user)

			return dispatch({
				type:'UPDATE_USER',
				user:user
			})
		}catch(err){
			Alert.alert('Внимание!', 'Проблемы с соединением!')
			console.log(err)
			return
		}
		
	},

	addCar: async (navigation, dispatch, user)=>{
		
		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})

		try{
			let {data} = await axios.post('/customer', user)

			dispatch({
				type:'UPDATE_USER',
				user:user
			})

			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})	

			const { goBack } = navigation	
			return goBack()
		}catch(err){

			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})
			Alert.alert('Внимание!', 'Проблемы с соединением!')
			console.log(err)
			return			
		}
	},

	deletMyCar: async (dispatch, user, car)=>{
		try{
			user.cars = underscore.without(user.cars, underscore.findWhere(user.cars, {
				autoModel:car.autoModel, 
				autoNum:car.autoNum,
			}));
			
			let {data} = await axios.post('/customer', user)
			
			return dispatch({
				type:'UPDATE_USER',
				user:user
			})
		}catch(err){
			console.log(err)
			return			
		}
	}
}