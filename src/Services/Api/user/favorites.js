import axios from '../axios'
import { Alert } from 'react-native'
import underscore from 'underscore'
import Api from '../index'
export default {
	
	getFavorites: async (dispatch, userGeo, bodyCar, filterServices, favorites)=>{
		let {latitude, longitude} = userGeo
		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})
		try{
			let {data} = await axios.post(`/companies/favorites`, {
				filterServices:JSON.stringify(filterServices),
				bodyCar,
				latitude,
				longitude,
				favorites
			})
		
			dispatch({
				type:'UPDATE_COMPANIES',
				carWashes:data.data,
				countCarWashes:data.data.length
			})
		}catch(err){
			Alert.alert("Внимание!", 'Ошибка соединения. Пропробуйте позже')
			
		}
		return dispatch({
			type:'REQUEST_STATUS',
			status:false
		})
	},
	removeFavorite: async (dispatch, navigation, favorites, user, id)=>{
	  	user.favorites = underscore.without(user.favorites, id)

  		Api.user.userProfile.updateUser(dispatch, user)

		favorites = underscore.without(favorites, underscore.findWhere(favorites, {
			id
		}));

		return dispatch({
			type:'UPDATE_COMPANIES',
			carWashes:favorites,
			countCarWashes:favorites.length
		})
	}
}