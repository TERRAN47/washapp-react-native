import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'
import underscore from 'underscore'

const { width, height } = Dimensions.get('window')

export default {

	getCarwash: async (dispatch, userGeo, bodyCar, filterServices,  user, distance) => {
		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})
		try{
			
			const { data } = await axios.get('/companies', {
				params:{
					city_id:user.city.id,
					filterServices:JSON.stringify(filterServices),
					bodyCar,
					distance:distance,
					latitude:userGeo.latitude,//51.156144,
					longitude:userGeo.longitude//71.493068
					//latitude:51.156144,//userGeo.latitude,
					//longitude:71.493068//userGeo.longitude
				}
			})

			dispatch({
				type:'UPDATE_COMPANIES',
				carWashes:data.data,
				countCarWashes:data.data.length
			})
		}catch(err){
			Alert.alert("Внимание", 'Ошибка соединения')
			console.log(err)
			dispatch({
				type:'UPDATE_COMPANIES',
				carWashes:null,
				countCarWashes:null
			})
		}
		return dispatch({
			type:'REQUEST_STATUS',
			status:false
		})
	},
	deletQueue: async (dispatch, navigation) => {
		try{
			await axios.get('/delet/my/queue')
	        dispatch({
	          type:'RECORD_INFO',
	          recordInfo:null
	        })
	     	
	       return navigation.navigate('profile')			
		}catch(err){
			console.log(err)
			Alert.alert("Внимание!", 'Произошла ошибка соединения')
		}

	},
	saveRating: (dispatch, navigation, company_id, rating, user_id) => {
		try{
			const { data } = axios.post('/rating/save', {
				company_id,
				user_id,
				rating
			})

		}catch(err){
			console.log(err)
			Alert.alert("Внимание!", 'Произошла ошибка соединения')
		}
	},
	sortCarWash: async (dispatch, carWashes, sort, type) => {
		
		if(sort == ''){
			carWashes = underscore.sortBy(carWashes, type);
			sort = 'asc'
		}else if(sort == 'asc'){
			carWashes = underscore.sortBy(carWashes, type).reverse();
			sort = 'desc'
		}else{
			sort = ''
		}

      	if(type == 'summ'){
			dispatch({
				type:'UPDATE_SORTSUMM',
				sortSumm:sort,
			})
      	}else if(type == 'distance'){
			dispatch({
				type:'UPDATE_DISTANCE',
				distance:sort,
			})
      	}else{
			dispatch({
				type:'UPDATE_RATING',
				rating:sort,
			})
     	}

		dispatch({
			type:'UPDATE_COMPANIES',
			carWashes:carWashes,
			countCarWashes:carWashes.length
		})
		return
	},
	getPrices: async (dispatch, activCar) => {
		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})

		try{
			const { data } = await axios.get('/get/prices')
			if(data.bodyworks){

				let bodywork = underscore.findWhere(data.bodyworks, {
					title:activCar.avtoType.typeName
				})

		        dispatch({
		          type:"ACTIVE_CAR",
		          activCar
		        })

		        dispatch({
		            type:'CLIENT_SERVICES',
		            services:bodywork.prices
		        })

				return dispatch({
					type:'REQUEST_STATUS',
					status:false
				})
			}
		}catch(err){
			console.log(err)
			Alert.alert("Внимание!", 'Произошла ошибка соединения')
			return dispatch({
				type:'REQUEST_STATUS',
				status:false
			})
		}
	},
	gitCarWashInfo: async (dispatch, navigation, recordInfo) => {
		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})
		try{

			const { data } = await axios.get('/company/info', {params:{id:recordInfo.company_id}})
			
			if(data.data){
				let queue = {user_id:recordInfo.user_id,  car:recordInfo.car, company_id:recordInfo.company_id}//services:selectServices,

/*				dispatch({
					type:'UPDATE_COMP_PRICE',
					companyPrice:data.data.price,
				})*/
				dispatch({
					type:'UPDATE_QEUE',
					queue,
					itemCompany:data.data
				})
				navigation.navigate('myRecords')
			}
		}catch(err){
			console.log(err)
		}
		return dispatch({
			type:'REQUEST_STATUS',
			status:false
		})
	},


}