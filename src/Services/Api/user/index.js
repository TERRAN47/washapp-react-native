import userProfile from './userProfile'
import serchCarwash from './serchCarwash'
import favorites from './favorites'
import orders from './orders'
import bonus from './bonus'

export default {
	userProfile,
	orders,
	bonus,
	favorites,
	serchCarwash,
}