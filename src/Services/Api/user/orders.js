import axios from '../axios'
import { Alert } from 'react-native'
import underscore from 'underscore'
import { NavigationActions, StackActions } from 'react-navigation'

export default {
	
	createQueue: async (dispatch, navigation, item, queue, company)=>{

		let companyPrice = underscore.findWhere(company.price, {
			title:queue.car.avtoType.typeName
		})

		dispatch({
			type:'UPDATE_COMP_PRICE',
			companyPrice:companyPrice.prices
		})

		dispatch({
			type:'UPDATE_QEUE',
			queue,
			itemCompany:item
		})

		navigation.navigate('myRecords')
	},
	confirmInvite: async (dispatch, navigation, invitedBox) => {

		try{
			const { data } = await axios.get('/confirm/invite')

			if(data[0] == 1){
				navigation.navigate('carInBox', {timeInBox:invitedBox.car.invite_box_time})
			}else{
				Alert.alert("Внимание!", 'Ошибка соединения')
			}
		}catch(err){
			Alert.alert("Внимание!", 'Ошибка соединения')
		}
	},
	driveUp: async (dispatch, company_id, branch_id, myCar, socket) => {
		if(myCar){
			try{

				const { data } = await axios.post('/queue/drive-up', {
					company_id,
					branch_id,
					myCar
				})

			    socket.emit('selectFirstCarFromQueue', {
			      id:company_id
			    })
				return 'ok'
			}catch(err){
				Alert.alert("Внимание!", 'Ошибка попробуйте снова')
				return 'ERROR'
			}			
		}else{
			Alert.alert("Внимание!", 'Ошибка попробуйте снова')
			return 'ERROR'
		}
	},
	updateQeueServices: async (dispatch, navigation,  company_id, user_id, services)=>{
		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})
		
		try{
		    let filterServices = underscore.where(services, {
		      status:true
		    })

			const { data } = await axios.post('/companies/update/qeue/services', {
				company_id,
				user_id,
				services:filterServices
			})

		    dispatch({
		      type:'SELECT_SERVICES',
		      selectServices:filterServices
		    })
		}catch(err){
			Alert.alert('Внимание!', 'Произошла ошибка попробуйте позже')
		}
		dispatch({
			type:'REQUEST_STATUS',
			status:false
		})

		return navigation.goBack()

	},
}