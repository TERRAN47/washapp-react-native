import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'
import _ from 'underscore'

const { width, height } = Dimensions.get('window')

export default {
	getClients: async (dispatch, branchId) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get(`/companies/merchant/clients/${branchId}`)
			console.log(1234,data)
			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_CLIENTS_LIST',
				clients:data.data && data.data.length > 0 ? data.data : null
			})

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_CLIENTS_LIST',
				clients:null
			})

		}

	}
}