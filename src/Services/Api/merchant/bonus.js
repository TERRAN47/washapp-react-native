import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'
import _ from 'underscore'

const { width, height } = Dimensions.get('window')

export default {
	addBonus: async (dispatch, bonuses, id, bonus) => {

		console.log(2222, bonuses, id, bonus)

		let newBonuses = [...bonuses, {
			description:bonus
		}]

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post(`/companies/merchant/bonus`, {
				id,
				bonuses:newBonuses
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			Alert.alert('сообщение', data.data)

			return dispatch({
				type:'UPDATE_BONUSES_LIST',
				bonuses:newBonuses,
				id
			})

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_BONUSES_LIST',
				bonuses:null
			})

		}

	},
	removeBonus: async (dispatch, newBonuses, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post(`/companies/merchant/bonus`, {
				id,
				bonuses:newBonuses
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			Alert.alert('сообщение', data.data)

			return dispatch({
				type:'UPDATE_BONUSES_LIST',
				bonuses:newBonuses,
				id
			})

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_BONUSES_LIST',
				bonuses:null
			})

		}

	}
}