import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'

const { width, height } = Dimensions.get('window')

export default {
	testIOSPAY: async (details) => {


		try{

			let { data } = await axios.post(`/payments/apple-pay-marchant`, {
				details
			})

			return {
				status:true
			}

		}catch(err){
			return {
				status:false
			}
		}

	},
	payments: async (dispatch) => {
 
		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get(`/payments`)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			console.log(555,data)
			return dispatch({
				type:'UPDATE_LIST_PAYMENTS_USER',
				list:data.length > 0 ? data : null
			})

		}catch(err){

			dispatch({
				type:'UPDATE_LIST_PAYMENTS_USER',
				list:null
			})

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}
	},
	createPayment: async (dispatch, price, navigation) => {

		if(price){
			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			try{

				let { data } = await axios.post(`/payment/create`, {
					...price
				})

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				dispatch({
					type:'UPDATE_ITEM_PAYMENTS_USER',
					item:data
				})

				return navigation.navigate('paymentSuccess', {
					code:data.code
				})

			}catch(err){
				return dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

			}
		}else{
			return Alert.alert('Сообщение', 'Выберите сумму для пополнения')
		}

	},
	promoCode: async (dispatch, promocode) => {

		if(promocode != ''){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			try{

				let { data } = await axios.get(`/promo/${promocode}`)

				Alert.alert('Сообщение', 'Промокод успешно был активирован. Баланс пополнен')

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				return dispatch({
					type:'UPDATE_MARCHANT_BALANCE',
					balance:data
				})

			}catch(err){

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})	

				return Alert.alert('Сообщение', err.response.data)

			}

		}else{

			return Alert.alert('Сообщение', 'Введите промокод')

		}

	}
}