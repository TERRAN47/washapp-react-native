import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'
import _ from 'underscore'

const { width, height } = Dimensions.get('window')

export default {
	addMemberToBox:async (dispatch, navigation, member, boxList, boxIndex, id, companyIndex) => {
		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		boxList[boxIndex].member = member

		try{
			const {data} = await axios.post('/companies/merchant/boxes/add-member',{
			 	id,
			 	boxes:boxList
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			dispatch({
				type:'UPDATE_BOX_COMPANY',
				boxList,
				companyIndex
			})

			return navigation.goBack()
		}catch(err){
			Alert.alert('Сообщение', 'ошибка, попробуйте еще раз')

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})	
		}
	},

	switchCarSort: async (dispatch, carId, targetIndex, company_id) => {
		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{
			let request = await axios.get('/switch/car/turn', {
				params:{
					carId,
					targetIndex,
					company_id
				}
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			return 'ok'
		}catch(err){
			Alert.alert('Сообщение', 'ошибка, попробуйте еще раз')
			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
		}
	},

	sendInvitePay: async (user_device_id) => {

		try{
			axios.get('/send/invitePay', {
				params:{
					user_device_id
				}
			})
		}catch(err){
			
		}
	},

	deletCompany: async (dispatch, companyID, companies) => {
		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})
		try{
			const { data } = await axios.get('/delet/company', {
				params:{
					id:companyID
				}
			})
		    companies = _.without(companies, _.findWhere(companies, {
		      id:companyID
		    }));
			dispatch({
				type:'UPDATE_MARCHANT_COMPANIES',
				data:companies,
				count:companies.length
			})
			alert(data.data)
		}catch(err){
			Alert.alert('Сообщение', 'ошибка, попробуйте еще раз')
		}
		return 	dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:false
		})
	},
	updateQeueBoxServices: async (dispatch, navigation,  company_id, services, boxInfo)=>{

		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})
		try{
			let user_id = boxInfo.car.user_id
			let boxIndex = boxInfo.index	

		    let filterServices = _.where(services, {
		      status:true
		    })

		    boxInfo.car.services = filterServices

			await axios.post('/update/qeue/box/services', {
				company_id,
				user_id,
				boxInfo,
				services:filterServices
			})

			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})			
			return 'ok'
		}catch(err){
			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})
			Alert.alert('Внимание!', 'Произошла ошибка попробуйте позже')
			return 'ERROR'
		}


		return navigation.goBack()

	},
	savePrices: async (dispatch, id, prices, bodyCars) => {

		let bodyCarsArray = bodyCars

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		bodyCarsArray.map((car, index)=>{

			prices.map((price, priceIndex)=>{

				if(car.title == price.title){

					bodyCarsArray[index].price = price.price

				}

			})

		})

		try{

			let { data } = await axios.post('/companies/price/save', {
				id,
				price:bodyCarsArray
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			dispatch({
				type:'UPDATE_PRICE_COMPANY',
				id,
				price:bodyCarsArray
			})

			return Alert.alert('Сообщение', 'Данные успешно сохранены')

		}catch(err){

			Alert.alert('Сообщение', 'ошибка, попробуйте еще раз')

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}

	},
	getListCarWash: async (dispatch, latitude, longitude) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get('/companies/merchant', {
				params:{
					latitude,
					longitude
				}
			})
			console.log(66, data)
			// let s = data.replace(/[\u0000-\u0019]+/g,"");
			// console.log(777, JSON.parse(s))
			//data = JSON.parse(data)
			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_MARCHANT_COMPANIES',
				data:data.data.count > 0 ? data.data.rows : null,
				count:data.data.count > 0 ? data.data.count : 0
			})

		}catch(err){
			console.log(123, err)
			alert("Ошибка соединения, попробуйте снова")
			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
		}
	},
	createCompany: async (dispatch, navigation, data, latitude, longitude) => {

		let {
			selectCoords,
			selectCoordStatus,
			city_select,
			logo,
			title,
			description,
			adress,
			phones,
			time,
			user
		} = data

		let logoPathUploaded = ''
		console.log(111, phones)
		if(selectCoordStatus && city_select && logo && title && description && adress && phones.length > 0){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			//upload logo
			const logoData = new FormData()

	      	logoData.append('logo', {
		  		uri : logo.uri,
		  		type: logo.type,
		  		name: logo.name
	    	}, logo.name)

		    try{

		    	let { data } = await axios.put('/upload/logo', logoData, {
			  		headers: {
			    		'Content-Type': 'multipart/form-data; charset=utf-8;'
			  		}
				})

				logoPathUploaded = data.uri

		    }catch(err){
		    	console.log(err)
				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})
		    	return Alert.alert('Сообщение', 'Ошибка при загрузке лого, попробуйте еще раз')
		    }

		    //create company
		    try{

		    	let { data, status } = await axios.post('/companies', {
					adress,
					logo:logoPathUploaded,
					title,
					description,
					user,
					phones,
					position:selectCoords,
					city_id:city_select.id,
					city:city_select,
					time
		    	})

		    	if(status == 208){
		    		return Alert.alert('Сообщение', data.data)
		    	}

		    } catch(err){
		    	console.log(err)
				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})
		    	return Alert.alert('Сообщение', 'Ошибка при создании записи, попробуйте еще раз')
		    }

		    //update companies
			try{

				let { data } = await axios.get('/companies/merchant', {
					params:{
						latitude,
						longitude
					}
				})

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				Alert.alert('Сообщение','Компания успешно создана')

				dispatch({
					type:'UPDATE_MARCHANT_COMPANIES',
					data:data.data.count > 0 ? data.data.rows : null,
					count:data.data.count > 0 ? data.data.count : 0
				})

				return navigation.goBack()

			}catch(err){

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				return dispatch({
					type:'UPDATE_MARCHANT_COMPANIES',
					data:null,
					count:0
				})
			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}

	},

	activateCompany: async (dispatch, id, companyIndex) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data, status } = await axios.post('/companies/merchant/activate', {
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			if(status == 203){

				return {
					status:false,
					message:data
				}
			}

			dispatch({
				type:'UPDATE_MARCHANT_BALANCE',
				balance:data.balance
			});

			dispatch({
				type:'ACTIVATED_COMPANY_MERCHANT',
				companyIndex
			});

			return {
				status:true,
				message:'Автомойка активна'
			}
		}catch(err){

			console.log(445, err)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			if(err.response.status == 400){
				return {
					status:false,
					message:err.response.data
				}
			}

			return {
				status:false,
				message:'Ошибка, попробуйте позже'
			}
		}
	},

	deactivateCompany: async (dispatch, id, companyIndex) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data, status } = await axios.post('/companies/merchant/deactivate', {
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			dispatch({
				type:'DEACTIVATED_COMPANY_MERCHANT',
				companyIndex
			})

			return {
				status:true,
				message:'Автомойка деактивирована'
			}

		}catch(err){

			console.log(445, err)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			return {
				status:false,
				message:'Ошибка, попробуйте позже'
			}

		}

	},

	updateCompany: async (dispatch, navigation, data, latitude, longitude, manager) => {

		let {
			selectCoords,
			selectCoordStatus,
			city_select,
			logo,
			title,
			phones,
			description,
			adress,
			time,
			user,
			uploadedLogo,
			company_id
		} = data

		let logoPathUploaded = ''

		if(selectCoordStatus && city_select && title && description && adress && phones.length > 0 && time.start != '' && time.end != ''){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			if(logo){

				//upload logo
				const logoData = new FormData()

		      	logoData.append('logo', {
			  		uri : logo.uri,
			  		type: logo.type,
			  		name: logo.name
		    	}, logo.name)

			    try{

			    	let { data } = await axios.put('/upload/logo', logoData, {
				  		headers: {
				    		'Content-Type': 'multipart/form-data; charset=utf-8;'
				  		}
					})

					logoPathUploaded = data.uri

			    }catch(err){
			    	console.log(err)
					dispatch({
						type:'STATUS_REQUEST_MERCHANT',
						status:false
					})
			    	return Alert.alert('Сообщение', 'Ошибка при загрузке лого, попробуйте еще раз')
			    }

			}else{
				logoPathUploaded = uploadedLogo
			}

		    //create company

		    let linkRestUpdate = manager ? '/companies/update/manager' : '/companies/update'

		    try{

		    	let { data, status } = await axios.post(linkRestUpdate, {
					adress,
					logo:logoPathUploaded,
					title,
					phones,
					description,
					user,
					position:selectCoords,
					city_id:city_select.id,
					city:city_select,
					time,
					id:company_id
		    	})

		    	if(status == 208){
		    		return Alert.alert('Сообщение', data.data)
		    	}
		    } catch(err){
				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})
		    	return Alert.alert('Сообщение', 'Ошибка при создании записи, попробуйте еще раз')
		    }

		    //update companies
			try{

				let { data } = await axios.get('/companies/merchant', {
					params:{
						latitude,
						longitude
					}
				})

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				Alert.alert('Сообщение','Информация успешно обновлена')

				dispatch({
					type:'UPDATE_MARCHANT_COMPANIES',
					data:data.data.count > 0 ? data.data.rows : null,
					count:data.data.count > 0 ? data.data.count : 0
				})

				return navigation.goBack()

			}catch(err){

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				return dispatch({
					type:'UPDATE_MARCHANT_COMPANIES',
					data:null,
					count:0
				})
			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}

	},
	addAdditionally: async (dispatch, title, list, companyIndex, id) => {

		if(title){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			let newList = [
				...list,
				{
					title,
					static:false,
					status:true
				}
			]

			try{

				let { data } = await axios.post('/companies/merchant/update/additionally', {
					items:newList,
					id
				})

				dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

				return dispatch({
					type:'UPDATE_LIST_ADDITIONALLY',
					list:newList,
					index:companyIndex
				})

			}catch(err){

				console.log(222, err)

				Alert.alert('Сообщение', 'Произошла ошибка на сервере, обратитесь к администратору')

				return dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

			}


		}else{

			return Alert.alert('Сообщение', 'Заполните поле')

		}

	},
	changeStatusAdditionally: async (dispatch, items, id, companyIndex) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/companies/merchant/update/additionally', {
				items:items[companyIndex].additionally,
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'CHANGE_LIST_ADDITIONALLY',
				items
			})

		}catch(err){

			Alert.alert('Сообщение', 'Произошла ошибка на сервере, обратитесь к администратору')

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
		}

	},
	changeStatusPayments: async (dispatch, items, id, companyIndex) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/companies/merchant/update/type-payments', {
				items:items[companyIndex].type_payments,
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'CHANGE_LIST_PAYMENTS',
				items
			})

		}catch(err){

			Alert.alert('Сообщение', 'Произошла ошибка на сервере, обратитесь к администратору')

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
		}

	},
	updateBoxes: async (dispatch, boxes, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/companies/merchant/update-boxes', {
				boxes,
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			Alert.alert('Сообщение', 'Успешно сохранено!')
			dispatch({
				type:'UPDATE_LIST_BOXES',
				boxes,
				id
			})
			return 'ok'
		}catch(err){
			console.log(err)
			Alert.alert('Сообщение', 'Ошибка соединения!')

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			return 'ERROR'
		}

	},
	saveCarToBox: async (dispatch, companyIndex, index, car, boxList, id, socket) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		boxList[index].car = car
		boxList[index].status = true
		boxList[index].index = index

		try{

			let data = await axios.post('/companies/merchant/boxes/save-car', {
				boxes:boxList,
				id,
				box:boxList[index],
				user_id:car.user_id,
				user_device_id:car.user_device_id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			socket.emit('updateBoxesAdmin', {
				id
			})
/*			socket.emit('selectFirstCarFromQueue', {
		    	id,
		    	index,
		    	box:boxList[index]
		    })*/

			return dispatch({
				type:'SAVE_CAR_TO_BOX',
				boxList,
				companyIndex
			})

		}catch(err){

			Alert.alert('Сообщение', 'Произошла ошибка на сервере, обратитесь к администратору')

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}
	},

	getCompaniesForVacancions: async (dispatch) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get('/companies/merchant/get-companies-for-vacancions')	

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return {
				status:true,
				data
			}

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return {
				status:false,
				message:'Ошибка, попробуйте позже'
			}

		}

	}
}