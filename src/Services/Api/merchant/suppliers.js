import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'

const { width, height } = Dimensions.get('window')

export default {
	getSuppliers: async (dispatch, cityId) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get(`/suppliers/${cityId}`,)

			dispatch({
				type:'UPDATE_SUPPLIERS_LIST',
				data:data.data && data.data.length > 0 ? data.data : null
			})

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})


		}catch(err){

			dispatch({
				type:'UPDATE_SUPPLIERS_LIST',
				data:null
			})

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}

	}
}