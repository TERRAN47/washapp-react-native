import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'

const { width, height } = Dimensions.get('window')

export default {
	getOrders: async (dispatch, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get(`/orders/${id}`)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_ORDERS_LIST',
				orders:data.data && data.data.length > 0 ? data.data : null,
				total:data.total,
				count:data.count,
				percent:data.percent,
				cash:data.cash
			})

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			dispatch({
				type:'UPDATE_ORDERS_LIST',
				orders:null,
				total:0,
				count:0,
				percent:0,
				cash:0
			})

			return Alert.alert('Сообщение', 'Упс, ошибка. Попробуйте еще раз')

		}

	},
	getCountWashes: async (dispatch, user_id, company_id, car) => {
		
		try{
			let { data } = await axios.post(`/get/count/washes`, {
				company_id,
				user_id,
				car
			})

			dispatch({
				type:'CLIENT_INFO',
				clientInfo:data.data
			})
			return 'OK'
		}catch(err){
			dispatch({
				type:'CLIENT_INFO',
				clientInfo:null
			})
			alert('Ошибка соединения попробуйте снова!')
		}
	},
	restartCountWash:async (company_id, user_id, resetStatus, clientCount) => {
		
		try{
			let { data } = await axios.get('/restart/count/client', {
				params:{
					company_id,
					user_id,
					resetStatus,
					clientCount
				}
			})	
			return 'OK'
		}catch(err){
			console.log(err)
			return "ОШИБКА"
		}
	},

	payBoxComplit:async (boxIndex, companyId, comment, summServices, countUsedAdd, reStatus) => {	
		try{
			let { data } = await axios.post('/merchant/payBox/complite', {
				boxIndex,
				companyId,
				comment,
				reStatus,
				summServices,
				countUsedAdd
			})	
			return data
		}catch(err){
			console.log(err)
			return "ОШИБКА"
		}
	},
	sendAndSaveOrders: async (dispatch, navigation, list, count, percent, cash, total, email, id, title) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/orders/save-order-and-close', {
				list,
				count,
				cash,
				percent,
				total,
				email,
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			dispatch({
				type:'UPDATE_SAVED_ORDERS',
				report:{
			      link:data.linkReport,
			      date:data.dateReport,
			      title,
			      id
				}
			})

			return {
		      linkReport:data.linkReport,
		      dateReport:data.dateReport,
		      status:true
			}

		}catch(err){

			console.log(223, err)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return {
		      status:false,
		      message:'Произошла ошибка, попробуйте еще раз'
			}

		}

	}
}