import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'

const { width, height } = Dimensions.get('window')

export default {
	newMember: async (dispatch, members, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/companies/create/member', {
				members,
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return Alert.alert('сообщение', 'Сотрудник успешно добавлен')

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return Alert.alert('Сообщение', 'Упс, ошибка. Попробуйте еще раз')

		}

	},
	deleteMember: async (dispatch, members, id, navigation) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/companies/delete/member', {
				members,
				id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			navigation.goBack()

			return Alert.alert('сообщение', 'Сотрудник успешно удален')

		}catch(err){

			console.log(444444, err)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return Alert.alert('Сообщение', 'Упс, ошибка. Попробуйте еще раз')

		}

	},
	updateMember: async (dispatch, members, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post('/companies/create/member', {
				members,
				id
			})

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})	

		}catch(err){

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}

	}
}