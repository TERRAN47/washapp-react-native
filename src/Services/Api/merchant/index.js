import main from './main'
import carwash from './carwash'
import members from './members'
import clients from './clients'
import orders from './orders'
import suppliers from './suppliers'
import payment from './payment'
import bonuses from './bonus'
import administrator from './administrator'
import vacancies from './vacancies'

export default {
	main,
	carwash,
	members,
	clients,
	orders,
	suppliers,
	payment,
	bonuses,
	administrator,
	vacancies
}