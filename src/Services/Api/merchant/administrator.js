import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'
import _ from 'underscore'

const { width, height } = Dimensions.get('window')

export default {
	findAdministrators: async (dispatch, letter) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get(`/search/administrators`, {
				params:{
					letter
				}
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_ADMIN_LIST',
				admins:data.data
			})

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_ADMIN_LIST',
				admins:null
			})
		}
	},

	addAdministrator: async (dispatch, admins, admin, id, merchant_id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		let pushAdmin = [...admins, admin]

		let newAdmins = _.uniq(pushAdmin, (person) =>{ 
			return person.id
		})

		try{

			let { data } = await axios.post(`/merchant/administrators/update`, {
				id,
				admins:newAdmins,
				admin,
				merchant_id
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			Alert.alert('Сообщение', 'Администратор добавлен')

			return dispatch({
				type:'UPDATE_ADMINS_LIST',
				admins:newAdmins,
				id
			})

		}catch(err){

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}		

	},
	removeAdministrator: async (dispatch, administrators, id, admin, index) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		let newAdmins = []

		administrators.map((adminItem, indexAdmin)=>{

			if(indexAdmin != index){
				newAdmins = [...newAdmins, adminItem]
			}

		})

		try{

			let { data } = await axios.post(`/merchant/administrators/remove`, {
				id,
				admins:newAdmins,
				admin
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_ADMINS_LIST',
				admins:newAdmins,
				id
			})

		}catch(err){

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}		

	}
}