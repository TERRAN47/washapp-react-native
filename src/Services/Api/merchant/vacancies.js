import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'

const { width, height } = Dimensions.get('window')

const vacancies = {

	getVacancies: async (dispatch) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get('/vacancies/merchant')

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})	

			return dispatch({
				type:'UPDATE_LIST_VACANCIES',
				data: data.length > 0 ? data : null
			})

		}catch(err){

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
		}
	},

	getVacanciesForClients: async (dispatch) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get('/vacancies/get-for-clients')

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})	

			return dispatch({
				type:'UPDATE_LIST_VACANCIES',
				data: data.length > 0 ? data : null
			})

		}catch(err){

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
		}

	},

	updateVacancy: async (dispatch, navigation, fields, id, index) => {

		let {
	      title,
	      charge,
	      requirements,
	      work_mode,
	      salary,
	      company,
	      salaryStatus
		} = fields

		let that = this

		if(title && charge && requirements && work_mode.start && work_mode.end && salary && company){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			try{

				let { data } = await axios.post(`/vacancies/update`, {
			      title,
			      charge,
			      requirements,
			      work_mode,
			      salary,
			      company,
			      id,
			      salaryStatus
				})

				return dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

			}catch(err){

				console.log(332, err)

				return dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}	

	},

	removeVacancy: async (dispatch, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.post(`/vacancies/remove`, {
		      id
			})

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}catch(err){

			return dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

		}

	},

	createVacancy: async (dispatch, navigation, fields) => {

		let {
	      title,
	      charge,
	      requirements,
	      work_mode,
	      salary,
	      company,
	      salaryStatus
		} = fields

		let that = this

		if(title && charge && requirements && work_mode.start && work_mode.end && salary && company){

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})

			try{

				let { data } = await axios.post(`/vacancies/create`, fields)

				return dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

			}catch(err){

				return dispatch({
					type:'STATUS_REQUEST_MERCHANT',
					status:false
				})

			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}

	}
}

export default vacancies