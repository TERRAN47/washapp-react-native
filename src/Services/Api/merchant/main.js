import axios from '../axios'
import { 
	Alert,
	Dimensions
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import { socket } from 'Services/Utils'
import _ from 'underscore'

const { width, height } = Dimensions.get('window')

export default {
	getBodyCars: async (dispatch, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			const { data } = await axios.get('/cars', {
				params:{
					id
				}
			})

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_LIST_BODYCAR',
				bodyCars:data.companyBodyCars.length > 0 ? data.companyBodyCars : data.bodyworks
			})			

		}catch(err){

			console.log(err)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})

			return dispatch({
				type:'UPDATE_LIST_BODYCAR',
				bodyCars:null
			})

		}

	},
	updateInformationCars: async (dispatch, bodyCars) => {

		return dispatch({
			type:'UPDATE_INFORMATION_CAR',
			bodyCars
		})

	},
	changeListBodyCars: async (dispatch, cars) => {

		return dispatch({
			type:'CHANGED_LIST_BODYCARS',
			bodyCars:cars
		})

	},
	switchAccountType: async (navigation, dispatch, type) => {

		const typeAccount = type == 'merchant' ? 'client' : 'merchant'
		const titleAccountMain = type == 'merchant' ? 'Клиент' : 'Автомойка'

		Alert.alert('Сообщение', `Переключить аккаунт на ${titleAccountMain}`,
		  	[
		    	{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
		    	{text: 'OK', onPress: async () => {
					await axios.post('/customer/type', {
						type:typeAccount
					})

					dispatch({
						type:'SWITCH_TYPE_ACCOUNT',
						typeAccount
					})

			        return navigation.dispatch(StackActions.reset({
			          index: 0,
			          key: null,
			          actions: [
			            NavigationActions.navigate({routeName: 'launch'})
			          ]
			        }))
		    	}}
		  	],
		  	{
		  		cancelable: false 
		  	}
		)

	},

	managerAccount: async (navigation, dispatch, screen) => {

        return navigation.dispatch(StackActions.reset({
          index: 0,
          key: null,
          actions: [
            NavigationActions.navigate({routeName: screen})
          ]
        }))	

	},
}