import axios from '../axios'
import { Alert } from 'react-native'

export default {
	saveCoords: async (navigation, dispatch, coords) => {
		dispatch({
			type:'SELECT_CURRENT_COORDS',
			latitude:coords.latitude,
			longitude:coords.longitude
		})
		return navigation.goBack()
	},
	saveCoordsForEditCompany: async (dispatch, latitude, longitude) => {

		console.log(latitude, longitude)

		return dispatch({
			type:'SELECT_CURRENT_COORDS',
			latitude,
			longitude
		})

	},
	saveUserGeo: async (dispatch) => {

	    return navigator.geolocation.getCurrentPosition(
	    	(data)=>{
				return dispatch({
					type:'CURENT_USER_GEOLOCATION',
					latitude:data.coords.latitude,
					longitude:data.coords.longitude
				})
	    	}, 
      		(error) => console.log(new Date(), error), 
      		{enableHighAccuracy: true, timeout: 10000, maximumAge: 3000}
      	)
	}
}