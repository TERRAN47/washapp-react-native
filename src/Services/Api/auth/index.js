import axios from '../axios'
import { 
	Alert,
} from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'
import OneSignal from 'react-native-onesignal'

export default {

	saveCity: async (navigation, dispatch, city, user, params, Api) => {
		const { goBack } = navigation

		if(params == 'client' && user){

			user.city = city
			
			Api.user.userProfile.updateUser(dispatch, user)

			dispatch({
				type:'UPDATE_USER',
				user
			})
		}	

		dispatch({
			type:'SAVE_CITY_FROM_SELECT',
			city
		})

		return goBack()
	},
	updateDeviceId: async (dispatch, user, deviceId) => {
		await axios.post('/update/ids', {ids:deviceId})
		dispatch({
			type:'UPDATE_USER',
			user:{...user, device_ids:deviceId}
		})
	},
	saveCityForCompanyEdit: async (dispatch, city) => {

		return dispatch({
			type:'SAVE_CITY_FROM_SELECT',
			city
		})
	},

	getLocations: async (dispatch) => {

		dispatch({
			type:'REQUEST_STATUS',
			status:true
		})

		try{

			const { data } = await axios.get('/locations')
	
			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})

			return dispatch({
				type:'UPDATE_LOCATIONS',
				data:data.locations
			})
			
		}catch(err){
			
			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})

			return dispatch({
				type:'UPDATE_LOCATIONS',
				data:null
			})
		}
	},

	authentication: async (Api, navigation, timeLoading = 2500, dispatch, type, user) => {

	    let token = await Api.getToken()

	    setTimeout(async ()=>{

		    if(token === null){

			    dispatch({
			    	type:'CLEAR_INFO_USER'
			    })

		        return navigation.dispatch(StackActions.reset({
		          index: 0,
		          key: null,
		          actions: [NavigationActions.navigate({routeName: 'auth'})]
		        }))
		    }else{
					OneSignal.addEventListener('ids', async (device)=>{
						if(device.userId && device.userId != ''){
							Api.auth.updateDeviceId(dispatch, user, device.userId)
						}
					})   
		
					if(type == 'merchant'){

						return navigation.dispatch(StackActions.reset({
							index: 0,
							key: null,
							actions: [
								NavigationActions.navigate({routeName: 'merchant'})
							]
						}))
					}else{
	
						await Api.auth.getLocations(dispatch)
						await Api.user.userProfile.checkMyRecord(dispatch)
					
						return navigation.dispatch(StackActions.reset({
							index: 0,
							key: null,
							actions: [
								NavigationActions.navigate({routeName: 'user'})
							]
						}))
					}
		    }
	    }, timeLoading)
	},

	login: async (password, phone, dispatch, navigation, Api) => {

		if(password && phone){
      phone = phone.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()\s]/g,"")
      phone = phone.slice(2)

			dispatch({
				type:'REQUEST_STATUS',
				status:true
			})

			try{

				let { data, status } = await axios.post('/login', {
					phone,
					password,
					ids:""
				})

				dispatch({
					type:'REQUEST_STATUS',
					status:false
				})

				if(status == 208){
					return Alert.alert('Сообщение', data.data)
				}else{

					await Api.saveToken(data.token)
					await Api.saveTokenForManager(data.managerToken)
			   
					dispatch({
						type:'UPDATE_USER',
						user:{...data.user}
					})

					return navigation.dispatch(
						StackActions.reset({
							index: 0,
							key: null,
							actions: [NavigationActions.navigate({
								routeName: 'launch'
							})]
						})
					)
				}
			}catch(err){
				console.log(777, err)
				return dispatch({
					type:'REQUEST_STATUS',
					status:false
				})
			}
			
		}else{
			return Alert.alert('Сообщение', 'Проверьте поля.')
		}
	},

	registration: async (dispatch, navigation, params) => {

		let {
	      phone,
	      password,
	      repeatPassword,
	      firstname,
	      type,
	      city_id,
	      checkStatus
		} = params

		if(phone && password && repeatPassword && firstname && city_id){
		phone = phone.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()\s]/g,"")
		phone = phone.slice(2)

			if(checkStatus){

				if(password == repeatPassword){

					dispatch({
						type:'REQUEST_STATUS',
						status:true
					})
	    			
					try{

						let { data, status } = await axios.post('/registration', {
							phone,
							firstname,
							password,
							type,
							city_id,
							ids:""
						})

						dispatch({
							type:'REQUEST_STATUS',
							status:false
						})				

						if(status == 208){
							return Alert.alert('Сообщение', data.data)
						}else{
							dispatch({
								type:'UPDATE_USER',
								user:data
							})
							return navigation.navigate('grant', {
								type:''
							})
						}
					}catch(err){
						
						dispatch({
							type:'REQUEST_STATUS',
							status:false
						})

						return Alert.alert('Сообщение', 'Произошла ошибка, попробуйте еще раз')
					}
				}else{
					return Alert.alert('Сообщение', 'Пароли не совпадают')
				}

			}else{
				return Alert.alert('Сообщение', 'Примите соглашение')
			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}

	},

	protectUser: async (code, Api, dispatch, navigation, phone) => {

	    if(code.length == 6){

			dispatch({
				type:'REQUEST_STATUS',
				status:true
			})

	    	let { data, status } = await axios.post('/protect', {
	    		code,
	    		phone
	    	})

			dispatch({
				type:'REQUEST_STATUS',
				status:false
			})

	    	if(status == 201){

		        await Api.saveToken(data.token)

				dispatch({
					type:'UPDATE_USER',
					user:data.user
				})

		       	return navigation.dispatch(
		          StackActions.reset({
		            index: 0,
		            key: null,
		            actions: [NavigationActions.navigate({
		              routeName: 'launch'
		            })]
		          })
		        )

	    	}else{

	    		return Alert.alert('Сообщение', data.data)

	    	}

	    }else{
	      Alert.alert('Сообщение', 'Введите корректный код')
	    }

	},
	logout: async (dispatch, navigation, Api) => {

	    await Api.removeToken()
	    	
	    navigation.dispatch(
	      StackActions.reset({
	        index: 0,
	        key: null,
	        actions: [NavigationActions.navigate({
	          routeName: 'launch'
	        })]
	      })
	    )

		dispatch({
			type:'CLEAR_CARWASH',
			carWashes:null
		})

		return dispatch({
			type:'CLEAR_INFO_USER'
		})	
	},

	sendCode: async (phone, dispatch, navigation) => {

		if(phone){
       
			dispatch({
				type:'REQUEST_STATUS',
				status:true
			})

			try{
        phone = phone.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()\s]/g,"")
        phone = phone.slice(2)
				const { data, status } = await axios.post('/sms', {
					phone
				})

				dispatch({
					type:'REQUEST_STATUS',
					status:false
				})

				if(status == 208){
					return Alert.alert('Сообщение', data.data)
				}else{

					return navigation.navigate('grant', {
						phone,
						sms:data.sms,
						type:'resetPassword'
					})

				}

			}catch(err){

				dispatch({
					type:'REQUEST_STATUS',
					status:false
				})

				return Alert.alert('ошибка', 'Ooops, ошибка, попробуйте чуть позже')
			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}
	},

	resetPassword: async (phone, password, repeatPassword, dispatch, navigation, Api) => {

		if(password == repeatPassword){

			dispatch({
				type:'REQUEST_STATUS',
				status:true
			})
			try{

				const { data, status } = await axios.post('/reset/password', {
					phone,
					password,
					ids:""
				})

				dispatch({
					type:'REQUEST_STATUS',
					status:false
				})

		        await Api.saveToken(data.token)

				dispatch({
					type:'UPDATE_USER',
					user:data.user
				})

		       	return navigation.dispatch(
		          StackActions.reset({
		            index: 0,
		            key: null,
		            actions: [NavigationActions.navigate({
		              routeName: 'launch'
		            })]
		          })
		        )
			}catch(err){
				dispatch({
					type:'REQUEST_STATUS',
					status:false
				})

				return Alert.alert('ошибка', 'Ooops, ошибка, попробуйте чуть позже')
			}

		}else{
			return Alert.alert('Сообщение', 'Пароли не совпадают')
		}
	},
	resetPasswordFromProfile: async (dispatch, navigation, password, newPassword, repeatNewPassword) => {

		if(password && newPassword && repeatNewPassword){

			if(newPassword == repeatNewPassword){

				dispatch({
					type:'REQUEST_STATUS',
					status:true
				})

				try{

					let { data, status } = await axios.post('/reset/password/profile', {
						password,
						newPassword
					})

					dispatch({
						type:'REQUEST_STATUS',
						status:false
					})

					if(status == 201){
						Alert.alert('Сообщение', 'Пароль успешно сброшен')
						return navigation.goBack()
					}else{
						return Alert.alert('Сообщение', `${data.data}`)
					}

				}catch(err){
					dispatch({
						type:'REQUEST_STATUS',
						status:false
					})
					return Alert.alert('Сообщение', 'ошибка, попробуйте еще раз')
				}

			}else{

				return Alert.alert('Сообщение', 'Новые пароли не совпадают')

			}

		}else{
			return Alert.alert('Сообщение', 'Заполните все поля')
		}

	},
	getProfile: async (dispatch, navigation, Api, id) => {

		dispatch({
			type:'STATUS_REQUEST_MERCHANT',
			status:true
		})

		try{

			let { data } = await axios.get('/customer')

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:false
			})
			console.log(123, data.merchant_id, id)
			if(data.type_account == 'client'){
				if(data.status_admin && data.merchant_id == id){
					return dispatch({
						type:'UPDATE_USER',
						user:data
					})
				}else{
					alert('Вы не являетесь администратором данной автомойки')
					Api.auth.logout(dispatch, navigation, Api)
				}
			}else{
				return dispatch({
					type:'UPDATE_USER',
					user:data
				})
			}
		}catch(err){

			console.log(err)

			dispatch({
				type:'STATUS_REQUEST_MERCHANT',
				status:true
			})
		}
	}
}