import axios from '../axios'
import { Alert } from 'react-native'
import underscore from 'underscore'

export default {

	changePassword: async (dispatch, user, name)=>{
		if(name){
			try{
				
				user.firstname = name
				let {data} = await axios.post('/customer', user)
				
				return dispatch({
					type:'UPDATE_USER',
					user:user
				})
			}catch(err){
				console.log(err)
				return
			}
		}
	},
}