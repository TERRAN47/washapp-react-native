import { combineReducers } from 'redux'

import {
  createNavigationReducer,
} from 'react-navigation-redux-helpers'

import user from 'Containers/User/reducer'
import auth from 'Containers/Auth/reducer'
import merchant from 'Containers/Merchant/reducer'
import companies from 'Containers/Merchant/Companies/reducer'
import locations from 'Containers/Location/reducer'
import maps from 'Containers/Maps/reducer'
import carWash from 'Containers/User/CarWashes/reducer'
import report from 'Containers/Merchant/Report/reducer'
import clients from 'Containers/Merchant/Clients/reducer'
import help from 'Containers/Merchant/Help/reducer'
import records from 'Containers/User/MyRecords/reducer'
import companyPrice from 'Containers/Merchant/Companies/price/reducer'
import suppliers from 'Containers/Merchant/Suppliers/reducer'
import payments from 'Containers/Merchant/Payment/reducer'
import queue from 'Containers/Merchant/Queue/reducer'
import bonuses from 'Containers/Merchant/Bonuses/reducer'
import administrator from 'Containers/Merchant/Administrators/reducer'
import clientBonus from 'Containers/User/Bonus/reducer'
import vacancies from 'Containers/Merchant/Vacancies/reducer'

import AppNavigator from '../../Navigator'

const navReducer = createNavigationReducer(AppNavigator)

export default combineReducers({
	//nav:navReducer,
	user,
	auth,
	records,
	merchant,
	companies,
	locations,
	maps,
	carWash,
	report,
	clients,
	clientBonus,
	help,
	companyPrice,
	suppliers,
	payments,
	queue,
	bonuses,
	vacancies,
	administrator
})
