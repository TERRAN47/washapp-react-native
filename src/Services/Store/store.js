import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import {AsyncStorage} from 'react-native'
import logger from 'redux-logger'

import {
  createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers'

export default (reducer) => {
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['auth', 'merchant', 'vacancies', 'maps', 'clientBonus', 'records', 'companyPrice']
  }

  const middlewareNavigation = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
  )

  const middleware = []
  const enhancers = []

  middleware.push(logger)
  middleware.push(middlewareNavigation)

  const pReducer = persistReducer(persistConfig, reducer)

  enhancers.push(applyMiddleware(...middleware))

  const store = createStore(pReducer, compose(...enhancers))
  const persistor = persistStore(store)

  return {
    store, persistor
  }

}
