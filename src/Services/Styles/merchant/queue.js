import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from '../dimensions'
import fonts from '../fonts'

const queue = StyleSheet.create({
	mainView:{
		flex:1,
		position:'relative'
	},
	listBoxView:{
		flex:1,
		paddingBottom:40
	},
	boxStatusFalse:{
		padding:10,
		borderRadius:30,
		marginBottom:10,
		backgroundColor: 'red'
	},
	boxStatusOff:{
		padding:10,
		borderRadius:30,
		marginBottom:10,
		backgroundColor: '#aaa'
	},
	boxStatusTrue:{
		padding:10,
		borderRadius:30,
		marginBottom:10,
		backgroundColor: 'green'		
	},
	boxStatusImage:{
		width:35,
		height:35,
		resizeMode:'contain',
	},
	itemBoxContentStyleLeftImage:{
		width:30,
		height:30,
		marginRight:5,
		resizeMode:'contain'
	},
	carImage:{
		resizeMode: 'contain',
		width: 40,
		height: 30
	},
	carButton:{
		borderRadius:30,
		marginTop:15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		width:'70%',
		height: 60,
		borderWidth: 2,
		borderColor:'#466dfa'
	},
	serviceText:{
		textAlign: 'center',
	},
	servicesButton:{
		borderRadius:30,
		marginTop:15,
		minHeight: 60,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		width:'70%',
		borderWidth: 2,
		borderColor:'#466dfa'
	},
	serviceListBlock:{

	},
	buttonMainListQueue:{
		position:'absolute',
		bottom:0,
		width:width-30,
		height:45,
		left:15,
		borderTopLeftRadius:11,
		borderTopRightRadius:11,
		backgroundColor:'#466dfa',
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'center',
		zIndex:1000
	},
	boxPaySuccess:{
		padding:7,
		backgroundColor:'#72d817',
		justifyContent:'center',
		alignItems:'center',
		borderRadius:7,
		marginBottom:10
	},
	buttonMainListQueueTitle:{
		fontSize:17,
		color:'#fff',
		marginRight:10,
		fontFamily:fonts.GOTHICB,
	},
	buttonMainListQueueDevider:{
		width:70,
		height:4,
		backgroundColor:'rgba(255,255,255,0.6)',
		position:'absolute',
		top:5
	},
	itemBoxStyle: {
		padding:10,
		marginBottom:2,
		position:'relative'
	},
	itemBoxContentStyle:{
		width:'100%',
		padding:10,
		borderRadius:10,
		borderWidth:3,
		borderColor:'rgba(0,0,0,0.1)',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	itemBoxContentStyleLeft:{
		justifyContent:'center',
		alignItems:'center',
		marginRight:15
	},

	itemBoxContentStyleLeftTitle:{
		fontSize:19,
		fontFamily:fonts.GOTHICB,
	},
	itemBoxContentStyleCenter:{
		flexDirection:'column',
		maxWidth:'53%',
		justifyContent:'center'
	},
	itemBoxContentStyleLeftMain:{
		flexDirection:'row',
		marginRight:10
	},
	itemBoxContentStyleCenterMember:{
		flexDirection:'row',
		width:'100%',
		alignItems:'center',
		marginBottom:15
	},
	itemBoxContentStyleCenterMemberTitle:{
		marginLeft:4,
		fontSize:12,
		fontFamily:fonts.GOTHIC
	},
	itemBoxContentStyleRight:{
		justifyContent:'center',
		minWidth:90,
		alignItems:'center'
	},
	itemBoxContentStyleRightBlueButton:{
		padding:7,
		backgroundColor:'#365ce8',
		justifyContent:'center',
		alignItems:'center',
		borderRadius:7,
		marginBottom:10
	},
	itemBoxContentStyleRightRedButton:{
		padding:7,
		backgroundColor:'#e84143',
		justifyContent:'center',
		alignItems:'center',
		borderRadius:7
	},
	itemBoxContentStyleRightButtonText:{
		color:'#fff',
		width:'100%',
		fontSize:12,
		textAlign: 'center',
		fontFamily:fonts.GOTHIC
	},
	contentMember:{
		flexDirection: 'row',
		padding:10,
		marginBottom:10
	}
})

export default queue