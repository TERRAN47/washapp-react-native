import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from '../dimensions'
import fonts from '../fonts'

const main = StyleSheet.create({
	viewWhite:{
		flex:1,
		backgroundColor:'#fff'
	},
	viewBack:{
		flex:1,
		backgroundColor:'#466dfa'
	},
	viewContentCompany:{
		flex:1,
		backgroundColor:'#fff',
		padding:10
	},
	headerSection:{
		width:'100%',
		marginBottom:10,
		height:150,
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',
		position:'relative'
	},
	headerSectionTitle:{
		textAlign:'center',
		fontFamily:fonts.GOTHICB,
		fontSize:20,
		marginBottom:5,
		color:'rgba(0,0,0,0.6)'
	},
	headerSectionNameCompany:{
		textAlign:'center',
		fontFamily:fonts.GOTHICB,
		fontSize:20,
		color:'rgba(255,255,255,0.85)',
		position:'absolute',
		zIndex:100
	},
	shadowViewBG:{
		width:'100%',
		height:'100%',
		backgroundColor:'rgba(0,0,0,0.58)',
		top:0,
		left:0,
		zIndex:10
	},
	logoHeader:{
		width:'100%',
		height:'100%',
		position:'absolute',
		resizeMode:'cover',
		top:0,
		left:0
	},
	maincontentWithWhiteBackground:{
		flex:1,
		backgroundColor:'#fff'
	}
})

export default main