import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from '../dimensions'

import fonts from '../fonts'

const drawerMerchant = StyleSheet.create({
	viewBack:{
		flex:1,
		backgroundColor:'#fff'
	},
	buttonDrawer:{
		flexDirection:'row',
		padding:15,
		alignItems:'center',
		borderBottomWidth:1,
		borderBottomColor:'rgba(0,0,0,0.2)'
	},
	iconDrawer:{
		marginRight:5,
		width:45,
		height:45,
		justifyContent:'center',
		alignItems:'center'
	},
	buttonDrawerImage:{
		marginRight:5,
		width:30,
		height:30,
		justifyContent:'center',
		alignItems:'center',
		resizeMode:'contain'
	},
	drawerTitleLink:{
		fontSize:16,
		fontFamily:fonts.GOTHICB
	}
})

export default drawerMerchant