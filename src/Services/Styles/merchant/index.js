import drawerMerchant from './drawer'
import main from './main'
import cabinetMerchant from './cabinet'
import forms from './forms'
import lists from './lists'
import company from './company'
import payment from './payment'
import queue from './queue'

export default {
	main,
	drawerMerchant,
	cabinetMerchant,
	forms,
	lists,
	company,
	payment,
	queue
}