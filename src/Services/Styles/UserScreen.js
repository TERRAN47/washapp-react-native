import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'
import fonts from './fonts'

const userscreen = StyleSheet.create({
	viewBack:{
		flex:1,
		backgroundColor:'#fff'
	},
	mainView:{
		flex:1,
		padding:10
	},
	text:{
		fontFamily:fonts.GOTHICB
	}
})

export default userscreen