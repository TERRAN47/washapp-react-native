import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'

const notFound = StyleSheet.create({
	view:{
		flex:1,
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:'#fff',
		justifyContent:'center'
	},
	title:{
		color:'rgba(0,0,0,0.55)',
		fontSize:18,
		textAlign:'center',
		marginTop:7,
		marginBottom:7
	},
	viewContent:{
		flexDirection:'column',
		alignItems:'center',
		justifyContent:'center'
	},
	buttonCreateCompany:{
		padding:15,
		marginTop:10,
		borderRadius:10,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#466dfa'
	},
	buttonCreateCompanyTitle:{
		color:'rgba(255,255,255,0.88)',
		textAlign:'center',
		fontFamily:fonts.GOTHIC,
		fontSize:18
	}
})

export default notFound