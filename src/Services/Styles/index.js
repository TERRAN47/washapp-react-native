import {
	Dimensions
} from 'react-native'

import launchscreen from './LaunchScreen'
import AuthScreen from './AuthScreen'
import userscreen from './UserScreen'
import header from './Header'
import fonts from './fonts'
import profilescreen from './Profile'
import merchant from './merchant'
import notFound from './NotFound'
import maps from './Maps'
import drawerClient from './clientDriwer'
import carWashes from './carWashes'
import myRecords from './MyRecords'
import loader from './Loader'
import modal from './modal'
import changePass from './ChangePassScreen'
import lists from './Lists'
import timer from './timer'

const { width, height } = Dimensions.get('window')

export default {
	launchscreen,
	modal,
	AuthScreen,
	changePass,
	carWashes,
	userscreen,
	myRecords,
	profilescreen,
	header,
	drawerClient,
	width,
	height,
	fonts,
	merchant,
	notFound,
	maps,
	loader,
	lists,
	timer
}