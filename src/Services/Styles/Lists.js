import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'
import { constans } from 'Services/Utils'
import fonts from './fonts'

const lists = StyleSheet.create({
	mainView:{
		width:width-20,
		margin:10
	},
	viewItem:{
		width:'100%',
		marginBottom:10,
		marginTop:10
	},
	mainItenDescriptionText:{
		fontSize:15,
		fontFamily:fonts.GOTHIC,
		marginBottom:5
	},
	formView:{
		flexDirection:'row',
		justifyContent:'space-between',
		padding:10,
		backgroundColor:'rgba(0,0,0,0.05)',
		borderRadius:5,
		marginTop:8,
		marginBottom:8
	},
	formViewLeft:{
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'space-between'	
	},
	formViewLeftTitle:{
		fontSize:15,
		fontFamily:fonts.GOTHIC
	},
	bottomBackSuccess:{
		width:'100%',
		height:50,
		backgroundColor:constans.defaultColor,
		justifyContent:'center',
		alignItems:'center',
		borderRadius:5
	},
	bottomBackSuccessTitle:{
		fontSize:17,
		color:'rgba(255,255,255,0.78)',
		fontFamily:fonts.GOTHICB,
	},
	formViewLeftInput:{
		width:160,
		paddingLeft:15,
		paddingRight:15,
		marginLeft:10,
		color:'rgba(0,0,0,0.77)',
		fontFamily:fonts.GOTHICB,
		fontSize:17,
		borderWidth:3,
		borderColor:'#fff',
		borderRadius:18
	},
	formViewButtonAdd:{
		width:50,
		height:50,
		borderRadius:25,
		backgroundColor:'#466dfa',
		justifyContent:'center',
		alignItems:'center'
	},
	mainItemView: {
		width:'100%',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
		paddingTop:10,
		paddingBottom:5,
		marginBottom:10,
		borderBottomWidth:2,
		borderBottomColor:'rgba(0,0,0,0.05)'
	},
	mainItemtitle:{
		color:'rgba(0,0,0,0.77)',
		fontSize:18,
		fontFamily:fonts.GOTHIC,	
	},
	formViewPayments: {
		marginTop:20,
		width:'100%',
	},
	formViewPaymentsTitle: {
		fontSize:20,
		fontFamily:fonts.GOTHICB,
		color:'#466dfa',
		marginBottom:15
	}
})

export default lists