import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'
import { constans } from 'Services/Utils'
import fonts from './fonts'

const header = StyleSheet.create({
	view:{
		width:'100%',
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:constans.defaultColor,
		height:60,
		justifyContent:'space-between'
	},
	clientView:{
		width:'100%',
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:constans.orangeColor,
		height:60,
		justifyContent:'space-between'
	},
	viewHide:{
		width:'100%',
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:'rgba(0,0,0,0.28)',
		position:'absolute',
		zIndex:10000,
		top:0,
		left:0,
		height:60,
		justifyContent:'space-between'
	},
	buttonHeader:{
		padding:10
	},
	headerTitle:{
		color:'#fff',
		minWidth:180,
		flex:1,
		textAlign: 'center',
		fontSize:20
	},
	headerMenuIconTrigger:{
		paddingTop:15,
		paddingBottom:5,
		paddingRight:15,
		paddingLeft:15,
		fontSize:22,
		color:'#fff'
	},
	headerMenuIcon:{
		fontSize:24,
		marginRight:10,
		position:'relative',
		top:1
	},
	menuOptionStyle:{
		flexDirection:'row',
		alignItems:'center'
	},
	headerMenuText:{
		fontSize:16,
		fontWeight:'bold'
	},
	menuOptionsViewStyle:{
		padding:10,
		position:'relative'
	},
	menuView:{
		backgroundColor:'red',
		width:'50%'
	},
	viewLeft:{
		flex:1,
	},
	viewRight:{
		flex:1,
		alignItems: 'flex-end'
	},
	payText:{
		color:'#fff',
		fontSize:18,
		fontFamily:fonts.GOTHICB
	}
})

export default header