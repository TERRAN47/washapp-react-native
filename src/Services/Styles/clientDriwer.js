import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'

const drawerClient = StyleSheet.create({
	viewBack:{
		flex:1,
		backgroundColor:'#fff'
	},
	buttonDrawer:{
		flexDirection:'row',
		padding:15,
		alignItems:'center',
		borderBottomWidth:1,
		borderBottomColor:'rgba(0,0,0,0.2)'
	},
	iconDrawer:{
		marginRight:5,
		width:26,
		height:25,
		justifyContent:'center',
		alignItems:'center'
	},
	drawerTitleLink:{
		fontSize:16,
		fontFamily:fonts.GOTHICB
	}
})

export default drawerClient