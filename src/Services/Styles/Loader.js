import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'

const loader = StyleSheet.create({
	view:{
		flex:1,
		flexDirection:'column',
		alignItems:'center',
		backgroundColor:'#fff',
		justifyContent:'center'
	},
	title:{
		textAlign:'center',
		marginTop:15,
		fontSize:20,
		color:'rgba(0,0,0,0.35)'
	}
})

export default loader