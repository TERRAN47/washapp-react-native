import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'

const maps = StyleSheet.create({
	view:{
		flex:1
	},
	titleMapMain:{
		padding:10,
		fontSize:15,
		fontFamily:fonts.GOTHICBI
	},
	mapStyle:{
		flex:1
	},
	mapContent:{
		flex:1,
		position:'relative'
	},
	topView:{
		position:'absolute',
		top:0,
		width:'100%',
		padding:10,
		backgroundColor:'rgba(0,0,0,0.5)',
		zIndex:1000
	},
	topViewTitle:{
		color:'#fff',
		fontSize:18,
		fontFamily:fonts.GOTHICBI
	},
	markerStyleImage:{
		width:60,
		height:60,
		resizeMode:'contain'
	},
	buttonSelectCoords:{
		width:width-50,
		padding:10,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#466dfa',
		borderRadius:8,
		position:'absolute',
		bottom:25,
		left:25,
		flexDirection:'column',
		zIndex:1000
	},
	buttonSelectCoordsTitle:{
		color:'#fff',
		fontFamily:fonts.GOTHICB,
		textAlign:'center',
		fontSize:17
	},
	viewInfo:{
		width:width-50,
		padding:10,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'rgba(0,0,0,0.55)',
		borderRadius:8,
		position:'absolute',
		bottom:25,
		left:25,
		flexDirection:'column',
		zIndex:1000	
	},
	viewInfoText:{
		color:'#fff',
		fontFamily:fonts.GOTHICI,
		textAlign:'center',
		fontSize:16
	}
})

export default maps