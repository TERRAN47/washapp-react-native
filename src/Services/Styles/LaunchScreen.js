import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'

const launchscreen = StyleSheet.create({
	view:{
		flex:1,
		width:'100%',
		height:'100%',
		justifyContent:'center',
		backgroundColor:'#466df9',
		alignItems:'center',
		position:'relative'
	},
	logo:{
		width:width/1.5,
		height:width/2.5,
		resizeMode:'contain',
		marginBottom:20
	},
	title:{
		color:'#fff',
		fontSize:27,
		fontFamily:fonts.GOTHICB,
		textAlign:'center'
	}
})

export default launchscreen