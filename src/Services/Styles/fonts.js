import {
	Platform
} from 'react-native'

export default {
	GOTHIC: Platform.OS === 'android'   ? 'GOTHIC'   : 'Jura-Light',
	GOTHICB: Platform.OS === 'android'  ? 'GOTHICB'  : 'Jura-Bold',
	GOTHICBI: Platform.OS === 'android' ? 'GOTHICBI' : 'Jura-Light',
	GOTHICI: Platform.OS === 'android'  ? 'GOTHICI'  : 'Jura-SemiBold',
}
