import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'
import {constans} from '../Utils'
import fonts from './fonts'

const ChangePassScreen = StyleSheet.create({
	viewBack:{
		width:'100%',
		justifyContent:'center',
		alignItems:'center',
		position:'relative'
	},
	inputForm:{
		padding:2,
		marginTop:10,
		paddingHorizontal: 5,
		borderRadius:5,
		textAlign: 'center',
		borderWidth: 1,
		width:'100%',
		borderColor:'#f3f3f3'
	},
	saveButton:{
		width: '80%',
		borderRadius:5,
		padding:10,
		marginVertical: 10,
		backgroundColor: 'rgb(113, 184, 77)'
	},

})

export default ChangePassScreen