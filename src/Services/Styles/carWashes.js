import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'
import {constans} from '../Utils'


const carWashes = StyleSheet.create({
	mainView:{
		flex:1,
		padding:10
	},
	flexBlock:{
		justifyContent: 'center',
		alignItems:  'center',
	},
	ratingBlock:{
		justifyContent: 'flex-start',
		alignItems:  'center',
	},
	washImage:{
		width: 100,
	},
	findButton:{
		padding:10,
		color:'#fff',
		marginLeft:10,
		borderRadius:10,
		backgroundColor:constans.orangeColor
	},
	bonusRightImg:{
		resizeMode: 'contain',
		width: 30,
		paddingHorizontal: 10,
		paddingRight:20,
		height:30,
	},
	countBonustext:{
		fontFamily:fonts.GOTHICB,
		fontSize:20
	},
	totalSumm:{
		marginTop:10,
		color:'#fff',
		borderTopWidth:1,
		borderColor:'#fff'
	},
	priceServicesBlock:{
		paddingBottom: 60,
		flex:1,
		minHeight: 400,
		justifyContent: 'center'
	},
	payText:{
		padding:5,
		fontFamily:fonts.GOTHICB,
		backgroundColor:'#72d817',
		color:'#fff'
	},
	inputDistance:{
		borderRadius:10,
		width:100,
		borderWidth: 1,
		borderColor: constans.orangeColor,
		textAlign: 'center',
		paddingVertical:5
	},
	queueBlock:{
		flexDirection: 'row',
		alignItems:'center'
	},
	textFavirites:{
		fontFamily:fonts.GOTHICB,
		fontSize:20,
		color:constans.orangeColor,
	},
	washName:{
		fontWeight: 'bold',
	},
	rowBlock:{
		flexDirection: 'row',

	},
	titleText:{
		fontFamily:fonts.GOTHICB,
		fontSize: 23,
		textAlign: 'center',
		marginBottom: 20,
	},
	rowCenterBlock:{
		flexDirection: 'row',
		alignItems:  'center',
		marginVertical:10,
	},
	rowServiceDeral:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding:15,
	},
	rowBetWeenBlock:{
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	priceList:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding:15,
		borderBottomWidth:1,
		borderColor:'#e3e3e3'
	},
	carWashBonusBlock:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding:5,
		marginTop:10,
	},
	bonusBlock:{
		borderRadius: 10,
		borderWidth: 2,
		marginTop:10,
		borderColor: '#EBECEC'
	},
	carWashBlock:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		borderRadius: 10,
		borderWidth: 2,
		padding:5,
		marginTop:10,
		borderColor: '#EBECEC'
	},
	washDetalService:{
		borderRadius:25,
		backgroundColor: constans.orangeColor,
		marginLeft:5,
		width: 50,
		justifyContent: 'center',
		alignItems:  'center',
		height:50,
	},
	inboxIcon:{
		borderRadius:15,
		backgroundColor: constans.orangeColor,
		width: 40,
		justifyContent: 'center',
		alignItems:  'center',
		height:40,
	},
	ternIcon:{
		borderRadius:15,
		backgroundColor: constans.orangeColor,
		width: 30,
		justifyContent: 'center',
		alignItems:  'center',
		height:30,
	},
	favoriteIcont:{
		width:50,
		height:50,
		marginTop:10,
	},
	washService:{
		borderRadius:15,
		backgroundColor: constans.orangeColor,
		marginLeft:5,
		width: 30,
		justifyContent: 'center',
		alignItems:  'center',
		height:30,
	},
	imageRenderService:{
		width: 25,
		height:25,
	},
	imageService:{
		width: 32,
		height:32,
	},
	textService:{
		fontFamily:fonts.GOTHICB,
		fontSize: 10,
		textAlign: 'center'
	},
	infoBlock:{
		justifyContent: 'space-between',
		paddingHorizontal: 15,
		flexDirection: 'row',
	},
	headerText:{
		fontFamily:fonts.GOTHICB,
		color:'#fff',
		textAlign: 'center'
	},
	text:{
		fontFamily:fonts.GOTHICB,
		fontSize: 12,
	},
	washIcon:{
		width: 23,
		height:21,
		marginLeft:20,
	},
	headerBlock:{
		backgroundColor: 'rgba(18, 18, 18, 0.85)',
		paddingVertical: 20,
	},
	centerItems:{
		justifyContent: 'center',
		alignItems: 'center',
		flex:1
	},
	headerImage:{
		width:'100%'
	},

	washDetalBody:{
		backgroundColor: '#fff',
		flex:1,
	},
	title:{
		padding:10,
		fontSize:20,
		color:'#fff',
		fontFamily:fonts.GOTHICB,
		textAlign: 'center',
		backgroundColor: constans.orangeColor,
	},

	blockButtons:{
		backgroundColor: '#EDEEF0',
		borderRadius:10,
		marginVertical: 15,
		width:'80%',
		borderWidth: 2,
		borderColor: '#E6E6E6',
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
	},
	iconButton:{
		marginLeft: 10
	},
	callButton:{
		paddingVertical: 10,
		justifyContent: 'center',
		flexDirection: 'row',
		flex:1,		
	},
	writeButton:{
		backgroundColor: constans.orangeColor,
		borderRadius:10,
		justifyContent: 'center',
		flexDirection: 'row',
		flex:1,	
		paddingVertical: 10,
	},
	openButton:{
		paddingHorizontal:10,
		paddingVertical: 5,
		color:'#fff',
		fontSize: 12,
		fontFamily:fonts.GOTHICB,
		borderRadius:5,
		textAlign: 'center',
		backgroundColor: '#365CE8'
	},
	filterBlock:{
		paddingHorizontal: 10,
		paddingTop:10,
	},
	servicesText:{
		fontFamily:fonts.GOTHICB,
		fontSize:16
	},
	servicesNext:{
		fontSize:20,
		fontFamily:fonts.GOTHICB,
		textAlign:'center'
	},
	blockDescript:{
		flex:1,
		paddingHorizontal: 15,
	},
	priceFooter:{
		position: 'absolute',
		width:'100%',
		backgroundColor: constans.orangeColor,
		justifyContent: 'center',
		alignItems: 'center',
		left:0,bottom:0,
		height:50
	}
})

export default carWashes