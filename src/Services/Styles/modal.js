import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'
import {constans} from '../Utils'

const modal = StyleSheet.create({
	view:{
		minHeight: height,
		width:width,
		position: 'absolute',
		backgroundColor: '#000',
		opacity: .5,
		padding:30,
		zIndex: 999,
		top:0,left:0
	},

	buttonCall:{
		flexDirection: 'row',
		alignItems:'center',
		margin:10,
		padding:5,
		borderWidth: 2,
		borderRadius: 10,
		borderColor:'#efefef'
	},
	bonusesBlock:{
		backgroundColor: '#efefef',
		borderRadius:15,
		padding:20
	},
	modalViewBlock:{
		minHeight: height,
		width: '100%',
		justifyContent:'center',
		alignItems:  'center',

	},
	textModal:{
		fontSize: 18,
		padding:10,
	},
	invitedIcon:{
		width:40,
		height:40, 
		resizeMode: 'contain', 
		marginVertical:10
	},
	minusButton:{
		backgroundColor:'#466dfa',
		borderRadius:10,
		marginLeft:10,
		padding:5
	},
	rowBlock:{
		marginTop:10,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	servicesBlock:{
		marginTop:15,
		alignItems: 'center'
	},
	textServices:{
		fontSize:16,
		fontFamily:fonts.GOTHIC,
	},
	modalBlock:{
		padding:10,
		justifyContent:'center',
		alignItems: 'center',
		marginTop:30,marginLeft:30,
		marginBottom: 30,
		zIndex: 9999,
		minHeight: height-90,
		width:width-60,
		backgroundColor: '#fff',
		borderRadius:10,
	},
	titleText:{
		color:'red',
		marginTop:30
	},
	boxTitle:{
		textAlign: 'center',
		fontSize:18,
		fontFamily:fonts.GOTHIC,
	},
	titleInvateText:{
		color:'#72d113',
		textAlign: 'center',
		fontFamily:fonts.GOTHICB,
	},
	buttonsOval:{
		alignItems:'center',
		backgroundColor:'#466dfa',
		borderRadius:35,
		width:70,
		height:70,
		justifyContent:'center'
	},
	noButton:{
		backgroundColor:constans.orangeColor,
		padding:10,
		width:150,
		borderRadius:5
	},
	noTextButton:{
		color:'#fff',
		textAlign: 'center',
	},
	yesTextButton:{
		color:'#fff',
		textAlign: 'center',
	},
	yesButton:{ 
		backgroundColor: constans.defaultColor,
		padding:10,
		width:150,
		margin:15,
		borderRadius:5,
	},
	confirmButton:{ 
		backgroundColor: constans.defaultColor,
		padding:10,
		fontFamily:fonts.GOTHICB,
		width:150,
		textAlign: 'center',
		margin:15,
		borderRadius:10,
		color:'#fff'
	},
	paidButton:{
		backgroundColor: constans.defaultColor,
		padding:10,
		width:170,
		fontSize:18,
		textAlign: 'center',
		margin:15,
		borderRadius:5,
		color:'#fff'
	},
	inputForm:{
		padding:5,
		margin:10,
		borderRadius:5,
		width:100,
		textAlign: 'center',
		borderWidth:1,
		borderColor:'rgba(0,0,0,0.13)',
	},
	inputComent:{
		padding:5,
		minHeight:100,
		margin:10,
		borderRadius:5,
		width:250,
		borderWidth:1,
		borderColor:'rgba(0,0,0,0.13)',
	},
	saveButton:{
		backgroundColor: constans.orangeColor,
		padding:10,
		width:150,
		fontSize:18,
		textAlign: 'center',
		margin:15,
		borderRadius:5,
		color:'#fff'
	},
	totalSummText:{
		backgroundColor: '#d9dada',
		padding:10,
		fontFamily:fonts.GOTHICB,
		fontSize:18,
		width:170,
		textAlign: 'center',
		margin:15,
		borderRadius:5,

	},	
	dafaultButton:{
		backgroundColor: '#d9dada',
		padding:10,
		width:150,
		textAlign: 'center',
		margin:15,
		fontFamily:fonts.GOTHICB,
		borderRadius:5,
	},

})

export default modal