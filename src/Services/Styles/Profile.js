import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'
import fonts from './fonts'
const profilescreen = StyleSheet.create({
	viewBack:{
		flex:1,
		backgroundColor:'#fff'
	},
	view:{
		justifyContent:'center',
		alignItems: 'center',
		minHeight: height-100,
	},
	serch:{
		borderRadius:10,
		borderWidth:1,
		padding:5,
		borderColor:'#D9D9D9'
	},
	serchBlock:{
		paddingHorizontal:10,
	},
	userInfoBlock:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop:15,
		alignItems: 'center',
	},
	selectMarka:{
		flexDirection: 'row',
	},
	typeAvtoBlock:{
		flexDirection: 'row',
		flex:1,
		marginVertical:10,
		borderColor:'#ebecec',
		borderBottomWidth: 1,
		paddingHorizontal: 15,
		alignItems: 'center'
	},
	typeName:{
		fontSize: 16,
		marginLeft: 10,
		width: '70%',
		fontFamily:fonts.GOTHIC
	},
	myCarItem:{
		flexDirection: 'row',
		borderBottomWidth: 1,
		height:70,
		marginTop:10,
		paddingBottom: 15,
		borderColor:'#dfe3e6',
	},
	carInfo:{
		flexDirection: 'row',
		flex:1,
		justifyContent:  'space-between',
	},
	carModel:{
		flexDirection: 'row',
		flex:3,
		justifyContent:  'space-between',
	},
	typeAvtoIcon:{
		resizeMode: 'contain',
		width: 100,
		paddingHorizontal: 10,
		height:40,
	},
	titleCat:{
		textAlign: 'center',
		fontSize: 17,
		fontWeight: 'bold',
	},
	autoColor:{
		margin:0,
		borderRadius:15,
		width:30,
		borderColor:'#dfe3e6',
		borderWidth: 1,
		height:30
	},
	modelText:{
		fontSize: 20,
	},
	flexBlock:{
		flex:1, 
		alignItems: 'center',
		justifyContent: 'center',
	},
	imgBlock:{
		alignItems: 'center',
		marginRight:15,
		justifyContent: 'center',
	},
	carItem:{
		flexDirection: 'row',
		borderBottomWidth: 1,
		borderColor: '#f3f3f3',
		marginTop:10,
	},
	carImage:{
		resizeMode: 'contain',
		width: 60,
		height: 50
	},
	childBLock:{
		flex:1,
		flexDirection: 'row', 
		alignItems:  'center',
	},
	mainView:{
		flex:1,
		padding:10,
	},
	iconRight:{
		flexDirection: 'row',
		justifyContent: 'flex-end',
		flex:1,
	},
	iconText:{
		marginRight: 10
	},
	carBlock:{
		alignItems: 'center',
		flex:1,
		paddingBottom:250,
		justifyContent: 'center'
	},
	inputForm:{
		padding:2,
		borderWidth: 0,
		fontFamily:fonts.GOTHIC,
		width: 250,
		minHeight:70,
		textAlign:  'center',
		margin:0
	},
	carButton:{
		borderRadius:30,
		marginTop:15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		width:'70%',
		height: 60,
		borderWidth: 2,
		borderColor:'#f39406'
	},
	colorBlocs:{
		flexDirection: 'row',
		justifyContent:  'center',
		alignItems: 'center',
		flexWrap: 'wrap',
	},
	color:{
		margin:10,
		borderRadius:25,
		width:50,
		borderColor:'#dfe3e6',
		borderWidth: 1,
		height:50
	},
	categoryBlock:{
		position: 'relative',
		paddingBottom:50,
		borderBottomWidth: 1,
		borderColor:'#ebecec'
	}
})

export default profilescreen