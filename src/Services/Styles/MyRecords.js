import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import fonts from './fonts'
import {constans} from '../Utils'

const myRecords = StyleSheet.create({
	viewBlock:{
		flex:1,
		backgroundColor:'#fff'
	},
	headerImage:{
		width:'100%'
	},
	headerBlock:{
		backgroundColor: 'rgba(18, 18, 18, 0.85)',
		paddingVertical: 15,
	},
	rowBlock:{
		flexDirection: 'row',

	},
	closeButton:{
		borderRadius:10,
		padding:10,
		color:'#fff',
		textAlign:'center',
		width:120,
		backgroundColor:'#df1e1e'
	},
	ratingText:{
		fontSize:12,
		fontWeight: 'bold',
		color:constans.orangeColor,
		marginVertical:10,
		textAlign: 'center'
	},
	ratingBlock:{
		flexDirection: 'row',
		paddingHorizontal:15,
		justifyContent: 'space-between'	
	},
	addIcon:{
		color:constans.orangeColor,
		marginTop:10
	},
	centerItems:{
		justifyContent: 'center',
		alignItems: 'center',
		margin:5
	},
	footerBLock:{
		paddingHorizontal: 10,
		flexDirection: 'row',
	},
	iComing:{
		flexDirection: 'row',
		flex:1,
		padding:10,
		borderTopLeftRadius: 10,
		justifyContent:  'center',
		backgroundColor: constans.orangeColor,
	},
	calling:{
		flexDirection: 'row',
		justifyContent:  'center',
		flex:1,
		borderTopRightRadius: 10,
		padding:10,
		backgroundColor: '#EBECEC'
	},
	callingOn:{
		flexDirection: 'row',
		justifyContent:  'center',
		flex:1,
		borderTopRightRadius: 10,
		borderTopLeftRadius: 10,
		padding:10,
		backgroundColor: constans.orangeColor,
	},
	line:{
		width: 3,
		backgroundColor: '#666561',
		borderRadius:1,
		height: '100%'
	},
	paddingBlock:{
		padding: 10
	},
	footerBlock:{

	},
	skipButton:{
		backgroundColor: '#cacaca',
		padding:5,
		textAlign:  'center',
		color:'#fff',
		width:100,
		borderRadius:5
	},
	queueCount:{
		borderRadius:20,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: constans.orangeColor,
		width:40,
		height: 40
	},
	myQueueBlock:{
		flexDirection: 'row',
		alignItems: 'center',
		flex:1,
		justifyContent: 'space-between',
	},
	skipBlock:{
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
		flex:1,
	},
	mapImage:{
		width:'100%',
		height:200,
		borderRadius:10
	},
	titleHeader:{
		color:'#fff',
		fontSize: 18,
		textAlign: 'center'
	},
	titleBlock:{
		backgroundColor: constans.orangeColor,
		color:'#fff',
		fontSize: 18,
		paddingVertical: 5,
		textAlign: 'center'
	}
})

export default myRecords