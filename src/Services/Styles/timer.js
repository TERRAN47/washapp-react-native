import { StyleSheet } from 'react-native'

import {
	width, 
	height
} from './dimensions'

import { constans } from 'Services/Utils'

import fonts from './fonts'

const timer = StyleSheet.create({
	viewTimer:{
		padding:10,
		fontSize:15,
		flexDirection:'row',
		alignItems:'center',
		width:'100%',
	},
	viewTimerDefault:{
		padding:10,
		flexDirection:'row',
		justifyContent:'center',
		alignItems:'center',
		position:'absolute',
		top:6,
		right:15,
		zIndex:1000
	},
	viewTimertitle:{
		fontSize:15,
		flexDirection:'row',
		justifyContent:'center',
		alignItems:'center',
		fontFamily:fonts.GOTHICB,
		color:'rgba(0,0,0,0.57)'
	}
})

export default timer