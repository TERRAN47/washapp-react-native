
const firtUpCase = (text) =>{
	return text.replace(/(^|\s)\S/g, l => l.toUpperCase())
}
const probelText = (summ)=>{ if(summ || summ == 0)return summ.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}
const orangeColor = '#FFCD19'
const defaultColor = '#466dfa'
const currency = '₸'
export default {
	probelText,
	currency,
	firtUpCase,
	orangeColor,
	defaultColor
}