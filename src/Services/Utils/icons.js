import logo from '../../Media/logo.png'
import cofeRoom from '../../Media/cofeRoom.png'
import countBox from '../../Media/countBox.png'
import countTurn from '../../Media/countTurn.png'
import point from '../../Media/point.png'
import pointWrite from '../../Media/pointWrite.png'
import switchUser from '../../Media/switchUser.png'
import timeWork from '../../Media/timeWork.png'
import waitRoom from '../../Media/waitRoom.png'
import game from '../../Media/game.png'
import favorites from '../../Media/favorites.png'
import memberBox from '../../Media/memberBox.png'
import memberBoxBlack from '../../Media/memberBoxBlack.png'
import boxGreen from '../../Media/boxGreen.png'
import qeueCarsGreen from '../../Media/qeueCarsGreen.png'
import ratingCar from '../../Media/ratingCar.png'
import washIcon from '../../Media/washIcon.png'
import iconBoxStatus from '../../Media/iconBoxStatus.png'
export default {
	logo,
	ratingCar,
	iconBoxStatus,
	boxGreen,
	qeueCarsGreen,
	memberBoxBlack,
	memberBox,
	washIcon,
	cofeRoom,
	countBox,
	countTurn,
	favorites,
	game,
	point,
	pointWrite,
	switchUser,
	timeWork,
	waitRoom
}