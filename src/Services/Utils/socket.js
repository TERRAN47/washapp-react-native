navigator.userAgent = "react-native"
import SocketIOClient from 'socket.io-client'
import config from 'Config'


export default () => {

	const connectionConfig = {
		jsonp: false,
        reconnection: true,
        reconnectionDelay: 500,
	  	reconnectionAttempts: 10, 
	  	transports: ['websocket'],
	  	timeout: 5000,
		pingTimeout: 30000,
		forceNew: true,
 		pingInterval: 30000
	}

	let socket = SocketIOClient(`${config.socket_url}:${config.socket_port}`, connectionConfig)

	return socket

}