import OneSignal from 'react-native-onesignal'
import config from '../../Config'

OneSignal.init(config.one_signal_id)
OneSignal.configure()

export default OneSignal