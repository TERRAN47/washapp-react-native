const renderDistance = (distance) => {

	distance = Math.round(distance)

	if(distance <= 1000){
		distance = distance + ' м.'
		return distance
	}else{
		distance = (distance/1000).toFixed(1) + ' км.'
		return distance
	}

}

export default renderDistance